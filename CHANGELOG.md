## [2.0.0-0.1] - 2018-12-24
### Changed
- cjson has been relocated into its own project
- Rename logging and streamingcontext modules so that their filename can be derived from their module name via automation

## [2.0.0-0.0] - 2018-12-22
### Added
- [stuart] Transpile `stuart.util` parse() function to C
- [stuart] `stuart.Context` textFile() and hadoopFile() now work and can load a model from romfs

### Changed
- Merge changes since Stuart 2.0.0-0
- Moses has been relocated into its own project
- url has been relocated into its own project
- Stuart ML has been relocated into its own project

## [1.0.1] - 2018-11-17
### Added
- [moses] Transpile functions to C: indexOf(), min(), max(), ones(), pack(), rep(), reverse(), same(), zeros(), and zip()
- [stuart] Transpile `FileSystem` module to C
- [stuart] Transpile `RDD` module functions to C: zip()
- [stuart-ml] Transpile linalg modules to C: `BLAS`, `DenseVector`, `SparseVector`, `util`, `Vector`, and `Vectors`
- [stuart-ml] Transpile clustering module to C: `KMeans` (existing model evaluation only), and `VectorWithNorm`
- [stuart-ml] Transpile util module to C: `MLUtils`

### Fixed
- [stuart-ml] Vectors:dense(), when called with variable args, causes error due to no table.pack() function found

## [1.0.0] - 2018-11-10
### Added
- [moses] Transpile functions to C: all(), any(), append(), contains(), detect(), difference(), fill(), include(), intersection(), isCallable(), isEmpty(), isFinite(), isFunction(), isInteger(), keys(), result(), some(), sort(), sortBy(), unique(), and values()
- [stuart] Provide a `interface` module for eLua with clockPrecision(), now(), and sleep()
- [stuart] Transpile `Context` module to C
- [stuart] Transpile `internal.Logger` module to C
- [stuart] Transpile `internal.logging` module to C
- [stuart] Transpile `Partition` module functions to C: \_flattenValues() and \_toLocalIterator()
- [stuart] Transpile `RDD` module functions to C: __tostring(), aggregate(), aggregateByKey(), cartesian(), coalesce(), combineByKey(), count(), countApprox(), countByKey(), countByValue(), distinct(), first(), flatMap(), flatMapValues(), fold(), foldByKey(), foreach(), foreachPartition(), glom(), groupBy(), groupByKey(), histogram(), intersection(), isEmpty(), join(), keyBy(), keys(), leftOuterJoin(), lookup(), mapPartitions(), mapPartitionsWithIndex(), mapValues(), max(), mean(), min(), popStdev(), popVariance(), reduceByKey(), repartition(), sampleStdev(), sampleVariance(), setName(), sortBy(), sortByKey(), stats(), stdev(), subtract(), subtractByKey(), sumApprox(), take(), top(), toString(), treeAggregate(), treeReduce(), union(), values(), variance(), and zipWithIndex()
- [stuart] Transpile `util.class` module to C
- [stuart] Transpile all streaming modules to C (excluding socket and http related receivers)

### Fixed
- Class instances were missing a __typename field

### Changed
- Centralized lua2c helper functions

## [0.2.0] - 2018-11-04
### Added
- [moses] Transpile functions to C: clone(), find(), each(), isEqual(), partition(), select(), and size()
- [stuart] Transpile `stuart.Partition` module functions to C: \_count(), \_flatten(), and new()
- [stuart] Transpile `stuart.RDD` module functions to C: collect(), filter(), map(), new(), reduce(), and toLocalIterator()
- [stuart] examples/SparkPi.luo now runs on STM32

## [0.1.9] - 2018-11-02
### Added
- [cjson] Link [lua-cjson 2.1.0](https://github.com/mpx/lua-cjson) and adapt to eLua LTR
- [moses] Transpile functions to C: identity(), isBoolean(), isFunction(), isNil(), isNumber(), isString(), isTable(), map(), noop(), range(), reduce(), slice(), and tabulate()
- [stuart] Transpile `stuart` and `stuart.util` modules to C

<small>(formatted per [keepachangelog-1.1.0](http://keepachangelog.com/en/1.0.0/))</small>
