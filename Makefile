.PHONY: build
build:
	docker build -t elua .
	docker run -v `pwd`:/export -it elua
