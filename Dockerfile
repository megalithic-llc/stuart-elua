FROM ubuntu:16.04

# ============================================================
# Install package prerequisites
# ============================================================
RUN apt-get -y update && apt-get -y install software-properties-common
RUN add-apt-repository ppa:team-gcc-arm-embedded/ppa
RUN apt-get -y update && apt-get -y install \
  build-essential \
  gcc-arm-none-eabi \
  git \
  libreadline-dev \
  ncurses-dev \
  wget \
  unzip

# Lua 5.1
ARG LUA_VERSION=5.1.4
RUN wget -q http://www.lua.org/ftp/lua-$LUA_VERSION.tar.gz \
  && tar zxvf lua-$LUA_VERSION.tar.gz \
  && cd lua-$LUA_VERSION \
  && make linux \
  && make install

# LuaRocks
ARG LUAROCKS_VERSION=2.3.0
RUN wget -q http://luarocks.github.io/luarocks/releases/luarocks-$LUAROCKS_VERSION.tar.gz \     
  && tar zxvf luarocks-$LUAROCKS_VERSION.tar.gz \
  && cd luarocks-$LUAROCKS_VERSION \
  && ./configure \
  && make \
  && make install

# eLua build process dependencies
RUN  luarocks install luafilesystem \
  && luarocks install lpack \
  && luarocks install md5 \
  && luarocks install amalg

# ============================================================
# Download eLua sources, and checkout a stable tag
# ============================================================
RUN git clone https://github.com/elua/elua.git /elua
WORKDIR /elua
ARG ELUA_GIT_VERSION=2e6a3af
RUN git checkout $ELUA_GIT_VERSION

# ============================================================
# Build eLua cross-compiler
# ============================================================
RUN lua cross-lua.lua

# ============================================================
# Add Lua scripts
# ============================================================
ADD examples/SparkPi.lua examples/

ADD test/stuart.lua test/
ADD test/stuart_class.lua test/
ADD test/stuart_Context.lua test/
ADD test/stuart_fileSystemFactory.lua test/
ADD test/stuart_LocalFileSystem.lua test/
ADD test/stuart_Partition.lua test/
ADD test/stuart_QueueInputDStream.lua test/
ADD test/stuart_RDD.lua test/
ADD test/stuart_SparkConf.lua test/
ADD test/stuart_StreamingContext.lua test/
ADD test/stuart_util.lua test/

# ============================================================
# Add our C-based sources
# ============================================================
COPY config/modules.lua /elua/config/
COPY src/auxmods.h /elua/src/modules/
COPY src/lua/lrotable.h /elua/src/lua/

ADD src/lua2c.c /elua/src/modules/
ADD src/lua2c.h /elua/src/modules/

ARG CJSON_VERSION=2.1.0-1
ADD https://raw.githubusercontent.com/BixData/cjson-elua/$CJSON_VERSION/src/cjson.c        /elua/src/modules/
ADD https://raw.githubusercontent.com/BixData/cjson-elua/$CJSON_VERSION/src/cjson-fpconv.c /elua/src/modules/
ADD https://raw.githubusercontent.com/BixData/cjson-elua/$CJSON_VERSION/src/cjson-fpconv.h /elua/src/modules/
ADD https://raw.githubusercontent.com/BixData/cjson-elua/$CJSON_VERSION/src/cjson-strbuf.c /elua/src/modules/
ADD https://raw.githubusercontent.com/BixData/cjson-elua/$CJSON_VERSION/src/cjson-strbuf.h /elua/src/modules/

ARG MOSES_VERSION=2.1.0-1
ADD https://raw.githubusercontent.com/BixData/moses-elua/$MOSES_VERSION/src/moses.c /elua/src/modules/

ARG URL_VERSION=1.2.1-1
ADD https://raw.githubusercontent.com/BixData/url-elua/$URL_VERSION/src/url.c /elua/src/modules/
ADD https://raw.githubusercontent.com/BixData/url-elua/$URL_VERSION/src/url_codec.c /elua/src/modules/
ADD https://raw.githubusercontent.com/BixData/url-elua/$URL_VERSION/src/url_codec.h /elua/src/modules/
ADD https://raw.githubusercontent.com/BixData/url-elua/$URL_VERSION/src/url_lauxhlib.c /elua/src/modules/
ADD https://raw.githubusercontent.com/BixData/url-elua/$URL_VERSION/src/url_lauxhlib.h /elua/src/modules/

ADD src/stuart.c /elua/src/modules/
ADD src/stuart_class.c /elua/src/modules/
ADD src/stuart_class.h /elua/src/modules/
ADD src/stuart_context.c /elua/src/modules/
ADD src/stuart_filesystem.c /elua/src/modules/
ADD src/stuart_filesystem.h /elua/src/modules/
ADD src/stuart_filesystemfactory.c /elua/src/modules/
ADD src/stuart_interface.c /elua/src/modules/
ADD src/stuart_localfilesystem.c /elua/src/modules/
ADD src/stuart_internal_logger.c /elua/src/modules/
ADD src/stuart_internal_logging.c /elua/src/modules/
ADD src/stuart_partition.c /elua/src/modules/
ADD src/stuart_rdd.c /elua/src/modules/
ADD src/stuart_sparkconf.c /elua/src/modules/
ADD src/stuart_streaming_dstream.c /elua/src/modules/
ADD src/stuart_streaming_dstream.h /elua/src/modules/
ADD src/stuart_streaming_queueinputdstream.c /elua/src/modules/
ADD src/stuart_streaming_receiver.c /elua/src/modules/
ADD src/stuart_streaming_receiverinputdstream.c /elua/src/modules/
ADD src/stuart_streaming_streamingcontext.c /elua/src/modules/
ADD src/stuart_streaming_transformeddstream.c /elua/src/modules/
ADD src/stuart_streaming_windoweddstream.c /elua/src/modules/
ADD src/stuart_util.c /elua/src/modules/

# ============================================================
# Compile tests into Lua bytecodes
# ============================================================
RUN mkdir -p romfs/examples romfs/test

RUN ./luac.cross -s -o romfs/examples/SparkPi.luo       examples/SparkPi.lua && \
    ./luac.cross -s -o romfs/test/class.luo             test/stuart_class.lua && \
    ./luac.cross -s -o romfs/test/stuart.luo            test/stuart.lua && \
    ./luac.cross -s -o romfs/test/Context.luo           test/stuart_Context.lua && \
    ./luac.cross -s -o romfs/test/fileSystemFactory.luo test/stuart_fileSystemFactory.lua && \
    ./luac.cross -s -o romfs/test/LocalFileSystem.luo   test/stuart_LocalFileSystem.lua && \
    ./luac.cross -s -o romfs/test/Partition.luo         test/stuart_Partition.lua && \
    ./luac.cross -s -o romfs/test/QueueInputDStream.luo test/stuart_QueueInputDStream.lua && \
    ./luac.cross -s -o romfs/test/RDD.luo               test/stuart_RDD.lua && \
    ./luac.cross -s -o romfs/test/SparkConf.luo         test/stuart_SparkConf.lua && \
    ./luac.cross -s -o romfs/test/StreamingContext.luo  test/stuart_StreamingContext.lua && \
    ./luac.cross -s -o romfs/test/util.luo              test/stuart_util.lua

# ============================================================
# Build eLua image
# ============================================================
ARG BOARD=STM3210E-EVAL
RUN lua build_elua.lua prog board=$BOARD

# ============================================================
# Configure runtime env, which is only used to copy build
# artifacts out of the Docker image.
#
# Sample usage:
#
# $ docker build -t elua .
# $ docker run -v `pwd`:/export -it elua
# 'elua_lua_stm3210e-eval.bin' -> '/export/elua_lua_stm3210e-eval.bin'
# 'elua_lua_stm3210e-eval.elf' -> '/export/elua_lua_stm3210e-eval.elf'
# 'elua_lua_stm3210e-eval.hex' -> '/export/elua_lua_stm3210e-eval.hex'
# ============================================================
VOLUME /export
CMD cp -v elua_lua* /export
