# Developing

The following method is used to transpile Lua sources to C.

## 1. Preprocessing

Edit the source Lua file and make the following changes:

### 1.1 Remove refs to current module or class framework closures

Replace all references to current module with a require of the current module.

Before:

```lua
local M = {}
function M.foo()
  M.bar()
end
function M.bar()
  ...
end
```

After:

```lua
local M = {}
function M.foo()
  local thismodule = require 'stuart.<current_module>'
  thismodule.bar()
end
function M.bar()
  ...
end
```

Before:

```lua
local class = require 'stuart.class'
local moses = require 'moses'
local Foo = class.new()
function Foo.bar()
  if class.istype(x, Foo) then return moses.noop() end
end
return Foo
```

After:

```lua
local class = require 'stuart.class'
local Foo = class.new()
function Foo.bar()
  local class = require 'stuart.class'
  local moses = require 'moses'
  if class.istype(x, Foo) then return moses.noop() end
end
return Foo
```

## 2. Transpiling

Invoke [lua2c](https://github.com/davidm/lua2c) to to transpile the source to C.

```sh
$ cd lua2c/lib
$ lua ../lua2c.lua MyModule.lua > MyModule.c
```

## 3. Post-processing

1. Rename all functions to namespace them, then sort them alphabetically
2. Remove static from function signatures
3. Update an eLua LTR ROMtable definition at the end of file

## Future Work

* I'm amazed by how good of a job [lua2c](https://github.com/davidm/lua2c) does. Impressive for a project that went inactive over 10 years ago! Now it needs some love.
	* When it generates helper functions for nested functions, they could use parameters instead of upvalues so that the helper functions are more "stateless", self-contained, testable, and less of a violation of encapsulation.
	* It could be given a new mode that generates module sources instead of main() sources. This would write out a `luaopen_mymodule(lua_State *)` function for you.
* A fork of [lua2c](https://github.com/davidm/lua2c) that is specific to eLua could write out an eLua LTR ROMtable for you.
