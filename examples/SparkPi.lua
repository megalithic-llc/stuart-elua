local moses = require 'moses'
local stuart = require 'stuart'

local slices = 2
local n = 10 * slices
local data = moses.range(1,n)
local sc = stuart.NewContext('local[1]', 'Spark Pi')
local count = sc:parallelize(data, slices):map(function(i)
  local x = math.random() * 2 - 1
  local y = math.random() * 2 - 1
  if x*x + y*y <= 1 then return 1 else return 0 end
end):sum()

print('Pi is roughly ' .. 4 * count / (n-1))
