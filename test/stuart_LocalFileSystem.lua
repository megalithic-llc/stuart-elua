print('Begin test')

local LocalFileSystem = require 'stuart.LocalFileSystem'


-- ============================================================================
-- Mini test framework
-- ============================================================================

local failures = 0

local function assertEquals(expected,actual,message)
  message = message or string.format('Expected %s but got %s', tostring(expected), tostring(actual))
  assert(actual==expected, message)
end

local function it(message, testFn)
  local status, err =  pcall(testFn)
  if status then
    print(string.format('✓ %s', message))
  else
    print(string.format('✖ %s', message))
    print(string.format('  FAILED: %s', err))
    failures = failures + 1
  end
end


-- ============================================================================
-- stuart.LocalFileSystem
-- ============================================================================

it('isDirectory() negative match', function()
  local fs = LocalFileSystem.new('/rom/data/mllib')
  assertEquals(false, fs:isDirectory('kmeans_data.txt'))
end)


-- ============================================================================
-- Mini test framework -- report results
-- ============================================================================

print(string.format('End of test: %d failures', failures))
