print('Begin test')

local stuart = require 'stuart'


-- ============================================================================
-- Mini test framework
-- ============================================================================

local failures = 0

local function assertEquals(expected,actual,message)
  message = message or string.format('Expected %s but got %s', tostring(expected), tostring(actual))
  assert(actual==expected, message)
end

local function it(message, testFn)
  local status, err =  pcall(testFn)
  if status then
    print(string.format('✓ %s', message))
  else
    print(string.format('✖ %s', message))
    print(string.format('  FAILED: %s', err))
    failures = failures + 1
  end
end


-- ============================================================================
-- stuart.streaming.StreamingContext
-- ============================================================================

it('can create multiple independent StreamingContext objects', function()
  local sc = stuart.NewContext()
  local ssc1 = stuart.NewStreamingContext(sc)
  local ssc2 = stuart.NewStreamingContext(sc)
end)

it('can timeout from awaitTerminationOrTimeout()', function()
  local sc = stuart.NewContext()
  local ssc = stuart.NewStreamingContext(sc)
  local now = require 'stuart.interface'.now
  local startTime = now() * 1000
  ssc:start()
  local timeoutSecs = .1
  ssc:awaitTerminationOrTimeout(timeoutSecs)
  local endTime = now() * 1000
  local elapsedTime = endTime - startTime
  assertEquals(true, elapsedTime >= 100 and elapsedTime < 5000)
end)


-- ============================================================================
-- Mini test framework -- report results
-- ============================================================================

print(string.format('End of test: %d failures', failures))
