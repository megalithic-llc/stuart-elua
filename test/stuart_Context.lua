print('Begin test')

local Context = require 'stuart.Context'


-- ============================================================================
-- Mini test framework
-- ============================================================================

local failures = 0

local function assertEquals(expected,actual,message)
  message = message or string.format('Expected %s but got %s', tostring(expected), tostring(actual))
  assert(actual==expected, message)
end

local function it(message, testFn)
  local status, err =  pcall(testFn)
  if status then
    print(string.format('✓ %s', message))
  else
    print(string.format('✖ %s', message))
    print(string.format('  FAILED: %s', err))
    failures = failures + 1
  end
end


-- ============================================================================
-- stuart.Context
-- ============================================================================

it('new(SparkConf)', function()
  local SparkConf = require 'stuart.SparkConf'
  local sparkConf = SparkConf.new():setMaster('local[1]'):setAppName('test/stuart_Context.lua')
  local sc = Context.new(sparkConf)
  assertEquals('local[1]', sc:master())
  assertEquals('test/stuart_Context.lua', sc:appName())
end)

it('new(string, string)', function()
  local sc = Context.new('local[1]', 'test/stuart_Context.lua')
  assertEquals('local[1]', sc:master())
  assertEquals('test/stuart_Context.lua', sc:appName())
end)

it('textFile()', function()
  local sc = Context.new('local[1]', 'test/stuart_Context.lua')
  local rdd = sc:textFile('/rom/data/mllib/kmeans_data.txt')
  assertEquals(6, #rdd:collect())
end)


-- ============================================================================
-- Mini test framework -- report results
-- ============================================================================

print(string.format('End of test: %d failures', failures))
