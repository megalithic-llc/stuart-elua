print('Begin test')

local fileSystemFactory = require 'stuart.fileSystemFactory'


-- ============================================================================
-- Mini test framework
-- ============================================================================

local failures = 0

local function assertEquals(expected,actual,message)
  message = message or string.format('Expected %s but got %s', tostring(expected), tostring(actual))
  assert(actual==expected, message)
end

local function it(message, testFn)
  local status, err =  pcall(testFn)
  if status then
    print(string.format('✓ %s', message))
  else
    print(string.format('✖ %s', message))
    print(string.format('  FAILED: %s', err))
    failures = failures + 1
  end
end


-- ============================================================================
-- stuart.fileSystemFactory
-- ============================================================================

it('returns a LocalFileSystem for an absolute path', function()
  local fs, openPath = fileSystemFactory.createForOpenPath('/rom/data/mllib/kmeans_data.txt')
  local class = require 'stuart.class'
  local LocalFileSystem = require 'stuart.LocalFileSystem'
  assertEquals(true, class.istype(fs, LocalFileSystem))
  assertEquals('/rom/data/mllib', fs:getUri())
  assertEquals('kmeans_data.txt', openPath)
end)


-- ============================================================================
-- Mini test framework -- report results
-- ============================================================================

print(string.format('End of test: %d failures', failures))
