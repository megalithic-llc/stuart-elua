print('Begin test')

local stuart = require 'stuart'
local moses = require 'moses'

local sc = stuart.NewContext()


-- ============================================================================
-- Mini test framework
-- ============================================================================

local failures = 0

local function assertContains(array, expected, message)
  for _,v in pairs(array) do
    if v == expected then return end
  end
  message = message or string.format('Expected array {%s} to contain %s', table.concat(array,','), tostring(expected))
  error(message)
end

local function assertContainsPair(array, value, message)
  for _, e in ipairs(array) do
    if moses.isEqual(e, value) then return end
  end
  local arrayAsStr = ''
  for i,v in ipairs(array) do
    if i > 1 then arrayAsStr = arrayAsStr .. ', ' end
    arrayAsStr = arrayAsStr .. '{' .. v[1] .. ',{' .. v[2][1] .. ',' .. v[2][2] .. '}}'
  end
  message = message or string.format('Expected array {%s} to contain pair {%s,%s}', arrayAsStr, tostring(value[1]), tostring(value[2]))
  error(message)
end

local function assertEquals(expected,actual,message)
  message = message or string.format('Expected %s but got %s', tostring(expected), tostring(actual))
  assert(actual==expected, message)
end

local function assertSame(expected,actual,message)
  message = message or string.format('Expected %s but got %s', tostring(expected), tostring(actual))
  assert(moses.isEqual(expected, actual), message)
end

local function it(message, testFn)
  local status, err =  pcall(testFn)
  if status then
    print(string.format('✓ %s', message))
  else
    print(string.format('✖ %s', message))
    print(string.format('  FAILED: %s', err))
    failures = failures + 1
  end
end


-- ============================================================================
-- stuart.streaming.QueueInputDStream
-- ============================================================================

it('can queue RDDs into a DStream', function()
  local ssc = stuart.NewStreamingContext(sc, 0.1)
  local rdd1 = sc:parallelize({'a', 'b'})
  local rdd2 = sc:parallelize({'c'})
  local rdds = {rdd1, rdd2}
  local dstream = ssc:queueStream(rdds)
  
  local r = {}
  dstream:foreachRDD(function(rdd)
    for _,v in ipairs(rdd:collect()) do table.insert(r, v) end
  end)
  
  ssc:start()
  ssc:awaitTerminationOrTimeout(0.3)
  
  assertContains(r, 'a')
  assertContains(r, 'b')
  assertContains(r, 'c')
end)

it('count()', function()
  local ssc = stuart.NewStreamingContext(sc, 0.1)
  local result = {}
  local dstream = ssc:queueStream({sc:makeRDD(moses.range(1,10)), sc:makeRDD({20,21})})
  dstream = dstream:count()
  dstream:foreachRDD(function(rdd)
    for _,v in ipairs(rdd:collect()) do result[#result+1] = v end
  end)

  ssc:start()
  ssc:awaitTerminationOrTimeout(0.25)
  
  local count = moses.reduce(result, function(r,e) return r+e end, 0)
  assertEquals(12, count)
end)

it('transform 1', function()
  local ssc = stuart.NewStreamingContext(sc, 0.1)
  local result = {}
  local dstream = ssc:queueStream({sc:makeRDD({1,2,3})})
  local x = dstream:transform(function(rdd)
    assertSame({1,2,3}, rdd:collect())
    return rdd:map(function(x) return x+1 end)
  end)
  x:foreachRDD(function(rdd)
    assertSame({2,3,4}, rdd:collect())
    result[#result+1] = {rdd:min(), rdd:max()}
  end)

  ssc:start()
  ssc:awaitTerminationOrTimeout(0.15)
  
  assertContainsPair(result, {2,4})
end)

it('transform 2', function()
  local ssc = stuart.NewStreamingContext(sc, 0.1)
  local result = {}
  local dstream = ssc:queueStream({sc:makeRDD({1,2,3}), sc:makeRDD({20,21})})
  local x = dstream:transform(function(rdd)
    return rdd:map(function(x) return x+1 end)
  end)
  x:foreachRDD(function(rdd) result[#result+1] = {rdd:min(), rdd:max()} end)

  ssc:start()
  ssc:awaitTerminationOrTimeout(0.25)
  
  assertContainsPair(result, {2,4})
  assertContainsPair(result, {21,22})
end)

it('transform 3', function()
  local ssc = stuart.NewStreamingContext(sc, 0.1)
  local result = {}
  ssc:queueStream({sc:makeRDD({1,2,3}), sc:makeRDD({5,6,7})})
    :transform(function(rdd)
      return rdd:map(function(x) return x+1 end) end)
    :transform(function(rdd)
      return rdd:map(function(x) return x*2 end) end)
    :foreachRDD(function(rdd)
      result[#result+1] = rdd:collect() end)

  ssc:start()
  ssc:awaitTerminationOrTimeout(0.25)
  
  assertContainsPair(result, {4,6,8})
  assertContainsPair(result, {12,14,16})
end)


-- ============================================================================
-- Mini test framework -- report results
-- ============================================================================

print(string.format('End of test: %d failures', failures))
