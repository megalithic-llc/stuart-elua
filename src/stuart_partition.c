#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
#include "common.h"
#include "auxmods.h"
#include "lrotable.h"
#include "platform_conf.h"
#define MIN_OPT_LEVEL 2
#include "lrodefs.h"
#include <assert.h>
#include "lua2c.h"
#include "stuart_class.h"


/* name: Partition:_init
 * function(data, index) */
static int stuart_Partition__init (lua_State * L) {
  enum { lc_nformalargs = 3 };
  lua_settop(L,3);

  /* self.data = data or {} */
  lua_pushvalue(L,2);
  if (!(lua_toboolean(L,-1))) {
    lua_pop(L,1);
    lua_newtable(L);
  }
  lua_pushliteral(L,"data");
  lua_insert(L,-2);
  lua_settable(L,1);
  assert(lua_gettop(L) == 3);

  /* self.index = index or 0 */
  lua_pushvalue(L,3);
  if (!(lua_toboolean(L,-1))) {
    lua_pop(L,1);
    lua_pushnumber(L,0);
  }
  lua_pushliteral(L,"index");
  lua_insert(L,-2);
  lua_settable(L,1);
  assert(lua_gettop(L) == 3);
  return 0;
}


/* name: Partition:_count
 * function() */
static int stuart_Partition__count (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* return #self.data */
  lua_pushliteral(L,"data");
  lua_gettable(L,1);
  const double lc2 = lua_objlen(L,-1);
  lua_pop(L,1);
  lua_pushnumber(L,lc2);
  return 1;
  assert(lua_gettop(L) == 1);
}


/* name: Partition:_flatten
 * function() */
static int stuart_Partition__flatten (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* local moses = require 'moses' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 2);

  /* self.data = moses.flatten(self.data) */
  lua_pushliteral(L,"flatten");
  lua_gettable(L,2);
  lua_pushliteral(L,"data");
  lua_gettable(L,1);
  lua_call(L,1,1);
  lua_pushliteral(L,"data");
  lua_insert(L,-2);
  lua_settable(L,1);
  assert(lua_gettop(L) == 2);

  /* return self */
  lua_pushvalue(L,1);
  return 1;
  assert(lua_gettop(L) == 2);
}


/* function(c) */
static int stuart_Partition__flattenValues_1 (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* t[#t+1] = c */
  lua_pushvalue(L,1);
  lc_getupvalue(L,lua_upvalueindex(1),0,4);
  lua_insert(L,-2);
  lc_getupvalue(L,lua_upvalueindex(1),0,4);
  const double lc7 = lua_objlen(L,-1);
  lua_pop(L,1);
  lua_pushnumber(L,lc7);
  lua_pushnumber(L,1);
  lc_add(L,-2,-1);
  lua_remove(L,-2);
  lua_remove(L,-2);
  lua_insert(L,-2);
  lua_settable(L,-3);
  lua_pop(L,1);
  assert(lua_gettop(L) == 1);
  return 0;
}


/* function(v) */
static int stuart_Partition__flattenValues_2 (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* table.insert(r, {e[1], v}) */
  lua_getfield(L,LUA_ENVIRONINDEX,"table");
  lua_pushliteral(L,"insert");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lc_getupvalue(L,lua_upvalueindex(1),0,2);
  lua_createtable(L,2,0);
  lc_getupvalue(L,lua_upvalueindex(1),0,3);
  lua_pushnumber(L,1);
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_rawseti(L,-2,1);
  lua_pushvalue(L,1);
  lua_rawseti(L,-2,2);
  lua_call(L,2,0);
  assert(lua_gettop(L) == 1);
  return 0;
}


/* function(r, e) */
static int stuart_Partition__flattenValues_3 (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);
  lc_newclosuretable(L,lua_upvalueindex(1));
  enum { lc3 = 3 };
  assert((lua_gettop(L) == lc3));
  lua_pushvalue(L,1);
  lua_rawseti(L,-2,2);
  lua_pushvalue(L,2);
  lua_rawseti(L,-2,3);

  /* local x = e[2] */
  lc_getupvalue(L,lc3,0,3);
  lua_pushnumber(L,2);
  lua_gettable(L,-2);
  lua_remove(L,-2);
  assert(lua_gettop(L) == 4);

  /* if moses.isString(x) then */
  enum { lc4 = 4 };
  lc_getupvalue(L,lc3,1,1);
  lua_pushliteral(L,"isString");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_pushvalue(L,4);
  lua_call(L,1,1);
  const int lc5 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc5) {

    /* local t = {} */
    lc_newclosuretable(L,lc3);
    enum { lc6 = 5 };
    assert((lua_gettop(L) == lc6));
    lua_newtable(L);
    lua_rawseti(L,lc6,4);
    assert(lua_gettop(L) == 5);

    /* x:gsub('.', function(c) t[#t+1] = c end) */
    lua_pushvalue(L,4);
    lua_pushliteral(L,"gsub");
    lua_gettable(L,-2);
    lua_insert(L,-2);
    lua_pushliteral(L,".");
    lua_pushvalue(L,lc6);
    lua_pushcclosure(L,stuart_Partition__flattenValues_1,1);
    lua_call(L,3,0);
    assert(lua_gettop(L) == 5);

    /* x = t */
    lc_getupvalue(L,lc6,0,4);
    lua_replace(L,4);
    assert(lua_gettop(L) == 5);
  }
  lua_settop(L,lc4);
  assert(lua_gettop(L) == 4);

  /* moses.map(x, function(v)
   *       table.insert(r, {e[1], v})
   *     end) */
  lc_getupvalue(L,lc3,1,1);
  lua_pushliteral(L,"map");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_pushvalue(L,4);
  lua_pushvalue(L,lc3);
  lua_pushcclosure(L,stuart_Partition__flattenValues_2,1);
  lua_call(L,2,0);
  assert(lua_gettop(L) == 4);

  /* return r */
  lc_getupvalue(L,lc3,0,2);
  return 1;
  assert(lua_gettop(L) == 4);
}


/* name: Partition:_flattenValues
 * function() */
static int stuart_Partition__flattenValues (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* local moses = require 'moses' */
  lc_newclosuretable(L,lua_upvalueindex(1));
  enum { lc2 = 2 };
  assert((lua_gettop(L) == lc2));
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  lua_rawseti(L,lc2,1);
  assert(lua_gettop(L) == 2);

  /* self.data = moses.reduce(self.data, function(r, e)
   *     local x = e[2]
   *     if moses.isString(x) then
   *       local t = {}
   *       x:gsub('.', function(c) t[#t+1] = c end)
   *       x = t
   *     end
   *     moses.map(x, function(v)
   *       table.insert(r, {e[1], v})
   *     end)
   *     return r
   *   end, {}) */
  lc_getupvalue(L,lc2,0,1);
  lua_pushliteral(L,"reduce");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_pushliteral(L,"data");
  lua_gettable(L,1);
  lua_pushvalue(L,lc2);
  lua_pushcclosure(L,stuart_Partition__flattenValues_3,1);
  lua_newtable(L);
  lua_call(L,3,1);
  lua_pushliteral(L,"data");
  lua_insert(L,-2);
  lua_settable(L,1);
  assert(lua_gettop(L) == 2);

  /* return self */
  lua_pushvalue(L,1);
  return 1;
  assert(lua_gettop(L) == 2);
}


/* function() */
static int stuart_Partition__toLocalIterator_1 (lua_State * L) {
  enum { lc_nformalargs = 0 };
  lua_settop(L,0);

  /* i = i + 1 */
  lc_getupvalue(L,lua_upvalueindex(1),0,6);
  lua_pushnumber(L,1);
  lc_add(L,-2,-1);
  lua_remove(L,-2);
  lua_remove(L,-2);
  lc_setupvalue(L,lua_upvalueindex(1),0,6);
  assert(lua_gettop(L) == 0);

  /* return self.data[i] */
  lc_getupvalue(L,lua_upvalueindex(1),1,5);
  lua_pushliteral(L,"data");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lc_getupvalue(L,lua_upvalueindex(1),0,6);
  lua_gettable(L,-2);
  lua_remove(L,-2);
  return 1;
  assert(lua_gettop(L) == 0);
}


/* name: Partition:_toLocalIterator
 * function() */
static int stuart_Partition__toLocalIterator (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);
  lc_newclosuretable(L,lua_upvalueindex(1));
  enum { lc11 = 2 };
  assert((lua_gettop(L) == lc11));
  lua_pushvalue(L,1);
  lua_rawseti(L,-2,5);

  /* local i = 0 */
  lc_newclosuretable(L,lc11);
  enum { lc12 = 3 };
  assert((lua_gettop(L) == lc12));
  lua_pushnumber(L,0);
  lua_rawseti(L,lc12,6);
  assert(lua_gettop(L) == 3);

  /* return function()
   *     i = i + 1
   *     return self.data[i]
   *   end */
  lua_pushvalue(L,lc12);
  lua_pushcclosure(L,stuart_Partition__toLocalIterator_1,1);
  return 1;
  assert(lua_gettop(L) == 3);
}


/* name: M.classof
 * function(obj) */
static int stuart_Partition_classof (lua_State * L) {
  return stuart_class_shared_classof(L, "stuart.Partition");
}


/* name: M.new
 * function(...) */
static int stuart_Partition_new (lua_State * L) {
  return stuart_class_shared_new(L, "stuart.Partition");
}


// ============================================================================
// Definition of the module Romtable.
// ============================================================================

const LUA_REG_TYPE stuart_partition_map[] = {
  { LSTRKEY("_count")          , LFUNCVAL(stuart_Partition__count) },
  { LSTRKEY("_flatten")        , LFUNCVAL(stuart_Partition__flatten) },
  { LSTRKEY("_flattenValues")  , LFUNCVAL(stuart_Partition__flattenValues) },
  { LSTRKEY("_init")           , LFUNCVAL(stuart_Partition__init) },
  { LSTRKEY("_toLocalIterator"), LFUNCVAL(stuart_Partition__toLocalIterator) },

  // class framework
  { LSTRKEY("__index")         , LRO_ROVAL(stuart_partition_map) },
  { LSTRKEY("_class")          , LRO_ROVAL(stuart_partition_map) },
  { LSTRKEY("classof")         , LFUNCVAL(stuart_Partition_classof) },
  { LSTRKEY("new" )            , LFUNCVAL(stuart_Partition_new) },

  { LNILKEY, LNILVAL }
};


// ============================================================================
// Registration of the module.
// ============================================================================
LUALIB_API int luaopen_stuart_partition( lua_State *L ){
#if LUA_OPTIMIZE_MEMORY > 0
  return 0;
#else
  LREGISTER( L, AUXLIB_STUART_PARTITION, stuart_partition_map );
  return 1;
#endif
}
