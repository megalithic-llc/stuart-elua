local class = require 'stuart.class'
local FileSystem = require 'stuart.FileSystem'

local LocalFileSystem = class.new(FileSystem)

function LocalFileSystem:_init(uri)
  self:super(uri)
end

function LocalFileSystem:isDirectory(path)
  local f = io.open(self.uri .. '/' .. (path or ''), 'r')
  local isDir = not f:read(0) and f:seek('end') ~= 0
  f:close()
  return isDir
end

function LocalFileSystem:listStatus(path)
  error('list directory capability not present')
end

function LocalFileSystem:open(path)
  local f = assert(io.open(self.uri .. '/' .. path, 'r'))
  local data = f:read '*all'
  return data
end

return LocalFileSystem
