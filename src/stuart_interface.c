#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
#include "common.h"
#include "auxmods.h"
#include "lrotable.h"
#include "platform_conf.h"
#define MIN_OPT_LEVEL 2
#include "lrodefs.h"
#include <assert.h>
#include "lua2c.h"


/* name: M.clockPrecision
 * function() */
static int stuart_interface_clockPrecision (lua_State * L) {
  enum { lc_nformalargs = 0 };
  lua_settop(L,0);
  lua_pushnumber(L,6);
  return 1;
}


/* name: M.now
 * function() */
static int stuart_interface_now (lua_State * L) {
  enum { lc_nformalargs = 0 };
  lua_settop(L,0);

  /* return tmr.read(tmr.SYS_TIMER) / 1e6 */
  lua_getfield(L,LUA_ENVIRONINDEX,"tmr");
  lua_pushliteral(L,"read");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_getfield(L,LUA_ENVIRONINDEX,"tmr");
  lua_pushliteral(L,"SYS_TIMER");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_call(L,1,1);
  lua_pushnumber(L,1000000);
  lc_div(L,-2,-1);
  lua_remove(L,-2);
  lua_remove(L,-2);
  return 1;
  assert(lua_gettop(L) == 0);
}


/* name: M.sleep
 * function(duration) */
static int stuart_interface_sleep (lua_State * L) {
  return 0; // eLua is not capable
}


// ============================================================================
// Definition of the module Romtable.
// ============================================================================

const LUA_REG_TYPE stuart_interface_map[] = {
  { LSTRKEY( "clockPrecision" ), LFUNCVAL( stuart_interface_clockPrecision ) },
  { LSTRKEY( "now" ), LFUNCVAL( stuart_interface_now ) },
  { LSTRKEY( "sleep" ), LFUNCVAL( stuart_interface_sleep ) },
  { LNILKEY, LNILVAL }
};


// ============================================================================
// Registration of the module.
// ============================================================================
LUALIB_API int luaopen_stuart_interface( lua_State *L ){
#if LUA_OPTIMIZE_MEMORY > 0
  return 0;
#else
  LREGISTER( L, AUXLIB_STUART_INTERFACE, stuart_interface_map );
  return 1;
#endif
}
