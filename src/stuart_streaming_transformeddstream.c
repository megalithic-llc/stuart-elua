#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
#include "common.h"
#include "auxmods.h"
#include "lrotable.h"
#include "platform_conf.h"
#define MIN_OPT_LEVEL 2
#include "lrodefs.h"
#include <assert.h>
#include "lua2c.h"
#include "stuart_class.h"
#include "stuart_streaming_dstream.h"

#define MODULE "stuart.streaming.TransformedDStream"
#define SUPERMODULE "stuart.streaming.DStream"


/* name: TransformedDStream:_init
 * function(ssc, transformFunc) */
int stuart_streaming_TransformedDStream__init (lua_State * L) {
  enum { lc_nformalargs = 3 };
  lua_settop(L,3);

  /* local DStream = require 'stuart.streaming.DStream' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,SUPERMODULE);
  lua_call(L,1,1);
  assert(lua_gettop(L) == 4);

  /* DStream._init(self, ssc) */
  lua_pushliteral(L,"_init");
  lua_gettable(L,4);
  lua_pushvalue(L,1);
  lua_pushvalue(L,2);
  lua_call(L,2,0);
  assert(lua_gettop(L) == 4);

  /* self.transformFunc = transformFunc */
  lua_pushvalue(L,3);
  lua_pushliteral(L,"transformFunc");
  lua_insert(L,-2);
  lua_settable(L,1);
  assert(lua_gettop(L) == 4);
  return 0;
}


/* name: TransformedDStream:_notify
 * function(validTime, rdd) */
int stuart_streaming_TransformedDStream__notify (lua_State * L) {
  enum { lc_nformalargs = 3 };
  lua_settop(L,3);

  /* rdd = self.transformFunc(rdd, validTime) */
  lua_pushliteral(L,"transformFunc");
  lua_gettable(L,1);
  lua_pushvalue(L,3);
  lua_pushvalue(L,2);
  lua_call(L,2,1);
  lua_replace(L,3);
  assert(lua_gettop(L) == 3);

  /* for _, dstream in ipairs(self.inputs) do
   * internal: local f, s, var = explist */
  enum { lc1 = 3 };
  lua_getfield(L,LUA_ENVIRONINDEX,"ipairs");
  lua_pushliteral(L,"inputs");
  lua_gettable(L,1);
  lua_call(L,1,3);
  while (1) {

    /* internal: local var_1, ..., var_n = f(s, var)
     *           if var_1 == nil then break end
     *           var = var_1 */
    lua_pushvalue(L,-3);
    lua_pushvalue(L,-3);
    lua_pushvalue(L,-3);
    lua_call(L,2,2);
    if (lua_isnil(L,-2)) {
      break;
    }
    lua_pushvalue(L,-2);
    lua_replace(L,-4);

    /* internal: local _ with idx 7
     * internal: local dstream with idx 8 */


    /* rdd = dstream:_notify(validTime, rdd) */
    lua_pushvalue(L,8);
    lua_pushliteral(L,"_notify");
    lua_gettable(L,-2);
    lua_insert(L,-2);
    lua_pushvalue(L,2);
    lua_pushvalue(L,3);
    lua_call(L,3,1);
    lua_replace(L,3);
    assert(lua_gettop(L) == 8);

    /* internal: stack cleanup on scope exit */
    lua_pop(L,2);
  }
  lua_settop(L,lc1);
  assert(lua_gettop(L) == 3);

  /* for _, dstream in ipairs(self.outputs) do
   * internal: local f, s, var = explist */
  enum { lc2 = 3 };
  lua_getfield(L,LUA_ENVIRONINDEX,"ipairs");
  lua_pushliteral(L,"outputs");
  lua_gettable(L,1);
  lua_call(L,1,3);
  while (1) {

    /* internal: local var_1, ..., var_n = f(s, var)
     *           if var_1 == nil then break end
     *           var = var_1 */
    lua_pushvalue(L,-3);
    lua_pushvalue(L,-3);
    lua_pushvalue(L,-3);
    lua_call(L,2,2);
    if (lua_isnil(L,-2)) {
      break;
    }
    lua_pushvalue(L,-2);
    lua_replace(L,-4);

    /* internal: local _ with idx 7
     * internal: local dstream with idx 8 */


    /* dstream:_notify(validTime, rdd) */
    lua_pushvalue(L,8);
    lua_pushliteral(L,"_notify");
    lua_gettable(L,-2);
    lua_insert(L,-2);
    lua_pushvalue(L,2);
    lua_pushvalue(L,3);
    lua_call(L,3,0);
    assert(lua_gettop(L) == 8);

    /* internal: stack cleanup on scope exit */
    lua_pop(L,2);
  }
  lua_settop(L,lc2);
  assert(lua_gettop(L) == 3);

  /* return rdd */
  lua_pushvalue(L,3);
  return 1;
  assert(lua_gettop(L) == 3);
}


/* name: M.classof
 * function(obj) */
int stuart_streaming_TransformedDStream_classof (lua_State * L) {
  return stuart_class_shared_classof(L, MODULE);
}


/* name: M.new
 * function(...) */
int stuart_streaming_TransformedDStream_new (lua_State * L) {
  return stuart_class_shared_new_with_super(L, MODULE, SUPERMODULE);
}


// ============================================================================
// Definition of the module Romtable.
// ============================================================================

const LUA_REG_TYPE stuart_xdstream_map[] = {
  { LSTRKEY("_init")           , LFUNCVAL (stuart_streaming_TransformedDStream__init) },
  { LSTRKEY("_notify")         , LFUNCVAL (stuart_streaming_TransformedDStream__notify) },

  // class framework
  { LSTRKEY("__index")         , LRO_ROVAL(stuart_xdstream_map) },
  { LSTRKEY("_base")           , LRO_ROVAL(stuart_dstream_map) },
  { LSTRKEY("_class")          , LRO_ROVAL(stuart_xdstream_map) },
  { LSTRKEY("classof")         , LFUNCVAL (stuart_streaming_TransformedDStream_classof) },
  { LSTRKEY("new")             , LFUNCVAL (stuart_streaming_TransformedDStream_new) },

  // inherited (not including __index, _class, _init, classof, and new)
  { LSTRKEY("_notify")         , LFUNCVAL (stuart_streaming_DStream__notify) },
  { LSTRKEY("count")           , LFUNCVAL (stuart_streaming_DStream_count) },
  { LSTRKEY("countByWindow")   , LFUNCVAL (stuart_streaming_DStream_countByWindow) },
  { LSTRKEY("foreachRDD")      , LFUNCVAL (stuart_streaming_DStream_foreachRDD) },
  { LSTRKEY("groupByKey")      , LFUNCVAL (stuart_streaming_DStream_groupByKey) },
  { LSTRKEY("map")             , LFUNCVAL (stuart_streaming_DStream_map) },
  { LSTRKEY("mapValues")       , LFUNCVAL (stuart_streaming_DStream_mapValues) },
  { LSTRKEY("poll")            , LFUNCVAL (stuart_streaming_DStream_poll) },
  { LSTRKEY("reduce")          , LFUNCVAL (stuart_streaming_DStream_reduce) },
  { LSTRKEY("start")           , LFUNCVAL (stuart_streaming_DStream_start) },
  { LSTRKEY("stop")            , LFUNCVAL (stuart_streaming_DStream_stop) },
  { LSTRKEY("transform")       , LFUNCVAL (stuart_streaming_DStream_transform) },
  { LSTRKEY("window")          , LFUNCVAL (stuart_streaming_DStream_window) },

  { LNILKEY, LNILVAL }
};


// ============================================================================
// Registration of the module.
// ============================================================================
LUALIB_API int luaopen_stuart_xdstream( lua_State *L ){
#if LUA_OPTIMIZE_MEMORY > 0
  return 0;
#else
  LREGISTER( L, AUXLIB_STUART_XDSTREAM, stuart_xdstream_map );
  return 1;
#endif
}
