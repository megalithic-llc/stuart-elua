#ifndef __STUART_FILESYSTEM_H__
#define __STUART_FILESYSTEM_H__

#include "lua.h"

extern LUA_REG_TYPE stuart_filesystem_map[];

int stuart_FileSystem__init (lua_State * L);
int stuart_FileSystem_getUri (lua_State * L);
int stuart_FileSystem_classof (lua_State * L);
int stuart_FileSystem_new (lua_State * L);

#endif
