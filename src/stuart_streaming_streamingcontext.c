#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
#include "common.h"
#include "auxmods.h"
#include "lrotable.h"
#include "platform_conf.h"
#define MIN_OPT_LEVEL 2
#include "lrodefs.h"
#include <assert.h>
#include "lua2c.h"
#include "stuart_class.h"


/* name: StreamingContext:_init
 * function(sc, batchDuration) */
int stuart_streaming_StreamingContext__init (lua_State * L) {
  enum { lc_nformalargs = 3 };
  lua_settop(L,3);

  /* self.sc = sc */
  lua_pushvalue(L,2);
  lua_pushliteral(L,"sc");
  lua_insert(L,-2);
  lua_settable(L,1);
  assert(lua_gettop(L) == 3);

  /* self.sparkContext = sc */
  lua_pushvalue(L,2);
  lua_pushliteral(L,"sparkContext");
  lua_insert(L,-2);
  lua_settable(L,1);
  assert(lua_gettop(L) == 3);

  /* self.batchDuration = batchDuration or 1 */
  lua_pushvalue(L,3);
  if (!(lua_toboolean(L,-1))) {
    lua_pop(L,1);
    lua_pushnumber(L,1);
  }
  lua_pushliteral(L,"batchDuration");
  lua_insert(L,-2);
  lua_settable(L,1);
  assert(lua_gettop(L) == 3);

  /* self.dstreams={} */
  lua_newtable(L);
  lua_pushliteral(L,"dstreams");
  lua_insert(L,-2);
  lua_settable(L,1);
  assert(lua_gettop(L) == 3);

  /* self.state='initialized' */
  lua_pushliteral(L,"initialized");
  lua_pushliteral(L,"state");
  lua_insert(L,-2);
  lua_settable(L,1);
  assert(lua_gettop(L) == 3);

  /* self.conf = sc.conf */
  lua_pushliteral(L,"conf");
  lua_gettable(L,2);
  lua_pushliteral(L,"conf");
  lua_insert(L,-2);
  lua_settable(L,1);
  assert(lua_gettop(L) == 3);
  return 0;
}


/* name: StreamingContext:awaitTermination
 * function() */
int stuart_streaming_StreamingContext_awaitTermination (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* self:awaitTerminationOrTimeout(0) */
  lua_pushvalue(L,1);
  lua_pushliteral(L,"awaitTerminationOrTimeout");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_pushnumber(L,0);
  lua_call(L,2,0);
  assert(lua_gettop(L) == 1);
  return 0;
}


/* name: StreamingContext:awaitTerminationOrTimeout
 * function(timeout) */
int stuart_streaming_StreamingContext_awaitTerminationOrTimeout (lua_State * L) {
  lua_checkstack(L,30);
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* local moses = require 'moses' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 3);

  /* if not moses.isNumber(timeout) or timeout < 0 then */
  enum { lc2 = 3 };
  lua_pushliteral(L,"isNumber");
  lua_gettable(L,3);
  lua_pushvalue(L,2);
  lua_call(L,1,1);
  lua_pushboolean(L,!(lua_toboolean(L,-1)));
  lua_remove(L,-2);
  if (!(lua_toboolean(L,-1))) {
    lua_pop(L,1);
    lua_pushnumber(L,0);
    const int lc3 = lua_lessthan(L,2,-1);
    lua_pop(L,1);
    lua_pushboolean(L,lc3);
  }
  const int lc4 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc4) {

    /* error('Invalid timeout') */
    lua_getfield(L,LUA_ENVIRONINDEX,"error");
    lua_pushliteral(L,"Invalid timeout");
    lua_call(L,1,0);
    assert(lua_gettop(L) == 3);
  }
  lua_settop(L,lc2);
  assert(lua_gettop(L) == 3);

  /* -- run loop
   * local now = require 'stuart.interface'.now */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart.interface");
  lua_call(L,1,1);
  lua_pushliteral(L,"now");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  assert(lua_gettop(L) == 4);

  /* local startTime = now() */
  lua_pushvalue(L,4);
  lua_call(L,0,1);
  assert(lua_gettop(L) == 5);

  /* local loopDurationGoal = self.batchDuration */
  lua_pushliteral(L,"batchDuration");
  lua_gettable(L,1);
  assert(lua_gettop(L) == 6);

  /* local individualDStreamDurationBudget = loopDurationGoal / #self.dstreams */
  lua_pushliteral(L,"dstreams");
  lua_gettable(L,1);
  const double lc5 = lua_objlen(L,-1);
  lua_pop(L,1);
  lua_pushnumber(L,lc5);
  lc_div(L,6,-1);
  lua_remove(L,-2);
  assert(lua_gettop(L) == 7);

  /* local sleep = require 'stuart.interface'.sleep */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart.interface");
  lua_call(L,1,1);
  lua_pushliteral(L,"sleep");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  assert(lua_gettop(L) == 8);

  /* while self.state == 'active' do
   *
   *     -- Decide whether to timeout */
  enum { lc6 = 8 };
  while (1) {
    lua_pushliteral(L,"state");
    lua_gettable(L,1);
    lua_pushliteral(L,"active");
    const int lc7 = lua_equal(L,-2,-1);
    lua_pop(L,2);
    lua_pushboolean(L,lc7);
    if (!(lua_toboolean(L,-1))) {
      break;
    }
    lua_pop(L,1);

    /* -- Decide whether to timeout
     * local currentTime = now() */
    lua_pushvalue(L,4);
    lua_call(L,0,1);
    assert(lua_gettop(L) == 9);

    /* if timeout > 0 then */
    enum { lc8 = 9 };
    lua_pushnumber(L,0);
    const int lc9 = lua_lessthan(L,-1,2);
    lua_pop(L,1);
    lua_pushboolean(L,lc9);
    const int lc10 = lua_toboolean(L,-1);
    lua_pop(L,1);
    if (lc10) {

      /* local elapsed = currentTime - startTime */
      lc_sub(L,9,5);
      assert(lua_gettop(L) == 10);

      /* if elapsed > timeout then */
      enum { lc11 = 10 };
      const int lc12 = lua_lessthan(L,2,10);
      lua_pushboolean(L,lc12);
      const int lc13 = lua_toboolean(L,-1);
      lua_pop(L,1);
      if (lc13) {

        /* break */
        break;
        assert(lua_gettop(L) == 10);
      }
      lua_settop(L,lc11);
      assert(lua_gettop(L) == 10);
    }
    lua_settop(L,lc8);
    assert(lua_gettop(L) == 9);

    /* -- Run each dstream poll() function, until it returns
     * for _, dstream in ipairs(self.dstreams) do
     * internal: local f, s, var = explist */
    enum { lc14 = 9 };
    lua_getfield(L,LUA_ENVIRONINDEX,"ipairs");
    lua_pushliteral(L,"dstreams");
    lua_gettable(L,1);
    lua_call(L,1,3);
    while (1) {

      /* internal: local var_1, ..., var_n = f(s, var)
       *           if var_1 == nil then break end
       *           var = var_1 */
      lua_pushvalue(L,-3);
      lua_pushvalue(L,-3);
      lua_pushvalue(L,-3);
      lua_call(L,2,2);
      if (lua_isnil(L,-2)) {
        break;
      }
      lua_pushvalue(L,-2);
      lua_replace(L,-4);

      /* internal: local _ with idx 13
       * internal: local dstream with idx 14 */


      /* local rdds = dstream:poll(individualDStreamDurationBudget) */
      lua_pushvalue(L,14);
      lua_pushliteral(L,"poll");
      lua_gettable(L,-2);
      lua_insert(L,-2);
      lua_pushvalue(L,7);
      lua_call(L,2,1);
      assert(lua_gettop(L) == 15);

      /* for _, rdd in ipairs(rdds) do
       * internal: local f, s, var = explist */
      enum { lc15 = 15 };
      lua_getfield(L,LUA_ENVIRONINDEX,"ipairs");
      lua_pushvalue(L,15);
      lua_call(L,1,3);
      while (1) {

        /* internal: local var_1, ..., var_n = f(s, var)
         *           if var_1 == nil then break end
         *           var = var_1 */
        lua_pushvalue(L,-3);
        lua_pushvalue(L,-3);
        lua_pushvalue(L,-3);
        lua_call(L,2,2);
        if (lua_isnil(L,-2)) {
          break;
        }
        lua_pushvalue(L,-2);
        lua_replace(L,-4);

        /* internal: local _ with idx 19
         * internal: local rdd with idx 20 */


        /* internal: stack cleanup on scope exit */
        lua_pop(L,2);
      }
      lua_settop(L,lc15);
      assert(lua_gettop(L) == 15);

      /* if rdds ~= nil and #rdds > 0 then */
      enum { lc16 = 15 };
      lua_pushnil(L);
      const int lc17 = lua_equal(L,15,-1);
      lua_pop(L,1);
      lua_pushboolean(L,lc17);
      lua_pushboolean(L,!(lua_toboolean(L,-1)));
      lua_remove(L,-2);
      if (lua_toboolean(L,-1)) {
        lua_pop(L,1);
        lua_pushnumber(L,0);
        const double lc18 = lua_objlen(L,15);
        lua_pushnumber(L,lc18);
        const int lc19 = lua_lessthan(L,-2,-1);
        lua_pop(L,2);
        lua_pushboolean(L,lc19);
      }
      const int lc20 = lua_toboolean(L,-1);
      lua_pop(L,1);
      if (lc20) {

        /* for _, rdd in ipairs(rdds) do
         * internal: local f, s, var = explist */
        enum { lc21 = 15 };
        lua_getfield(L,LUA_ENVIRONINDEX,"ipairs");
        lua_pushvalue(L,15);
        lua_call(L,1,3);
        while (1) {

          /* internal: local var_1, ..., var_n = f(s, var)
           *           if var_1 == nil then break end
           *           var = var_1 */
          lua_pushvalue(L,-3);
          lua_pushvalue(L,-3);
          lua_pushvalue(L,-3);
          lua_call(L,2,2);
          if (lua_isnil(L,-2)) {
            break;
          }
          lua_pushvalue(L,-2);
          lua_replace(L,-4);

          /* internal: local _ with idx 19
           * internal: local rdd with idx 20 */


          /* dstream:_notify(currentTime, rdd) */
          lua_pushvalue(L,14);
          lua_pushliteral(L,"_notify");
          lua_gettable(L,-2);
          lua_insert(L,-2);
          lua_pushvalue(L,9);
          lua_pushvalue(L,20);
          lua_call(L,3,0);
          assert(lua_gettop(L) == 20);

          /* internal: stack cleanup on scope exit */
          lua_pop(L,2);
        }
        lua_settop(L,lc21);
        assert(lua_gettop(L) == 15);
      }
      lua_settop(L,lc16);
      assert(lua_gettop(L) == 15);

      /* internal: stack cleanup on scope exit */
      lua_pop(L,3);
    }
    lua_settop(L,lc14);
    assert(lua_gettop(L) == 9);

    /* sleep(loopDurationGoal) */
    lua_pushvalue(L,8);
    lua_pushvalue(L,6);
    lua_call(L,1,0);
    assert(lua_gettop(L) == 9);

    /* internal: stack cleanup on scope exit */
    lua_pop(L,1);
  }
  lua_settop(L,lc6);
  assert(lua_gettop(L) == 8);
  return 0;
}


/* name: M.classof
 * function(obj) */
int stuart_streaming_StreamingContext_classof (lua_State * L) {
  return stuart_class_shared_classof(L, "stuart.streaming.StreamingContext");
}


/* name: StreamingContext:getState
 * function() */
int stuart_streaming_StreamingContext_getState (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* return self.state */
  lua_pushliteral(L,"state");
  lua_gettable(L,1);
  return 1;
  assert(lua_gettop(L) == 1);
}


/* name: M.new
 * function(...) */
int stuart_streaming_StreamingContext_new (lua_State * L) {
  return stuart_class_shared_new(L, "stuart.streaming.StreamingContext");
}


/* function(rdd) */
static int stuart_streaming_StreamingContext_queueStream_1 (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* if not class.istype(rdd, RDD) then */
  enum { lc26 = 1 };
  lc_getupvalue(L,lua_upvalueindex(1),0,3);
  lua_pushliteral(L,"istype");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_pushvalue(L,1);
  lc_getupvalue(L,lua_upvalueindex(1),1,2);
  lua_call(L,2,1);
  lua_pushboolean(L,!(lua_toboolean(L,-1)));
  lua_remove(L,-2);
  const int lc27 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc27) {

    /* rdd = self.sc:makeRDD(rdd) */
    lc_getupvalue(L,lua_upvalueindex(1),2,1);
    lua_pushliteral(L,"sc");
    lua_gettable(L,-2);
    lua_remove(L,-2);
    lua_pushliteral(L,"makeRDD");
    lua_gettable(L,-2);
    lua_insert(L,-2);
    lua_pushvalue(L,1);
    lua_call(L,2,1);
    lua_replace(L,1);
    assert(lua_gettop(L) == 1);
  }
  lua_settop(L,lc26);
  assert(lua_gettop(L) == 1);

  /* return rdd */
  lua_pushvalue(L,1);
  return 1;
  assert(lua_gettop(L) == 1);
}


/* name: StreamingContext:queueStream
 * function(rdds, oneAtATime) */
int stuart_streaming_StreamingContext_queueStream (lua_State * L) {
  enum { lc_nformalargs = 3 };
  lua_settop(L,3);
  lc_newclosuretable(L,lua_upvalueindex(1));
  enum { lc21 = 4 };
  assert((lua_gettop(L) == lc21));
  lua_pushvalue(L,1);
  lua_rawseti(L,-2,1);

  /* local moses = require 'moses' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 5);

  /* if not moses.isBoolean(oneAtATime) then */
  enum { lc22 = 5 };
  lua_pushliteral(L,"isBoolean");
  lua_gettable(L,5);
  lua_pushvalue(L,3);
  lua_call(L,1,1);
  lua_pushboolean(L,!(lua_toboolean(L,-1)));
  lua_remove(L,-2);
  const int lc23 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc23) {

    /* oneAtATime = true */
    lua_pushboolean(L,1);
    lua_replace(L,3);
    assert(lua_gettop(L) == 5);
  }
  lua_settop(L,lc22);
  assert(lua_gettop(L) == 5);

  /* local RDD = require 'stuart.RDD' */
  lc_newclosuretable(L,lc21);
  enum { lc24 = 6 };
  assert((lua_gettop(L) == lc24));
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart.RDD");
  lua_call(L,1,1);
  lua_rawseti(L,lc24,2);
  assert(lua_gettop(L) == 6);

  /* local class = require 'stuart.class' */
  lc_newclosuretable(L,lc24);
  enum { lc25 = 7 };
  assert((lua_gettop(L) == lc25));
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart.class");
  lua_call(L,1,1);
  lua_rawseti(L,lc25,3);
  assert(lua_gettop(L) == 7);

  /* rdds = moses.map(rdds, function(rdd)
   *     if not class.istype(rdd, RDD) then rdd = self.sc:makeRDD(rdd) end
   *     return rdd
   *   end) */
  lua_pushliteral(L,"map");
  lua_gettable(L,5);
  lua_pushvalue(L,2);
  lua_pushvalue(L,lc25);
  lua_pushcclosure(L,stuart_streaming_StreamingContext_queueStream_1,1);
  lua_call(L,2,1);
  lua_replace(L,2);
  assert(lua_gettop(L) == 7);

  /* local QueueInputDStream = require 'stuart.streaming.QueueInputDStream' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart.streaming.QueueInputDStream");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 8);

  /* local dstream = QueueInputDStream.new(self, rdds, oneAtATime) */
  lua_pushliteral(L,"new");
  lua_gettable(L,8);
  lc_getupvalue(L,lc25,2,1);
  lua_pushvalue(L,2);
  lua_pushvalue(L,3);
  lua_call(L,3,1);
  assert(lua_gettop(L) == 9);

  /* self.dstreams[#self.dstreams+1] = dstream */
  lua_pushvalue(L,9);
  lc_getupvalue(L,lc25,2,1);
  lua_pushliteral(L,"dstreams");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_insert(L,-2);
  lc_getupvalue(L,lc25,2,1);
  lua_pushliteral(L,"dstreams");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  const double lc29 = lua_objlen(L,-1);
  lua_pop(L,1);
  lua_pushnumber(L,lc29);
  lua_pushnumber(L,1);
  lc_add(L,-2,-1);
  lua_remove(L,-2);
  lua_remove(L,-2);
  lua_insert(L,-2);
  lua_settable(L,-3);
  lua_pop(L,1);
  assert(lua_gettop(L) == 9);

  /* return dstream */
  lua_pushvalue(L,9);
  return 1;
  assert(lua_gettop(L) == 9);
}


/* name: StreamingContext:receiverStream
 * function(receiver) */
int stuart_streaming_StreamingContext_receiverStream (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* local ReceiverInputDStream = require 'stuart.streaming.ReceiverInputDStream' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart.streaming.ReceiverInputDStream");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 3);

  /* local dstream = ReceiverInputDStream.new(self, receiver) */
  lua_pushliteral(L,"new");
  lua_gettable(L,3);
  lua_pushvalue(L,1);
  lua_pushvalue(L,2);
  lua_call(L,2,1);
  assert(lua_gettop(L) == 4);

  /* self.dstreams[#self.dstreams+1] = dstream */
  lua_pushvalue(L,4);
  lua_pushliteral(L,"dstreams");
  lua_gettable(L,1);
  lua_insert(L,-2);
  lua_pushliteral(L,"dstreams");
  lua_gettable(L,1);
  const double lc30 = lua_objlen(L,-1);
  lua_pop(L,1);
  lua_pushnumber(L,lc30);
  lua_pushnumber(L,1);
  lc_add(L,-2,-1);
  lua_remove(L,-2);
  lua_remove(L,-2);
  lua_insert(L,-2);
  lua_settable(L,-3);
  lua_pop(L,1);
  assert(lua_gettop(L) == 4);

  /* return dstream */
  lua_pushvalue(L,4);
  return 1;
  assert(lua_gettop(L) == 4);
}


/* name: StreamingContext:socketTextStream
 * function(hostname, port) */
static int stuart_streaming_StreamingContext_socketTextStream (lua_State * L) {
  enum { lc_nformalargs = 3 };
  lua_settop(L,3);

  /* local SocketInputDStream = require 'stuart.streaming.SocketInputDStream' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart.streaming.SocketInputDStream");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 4);

  /* local dstream = SocketInputDStream.new(self, hostname, port) */
  lua_pushliteral(L,"new");
  lua_gettable(L,4);
  lua_pushvalue(L,1);
  lua_pushvalue(L,2);
  lua_pushvalue(L,3);
  lua_call(L,3,1);
  assert(lua_gettop(L) == 5);

  /* self.dstreams[#self.dstreams+1] = dstream */
  lua_pushvalue(L,5);
  lua_pushliteral(L,"dstreams");
  lua_gettable(L,1);
  lua_insert(L,-2);
  lua_pushliteral(L,"dstreams");
  lua_gettable(L,1);
  const double lc31 = lua_objlen(L,-1);
  lua_pop(L,1);
  lua_pushnumber(L,lc31);
  lua_pushnumber(L,1);
  lc_add(L,-2,-1);
  lua_remove(L,-2);
  lua_remove(L,-2);
  lua_insert(L,-2);
  lua_settable(L,-3);
  lua_pop(L,1);
  assert(lua_gettop(L) == 5);

  /* return dstream */
  lua_pushvalue(L,5);
  return 1;
  assert(lua_gettop(L) == 5);
}


/* name: StreamingContext:start
 * function() */
int stuart_streaming_StreamingContext_start (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* if self.state == 'stopped' then */
  enum { lc32 = 1 };
  lua_pushliteral(L,"state");
  lua_gettable(L,1);
  lua_pushliteral(L,"stopped");
  const int lc33 = lua_equal(L,-2,-1);
  lua_pop(L,2);
  lua_pushboolean(L,lc33);
  const int lc34 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc34) {

    /* error('StreamingContext has already been stopped') */
    lua_getfield(L,LUA_ENVIRONINDEX,"error");
    lua_pushliteral(L,"StreamingContext has already been stopped");
    lua_call(L,1,0);
    assert(lua_gettop(L) == 1);
  }
  lua_settop(L,lc32);
  assert(lua_gettop(L) == 1);

  /* for _, dstream in ipairs(self.dstreams) do
   * internal: local f, s, var = explist */
  enum { lc35 = 1 };
  lua_getfield(L,LUA_ENVIRONINDEX,"ipairs");
  lua_pushliteral(L,"dstreams");
  lua_gettable(L,1);
  lua_call(L,1,3);
  while (1) {

    /* internal: local var_1, ..., var_n = f(s, var)
     *           if var_1 == nil then break end
     *           var = var_1 */
    lua_pushvalue(L,-3);
    lua_pushvalue(L,-3);
    lua_pushvalue(L,-3);
    lua_call(L,2,2);
    if (lua_isnil(L,-2)) {
      break;
    }
    lua_pushvalue(L,-2);
    lua_replace(L,-4);

    /* internal: local _ with idx 5
     * internal: local dstream with idx 6 */


    /* dstream:start() */
    lua_pushvalue(L,6);
    lua_pushliteral(L,"start");
    lua_gettable(L,-2);
    lua_insert(L,-2);
    lua_call(L,1,0);
    assert(lua_gettop(L) == 6);

    /* internal: stack cleanup on scope exit */
    lua_pop(L,2);
  }
  lua_settop(L,lc35);
  assert(lua_gettop(L) == 1);

  /* self.state = 'active' */
  lua_pushliteral(L,"active");
  lua_pushliteral(L,"state");
  lua_insert(L,-2);
  lua_settable(L,1);
  assert(lua_gettop(L) == 1);
  return 0;
}


/* name: StreamingContext:stop
 * function(stopSparkContext) */
int stuart_streaming_StreamingContext_stop (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* if stopSparkContext == nil then */
  enum { lc36 = 2 };
  lua_pushnil(L);
  const int lc37 = lua_equal(L,2,-1);
  lua_pop(L,1);
  lua_pushboolean(L,lc37);
  const int lc38 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc38) {

    /* stopSparkContext = self.conf:getBoolean('spark.streaming.stopSparkContextByDefault', true) */
    lua_pushliteral(L,"conf");
    lua_gettable(L,1);
    lua_pushliteral(L,"getBoolean");
    lua_gettable(L,-2);
    lua_insert(L,-2);
    lua_pushliteral(L,"spark.streaming.stopSparkContextByDefault");
    lua_pushboolean(L,1);
    lua_call(L,3,1);
    lua_replace(L,2);
    assert(lua_gettop(L) == 2);
  }
  lua_settop(L,lc36);
  assert(lua_gettop(L) == 2);

  /* for _, dstream in ipairs(self.dstreams) do
   * internal: local f, s, var = explist */
  enum { lc39 = 2 };
  lua_getfield(L,LUA_ENVIRONINDEX,"ipairs");
  lua_pushliteral(L,"dstreams");
  lua_gettable(L,1);
  lua_call(L,1,3);
  while (1) {

    /* internal: local var_1, ..., var_n = f(s, var)
     *           if var_1 == nil then break end
     *           var = var_1 */
    lua_pushvalue(L,-3);
    lua_pushvalue(L,-3);
    lua_pushvalue(L,-3);
    lua_call(L,2,2);
    if (lua_isnil(L,-2)) {
      break;
    }
    lua_pushvalue(L,-2);
    lua_replace(L,-4);

    /* internal: local _ with idx 6
     * internal: local dstream with idx 7 */


    /* dstream:stop() */
    lua_pushvalue(L,7);
    lua_pushliteral(L,"stop");
    lua_gettable(L,-2);
    lua_insert(L,-2);
    lua_call(L,1,0);
    assert(lua_gettop(L) == 7);

    /* internal: stack cleanup on scope exit */
    lua_pop(L,2);
  }
  lua_settop(L,lc39);
  assert(lua_gettop(L) == 2);

  /* self.state = 'stopped' */
  lua_pushliteral(L,"stopped");
  lua_pushliteral(L,"state");
  lua_insert(L,-2);
  lua_settable(L,1);
  assert(lua_gettop(L) == 2);

  /* if stopSparkContext then */
  enum { lc40 = 2 };
  if (lua_toboolean(L,2)) {

    /* self.sc:stop() */
    lua_pushliteral(L,"sc");
    lua_gettable(L,1);
    lua_pushliteral(L,"stop");
    lua_gettable(L,-2);
    lua_insert(L,-2);
    lua_call(L,1,0);
    assert(lua_gettop(L) == 2);
  }
  lua_settop(L,lc40);
  assert(lua_gettop(L) == 2);
  return 0;
}


// ============================================================================
// Definition of the module Romtable.
// ============================================================================

const LUA_REG_TYPE stuart_streamingctx_map[] = {
  { LSTRKEY("_init")                    , LFUNCVAL (stuart_streaming_StreamingContext__init) },
  { LSTRKEY("awaitTermination")         , LFUNCVAL (stuart_streaming_StreamingContext_awaitTermination) },
  { LSTRKEY("awaitTerminationOrTimeout"), LFUNCVAL (stuart_streaming_StreamingContext_awaitTerminationOrTimeout) },
  { LSTRKEY("getState")                 , LFUNCVAL (stuart_streaming_StreamingContext_getState) },
  { LSTRKEY("queueStream")              , LFUNCVAL (stuart_streaming_StreamingContext_queueStream) },
  { LSTRKEY("receiverStream")           , LFUNCVAL (stuart_streaming_StreamingContext_receiverStream) },
  { LSTRKEY("socketTextStream")         , LFUNCVAL (stuart_streaming_StreamingContext_socketTextStream) },
  { LSTRKEY("start")                    , LFUNCVAL (stuart_streaming_StreamingContext_start) },
  { LSTRKEY("stop")                     , LFUNCVAL (stuart_streaming_StreamingContext_stop) },

  // class framework
  { LSTRKEY("__index")                  , LRO_ROVAL(stuart_streamingctx_map) },
  { LSTRKEY("_class")                   , LRO_ROVAL(stuart_streamingctx_map) },
  { LSTRKEY("classof")                  , LFUNCVAL (stuart_streaming_StreamingContext_classof) },
  { LSTRKEY("new")                      , LFUNCVAL (stuart_streaming_StreamingContext_new) },

  { LNILKEY, LNILVAL }
};


// ============================================================================
// Registration of the module.
// ============================================================================
LUALIB_API int luaopen_stuart_streamingctx( lua_State *L ){
#if LUA_OPTIMIZE_MEMORY > 0
  return 0;
#else
  LREGISTER( L, AUXLIB_STUART_STREAMINGCTX, stuart_streamingctx_map );
  return 1;
#endif
}
