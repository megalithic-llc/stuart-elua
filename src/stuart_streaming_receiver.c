#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
#include "common.h"
#include "auxmods.h"
#include "lrotable.h"
#include "platform_conf.h"
#define MIN_OPT_LEVEL 2
#include "lrodefs.h"
#include <assert.h>
#include "lua2c.h"
#include "stuart_class.h"


/* name: Receiver:_init
 * function(ssc) */
int stuart_streaming_Receiver__init (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* self.ssc = ssc */
  lua_pushvalue(L,2);
  lua_pushliteral(L,"ssc");
  lua_insert(L,-2);
  lua_settable(L,1);
  assert(lua_gettop(L) == 2);
  return 0;
}


/* name: M.classof
 * function(obj) */
int stuart_streaming_Receiver_classof (lua_State * L) {
  return stuart_class_shared_classof(L, "stuart.streaming.Receiver");
}


/* name: M.new
 * function(...) */
int stuart_streaming_Receiver_new (lua_State * L) {
  return stuart_class_shared_new(L, "stuart.streaming.Receiver");
}


/* name: Receiver:onStart
 * function() */
int stuart_streaming_Receiver_onStart (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);
  return 0;
}


/* name: Receiver:onStop
 * function() */
int stuart_streaming_Receiver_onStop (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);
  return 0;
}


/* name: Receiver:poll
 * function() */
int stuart_streaming_Receiver_poll (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);
  return 0;
}


// ============================================================================
// Definition of the module Romtable.
// ============================================================================

const LUA_REG_TYPE stuart_receiver_map[] = {
  { LSTRKEY("_init")           , LFUNCVAL (stuart_streaming_Receiver__init) },
  { LSTRKEY("onStart")         , LFUNCVAL (stuart_streaming_Receiver_onStart) },
  { LSTRKEY("onStop")          , LFUNCVAL (stuart_streaming_Receiver_onStop) },
  { LSTRKEY("poll")            , LFUNCVAL (stuart_streaming_Receiver_poll) },

  // class framework
  { LSTRKEY("__index")         , LRO_ROVAL(stuart_receiver_map) },
  { LSTRKEY("_class")          , LRO_ROVAL(stuart_receiver_map) },
  { LSTRKEY("classof")         , LFUNCVAL (stuart_streaming_Receiver_classof) },
  { LSTRKEY("new")             , LFUNCVAL (stuart_streaming_Receiver_new) },

  { LNILKEY, LNILVAL }
};


// ============================================================================
// Registration of the module.
// ============================================================================
LUALIB_API int luaopen_stuart_receiver( lua_State *L ){
#if LUA_OPTIMIZE_MEMORY > 0
  return 0;
#else
  LREGISTER( L, AUXLIB_STUART_RECEIVER, stuart_receiver_map );
  return 1;
#endif
}
