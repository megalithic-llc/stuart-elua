#ifndef __STUART_STREAMING_DSTREAM_H__
#define __STUART_STREAMING_DSTREAM_H__

#include "lua.h"

extern LUA_REG_TYPE stuart_dstream_map[];

int stuart_streaming_DStream__init (lua_State * L);
int stuart_streaming_DStream__notify (lua_State * L);
int stuart_streaming_DStream_count (lua_State * L);
int stuart_streaming_DStream_countByWindow (lua_State * L);
int stuart_streaming_DStream_foreachRDD (lua_State * L);
int stuart_streaming_DStream_groupByKey (lua_State * L);
int stuart_streaming_DStream_map (lua_State * L);
int stuart_streaming_DStream_mapValues (lua_State * L);
int stuart_streaming_DStream_poll (lua_State * L);
int stuart_streaming_DStream_reduce (lua_State * L);
int stuart_streaming_DStream_start (lua_State * L);
int stuart_streaming_DStream_stop (lua_State * L);
int stuart_streaming_DStream_transform (lua_State * L);
int stuart_streaming_DStream_window (lua_State * L);

#endif
