local M = {}

M.clockPrecision = function()
  -- eLua timers are always specified in microseconds
  -- http://www.eluaproject.net/doc/v0.8/en_arch_platform_timers.html
  return 6
end

M.now = function()
  return tmr.read(tmr.SYS_TIMER) / 1e6
end

M.sleep = function(duration)
  return 0; -- eLua is not capable
end

return M
