#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
#include "common.h"
#include "auxmods.h"
#include "lrotable.h"
#include "platform_conf.h"
#define MIN_OPT_LEVEL 2
#include "lrodefs.h"
#include <assert.h>
#include "lua2c.h"
#include "stuart_class.h"


/* name: Context:_init
 * function(arg1, arg2, arg3, arg4) */
int stuart_Context__init (lua_State * L) {
  enum { lc_nformalargs = 5 };
  lua_settop(L,5);

  /* local SparkConf = require 'stuart.SparkConf' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart.SparkConf");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 6);

  /* local class = require 'stuart.class' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart.class");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 7);

  /* if arg1 == nil and arg2 == nil then */
  enum { lc1 = 7 };
  lua_pushvalue(L,2);
  lua_pushnil(L);
  const int lc2 = lua_equal(L,-2,-1);
  lua_pop(L,2);
  lua_pushboolean(L,lc2);
  if (lua_toboolean(L,-1)) {
    lua_pop(L,1);
    lua_pushvalue(L,3);
    lua_pushnil(L);
    const int lc3 = lua_equal(L,-2,-1);
    lua_pop(L,2);
    lua_pushboolean(L,lc3);
  }
  const int lc4 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc4) {

    /* self.conf = SparkConf.new() */
    lua_pushliteral(L,"new");
    lua_gettable(L,6);
    lua_call(L,0,1);
    lua_pushliteral(L,"conf");
    lua_insert(L,-2);
    lua_settable(L,1);
    assert(lua_gettop(L) == 7);
  }
  else {

    /* elseif class.istype(arg1, SparkConf) then */
    enum { lc5 = 7 };
    lua_pushliteral(L,"istype");
    lua_gettable(L,7);
    lua_pushvalue(L,2);
    lua_pushvalue(L,6);
    lua_call(L,2,1);
    const int lc6 = lua_toboolean(L,-1);
    lua_pop(L,1);
    if (lc6) {

      /* self.conf = arg1 */
      lua_pushvalue(L,2);
      lua_pushliteral(L,"conf");
      lua_insert(L,-2);
      lua_settable(L,1);
      assert(lua_gettop(L) == 7);
    }
    else {

      /* else
       * local Context = require 'stuart.Context' */
      lua_getfield(L,LUA_ENVIRONINDEX,"require");
      lua_pushliteral(L,"stuart.Context");
      lua_call(L,1,1);
      assert(lua_gettop(L) == 8);

      /* self.conf = Context._updatedConf(SparkConf.new(), arg1, arg2, arg3, arg4) */
      lua_pushliteral(L,"_updatedConf");
      lua_gettable(L,8);
      lua_pushliteral(L,"new");
      lua_gettable(L,6);
      lua_call(L,0,1);
      lua_pushvalue(L,2);
      lua_pushvalue(L,3);
      lua_pushvalue(L,4);
      lua_pushvalue(L,5);
      lua_call(L,5,1);
      lua_pushliteral(L,"conf");
      lua_insert(L,-2);
      lua_settable(L,1);
      assert(lua_gettop(L) == 8);
    }
    lua_settop(L,lc5);
  }
  lua_settop(L,lc1);
  assert(lua_gettop(L) == 7);

  /* self.defaultParallelism = 1 */
  lua_pushnumber(L,1);
  lua_pushliteral(L,"defaultParallelism");
  lua_insert(L,-2);
  lua_settable(L,1);
  assert(lua_gettop(L) == 7);

  /* self.lastRddId = 0 */
  lua_pushnumber(L,0);
  lua_pushliteral(L,"lastRddId");
  lua_insert(L,-2);
  lua_settable(L,1);
  assert(lua_gettop(L) == 7);

  /* self.stopped = false */
  lua_pushboolean(L,0);
  lua_pushliteral(L,"stopped");
  lua_insert(L,-2);
  lua_settable(L,1);
  assert(lua_gettop(L) == 7);

  /* local logging = require 'stuart.internal.logging' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart.internal.logging");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 8);

  /* logging.logInfo('Running Stuart (Embedded Spark 2.2.0)') */
  lua_pushliteral(L,"logInfo");
  lua_gettable(L,8);
  lua_pushliteral(L,"Running Stuart (Embedded Spark 2.2.0)");
  lua_call(L,1,0);
  assert(lua_gettop(L) == 8);
  return 0;
}


/* name: Context._updatedConf
 * function(conf, master, appName, sparkHome) */
int stuart_Context__updatedConf (lua_State * L) {
  enum { lc_nformalargs = 4 };
  lua_settop(L,4);

  /* local res = conf:clone() */
  lua_pushvalue(L,1);
  lua_pushliteral(L,"clone");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  assert(lua_gettop(L) == 5);

  /* res:setMaster(master) */
  lua_pushvalue(L,5);
  lua_pushliteral(L,"setMaster");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_pushvalue(L,2);
  lua_call(L,2,0);
  assert(lua_gettop(L) == 5);

  /* res:setAppName(appName) */
  lua_pushvalue(L,5);
  lua_pushliteral(L,"setAppName");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_pushvalue(L,3);
  lua_call(L,2,0);
  assert(lua_gettop(L) == 5);

  /* if sparkHome ~= nil then */
  enum { lc53 = 5 };
  lua_pushnil(L);
  const int lc54 = lua_equal(L,4,-1);
  lua_pop(L,1);
  lua_pushboolean(L,lc54);
  lua_pushboolean(L,!(lua_toboolean(L,-1)));
  lua_remove(L,-2);
  const int lc55 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc55) {

    /* res:setSparkHome(sparkHome) */
    lua_pushvalue(L,5);
    lua_pushliteral(L,"setSparkHome");
    lua_gettable(L,-2);
    lua_insert(L,-2);
    lua_pushvalue(L,4);
    lua_call(L,2,0);
    assert(lua_gettop(L) == 5);
  }
  lua_settop(L,lc53);
  assert(lua_gettop(L) == 5);

  /* return res */
  lua_pushvalue(L,5);
  return 1;
  assert(lua_gettop(L) == 5);
}


/* name: Context:appName
 * function() */
int stuart_Context_appName (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* return self.conf:get('spark.app.name') */
  const int lc7 = lua_gettop(L);
  lua_pushliteral(L,"conf");
  lua_gettable(L,1);
  lua_pushliteral(L,"get");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_pushliteral(L,"spark.app.name");
  lua_call(L,2,LUA_MULTRET);
  return (lua_gettop(L) - lc7);
  assert(lua_gettop(L) == 1);
}


/* name: M.classof
 * function(obj) */
int stuart_Context_classof (lua_State * L) {
  return stuart_class_shared_classof(L, "stuart.Context");
}


/* name: Context:emptyRDD
 * function() */
int stuart_Context_emptyRDD (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* local rdd = self:parallelize({}, 0) */
  lua_pushvalue(L,1);
  lua_pushliteral(L,"parallelize");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_newtable(L);
  lua_pushnumber(L,0);
  lua_call(L,3,1);
  assert(lua_gettop(L) == 2);

  /* return rdd */
  lua_pushvalue(L,2);
  return 1;
  assert(lua_gettop(L) == 2);
}


/* name: Context:getConf
 * function() */
int stuart_Context_getConf (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* return self.conf:clone() */
  const int lc8 = lua_gettop(L);
  lua_pushliteral(L,"conf");
  lua_gettable(L,1);
  lua_pushliteral(L,"clone");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,LUA_MULTRET);
  return (lua_gettop(L) - lc8);
  assert(lua_gettop(L) == 1);
}


/* name: Context:getNextId
 * function() */
int stuart_Context_getNextId (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* self.lastRddId = self.lastRddId + 1 */
  lua_pushliteral(L,"lastRddId");
  lua_gettable(L,1);
  lua_pushnumber(L,1);
  lc_add(L,-2,-1);
  lua_remove(L,-2);
  lua_remove(L,-2);
  lua_pushliteral(L,"lastRddId");
  lua_insert(L,-2);
  lua_settable(L,1);
  assert(lua_gettop(L) == 1);

  /* return self.lastRddId */
  lua_pushliteral(L,"lastRddId");
  lua_gettable(L,1);
  return 1;
  assert(lua_gettop(L) == 1);
}


/* name: Context:hadoopFile
 * function(path, minPartitions) */
int stuart_Context_hadoopFile (lua_State * L) {
  lua_checkstack(L,30);
  enum { lc_nformalargs = 3 };
  lua_settop(L,3);

  /* local fileSystemFactory = require 'stuart.fileSystemFactory' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart.fileSystemFactory");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 4);

  /* local fs, openPath = fileSystemFactory.createForOpenPath(path) */
  lua_pushliteral(L,"createForOpenPath");
  lua_gettable(L,4);
  lua_pushvalue(L,2);
  lua_call(L,1,2);
  assert(lua_gettop(L) == 6);

  /* if fs:isDirectory(openPath) then */
  enum { lc11 = 6 };
  lua_pushvalue(L,5);
  lua_pushliteral(L,"isDirectory");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_pushvalue(L,6);
  lua_call(L,2,1);
  const int lc12 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc12) {

    /* local fileStatuses = fs:listStatus(openPath) */
    lua_pushvalue(L,5);
    lua_pushliteral(L,"listStatus");
    lua_gettable(L,-2);
    lua_insert(L,-2);
    lua_pushvalue(L,6);
    lua_call(L,2,1);
    assert(lua_gettop(L) == 7);

    /* local lines = {} */
    lua_newtable(L);
    assert(lua_gettop(L) == 8);

    /* for _,fileStatus in ipairs(fileStatuses) do
     * internal: local f, s, var = explist */
    enum { lc13 = 8 };
    lua_getfield(L,LUA_ENVIRONINDEX,"ipairs");
    lua_pushvalue(L,7);
    lua_call(L,1,3);
    while (1) {

      /* internal: local var_1, ..., var_n = f(s, var)
       *           if var_1 == nil then break end
       *           var = var_1 */
      lua_pushvalue(L,-3);
      lua_pushvalue(L,-3);
      lua_pushvalue(L,-3);
      lua_call(L,2,2);
      if (lua_isnil(L,-2)) {
        break;
      }
      lua_pushvalue(L,-2);
      lua_replace(L,-4);

      /* internal: local _ with idx 12
       * internal: local fileStatus with idx 13 */


      /* if fileStatus.type == 'FILE' and fileStatus.pathSuffix:sub(1,1) ~= '.' and fileStatus.pathSuffix:sub(1,1) ~= '_' then */
      enum { lc14 = 13 };
      lua_pushliteral(L,"type");
      lua_gettable(L,13);
      lua_pushliteral(L,"FILE");
      const int lc15 = lua_equal(L,-2,-1);
      lua_pop(L,2);
      lua_pushboolean(L,lc15);
      if (lua_toboolean(L,-1)) {
        lua_pop(L,1);
        lua_pushliteral(L,"pathSuffix");
        lua_gettable(L,13);
        lua_pushliteral(L,"sub");
        lua_gettable(L,-2);
        lua_insert(L,-2);
        lua_pushnumber(L,1);
        lua_pushnumber(L,1);
        lua_call(L,3,1);
        lua_pushliteral(L,".");
        const int lc16 = lua_equal(L,-2,-1);
        lua_pop(L,2);
        lua_pushboolean(L,lc16);
        lua_pushboolean(L,!(lua_toboolean(L,-1)));
        lua_remove(L,-2);
      }
      if (lua_toboolean(L,-1)) {
        lua_pop(L,1);
        lua_pushliteral(L,"pathSuffix");
        lua_gettable(L,13);
        lua_pushliteral(L,"sub");
        lua_gettable(L,-2);
        lua_insert(L,-2);
        lua_pushnumber(L,1);
        lua_pushnumber(L,1);
        lua_call(L,3,1);
        lua_pushliteral(L,"_");
        const int lc17 = lua_equal(L,-2,-1);
        lua_pop(L,2);
        lua_pushboolean(L,lc17);
        lua_pushboolean(L,!(lua_toboolean(L,-1)));
        lua_remove(L,-2);
      }
      const int lc18 = lua_toboolean(L,-1);
      lua_pop(L,1);
      if (lc18) {

        /* local uri = openPath .. '/' .. fileStatus.pathSuffix */
        lua_pushvalue(L,6);
        lua_pushliteral(L,"/");
        lua_pushliteral(L,"pathSuffix");
        lua_gettable(L,13);
        lua_concat(L,2);
        lua_concat(L,2);
        assert(lua_gettop(L) == 14);

        /* local content, status = fs:open(uri) */
        lua_pushvalue(L,5);
        lua_pushliteral(L,"open");
        lua_gettable(L,-2);
        lua_insert(L,-2);
        lua_pushvalue(L,14);
        lua_call(L,2,2);
        assert(lua_gettop(L) == 16);

        /* if status and status >= 400 then */
        enum { lc19 = 16 };
        lua_pushvalue(L,16);
        if (lua_toboolean(L,-1)) {
          lua_pop(L,1);
          lua_pushnumber(L,400);
          const int lc20 = lc_le(L,-1,16);
          lua_pop(L,1);
          lua_pushboolean(L,lc20);
        }
        const int lc21 = lua_toboolean(L,-1);
        lua_pop(L,1);
        if (lc21) {

          /* error(content) */
          lua_getfield(L,LUA_ENVIRONINDEX,"error");
          lua_pushvalue(L,15);
          lua_call(L,1,0);
          assert(lua_gettop(L) == 16);
        }
        lua_settop(L,lc19);
        assert(lua_gettop(L) == 16);

        /* for line in content:gmatch('[^\r\n]+') do
         * internal: local f, s, var = explist */
        enum { lc22 = 16 };
        lua_pushvalue(L,15);
        lua_pushliteral(L,"gmatch");
        lua_gettable(L,-2);
        lua_insert(L,-2);
        lua_pushliteral(L,"[^\13\n]+");
        lua_call(L,2,3);
        while (1) {

          /* internal: local var_1, ..., var_n = f(s, var)
           *           if var_1 == nil then break end
           *           var = var_1 */
          lua_pushvalue(L,-3);
          lua_pushvalue(L,-3);
          lua_pushvalue(L,-3);
          lua_call(L,2,1);
          if (lua_isnil(L,-1)) {
            break;
          }
          lua_pushvalue(L,-1);
          lua_replace(L,-3);

          /* internal: local line with idx 20 */


          /* lines[#lines+1] = line */
          lua_pushvalue(L,20);
          const double lc23 = lua_objlen(L,8);
          lua_pushnumber(L,lc23);
          lua_pushnumber(L,1);
          lc_add(L,-2,-1);
          lua_remove(L,-2);
          lua_remove(L,-2);
          lua_insert(L,-2);
          lua_settable(L,8);
          assert(lua_gettop(L) == 20);

          /* internal: stack cleanup on scope exit */
          lua_pop(L,1);
        }
        lua_settop(L,lc22);
        assert(lua_gettop(L) == 16);
      }
      lua_settop(L,lc14);
      assert(lua_gettop(L) == 13);

      /* internal: stack cleanup on scope exit */
      lua_pop(L,2);
    }
    lua_settop(L,lc13);
    assert(lua_gettop(L) == 8);

    /* return self:parallelize(lines, minPartitions) */
    const int lc24 = lua_gettop(L);
    lua_pushvalue(L,1);
    lua_pushliteral(L,"parallelize");
    lua_gettable(L,-2);
    lua_insert(L,-2);
    lua_pushvalue(L,8);
    lua_pushvalue(L,3);
    lua_call(L,3,LUA_MULTRET);
    return (lua_gettop(L) - lc24);
    assert(lua_gettop(L) == 8);
  }
  else {

    /* else
     * local content = fs:open(openPath) */
    lua_pushvalue(L,5);
    lua_pushliteral(L,"open");
    lua_gettable(L,-2);
    lua_insert(L,-2);
    lua_pushvalue(L,6);
    lua_call(L,2,1);
    assert(lua_gettop(L) == 7);

    /* local lines = {} */
    lua_newtable(L);
    assert(lua_gettop(L) == 8);

    /* for line in content:gmatch('[^\r\n]+') do
     * internal: local f, s, var = explist */
    enum { lc25 = 8 };
    lua_pushvalue(L,7);
    lua_pushliteral(L,"gmatch");
    lua_gettable(L,-2);
    lua_insert(L,-2);
    lua_pushliteral(L,"[^\13\n]+");
    lua_call(L,2,3);
    while (1) {

      /* internal: local var_1, ..., var_n = f(s, var)
       *           if var_1 == nil then break end
       *           var = var_1 */
      lua_pushvalue(L,-3);
      lua_pushvalue(L,-3);
      lua_pushvalue(L,-3);
      lua_call(L,2,1);
      if (lua_isnil(L,-1)) {
        break;
      }
      lua_pushvalue(L,-1);
      lua_replace(L,-3);

      /* internal: local line with idx 12 */


      /* lines[#lines+1] = line */
      lua_pushvalue(L,12);
      const double lc26 = lua_objlen(L,8);
      lua_pushnumber(L,lc26);
      lua_pushnumber(L,1);
      lc_add(L,-2,-1);
      lua_remove(L,-2);
      lua_remove(L,-2);
      lua_insert(L,-2);
      lua_settable(L,8);
      assert(lua_gettop(L) == 12);

      /* internal: stack cleanup on scope exit */
      lua_pop(L,1);
    }
    lua_settop(L,lc25);
    assert(lua_gettop(L) == 8);

    /* return self:parallelize(lines, minPartitions) */
    const int lc27 = lua_gettop(L);
    lua_pushvalue(L,1);
    lua_pushliteral(L,"parallelize");
    lua_gettable(L,-2);
    lua_insert(L,-2);
    lua_pushvalue(L,8);
    lua_pushvalue(L,3);
    lua_call(L,3,LUA_MULTRET);
    return (lua_gettop(L) - lc27);
    assert(lua_gettop(L) == 8);
  }
  lua_settop(L,lc11);
  assert(lua_gettop(L) == 6);
  return 0;
}


/* name: Context:isStopped
 * function() */
int stuart_Context_isStopped (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* return self.stopped */
  lua_pushliteral(L,"stopped");
  lua_gettable(L,1);
  return 1;
  assert(lua_gettop(L) == 1);
}


/* name: Context:master
 * function() */
int stuart_Context_master (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* return self.conf:get('spark.master') */
  const int lc27 = lua_gettop(L);
  lua_pushliteral(L,"conf");
  lua_gettable(L,1);
  lua_pushliteral(L,"get");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_pushliteral(L,"spark.master");
  lua_call(L,2,LUA_MULTRET);
  return (lua_gettop(L) - lc27);
  assert(lua_gettop(L) == 1);
}


/* name: M.new
 * function(...) */
int stuart_Context_new (lua_State * L) {
  return stuart_class_shared_new(L, "stuart.Context");
}


/* function(chunk, i) */
int stuart_Context_parallelize_1 (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* return Partition.new(chunk, i) */
  const int lc44 = lua_gettop(L);
  lc_getupvalue(L,lua_upvalueindex(1),0,1);
  lua_pushliteral(L,"new");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_pushvalue(L,1);
  lua_pushvalue(L,2);
  lua_call(L,2,LUA_MULTRET);
  return (lua_gettop(L) - lc44);
  assert(lua_gettop(L) == 2);
}


/* name: Context:parallelize
 * function(x, numPartitions) */
int stuart_Context_parallelize (lua_State * L) {
  enum { lc_nformalargs = 3 };
  lua_settop(L,3);

  /* assert(not self.stopped) */
  lua_getfield(L,LUA_ENVIRONINDEX,"assert");
  lua_pushliteral(L,"stopped");
  lua_gettable(L,1);
  lua_pushboolean(L,!(lua_toboolean(L,-1)));
  lua_remove(L,-2);
  lua_call(L,1,0);
  assert(lua_gettop(L) == 3);

  /* local moses = require 'moses' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 4);

  /* if not moses.isNumber(numPartitions) then */
  enum { lc28 = 4 };
  lua_pushliteral(L,"isNumber");
  lua_gettable(L,4);
  lua_pushvalue(L,3);
  lua_call(L,1,1);
  lua_pushboolean(L,!(lua_toboolean(L,-1)));
  lua_remove(L,-2);
  const int lc29 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc29) {

    /* numPartitions = self.defaultParallelism */
    lua_pushliteral(L,"defaultParallelism");
    lua_gettable(L,1);
    lua_replace(L,3);
    assert(lua_gettop(L) == 4);
  }
  lua_settop(L,lc28);
  assert(lua_gettop(L) == 4);

  /* local Partition = require 'stuart.Partition' */
  lc_newclosuretable(L,lua_upvalueindex(1));
  enum { lc30 = 5 };
  assert((lua_gettop(L) == lc30));
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart.Partition");
  lua_call(L,1,1);
  lua_rawseti(L,lc30,1);
  assert(lua_gettop(L) == 5);

  /* local RDD = require 'stuart.RDD' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart.RDD");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 6);

  /* if numPartitions == 1 then */
  enum { lc31 = 6 };
  lua_pushnumber(L,1);
  const int lc32 = lua_equal(L,3,-1);
  lua_pop(L,1);
  lua_pushboolean(L,lc32);
  const int lc33 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc33) {

    /* local p = Partition.new(x, 0) */
    lc_getupvalue(L,lc30,0,1);
    lua_pushliteral(L,"new");
    lua_gettable(L,-2);
    lua_remove(L,-2);
    lua_pushvalue(L,2);
    lua_pushnumber(L,0);
    lua_call(L,2,1);
    assert(lua_gettop(L) == 7);

    /* return RDD.new(self, {p}) */
    const int lc34 = lua_gettop(L);
    lua_pushliteral(L,"new");
    lua_gettable(L,6);
    lua_pushvalue(L,1);
    lua_createtable(L,1,0);
    lua_pushvalue(L,7);
    lua_rawseti(L,-2,1);
    lua_call(L,2,LUA_MULTRET);
    return (lua_gettop(L) - lc34);
    assert(lua_gettop(L) == 7);
  }
  lua_settop(L,lc31);
  assert(lua_gettop(L) == 6);

  /* local chunks = {} */
  lua_newtable(L);
  assert(lua_gettop(L) == 7);

  /* local chunkSize = math.ceil(#x / numPartitions) */
  lua_getfield(L,LUA_ENVIRONINDEX,"math");
  lua_pushliteral(L,"ceil");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  const double lc35 = lua_objlen(L,2);
  lua_pushnumber(L,lc35);
  lc_div(L,-1,3);
  lua_remove(L,-2);
  lua_call(L,1,1);
  assert(lua_gettop(L) == 8);

  /* if chunkSize > 0 then */
  enum { lc36 = 8 };
  lua_pushnumber(L,0);
  const int lc37 = lua_lessthan(L,-1,8);
  lua_pop(L,1);
  lua_pushboolean(L,lc37);
  const int lc38 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc38) {

    /* chunks = moses.tabulate(moses.partition(x, chunkSize)) */
    lua_pushliteral(L,"tabulate");
    lua_gettable(L,4);
    const int lc39 = lua_gettop(L);
    lua_pushliteral(L,"partition");
    lua_gettable(L,4);
    lua_pushvalue(L,2);
    lua_pushvalue(L,8);
    lua_call(L,2,LUA_MULTRET);
    lua_call(L,(lua_gettop(L) - lc39),1);
    lua_replace(L,7);
    assert(lua_gettop(L) == 8);
  }
  lua_settop(L,lc36);
  assert(lua_gettop(L) == 8);

  /* while #chunks < numPartitions do */
  enum { lc40 = 8 };
  while (1) {
    const double lc41 = lua_objlen(L,7);
    lua_pushnumber(L,lc41);
    const int lc42 = lua_lessthan(L,-1,3);
    lua_pop(L,1);
    lua_pushboolean(L,lc42);
    if (!(lua_toboolean(L,-1))) {
      break;
    }
    lua_pop(L,1);

    /* chunks[#chunks+1] = {} */
    lua_newtable(L);
    const double lc43 = lua_objlen(L,7);
    lua_pushnumber(L,lc43);
    lua_pushnumber(L,1);
    lc_add(L,-2,-1);
    lua_remove(L,-2);
    lua_remove(L,-2);
    lua_insert(L,-2);
    lua_settable(L,7);
    assert(lua_gettop(L) == 8);
  }
  lua_settop(L,lc40);
  assert(lua_gettop(L) == 8);

  /* -- pad-right empty partitions
   * local partitions = moses.map(chunks, function(chunk, i)
   *     return Partition.new(chunk, i)
   *   end) */
  lua_pushliteral(L,"map");
  lua_gettable(L,4);
  lua_pushvalue(L,7);
  lua_pushvalue(L,lc30);
  lua_pushcclosure(L,stuart_Context_parallelize_1,1);
  lua_call(L,2,1);
  assert(lua_gettop(L) == 9);

  /* return RDD.new(self, partitions) */
  const int lc46 = lua_gettop(L);
  lua_pushliteral(L,"new");
  lua_gettable(L,6);
  lua_pushvalue(L,1);
  lua_pushvalue(L,9);
  lua_call(L,2,LUA_MULTRET);
  return (lua_gettop(L) - lc46);
  assert(lua_gettop(L) == 9);
}


/* name: Context:setLogLevel
 * function(level) */
int stuart_Context_setLogLevel (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* local logging = require 'stuart.internal.logging' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart.internal.logging");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 3);

  /* logging.log:setLevel(level) */
  lua_pushliteral(L,"log");
  lua_gettable(L,3);
  lua_pushliteral(L,"setLevel");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_pushvalue(L,2);
  lua_call(L,2,0);
  assert(lua_gettop(L) == 3);
  return 0;
}


/* name: Context:stop
 * function() */
int stuart_Context_stop (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* self.stopped = true */
  lua_pushboolean(L,1);
  lua_pushliteral(L,"stopped");
  lua_insert(L,-2);
  lua_settable(L,1);
  assert(lua_gettop(L) == 1);
  return 0;
}


/* name: Context:textFile
 * function(path, minPartitions) */
int stuart_Context_textFile (lua_State * L) {
  enum { lc_nformalargs = 3 };
  lua_settop(L,3);

  /* assert(not self.stopped) */
  lua_getfield(L,LUA_ENVIRONINDEX,"assert");
  lua_pushliteral(L,"stopped");
  lua_gettable(L,1);
  lua_pushboolean(L,!(lua_toboolean(L,-1)));
  lua_remove(L,-2);
  lua_call(L,1,0);
  assert(lua_gettop(L) == 3);

  /* return self:hadoopFile(path, minPartitions) */
  const int lc47 = lua_gettop(L);
  lua_pushvalue(L,1);
  lua_pushliteral(L,"hadoopFile");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_pushvalue(L,2);
  lua_pushvalue(L,3);
  lua_call(L,3,LUA_MULTRET);
  return (lua_gettop(L) - lc47);
  assert(lua_gettop(L) == 3);
}


/* name: Context:union
 * function(rdds) */
int stuart_Context_union (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* local t = rdds[1] */
  lua_pushnumber(L,1);
  lua_gettable(L,2);
  assert(lua_gettop(L) == 3);

  /* for i = 2, #rdds do */
  lua_pushnumber(L,2);
  const double lc51 = lua_objlen(L,2);
  lua_pushnumber(L,lc51);
  if (!((lua_isnumber(L,-2) && lua_isnumber(L,-1)))) {
    luaL_error(L,"'for' limit must be a number");
  }
  double lc48_var = lua_tonumber(L,-2);
  const double lc49_limit = lua_tonumber(L,-1);
  const double lc50_step = 1;
  lua_pop(L,2);
  enum { lc52 = 3 };
  while ((((lc50_step > 0) && (lc48_var <= lc49_limit)) || ((lc50_step <= 0) && (lc48_var >= lc49_limit)))) {

    /* internal: local i at index 4 */
    lua_pushnumber(L,lc48_var);

    /* t = t:union(rdds[i]) */
    lua_pushvalue(L,3);
    lua_pushliteral(L,"union");
    lua_gettable(L,-2);
    lua_insert(L,-2);
    lua_pushvalue(L,4);
    lua_gettable(L,2);
    lua_call(L,2,1);
    lua_replace(L,3);
    assert(lua_gettop(L) == 4);

    /* internal: stack cleanup on scope exit */
    lua_pop(L,1);
    lc48_var += lc50_step;
  }
  lua_settop(L,lc52);
  assert(lua_gettop(L) == 3);

  /* return t */
  lua_pushvalue(L,3);
  return 1;
  assert(lua_gettop(L) == 3);
}


// ============================================================================
// Definition of the module Romtable.
// ============================================================================

const LUA_REG_TYPE stuart_context_map[] = {
  { LSTRKEY("_init")       , LFUNCVAL (stuart_Context__init) },
  { LSTRKEY("_updatedConf"), LFUNCVAL (stuart_Context__updatedConf) },
  { LSTRKEY("appName")     , LFUNCVAL (stuart_Context_appName) },
  { LSTRKEY("emptyRDD")    , LFUNCVAL (stuart_Context_emptyRDD) },
  { LSTRKEY("getConf")     , LFUNCVAL (stuart_Context_getConf) },
  { LSTRKEY("getNextId")   , LFUNCVAL (stuart_Context_getNextId) },
  { LSTRKEY("hadoopFile")  , LFUNCVAL (stuart_Context_hadoopFile) },
  { LSTRKEY("isStopped")   , LFUNCVAL (stuart_Context_isStopped) },
  { LSTRKEY("makeRDD")     , LFUNCVAL (stuart_Context_parallelize) },
  { LSTRKEY("master")      , LFUNCVAL (stuart_Context_master) },
  { LSTRKEY("parallelize") , LFUNCVAL (stuart_Context_parallelize) },
  { LSTRKEY("setLogLevel") , LFUNCVAL (stuart_Context_setLogLevel) },
  { LSTRKEY("stop")        , LFUNCVAL (stuart_Context_stop) },
  { LSTRKEY("textFile")    , LFUNCVAL (stuart_Context_textFile) },
  { LSTRKEY("union")       , LFUNCVAL (stuart_Context_union) },

  // class framework
  { LSTRKEY("__index")     , LRO_ROVAL(stuart_context_map) },
  { LSTRKEY("_class")      , LRO_ROVAL(stuart_context_map) },
  { LSTRKEY("classof")     , LFUNCVAL (stuart_Context_classof) },
  { LSTRKEY("new")         , LFUNCVAL (stuart_Context_new) },

  { LNILKEY, LNILVAL }
};


// ============================================================================
// Registration of the module.
// ============================================================================
LUALIB_API int luaopen_stuart_context( lua_State *L ){
#if LUA_OPTIMIZE_MEMORY > 0
  return 0;
#else
  LREGISTER( L, AUXLIB_STUART_CONTEXT, stuart_context_map );
  return 1;
#endif
}
