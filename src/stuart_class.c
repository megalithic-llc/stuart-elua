#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
#include "common.h"
#include "auxmods.h"
#include "lrotable.h"
#include "platform_conf.h"
#define MIN_OPT_LEVEL 2
#include "lrodefs.h"
#include <assert.h>
#include "lua2c.h"


/* name: M.istype
 * function(obj, super) */
int stuart_class_istype (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* return super.classof(obj) */
  const int lc1 = lua_gettop(L);
  lua_pushliteral(L,"classof");
  lua_gettable(L,2);
  lua_pushvalue(L,1);
  lua_call(L,1,LUA_MULTRET);
  return (lua_gettop(L) - lc1);
  assert(lua_gettop(L) == 2);
}


/* name: klass.classof
 * function(obj) */
int stuart_class_new_1 (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* local m = getmetatable(obj) */
  lua_getfield(L,LUA_ENVIRONINDEX,"getmetatable");
  lua_pushvalue(L,1);
  lua_call(L,1,1);
  assert(lua_gettop(L) == 2);

  /* -- an object created by class() ?
   * if not m or not m._class then */
  enum { lc5 = 2 };
  lua_pushboolean(L,!(lua_toboolean(L,2)));
  if (!(lua_toboolean(L,-1))) {
    lua_pop(L,1);
    lua_pushliteral(L,"_class");
    lua_gettable(L,2);
    lua_pushboolean(L,!(lua_toboolean(L,-1)));
    lua_remove(L,-2);
  }
  const int lc6 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc6) {

    /* return false */
    lua_pushboolean(L,0);
    return 1;
    assert(lua_gettop(L) == 2);
  }
  lua_settop(L,lc5);
  assert(lua_gettop(L) == 2);

  /* while m do -- follow the inheritance chain -- */
  enum { lc7 = 2 };
  while (1) {
    if (!(lua_toboolean(L,2))) {
      break;
    }

    /* -- follow the inheritance chain --
     * if m == klass then */
    enum { lc8 = 2 };
    lc_getupvalue(L,lua_upvalueindex(1),0,2);
    const int lc9 = lua_equal(L,2,-1);
    lua_pop(L,1);
    lua_pushboolean(L,lc9);
    const int lc10 = lua_toboolean(L,-1);
    lua_pop(L,1);
    if (lc10) {

      /* return true */
      lua_pushboolean(L,1);
      return 1;
      assert(lua_gettop(L) == 2);
    }
    lua_settop(L,lc8);
    assert(lua_gettop(L) == 2);

    /* m = rawget(m,'_base') */
    lua_getfield(L,LUA_ENVIRONINDEX,"rawget");
    lua_pushvalue(L,2);
    lua_pushliteral(L,"_base");
    lua_call(L,2,1);
    lua_replace(L,2);
    assert(lua_gettop(L) == 2);
  }
  lua_settop(L,lc7);
  assert(lua_gettop(L) == 2);

  /* return false */
  lua_pushboolean(L,0);
  return 1;
  assert(lua_gettop(L) == 2);
}


/* name: klass.new
 * function(...) */
int stuart_class_new_2 (lua_State * L) {
  enum { lc_nformalargs = 0 };
  const int lc_nactualargs = lua_gettop(L);
  const int lc_nextra = (lc_nactualargs - lc_nformalargs);

  /* local obj = setmetatable({},klass) */
  lua_getfield(L,LUA_ENVIRONINDEX,"setmetatable");
  lua_newtable(L);
  lc_getupvalue(L,lua_upvalueindex(1),0,2);
  lua_call(L,2,1);
  assert(lua_gettop(L) - lc_nextra == 1);

  /* if rawget(klass,'_init') then */
  enum { lc11 = 1 };
  lua_getfield(L,LUA_ENVIRONINDEX,"rawget");
  lc_getupvalue(L,lua_upvalueindex(1),0,2);
  lua_pushliteral(L,"_init");
  lua_call(L,2,1);
  const int lc12 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc12) {

    /* local res = klass._init(obj,...) */
    lc_getupvalue(L,lua_upvalueindex(1),0,2);
    lua_pushliteral(L,"_init");
    lua_gettable(L,-2);
    lua_remove(L,-2);
    const int lc13 = lua_gettop(L);
    lua_pushvalue(L,(1 + lc_nextra));
    {int i; for (i=lc_nformalargs+1; i<=lc_nactualargs; i++) { lua_pushvalue(L, i); }}
    lua_call(L,(lua_gettop(L) - lc13),1);
    assert(lua_gettop(L) - lc_nextra == 2);

    /* -- call our constructor
     * if res then */
    enum { lc14 = 2 };
    if (lua_toboolean(L,(2 + lc_nextra))) {

      /* -- which can return a new self
       * obj = setmetatable(res,klass) */
      lua_getfield(L,LUA_ENVIRONINDEX,"setmetatable");
      lua_pushvalue(L,(2 + lc_nextra));
      lc_getupvalue(L,lua_upvalueindex(1),0,2);
      lua_call(L,2,1);
      lua_replace(L,(1 + lc_nextra));
      assert(lua_gettop(L) - lc_nextra == 2);
    }
    lua_settop(L,(lc14 + lc_nextra));
    assert(lua_gettop(L) - lc_nextra == 2);
  }
  else {

    /* elseif base_ctor then */
    enum { lc15 = 1 };
    lc_getupvalue(L,lua_upvalueindex(1),0,1);
    const int lc16 = lua_toboolean(L,-1);
    lua_pop(L,1);
    if (lc16) {

      /* -- call base ctor automatically
       * base_ctor(obj,...) */
      lc_getupvalue(L,lua_upvalueindex(1),0,1);
      const int lc17 = lua_gettop(L);
      lua_pushvalue(L,(1 + lc_nextra));
      {int i; for (i=lc_nformalargs+1; i<=lc_nactualargs; i++) { lua_pushvalue(L, i); }}
      lua_call(L,(lua_gettop(L) - lc17),0);
      assert(lua_gettop(L) - lc_nextra == 1);
    }
    lua_settop(L,(lc15 + lc_nextra));
  }
  lua_settop(L,(lc11 + lc_nextra));
  assert(lua_gettop(L) - lc_nextra == 1);

  /* return obj */
  lua_pushvalue(L,(1 + lc_nextra));
  return 1;
  assert(lua_gettop(L) - lc_nextra == 1);
}


/* function() */
int stuart_class_new_noop (lua_State * L) {
  enum { lc_nformalargs = 0 };
  lua_settop(L,0);
  return 0;
}


/* name: M.new
 * function(base) */
int stuart_class_new (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* local klass, base_ctor = {} */
  lc_newclosuretable(L,lua_upvalueindex(1));
  enum { lc2 = 2 };
  assert((lua_gettop(L) == lc2));
  lua_newtable(L);
  lua_settop(L,(lua_gettop(L) + 1));
  lua_rawseti(L,lc2,1);
  lua_rawseti(L,lc2,2);
  assert(lua_gettop(L) == 2);

  /* if base then */
  enum { lc3 = 2 };
  if (lua_toboolean(L,1)) {

    /* for k,v in pairs(base) do
     * internal: local f, s, var = explist */
    enum { lc4 = 2 };
    lua_getfield(L,LUA_ENVIRONINDEX,"pairs");
    lua_pushvalue(L,1);
    lua_call(L,1,3);
    while (1) {

      /* internal: local var_1, ..., var_n = f(s, var)
       *           if var_1 == nil then break end
       *           var = var_1 */
      lua_pushvalue(L,-3);
      lua_pushvalue(L,-3);
      lua_pushvalue(L,-3);
      lua_call(L,2,2);
      if (lua_isnil(L,-2)) {
        break;
      }
      lua_pushvalue(L,-2);
      lua_replace(L,-4);

      /* internal: local k with idx 6
       * internal: local v with idx 7 */


      /* klass[k]=v */
      lua_pushvalue(L,7);
      lc_getupvalue(L,lc2,0,2);
      lua_insert(L,-2);
      lua_pushvalue(L,6);
      lua_insert(L,-2);
      lua_settable(L,-3);
      lua_pop(L,1);
      assert(lua_gettop(L) == 7);

      /* internal: stack cleanup on scope exit */
      lua_pop(L,2);
    }
    lua_settop(L,lc4);
    assert(lua_gettop(L) == 2);

    /* klass._base = base */
    lua_pushvalue(L,1);
    lc_getupvalue(L,lc2,0,2);
    lua_insert(L,-2);
    lua_pushliteral(L,"_base");
    lua_insert(L,-2);
    lua_settable(L,-3);
    lua_pop(L,1);
    assert(lua_gettop(L) == 2);

    /* base_ctor = rawget(base,'_init') */
    lua_getfield(L,LUA_ENVIRONINDEX,"rawget");
    lua_pushvalue(L,1);
    lua_pushliteral(L,"_init");
    lua_call(L,2,1);
    if (!(lua_toboolean(L,-1))) {
      lua_pop(L,1);
      lua_pushcfunction(L,stuart_class_new_noop);
    }
    lc_setupvalue(L,lc2,0,1);
    assert(lua_gettop(L) == 2);
  }
  lua_settop(L,lc3);
  assert(lua_gettop(L) == 2);

  /* klass.__index = klass */
  lc_getupvalue(L,lc2,0,2);
  lc_getupvalue(L,lc2,0,2);
  lua_insert(L,-2);
  lua_pushliteral(L,"__index");
  lua_insert(L,-2);
  lua_settable(L,-3);
  lua_pop(L,1);
  assert(lua_gettop(L) == 2);

  /* klass._class = klass */
  lc_getupvalue(L,lc2,0,2);
  lc_getupvalue(L,lc2,0,2);
  lua_insert(L,-2);
  lua_pushliteral(L,"_class");
  lua_insert(L,-2);
  lua_settable(L,-3);
  lua_pop(L,1);
  assert(lua_gettop(L) == 2);

  /* klass.classof = function(obj)
   *     local m = getmetatable(obj) -- an object created by class() ?
   *     if not m or not m._class then return false end
   *     while m do -- follow the inheritance chain --
   *       if m == klass then return true end
   *       m = rawget(m,'_base')
   *     end
   *     return false
   *   end */
  lua_pushvalue(L,lc2);
  lua_pushcclosure(L,stuart_class_new_1,1);
  lc_getupvalue(L,lc2,0,2);
  lua_insert(L,-2);
  lua_pushliteral(L,"classof");
  lua_insert(L,-2);
  lua_settable(L,-3);
  lua_pop(L,1);
  assert(lua_gettop(L) == 2);

  /* klass.new = function(...)
   *     local obj = setmetatable({},klass)
   *     if rawget(klass,'_init') then
   *       klass.super = base_ctor
   *       local res = klass._init(obj,...) -- call our constructor
   *       if res then -- which can return a new self..
   *         obj = setmetatable(res,klass)
   *       end
   *     elseif base_ctor then -- call base ctor automatically
   *         base_ctor(obj,...)
   *     end
   *     return obj
   *   end */
  lua_pushvalue(L,lc2);
  lua_pushcclosure(L,stuart_class_new_2,1);
  lc_getupvalue(L,lc2,0,2);
  lua_insert(L,-2);
  lua_pushliteral(L,"new");
  lua_insert(L,-2);
  lua_settable(L,-3);
  lua_pop(L,1);
  assert(lua_gettop(L) == 2);

  /* --setmetatable(klass, {__call=klass.new})
   * return klass */
  lc_getupvalue(L,lc2,0,2);
  return 1;
  assert(lua_gettop(L) == 2);
}


// ============================================================================
// Shared classof() and new() functions used by other modules -- easy to centralize
// ============================================================================

/* name: M.classof
 * function(obj) */
int stuart_class_shared_classof (lua_State * L, const char *currentClassModule) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* local m = getmetatable(obj) */
  lua_getfield(L,LUA_ENVIRONINDEX,"getmetatable");
  lua_pushvalue(L,1);
  lua_call(L,1,1);
  assert(lua_gettop(L) == 2);

  /* -- an object created by class() ?
   * if not m or not m._class then */
  enum { lc1 = 2 };
  lua_pushboolean(L,!(lua_toboolean(L,2)));
  if (!(lua_toboolean(L,-1))) {
    lua_pop(L,1);
    lua_pushliteral(L,"_class");
    lua_gettable(L,2);
    lua_pushboolean(L,!(lua_toboolean(L,-1)));
    lua_remove(L,-2);
  }
  const int lc2 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc2) {

    /* return false */
    lua_pushboolean(L,0);
    return 1;
    assert(lua_gettop(L) == 2);
  }
  lua_settop(L,lc1);
  assert(lua_gettop(L) == 2);

  /* local klass = require 'stuart.<whatever>' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushstring(L,currentClassModule);
  lua_call(L,1,1);
  assert(lua_gettop(L) == 3);

  /* while m do -- follow the inheritance chain -- */
  enum { lc3 = 3 };
  while (1) {
    if (!(lua_toboolean(L,2))) {
      break;
    }

    /* -- follow the inheritance chain --
     * if m == klass then */
    enum { lc4 = 3 };
    const int lc5 = lua_equal(L,2,3);
    lua_pushboolean(L,lc5);
    const int lc6 = lua_toboolean(L,-1);
    lua_pop(L,1);
    if (lc6) {

      /* return true */
      lua_pushboolean(L,1);
      return 1;
      assert(lua_gettop(L) == 3);
    }
    lua_settop(L,lc4);
    assert(lua_gettop(L) == 3);

    /* m = rawget(m,'_base') */
    lua_getfield(L,LUA_ENVIRONINDEX,"rawget");
    lua_pushvalue(L,2);
    lua_pushliteral(L,"_base");
    lua_call(L,2,1);
    lua_replace(L,2);
    assert(lua_gettop(L) == 3);
  }
  lua_settop(L,lc3);
  assert(lua_gettop(L) == 3);

  /* return false */
  lua_pushboolean(L,0);
  return 1;
  assert(lua_gettop(L) == 3);
}


/* name: M.new
 * function(...) */
int stuart_class_shared_new (lua_State * L, const char *currentClassModule) {
  enum { lc_nformalargs = 0 };
  const int lc_nactualargs = lua_gettop(L);
  const int lc_nextra = (lc_nactualargs - lc_nformalargs);

  /* local klass = require 'stuart.<whatever>' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushstring(L,currentClassModule);
  lua_call(L,1,1);
  assert(lua_gettop(L) - lc_nextra == 1);

  /* local base_ctor = nil */
  lua_pushnil(L);
  assert(lua_gettop(L) - lc_nextra == 2);

  /* local obj = setmetatable({},klass) */
  lua_getfield(L,LUA_ENVIRONINDEX,"setmetatable");
  lua_newtable(L);
  lua_pushvalue(L,(1 + lc_nextra));
  lua_call(L,2,1);
  assert(lua_gettop(L) - lc_nextra == 3);

  /* if rawget(klass,'_init') then */
  enum { lc7 = 3 };
  lua_getfield(L,LUA_ENVIRONINDEX,"rawget");
  lua_pushvalue(L,(1 + lc_nextra));
  lua_pushliteral(L,"_init");
  lua_call(L,2,1);
  const int lc8 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc8) {

    /* --klass.super = base_ctor
     * local res = klass._init(obj,...) */
    lua_pushliteral(L,"_init");
    lua_gettable(L,(1 + lc_nextra));
    const int lc9 = lua_gettop(L);
    lua_pushvalue(L,(3 + lc_nextra));
    {int i; for (i=lc_nformalargs+1; i<=lc_nactualargs; i++) { lua_pushvalue(L, i); }}
    lua_call(L,(lua_gettop(L) - lc9),1);
    assert(lua_gettop(L) - lc_nextra == 4);

    /* -- call our constructor
     * if res then */
    enum { lc10 = 4 };
    if (lua_toboolean(L,(4 + lc_nextra))) {

      /* -- which can return a new self..
       * obj = setmetatable(res,klass) */
      lua_getfield(L,LUA_ENVIRONINDEX,"setmetatable");
      lua_pushvalue(L,(4 + lc_nextra));
      lua_pushvalue(L,(1 + lc_nextra));
      lua_call(L,2,1);
      lua_replace(L,(3 + lc_nextra));
      assert(lua_gettop(L) - lc_nextra == 4);
    }
    lua_settop(L,(lc10 + lc_nextra));
    assert(lua_gettop(L) - lc_nextra == 4);
  }
  else {

    /* elseif base_ctor then */
    enum { lc11 = 3 };
    if (lua_toboolean(L,(2 + lc_nextra))) {

      /* -- call base ctor automatically
       * base_ctor(obj,...) */
      lua_pushvalue(L,(2 + lc_nextra));
      const int lc12 = lua_gettop(L);
      lua_pushvalue(L,(3 + lc_nextra));
      {int i; for (i=lc_nformalargs+1; i<=lc_nactualargs; i++) { lua_pushvalue(L, i); }}
      lua_call(L,(lua_gettop(L) - lc12),0);
      assert(lua_gettop(L) - lc_nextra == 3);
    }
    lua_settop(L,(lc11 + lc_nextra));
  }
  lua_settop(L,(lc7 + lc_nextra));
  assert(lua_gettop(L) - lc_nextra == 3);

  /* return obj */
  lua_pushvalue(L,(3 + lc_nextra));
  return 1;
  assert(lua_gettop(L) - lc_nextra == 3);
}


/* name: M.new
 * function(...) */
int stuart_class_shared_new_with_super (lua_State * L, const char *currentClassModule, const char *superClassModule) {
  enum { lc_nformalargs = 0 };
  const int lc_nactualargs = lua_gettop(L);
  const int lc_nextra = (lc_nactualargs - lc_nformalargs);

  /* local klass = require 'stuart.streaming.QueueInputDStream' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushstring(L,currentClassModule);
  lua_call(L,1,1);
  assert(lua_gettop(L) - lc_nextra == 1);

  /* local has_base, base = pcall(require,'stuart.streaming.DStream') */
  lua_getfield(L,LUA_ENVIRONINDEX,"pcall");
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushstring(L,superClassModule);
  lua_call(L,2,2);
  assert(lua_gettop(L) - lc_nextra == 3);

  /* local base_ctor */
  lua_settop(L,(lua_gettop(L) + 1));
  assert(lua_gettop(L) - lc_nextra == 4);

  /* if has_base then */
  enum { lc7 = 4 };
  if (lua_toboolean(L,(2 + lc_nextra))) {

    /* base_ctor = rawget(base,'_init') */
    lua_getfield(L,LUA_ENVIRONINDEX,"rawget");
    lua_pushvalue(L,(3 + lc_nextra));
    lua_pushliteral(L,"_init");
    lua_call(L,2,1);
    lua_replace(L,(4 + lc_nextra));
    assert(lua_gettop(L) - lc_nextra == 4);
  }
  lua_settop(L,(lc7 + lc_nextra));
  assert(lua_gettop(L) - lc_nextra == 4);

  /* local obj = setmetatable({},klass) */
  lua_getfield(L,LUA_ENVIRONINDEX,"setmetatable");
  lua_newtable(L);
  lua_pushvalue(L,(1 + lc_nextra));
  lua_call(L,2,1);
  assert(lua_gettop(L) - lc_nextra == 5);

  /* if rawget(klass,'_init') then */
  enum { lc8 = 5 };
  lua_getfield(L,LUA_ENVIRONINDEX,"rawget");
  lua_pushvalue(L,(1 + lc_nextra));
  lua_pushliteral(L,"_init");
  lua_call(L,2,1);
  const int lc9 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc9) {

    /* --klass.super = base_ctor
     * local res = klass._init(obj,...) */
    lua_pushliteral(L,"_init");
    lua_gettable(L,(1 + lc_nextra));
    const int lc10 = lua_gettop(L);
    lua_pushvalue(L,(5 + lc_nextra));
    {int i; for (i=lc_nformalargs+1; i<=lc_nactualargs; i++) { lua_pushvalue(L, i); }}
    lua_call(L,(lua_gettop(L) - lc10),1);
    assert(lua_gettop(L) - lc_nextra == 6);

    /* -- call our constructor
     * if res then */
    enum { lc11 = 6 };
    if (lua_toboolean(L,(6 + lc_nextra))) {

      /* -- which can return a new self..
       * obj = setmetatable(res,klass) */
      lua_getfield(L,LUA_ENVIRONINDEX,"setmetatable");
      lua_pushvalue(L,(6 + lc_nextra));
      lua_pushvalue(L,(1 + lc_nextra));
      lua_call(L,2,1);
      lua_replace(L,(5 + lc_nextra));
      assert(lua_gettop(L) - lc_nextra == 6);
    }
    lua_settop(L,(lc11 + lc_nextra));
    assert(lua_gettop(L) - lc_nextra == 6);
  }
  else {

    /* elseif base_ctor then */
    enum { lc12 = 5 };
    if (lua_toboolean(L,(4 + lc_nextra))) {

      /* -- call base ctor automatically
       * base_ctor(obj,...) */
      lua_pushvalue(L,(4 + lc_nextra));
      const int lc13 = lua_gettop(L);
      lua_pushvalue(L,(5 + lc_nextra));
      {int i; for (i=lc_nformalargs+1; i<=lc_nactualargs; i++) { lua_pushvalue(L, i); }}
      lua_call(L,(lua_gettop(L) - lc13),0);
      assert(lua_gettop(L) - lc_nextra == 5);
    }
    lua_settop(L,(lc12 + lc_nextra));
  }
  lua_settop(L,(lc8 + lc_nextra));
  assert(lua_gettop(L) - lc_nextra == 5);

  /* return obj */
  lua_pushvalue(L,(5 + lc_nextra));
  return 1;
  assert(lua_gettop(L) - lc_nextra == 5);
}


// ============================================================================
// Definition of the module Romtable.
// ============================================================================

const LUA_REG_TYPE stuart_class_map[] = {
  { LSTRKEY("istype") , LFUNCVAL(stuart_class_istype) },
  { LSTRKEY("new")    , LFUNCVAL(stuart_class_new) },
  { LNILKEY, LNILVAL }
};


// ============================================================================
// Registration of the module.
// ============================================================================

LUALIB_API int luaopen_stuart_class( lua_State *L ){
#if LUA_OPTIMIZE_MEMORY > 0
  return 0;
#else
  LREGISTER( L, AUXLIB_STUART_CLASS, stuart_class_map );
  return 1;
#endif
}
