// Auxiliary Lua modules. All of them are declared here, then each platform
// decides what module(s) to register in the src/platform/xxxxx/platform_conf.h file
// FIXME: no longer platform_conf.h - either CPU header file, or board file

#ifndef __AUXMODS_H__
#define __AUXMODS_H__

#include "lua.h"

#define AUXLIB_PIO      "pio"
LUALIB_API int ( luaopen_pio )( lua_State *L );

#define AUXLIB_SPI      "spi"
LUALIB_API int ( luaopen_spi )( lua_State *L );

#define AUXLIB_CAN      "can"
LUALIB_API int ( luaopen_can )( lua_State *L );

#define AUXLIB_TMR      "tmr"
LUALIB_API int ( luaopen_tmr )( lua_State *L );

#define AUXLIB_PD       "pd"
LUALIB_API int ( luaopen_pd )( lua_State *L );

#define AUXLIB_UART     "uart"
LUALIB_API int ( luaopen_uart )( lua_State *L );

#define AUXLIB_TERM     "term"
LUALIB_API int ( luaopen_term )( lua_State *L );

#define AUXLIB_PWM      "pwm"
LUALIB_API int ( luaopen_pwm )( lua_State *L );

#define AUXLIB_PACK     "pack"
LUALIB_API int ( luaopen_pack )( lua_State *L );

#define AUXLIB_BIT      "bit"
LUALIB_API int ( luaopen_bit )( lua_State *L );

#define AUXLIB_NET      "net"
LUALIB_API int ( luaopen_net )( lua_State *L );

#define AUXLIB_CPU      "cpu"
LUALIB_API int ( luaopen_cpu )( lua_State* L );

#define AUXLIB_ADC      "adc"
LUALIB_API int ( luaopen_adc )( lua_State *L );

#define AUXLIB_RPC   "rpc"
LUALIB_API int ( luaopen_rpc )( lua_State *L );

#define AUXLIB_BITARRAY "bitarray"
LUALIB_API int ( luaopen_bitarray )( lua_State *L );

#define AUXLIB_ELUA "elua"
LUALIB_API int ( luaopen_elua )( lua_State *L );

#define AUXLIB_I2C  "i2c"
LUALIB_API int ( luaopen_i2c )( lua_State *L );

// begin Stuart

#define AUXLIB_CJSON "cjson"
LUALIB_API int ( luaopen_cjson )( lua_State *L );

#define AUXLIB_LAUXHLIB "lauxhlib"
LUALIB_API int ( luaopen_lauxhlib )( lua_State *L );

#define AUXLIB_MOSES "moses"
LUALIB_API int ( luaopen_moses )( lua_State *L );

#define AUXLIB_STUART "stuart"
LUALIB_API int ( luaopen_stuart )( lua_State *L );

#define AUXLIB_STUART_CLASS "stuart.class"
LUALIB_API int ( luaopen_stuart_class )( lua_State *L );

#define AUXLIB_STUART_CONTEXT "stuart.Context"
LUALIB_API int ( luaopen_stuart_context )( lua_State *L );

#define AUXLIB_STUART_FILESYSTEM "stuart.FileSystem"
LUALIB_API int ( luaopen_stuart_filesystem )( lua_State *L );

#define AUXLIB_STUART_FILESYSTEMF "stuart.fileSystemFactory"
LUALIB_API int ( luaopen_stuart_filesystemf )( lua_State *L );

#define AUXLIB_STUART_FILESYSTEML "stuart.LocalFileSystem"
LUALIB_API int ( luaopen_stuart_filesysteml )( lua_State *L );

#define AUXLIB_STUART_INTERFACE "stuart.interface"
LUALIB_API int ( luaopen_stuart_interface )( lua_State *L );

#define AUXLIB_STUART_LOGGER "stuart.internal.Logger"
LUALIB_API int ( luaopen_stuart_logger )( lua_State *L );

#define AUXLIB_STUART_LOGGING "stuart.internal.logging"
LUALIB_API int ( luaopen_stuart_logging )( lua_State *L );

#define AUXLIB_STUART_PARTITION "stuart.Partition"
LUALIB_API int ( luaopen_stuart_partition )( lua_State *L );

#define AUXLIB_STUART_RDD "stuart.RDD"
LUALIB_API int ( luaopen_stuart_rdd )( lua_State *L );

#define AUXLIB_STUART_RDSTREAM "stuart.streaming.ReceiverInputDStream"
LUALIB_API int ( luaopen_stuart_rdstream )( lua_State *L );

#define AUXLIB_STUART_SPARKCONF "stuart.SparkConf"
LUALIB_API int ( luaopen_stuart_sparkconf )( lua_State *L );

#define AUXLIB_STUART_DSTREAM "stuart.streaming.DStream"
LUALIB_API int ( luaopen_stuart_dstream )( lua_State *L );

#define AUXLIB_STUART_QINPUTDSTREAM "stuart.streaming.QueueInputDStream"
LUALIB_API int ( luaopen_stuart_qinputdstream )( lua_State *L );

#define AUXLIB_STUART_RECEIVER "stuart.streaming.Receiver"
LUALIB_API int ( luaopen_stuart_receiver)( lua_State *L );

#define AUXLIB_STUART_STREAMINGCTX "stuart.streaming.StreamingContext"
LUALIB_API int ( luaopen_stuart_streamingctx)( lua_State *L );

#define AUXLIB_STUART_UTIL "stuart.util"
LUALIB_API int ( luaopen_stuart_util )( lua_State *L );

#define AUXLIB_STUART_WDSTREAM "stuart.streaming.WindowedDStream"
LUALIB_API int ( luaopen_stuart_wdstream )( lua_State *L );

#define AUXLIB_STUART_XDSTREAM "stuart.streaming.TransformedDStream"
LUALIB_API int ( luaopen_stuart_xdstream )( lua_State *L );

#define AUXLIB_URL "url"
LUALIB_API int ( luaopen_url )( lua_State *L );

#define AUXLIB_URL_CODEC "url.codec"
LUALIB_API int ( luaopen_url_codec )( lua_State *L );

// end Stuart

// Helper macros
#define MOD_CHECK_ID( mod, id )\
  if( !platform_ ## mod ## _exists( id ) )\
    return luaL_error( L, #mod" %d does not exist", ( unsigned )id )

#define MOD_CHECK_TIMER( id )\
  if( id == PLATFORM_TIMER_SYS_ID && !platform_timer_sys_available() )\
    return luaL_error( L, "the system timer is not available on this platform" );\
  if( !platform_timer_exists( id ) )\
    return luaL_error( L, "timer %d does not exist", ( unsigned )id )\

#define MOD_CHECK_RES_ID( mod, id, resmod, resid )\
  if( !platform_ ## mod ## _check_ ## resmod ## _id( id, resid ) )\
    return luaL_error( L, #resmod" %d not valid with " #mod " %d", ( unsigned )resid, ( unsigned )id )

#define MOD_REG_NUMBER( L, name, val )\
  lua_pushnumber( L, val );\
  lua_setfield( L, -2, name )

#endif

