#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
#include "common.h"
#include "auxmods.h"
#include "lrotable.h"
#include "platform_conf.h"
#define MIN_OPT_LEVEL 2
#include "lrodefs.h"
#include <assert.h>
#include "lua2c.h"
#include "stuart_class.h"
#include "stuart_streaming_dstream.h"

#define MODULE "stuart.streaming.ReceiverInputDStream"
#define SUPERMODULE "stuart.streaming.DStream"


/* name: ReceiverInputDStream:_init
 * function(ssc, receiver) */
int stuart_streaming_ReceiverInputDStream__init (lua_State * L) {
  enum { lc_nformalargs = 3 };
  lua_settop(L,3);

  /* local DStream = require 'stuart.streaming.DStream' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,SUPERMODULE);
  lua_call(L,1,1);
  assert(lua_gettop(L) == 4);

  /* DStream._init(self, ssc) */
  lua_pushliteral(L,"_init");
  lua_gettable(L,4);
  lua_pushvalue(L,1);
  lua_pushvalue(L,2);
  lua_call(L,2,0);
  assert(lua_gettop(L) == 4);

  /* self.receiver = receiver */
  lua_pushvalue(L,3);
  lua_pushliteral(L,"receiver");
  lua_insert(L,-2);
  lua_settable(L,1);
  assert(lua_gettop(L) == 4);
  return 0;
}


/* name: M.classof
 * function(obj) */
int stuart_streaming_ReceiverInputDStream_classof (lua_State * L) {
  return stuart_class_shared_classof(L, MODULE);
}


/* name: M.new
 * function(...) */
int stuart_streaming_ReceiverInputDStream_new (lua_State * L) {
  return stuart_class_shared_new_with_super(L, MODULE, SUPERMODULE);
}


/* name: ReceiverInputDStream:poll
 * function(durationBudget) */
int stuart_streaming_ReceiverInputDStream_poll (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* return self.receiver:poll(durationBudget) */
  const int lc1 = lua_gettop(L);
  lua_pushliteral(L,"receiver");
  lua_gettable(L,1);
  lua_pushliteral(L,"poll");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_pushvalue(L,2);
  lua_call(L,2,LUA_MULTRET);
  return (lua_gettop(L) - lc1);
  assert(lua_gettop(L) == 2);
}


/* name: ReceiverInputDStream:start
 * function() */
int stuart_streaming_ReceiverInputDStream_start (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* self.receiver:onStart() */
  lua_pushliteral(L,"receiver");
  lua_gettable(L,1);
  lua_pushliteral(L,"onStart");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,0);
  assert(lua_gettop(L) == 1);
  return 0;
}


/* name: ReceiverInputDStream:stop
 * function() */
int stuart_streaming_ReceiverInputDStream_stop (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* self.receiver:onStop() */
  lua_pushliteral(L,"receiver");
  lua_gettable(L,1);
  lua_pushliteral(L,"onStop");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,0);
  assert(lua_gettop(L) == 1);
  return 0;
}


// ============================================================================
// Definition of the module Romtable.
// ============================================================================

const LUA_REG_TYPE stuart_rdstream_map[] = {
  { LSTRKEY("_init")           , LFUNCVAL (stuart_streaming_ReceiverInputDStream__init) },
  { LSTRKEY("poll")            , LFUNCVAL (stuart_streaming_ReceiverInputDStream_poll) },
  { LSTRKEY("start")           , LFUNCVAL (stuart_streaming_ReceiverInputDStream_start) },
  { LSTRKEY("stop")            , LFUNCVAL (stuart_streaming_ReceiverInputDStream_stop) },

  // class framework
  { LSTRKEY("__index")         , LRO_ROVAL(stuart_rdstream_map) },
  { LSTRKEY("_base")           , LRO_ROVAL(stuart_dstream_map) },
  { LSTRKEY("_class")          , LRO_ROVAL(stuart_rdstream_map) },
  { LSTRKEY("classof")         , LFUNCVAL (stuart_streaming_ReceiverInputDStream_classof) },
  { LSTRKEY("new")             , LFUNCVAL (stuart_streaming_ReceiverInputDStream_new) },

  // inherited (not including __index, _class, _init, classof, and new)
  { LSTRKEY("_notify")         , LFUNCVAL (stuart_streaming_DStream__notify) },
  { LSTRKEY("count")           , LFUNCVAL (stuart_streaming_DStream_count) },
  { LSTRKEY("countByWindow")   , LFUNCVAL (stuart_streaming_DStream_countByWindow) },
  { LSTRKEY("foreachRDD")      , LFUNCVAL (stuart_streaming_DStream_foreachRDD) },
  { LSTRKEY("groupByKey")      , LFUNCVAL (stuart_streaming_DStream_groupByKey) },
  { LSTRKEY("map")             , LFUNCVAL (stuart_streaming_DStream_map) },
  { LSTRKEY("mapValues")       , LFUNCVAL (stuart_streaming_DStream_mapValues) },
  { LSTRKEY("poll")            , LFUNCVAL (stuart_streaming_DStream_poll) },
  { LSTRKEY("reduce")          , LFUNCVAL (stuart_streaming_DStream_reduce) },
  { LSTRKEY("start")           , LFUNCVAL (stuart_streaming_DStream_start) },
  { LSTRKEY("stop")            , LFUNCVAL (stuart_streaming_DStream_stop) },
  { LSTRKEY("transform")       , LFUNCVAL (stuart_streaming_DStream_transform) },
  { LSTRKEY("window")          , LFUNCVAL (stuart_streaming_DStream_window) },

  { LNILKEY, LNILVAL }
};


// ============================================================================
// Registration of the module.
// ============================================================================
LUALIB_API int luaopen_stuart_rdstream( lua_State *L ){
#if LUA_OPTIMIZE_MEMORY > 0
  return 0;
#else
  LREGISTER( L, AUXLIB_STUART_RDSTREAM, stuart_rdstream_map );
  return 1;
#endif
}
