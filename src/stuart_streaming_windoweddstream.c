#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
#include "common.h"
#include "auxmods.h"
#include "lrotable.h"
#include "platform_conf.h"
#define MIN_OPT_LEVEL 2
#include "lrodefs.h"
#include <assert.h>
#include "lua2c.h"
#include "stuart_class.h"
#include "stuart_streaming_dstream.h"

#define MODULE "stuart.streaming.WindowedDStream"
#define SUPERMODULE "stuart.streaming.DStream"


/* name: WindowedDStream:_init
 * function(ssc, windowDuration) */
int stuart_streaming_WindowedDStream__init (lua_State * L) {
  enum { lc_nformalargs = 3 };
  lua_settop(L,3);

  /* local DStream = require 'stuart.streaming.DStream' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,SUPERMODULE);
  lua_call(L,1,1);
  assert(lua_gettop(L) == 4);

  /* DStream._init(self, ssc) */
  lua_pushliteral(L,"_init");
  lua_gettable(L,4);
  lua_pushvalue(L,1);
  lua_pushvalue(L,2);
  lua_call(L,2,0);
  assert(lua_gettop(L) == 4);

  /* self.windowDuration = windowDuration */
  lua_pushvalue(L,3);
  lua_pushliteral(L,"windowDuration");
  lua_insert(L,-2);
  lua_settable(L,1);
  assert(lua_gettop(L) == 4);

  /* self.window = {} */
  lua_newtable(L);
  lua_pushliteral(L,"window");
  lua_insert(L,-2);
  lua_settable(L,1);
  assert(lua_gettop(L) == 4);
  return 0;
}


/* name: M.classof
 * function(obj) */
int stuart_streaming_WindowedDStream_classof (lua_State * L) {
  return stuart_class_shared_classof(L, MODULE);
}


/* name: M.new
 * function(...) */
int stuart_streaming_WindowedDStream_new (lua_State * L) {
  return stuart_class_shared_new_with_super(L, MODULE, SUPERMODULE);
}


/* name: WindowedDStream:_notify
 * function(validTime, rdd) */
int stuart_streaming_WindowedDStream__notify (lua_State * L) {
  enum { lc_nformalargs = 3 };
  lua_settop(L,3);

  /* -- expire items from the window
   * while #self.window > 0 and validTime - self.window[1]._validTime > self.windowDuration do */
  enum { lc1 = 3 };
  while (1) {
    lua_pushnumber(L,0);
    lua_pushliteral(L,"window");
    lua_gettable(L,1);
    const double lc2 = lua_objlen(L,-1);
    lua_pop(L,1);
    lua_pushnumber(L,lc2);
    const int lc3 = lua_lessthan(L,-2,-1);
    lua_pop(L,2);
    lua_pushboolean(L,lc3);
    if (lua_toboolean(L,-1)) {
      lua_pop(L,1);
      lua_pushliteral(L,"windowDuration");
      lua_gettable(L,1);
      lua_pushliteral(L,"window");
      lua_gettable(L,1);
      lua_pushnumber(L,1);
      lua_gettable(L,-2);
      lua_remove(L,-2);
      lua_pushliteral(L,"_validTime");
      lua_gettable(L,-2);
      lua_remove(L,-2);
      lc_sub(L,2,-1);
      lua_remove(L,-2);
      const int lc4 = lua_lessthan(L,-2,-1);
      lua_pop(L,2);
      lua_pushboolean(L,lc4);
    }
    if (!(lua_toboolean(L,-1))) {
      break;
    }
    lua_pop(L,1);

    /* table.remove(self.window, 1) */
    lua_getfield(L,LUA_ENVIRONINDEX,"table");
    lua_pushliteral(L,"remove");
    lua_gettable(L,-2);
    lua_remove(L,-2);
    lua_pushliteral(L,"window");
    lua_gettable(L,1);
    lua_pushnumber(L,1);
    lua_call(L,2,0);
    assert(lua_gettop(L) == 3);
  }
  lua_settop(L,lc1);
  assert(lua_gettop(L) == 3);

  /* -- add current rdd to the window
   * rdd._validTime = validTime */
  lua_pushvalue(L,2);
  lua_pushliteral(L,"_validTime");
  lua_insert(L,-2);
  lua_settable(L,3);
  assert(lua_gettop(L) == 3);

  /* self.window[#self.window+1] = rdd */
  lua_pushvalue(L,3);
  lua_pushliteral(L,"window");
  lua_gettable(L,1);
  lua_insert(L,-2);
  lua_pushliteral(L,"window");
  lua_gettable(L,1);
  const double lc5 = lua_objlen(L,-1);
  lua_pop(L,1);
  lua_pushnumber(L,lc5);
  lua_pushnumber(L,1);
  lc_add(L,-2,-1);
  lua_remove(L,-2);
  lua_remove(L,-2);
  lua_insert(L,-2);
  lua_settable(L,-3);
  lua_pop(L,1);
  assert(lua_gettop(L) == 3);

  /* local unioned = self.ssc.sc:union(self.window) */
  lua_pushliteral(L,"ssc");
  lua_gettable(L,1);
  lua_pushliteral(L,"sc");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_pushliteral(L,"union");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_pushliteral(L,"window");
  lua_gettable(L,1);
  lua_call(L,2,1);
  assert(lua_gettop(L) == 4);

  /* for _, dstream in ipairs(self.inputs) do
   * internal: local f, s, var = explist */
  enum { lc6 = 4 };
  lua_getfield(L,LUA_ENVIRONINDEX,"ipairs");
  lua_pushliteral(L,"inputs");
  lua_gettable(L,1);
  lua_call(L,1,3);
  while (1) {

    /* internal: local var_1, ..., var_n = f(s, var)
     *           if var_1 == nil then break end
     *           var = var_1 */
    lua_pushvalue(L,-3);
    lua_pushvalue(L,-3);
    lua_pushvalue(L,-3);
    lua_call(L,2,2);
    if (lua_isnil(L,-2)) {
      break;
    }
    lua_pushvalue(L,-2);
    lua_replace(L,-4);

    /* internal: local _ with idx 8
     * internal: local dstream with idx 9 */


    /* rdd = dstream:_notify(validTime, unioned) */
    lua_pushvalue(L,9);
    lua_pushliteral(L,"_notify");
    lua_gettable(L,-2);
    lua_insert(L,-2);
    lua_pushvalue(L,2);
    lua_pushvalue(L,4);
    lua_call(L,3,1);
    lua_replace(L,3);
    assert(lua_gettop(L) == 9);

    /* internal: stack cleanup on scope exit */
    lua_pop(L,2);
  }
  lua_settop(L,lc6);
  assert(lua_gettop(L) == 4);

  /* for _, dstream in ipairs(self.outputs) do
   * internal: local f, s, var = explist */
  enum { lc7 = 4 };
  lua_getfield(L,LUA_ENVIRONINDEX,"ipairs");
  lua_pushliteral(L,"outputs");
  lua_gettable(L,1);
  lua_call(L,1,3);
  while (1) {

    /* internal: local var_1, ..., var_n = f(s, var)
     *           if var_1 == nil then break end
     *           var = var_1 */
    lua_pushvalue(L,-3);
    lua_pushvalue(L,-3);
    lua_pushvalue(L,-3);
    lua_call(L,2,2);
    if (lua_isnil(L,-2)) {
      break;
    }
    lua_pushvalue(L,-2);
    lua_replace(L,-4);

    /* internal: local _ with idx 8
     * internal: local dstream with idx 9 */


    /* dstream:_notify(validTime, unioned) */
    lua_pushvalue(L,9);
    lua_pushliteral(L,"_notify");
    lua_gettable(L,-2);
    lua_insert(L,-2);
    lua_pushvalue(L,2);
    lua_pushvalue(L,4);
    lua_call(L,3,0);
    assert(lua_gettop(L) == 9);

    /* internal: stack cleanup on scope exit */
    lua_pop(L,2);
  }
  lua_settop(L,lc7);
  assert(lua_gettop(L) == 4);

  /* return rdd */
  lua_pushvalue(L,3);
  return 1;
  assert(lua_gettop(L) == 4);
}


// ============================================================================
// Definition of the module Romtable.
// ============================================================================

const LUA_REG_TYPE stuart_wdstream_map[] = {
  { LSTRKEY("_init")           , LFUNCVAL (stuart_streaming_WindowedDStream__init) },
  { LSTRKEY("_notify")         , LFUNCVAL (stuart_streaming_WindowedDStream__notify) },

  // class framework
  { LSTRKEY("__index")         , LRO_ROVAL(stuart_wdstream_map) },
  { LSTRKEY("_base")           , LRO_ROVAL(stuart_dstream_map) },
  { LSTRKEY("_class")          , LRO_ROVAL(stuart_wdstream_map) },
  { LSTRKEY("classof")         , LFUNCVAL (stuart_streaming_WindowedDStream_classof) },
  { LSTRKEY("new")             , LFUNCVAL (stuart_streaming_WindowedDStream_new) },

  // inherited (not including __index, _class, _init, classof, and new)
  { LSTRKEY("_notify")         , LFUNCVAL (stuart_streaming_DStream__notify) },
  { LSTRKEY("count")           , LFUNCVAL (stuart_streaming_DStream_count) },
  { LSTRKEY("countByWindow")   , LFUNCVAL (stuart_streaming_DStream_countByWindow) },
  { LSTRKEY("foreachRDD")      , LFUNCVAL (stuart_streaming_DStream_foreachRDD) },
  { LSTRKEY("groupByKey")      , LFUNCVAL (stuart_streaming_DStream_groupByKey) },
  { LSTRKEY("map")             , LFUNCVAL (stuart_streaming_DStream_map) },
  { LSTRKEY("mapValues")       , LFUNCVAL (stuart_streaming_DStream_mapValues) },
  { LSTRKEY("poll")            , LFUNCVAL (stuart_streaming_DStream_poll) },
  { LSTRKEY("reduce")          , LFUNCVAL (stuart_streaming_DStream_reduce) },
  { LSTRKEY("start")           , LFUNCVAL (stuart_streaming_DStream_start) },
  { LSTRKEY("stop")            , LFUNCVAL (stuart_streaming_DStream_stop) },
  { LSTRKEY("transform")       , LFUNCVAL (stuart_streaming_DStream_transform) },
  { LSTRKEY("window")          , LFUNCVAL (stuart_streaming_DStream_window) },

  { LNILKEY, LNILVAL }
};


// ============================================================================
// Registration of the module.
// ============================================================================
LUALIB_API int luaopen_stuart_wdstream( lua_State *L ){
#if LUA_OPTIMIZE_MEMORY > 0
  return 0;
#else
  LREGISTER( L, AUXLIB_STUART_WDSTREAM, stuart_wdstream_map );
  return 1;
#endif
}
