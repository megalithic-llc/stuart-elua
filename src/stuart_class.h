#ifndef __STUART_CLASS_H__
#define __STUART_CLASS_H__

#include "lua.h"

int stuart_class_shared_classof (lua_State * L, const char *currentClassModule);
int stuart_class_shared_new (lua_State * L, const char *currentClassModule);
int stuart_class_shared_new_with_super (lua_State * L, const char *currentClassModule, const char *superClassModule);

#endif
