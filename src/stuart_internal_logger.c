#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
#include "common.h"
#include "auxmods.h"
#include "lrotable.h"
#include "platform_conf.h"
#define MIN_OPT_LEVEL 2
#include "lrodefs.h"
#include <assert.h>
#include "lua2c.h"
#include "stuart_class.h"

static const int STUART_INTERNAL_LOGGER_LEVEL_FATAL = 50000;
static const int STUART_INTERNAL_LOGGER_LEVEL_ERROR = 40000;
static const int STUART_INTERNAL_LOGGER_LEVEL_WARN  = 30000;
static const int STUART_INTERNAL_LOGGER_LEVEL_INFO  = 20000;
static const int STUART_INTERNAL_LOGGER_LEVEL_DEBUG = 10000;
static const int STUART_INTERNAL_LOGGER_LEVEL_TRACE = 0;


/* name: M:_init
 * function() */
static int stuart_Logger__init (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* self.level = INFO */
  lua_pushnumber(L,STUART_INTERNAL_LOGGER_LEVEL_INFO);
  lua_pushliteral(L,"level");
  lua_insert(L,-2);
  lua_settable(L,1);
  assert(lua_gettop(L) == 1);

  /* self.levelName = {
   *     [50000] = 'FATAL',
   *     [40000] = 'ERROR',
   *     [30000] = 'WARN',
   *     [20000] = 'INFO',
   *     [10000] = 'DEBUG',
   *     [0] = 'TRACE'
   *   } */
  lua_createtable(L,0,6);
  lua_pushnumber(L,STUART_INTERNAL_LOGGER_LEVEL_FATAL);
  lua_pushliteral(L,"FATAL");
  lua_rawset(L,-3);
  lua_pushnumber(L,STUART_INTERNAL_LOGGER_LEVEL_ERROR);
  lua_pushliteral(L,"ERROR");
  lua_rawset(L,-3);
  lua_pushnumber(L,STUART_INTERNAL_LOGGER_LEVEL_WARN);
  lua_pushliteral(L,"WARN");
  lua_rawset(L,-3);
  lua_pushnumber(L,STUART_INTERNAL_LOGGER_LEVEL_INFO);
  lua_pushliteral(L,"INFO");
  lua_rawset(L,-3);
  lua_pushnumber(L,STUART_INTERNAL_LOGGER_LEVEL_DEBUG);
  lua_pushliteral(L,"DEBUG");
  lua_rawset(L,-3);
  lua_pushnumber(L,STUART_INTERNAL_LOGGER_LEVEL_TRACE);
  lua_pushliteral(L,"TRACE");
  lua_rawset(L,-3);
  lua_pushliteral(L,"levelName");
  lua_insert(L,-2);
  lua_settable(L,1);
  assert(lua_gettop(L) == 1);
  return 0;
}


/* name: M.classof
 * function(obj) */
static int stuart_Logger_classof (lua_State * L) {
  return stuart_class_shared_classof(L, "stuart.internal.Logger");
}


/* name: M:debug
 * function(msg) */
static int stuart_Logger_debug (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* if self.level <= 10000 then */
  enum { lc1 = 2 };
  lua_pushliteral(L,"level");
  lua_gettable(L,1);
  lua_pushnumber(L,STUART_INTERNAL_LOGGER_LEVEL_DEBUG);
  const int lc2 = lc_le(L,-2,-1);
  lua_pop(L,2);
  lua_pushboolean(L,lc2);
  const int lc3 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc3) {

    /* self:log{level=10000, message=msg} */
    lua_pushvalue(L,1);
    lua_pushliteral(L,"log");
    lua_gettable(L,-2);
    lua_insert(L,-2);
    lua_createtable(L,0,2);
    lua_pushliteral(L,"level");
    lua_pushnumber(L,STUART_INTERNAL_LOGGER_LEVEL_DEBUG);
    lua_rawset(L,-3);
    lua_pushliteral(L,"message");
    lua_pushvalue(L,2);
    lua_rawset(L,-3);
    lua_call(L,2,0);
    assert(lua_gettop(L) == 2);
  }
  lua_settop(L,lc1);
  assert(lua_gettop(L) == 2);
  return 0;
}


/* name: M:error
 * function(msg) */
static int stuart_Logger_error (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* if self.level <= 40000 then */
  enum { lc4 = 2 };
  lua_pushliteral(L,"level");
  lua_gettable(L,1);
  lua_pushnumber(L,STUART_INTERNAL_LOGGER_LEVEL_ERROR);
  const int lc5 = lc_le(L,-2,-1);
  lua_pop(L,2);
  lua_pushboolean(L,lc5);
  const int lc6 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc6) {

    /* self:log{level=40000, message=msg} */
    lua_pushvalue(L,1);
    lua_pushliteral(L,"log");
    lua_gettable(L,-2);
    lua_insert(L,-2);
    lua_createtable(L,0,2);
    lua_pushliteral(L,"level");
    lua_pushnumber(L,STUART_INTERNAL_LOGGER_LEVEL_ERROR);
    lua_rawset(L,-3);
    lua_pushliteral(L,"message");
    lua_pushvalue(L,2);
    lua_rawset(L,-3);
    lua_call(L,2,0);
    assert(lua_gettop(L) == 2);
  }
  lua_settop(L,lc4);
  assert(lua_gettop(L) == 2);
  return 0;
}


/* name: M:info
 * function(msg) */
static int stuart_Logger_info (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* if self.level <= 20000 then */
  enum { lc7 = 2 };
  lua_pushliteral(L,"level");
  lua_gettable(L,1);
  lua_pushnumber(L,STUART_INTERNAL_LOGGER_LEVEL_INFO);
  const int lc8 = lc_le(L,-2,-1);
  lua_pop(L,2);
  lua_pushboolean(L,lc8);
  const int lc9 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc9) {

    /* self:log{level=20000, message=msg} */
    lua_pushvalue(L,1);
    lua_pushliteral(L,"log");
    lua_gettable(L,-2);
    lua_insert(L,-2);
    lua_createtable(L,0,2);
    lua_pushliteral(L,"level");
    lua_pushnumber(L,STUART_INTERNAL_LOGGER_LEVEL_INFO);
    lua_rawset(L,-3);
    lua_pushliteral(L,"message");
    lua_pushvalue(L,2);
    lua_rawset(L,-3);
    lua_call(L,2,0);
    assert(lua_gettop(L) == 2);
  }
  lua_settop(L,lc7);
  assert(lua_gettop(L) == 2);
  return 0;
}


/* name: M:log
 * function(event) */
static int stuart_Logger_log (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* local s = {self.levelName[event.level], event.message} */
  lua_createtable(L,2,0);
  lua_pushliteral(L,"levelName");
  lua_gettable(L,1);
  lua_pushliteral(L,"level");
  lua_gettable(L,2);
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_rawseti(L,-2,1);
  lua_pushliteral(L,"message");
  lua_gettable(L,2);
  lua_rawseti(L,-2,2);
  assert(lua_gettop(L) == 3);

  /* if io ~= nil then */
  enum { lc10 = 3 };
  lua_getfield(L,LUA_ENVIRONINDEX,"io");
  lua_pushnil(L);
  const int lc11 = lua_equal(L,-2,-1);
  lua_pop(L,2);
  lua_pushboolean(L,lc11);
  lua_pushboolean(L,!(lua_toboolean(L,-1)));
  lua_remove(L,-2);
  const int lc12 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc12) {

    /* io.stderr:write(table.concat(s,' ') .. '\n') */
    lua_getfield(L,LUA_ENVIRONINDEX,"io");
    lua_pushliteral(L,"stderr");
    lua_gettable(L,-2);
    lua_remove(L,-2);
    lua_pushliteral(L,"write");
    lua_gettable(L,-2);
    lua_insert(L,-2);
    lua_getfield(L,LUA_ENVIRONINDEX,"table");
    lua_pushliteral(L,"concat");
    lua_gettable(L,-2);
    lua_remove(L,-2);
    lua_pushvalue(L,3);
    lua_pushliteral(L," ");
    lua_call(L,2,1);
    lua_pushliteral(L,"\n");
    lua_concat(L,2);
    lua_call(L,2,0);
    assert(lua_gettop(L) == 3);
  }
  else {

    /* else
     * print(table.concat(s,' ') .. '\n') */
    lua_getfield(L,LUA_ENVIRONINDEX,"print");
    lua_getfield(L,LUA_ENVIRONINDEX,"table");
    lua_pushliteral(L,"concat");
    lua_gettable(L,-2);
    lua_remove(L,-2);
    lua_pushvalue(L,3);
    lua_pushliteral(L," ");
    lua_call(L,2,1);
    lua_pushliteral(L,"\n");
    lua_concat(L,2);
    lua_call(L,1,0);
    assert(lua_gettop(L) == 3);
  }
  lua_settop(L,lc10);
  assert(lua_gettop(L) == 3);
  return 0;
}


/* name: M.new
 * function(...) */
static int stuart_Logger_new (lua_State * L) {
  return stuart_class_shared_new(L, "stuart.internal.Logger");
}


/* name: M:setLevel
 * function(level) */
static int stuart_Logger_setLevel (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* self.level = level */
  lua_pushvalue(L,2);
  lua_pushliteral(L,"level");
  lua_insert(L,-2);
  lua_settable(L,1);
  assert(lua_gettop(L) == 2);
  return 0;
}


/* name: M:trace
 * function(msg) */
static int stuart_Logger_trace (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* if self.level <= 0 then */
  enum { lc13 = 2 };
  lua_pushliteral(L,"level");
  lua_gettable(L,1);
  lua_pushnumber(L,0);
  const int lc14 = lc_le(L,-2,-1);
  lua_pop(L,2);
  lua_pushboolean(L,lc14);
  const int lc15 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc15) {

    /* self:log{level=0, message=msg} */
    lua_pushvalue(L,1);
    lua_pushliteral(L,"log");
    lua_gettable(L,-2);
    lua_insert(L,-2);
    lua_createtable(L,0,2);
    lua_pushliteral(L,"level");
    lua_pushnumber(L,STUART_INTERNAL_LOGGER_LEVEL_TRACE);
    lua_rawset(L,-3);
    lua_pushliteral(L,"message");
    lua_pushvalue(L,2);
    lua_rawset(L,-3);
    lua_call(L,2,0);
    assert(lua_gettop(L) == 2);
  }
  lua_settop(L,lc13);
  assert(lua_gettop(L) == 2);
  return 0;
}


/* name: M:warn
 * function(msg) */
static int stuart_Logger_warn (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* if self.level <= 30000 then */
  enum { lc16 = 2 };
  lua_pushliteral(L,"level");
  lua_gettable(L,1);
  lua_pushnumber(L,STUART_INTERNAL_LOGGER_LEVEL_WARN);
  const int lc17 = lc_le(L,-2,-1);
  lua_pop(L,2);
  lua_pushboolean(L,lc17);
  const int lc18 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc18) {

    /* self:log{level=30000, message=msg} */
    lua_pushvalue(L,1);
    lua_pushliteral(L,"log");
    lua_gettable(L,-2);
    lua_insert(L,-2);
    lua_createtable(L,0,2);
    lua_pushliteral(L,"level");
    lua_pushnumber(L,STUART_INTERNAL_LOGGER_LEVEL_WARN);
    lua_rawset(L,-3);
    lua_pushliteral(L,"message");
    lua_pushvalue(L,2);
    lua_rawset(L,-3);
    lua_call(L,2,0);
    assert(lua_gettop(L) == 2);
  }
  lua_settop(L,lc16);
  assert(lua_gettop(L) == 2);
  return 0;
}


// ============================================================================
// Definition of the module Romtable.
// ============================================================================

const LUA_REG_TYPE stuart_logger_map[] = {
  { LSTRKEY("_init")    , LFUNCVAL(stuart_Logger__init) },
  { LSTRKEY("debug")    , LFUNCVAL(stuart_Logger_debug) },
  { LSTRKEY("error")    , LFUNCVAL(stuart_Logger_error) },
  { LSTRKEY("info")     , LFUNCVAL(stuart_Logger_info) },
  { LSTRKEY("log")      , LFUNCVAL(stuart_Logger_log) },
  { LSTRKEY("setLevel") , LFUNCVAL(stuart_Logger_setLevel) },
  { LSTRKEY("trace")    , LFUNCVAL(stuart_Logger_trace) },
  { LSTRKEY("warn")     , LFUNCVAL(stuart_Logger_warn) },

  // class framework
  { LSTRKEY("__index")  , LRO_ROVAL(stuart_logger_map) },
  { LSTRKEY("_class")   , LRO_ROVAL(stuart_logger_map) },
  { LSTRKEY("classof")  , LFUNCVAL(stuart_Logger_classof) },
  { LSTRKEY("new")      , LFUNCVAL(stuart_Logger_new) },

  { LNILKEY, LNILVAL }
};


// ============================================================================
// Registration of the module.
// ============================================================================
LUALIB_API int luaopen_stuart_logger( lua_State *L ){
#if LUA_OPTIMIZE_MEMORY > 0
  return 0;
#else
  LREGISTER( L, AUXLIB_STUART_LOGGER, stuart_logger_map );
  return 1;
#endif
}
