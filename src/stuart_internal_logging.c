#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
#include "common.h"
#include "auxmods.h"
#include "lrotable.h"
#include "platform_conf.h"
#define MIN_OPT_LEVEL 2
#include "lrodefs.h"
#include <assert.h>
#include "lua2c.h"

static const char STUART_LOGGING_LOG_GLOBAL[] = "stuart_internal_logging_log";


/* name: M.getOrCreateLog
 * function() */
static int stuart_logging_getOrCreateLog (lua_State * L) {
  enum { lc_nformalargs = 0 };
  lua_settop(L,0);

  /* local log = global_log */
  lua_getfield(L,LUA_ENVIRONINDEX,STUART_LOGGING_LOG_GLOBAL);
  assert(lua_gettop(L) == 1);

  /* if log == nil then */
  enum { lc1 = 1 };
  lua_pushnil(L);
  const int lc2 = lua_equal(L,1,-1);
  lua_pop(L,1);
  lua_pushboolean(L,lc2);
  const int lc3 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc3) {

    /* local Logger = require 'stuart.internal.Logger' */
    lua_getfield(L,LUA_ENVIRONINDEX,"require");
    lua_pushliteral(L,"stuart.internal.Logger");
    lua_call(L,1,1);
    assert(lua_gettop(L) == 2);

    /* log = Logger.new() */
    lua_pushliteral(L,"new");
    lua_gettable(L,2);
    lua_call(L,0,1);
    lua_replace(L,1);
    assert(lua_gettop(L) == 2);

    /* global_log = log */
    lua_pushvalue(L,1);
    lua_setfield(L,LUA_ENVIRONINDEX,STUART_LOGGING_LOG_GLOBAL);
    assert(lua_gettop(L) == 2);
  }
  lua_settop(L,lc1);
  assert(lua_gettop(L) == 1);

  /* return log */
  lua_pushvalue(L,1);
  return 1;
  assert(lua_gettop(L) == 1);
}


/* name: M.logDebug
 * function(msg) */
static int stuart_logging_logDebug (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* local thismodule = require 'stuart.internal.logging' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart.internal.logging");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 2);

  /* local log = thismodule.getOrCreateLog() */
  lua_pushliteral(L,"getOrCreateLog");
  lua_gettable(L,2);
  lua_call(L,0,1);
  assert(lua_gettop(L) == 3);

  /* log:debug(msg) */
  lua_pushvalue(L,3);
  lua_pushliteral(L,"debug");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_pushvalue(L,1);
  lua_call(L,2,0);
  assert(lua_gettop(L) == 3);
  return 0;
}


/* name: M.logError
 * function(msg) */
static int stuart_logging_logError (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* local thismodule = require 'stuart.internal.logging' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart.internal.logging");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 2);

  /* local log = thismodule.getOrCreateLog() */
  lua_pushliteral(L,"getOrCreateLog");
  lua_gettable(L,2);
  lua_call(L,0,1);
  assert(lua_gettop(L) == 3);

  /* log:error(msg) */
  lua_pushvalue(L,3);
  lua_pushliteral(L,"error");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_pushvalue(L,1);
  lua_call(L,2,0);
  assert(lua_gettop(L) == 3);
  return 0;
}


/* name: M.logInfo
 * function(msg) */
static int stuart_logging_logInfo (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* local thismodule = require 'stuart.internal.logging' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart.internal.logging");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 2);

  /* local log = thismodule.getOrCreateLog() */
  lua_pushliteral(L,"getOrCreateLog");
  lua_gettable(L,2);
  lua_call(L,0,1);
  assert(lua_gettop(L) == 3);

  /* log:info(msg) */
  lua_pushvalue(L,3);
  lua_pushliteral(L,"info");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_pushvalue(L,1);
  lua_call(L,2,0);
  assert(lua_gettop(L) == 3);
  return 0;
}


/* name: M.logTrace
 * function(msg) */
static int stuart_logging_logTrace (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* local thismodule = require 'stuart.internal.logging' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart.internal.logging");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 2);

  /* local log = thismodule.getOrCreateLog() */
  lua_pushliteral(L,"getOrCreateLog");
  lua_gettable(L,2);
  lua_call(L,0,1);
  assert(lua_gettop(L) == 3);

  /* log:trace(msg) */
  lua_pushvalue(L,3);
  lua_pushliteral(L,"trace");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_pushvalue(L,1);
  lua_call(L,2,0);
  assert(lua_gettop(L) == 3);
  return 0;
}


/* name: M.logWarning
 * function(msg) */
static int stuart_logging_logWarning (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* local thismodule = require 'stuart.internal.logging' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart.internal.logging");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 2);

  /* local log = thismodule.getOrCreateLog() */
  lua_pushliteral(L,"getOrCreateLog");
  lua_gettable(L,2);
  lua_call(L,0,1);
  assert(lua_gettop(L) == 3);

  /* log:warn(msg) */
  lua_pushvalue(L,3);
  lua_pushliteral(L,"warn");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_pushvalue(L,1);
  lua_call(L,2,0);
  assert(lua_gettop(L) == 3);
  return 0;
}


// ============================================================================
// Definition of the module Romtable.
// ============================================================================

const LUA_REG_TYPE stuart_logging_map[] = {
  { LSTRKEY( "getOrCreateLog" ), LFUNCVAL( stuart_logging_getOrCreateLog ) },
  { LSTRKEY( "logDebug" ), LFUNCVAL( stuart_logging_logDebug ) },
  { LSTRKEY( "logError" ), LFUNCVAL( stuart_logging_logError ) },
  { LSTRKEY( "logInfo" ), LFUNCVAL( stuart_logging_logInfo ) },
  { LSTRKEY( "logTrace" ), LFUNCVAL( stuart_logging_logTrace ) },
  { LSTRKEY( "logWarning" ), LFUNCVAL( stuart_logging_logWarning ) },
  { LNILKEY, LNILVAL }
};


// ============================================================================
// Registration of the module.
// ============================================================================
LUALIB_API int luaopen_stuart_logging( lua_State *L ){
#if LUA_OPTIMIZE_MEMORY > 0
  return 0;
#else
  LREGISTER( L, AUXLIB_STUART_LOGGING, stuart_logging_map );
  return 1;
#endif
}
