#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
#include "common.h"
#include "auxmods.h"
#include "lrotable.h"
#include "platform_conf.h"
#define MIN_OPT_LEVEL 2
#include "lrodefs.h"
#include <assert.h>
#include "lua2c.h"
#include "stuart_class.h"


/* name: SparkConf:_init
 * function() */
static int stuart_SparkConf__init (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* self.settings = {} */
  lua_newtable(L);
  lua_pushliteral(L,"settings");
  lua_insert(L,-2);
  lua_settable(L,1);
  assert(lua_gettop(L) == 1);
  return 0;
}


/* name: SparkConf:appName
 * function() */
static int stuart_SparkConf_appName (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* return self:get('spark.app.name') */
  const int lc1 = lua_gettop(L);
  lua_pushvalue(L,1);
  lua_pushliteral(L,"get");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_pushliteral(L,"spark.app.name");
  lua_call(L,2,LUA_MULTRET);
  return (lua_gettop(L) - lc1);
  assert(lua_gettop(L) == 1);
}


/* name: SparkConf:clone
 * function() */
static int stuart_SparkConf_clone (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* local SparkConf = require 'stuart.SparkConf' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart.SparkConf");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 2);

  /* local cloned = SparkConf.new() */
  lua_pushliteral(L,"new");
  lua_gettable(L,2);
  lua_call(L,0,1);
  assert(lua_gettop(L) == 3);

  /* local moses = require 'moses' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 4);

  /* cloned.settings = moses.clone(self.settings) */
  lua_pushliteral(L,"clone");
  lua_gettable(L,4);
  lua_pushliteral(L,"settings");
  lua_gettable(L,1);
  lua_call(L,1,1);
  lua_pushliteral(L,"settings");
  lua_insert(L,-2);
  lua_settable(L,3);
  assert(lua_gettop(L) == 4);

  /* return cloned */
  lua_pushvalue(L,3);
  return 1;
  assert(lua_gettop(L) == 4);
}


/* name: SparkConf:contains
 * function(key) */
static int stuart_SparkConf_contains (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* return self.settings[key] ~= nil */
  lua_pushliteral(L,"settings");
  lua_gettable(L,1);
  lua_pushvalue(L,2);
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_pushnil(L);
  const int lc2 = lua_equal(L,-2,-1);
  lua_pop(L,2);
  lua_pushboolean(L,lc2);
  lua_pushboolean(L,!(lua_toboolean(L,-1)));
  lua_remove(L,-2);
  return 1;
  assert(lua_gettop(L) == 2);
}


/* name: M.classof
 * function(obj) */
static int stuart_SparkConf_classof (lua_State * L) {
  return stuart_class_shared_classof(L, "stuart.SparkConf");
}


/* name: SparkConf:get
 * function(key, defaultValue) */
static int stuart_SparkConf_get (lua_State * L) {
  enum { lc_nformalargs = 3 };
  lua_settop(L,3);

  /* return self.settings[key] or defaultValue */
  lua_pushliteral(L,"settings");
  lua_gettable(L,1);
  lua_pushvalue(L,2);
  lua_gettable(L,-2);
  lua_remove(L,-2);
  if (!(lua_toboolean(L,-1))) {
    lua_pop(L,1);
    lua_pushvalue(L,3);
  }
  return 1;
  assert(lua_gettop(L) == 3);
}


/* name: SparkConf:getAll
 * function() */
static int stuart_SparkConf_getAll (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* local t = {} */
  lua_newtable(L);
  assert(lua_gettop(L) == 2);

  /* for k,v in pairs(self.settings) do
   * internal: local f, s, var = explist */
  enum { lc4 = 2 };
  lua_getfield(L,LUA_ENVIRONINDEX,"pairs");
  lua_pushliteral(L,"settings");
  lua_gettable(L,1);
  lua_call(L,1,3);
  while (1) {

    /* internal: local var_1, ..., var_n = f(s, var)
     *           if var_1 == nil then break end
     *           var = var_1 */
    lua_pushvalue(L,-3);
    lua_pushvalue(L,-3);
    lua_pushvalue(L,-3);
    lua_call(L,2,2);
    if (lua_isnil(L,-2)) {
      break;
    }
    lua_pushvalue(L,-2);
    lua_replace(L,-4);

    /* internal: local k with idx 6
     * internal: local v with idx 7 */


    /* t[#t+1] = {k, v} */
    lua_createtable(L,2,0);
    lua_pushvalue(L,6);
    lua_rawseti(L,-2,1);
    lua_pushvalue(L,7);
    lua_rawseti(L,-2,2);
    const double lc5 = lua_objlen(L,2);
    lua_pushnumber(L,lc5);
    lua_pushnumber(L,1);
    lc_add(L,-2,-1);
    lua_remove(L,-2);
    lua_remove(L,-2);
    lua_insert(L,-2);
    lua_settable(L,2);
    assert(lua_gettop(L) == 7);

    /* internal: stack cleanup on scope exit */
    lua_pop(L,2);
  }
  lua_settop(L,lc4);
  assert(lua_gettop(L) == 2);

  /* return t */
  lua_pushvalue(L,2);
  return 1;
  assert(lua_gettop(L) == 2);
}


/* name: SparkConf:getAppId
 * function() */
static int stuart_SparkConf_getAppId (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* return self:get('spark.app.id') */
  const int lc3 = lua_gettop(L);
  lua_pushvalue(L,1);
  lua_pushliteral(L,"get");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_pushliteral(L,"spark.app.id");
  lua_call(L,2,LUA_MULTRET);
  return (lua_gettop(L) - lc3);
  assert(lua_gettop(L) == 1);
}


/* name: SparkConf:getBoolean
 * function(key, defaultValue) */
static int stuart_SparkConf_getBoolean (lua_State * L) {
  enum { lc_nformalargs = 3 };
  lua_settop(L,3);

  /* local s = self.settings[key] */
  lua_pushliteral(L,"settings");
  lua_gettable(L,1);
  lua_pushvalue(L,2);
  lua_gettable(L,-2);
  lua_remove(L,-2);
  assert(lua_gettop(L) == 4);

  /* if s == 'true' then */
  enum { lc6 = 4 };
  lua_pushliteral(L,"true");
  const int lc7 = lua_equal(L,4,-1);
  lua_pop(L,1);
  lua_pushboolean(L,lc7);
  const int lc8 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc8) {

    /* return true */
    lua_pushboolean(L,1);
    return 1;
    assert(lua_gettop(L) == 4);
  }
  lua_settop(L,lc6);
  assert(lua_gettop(L) == 4);

  /* if s == 'false' then */
  enum { lc9 = 4 };
  lua_pushliteral(L,"false");
  const int lc10 = lua_equal(L,4,-1);
  lua_pop(L,1);
  lua_pushboolean(L,lc10);
  const int lc11 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc11) {

    /* return false */
    lua_pushboolean(L,0);
    return 1;
    assert(lua_gettop(L) == 4);
  }
  lua_settop(L,lc9);
  assert(lua_gettop(L) == 4);

  /* return defaultValue */
  lua_pushvalue(L,3);
  return 1;
  assert(lua_gettop(L) == 4);
}


/* name: SparkConf:master
 * function() */
static int stuart_SparkConf_master (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* return self:get('spark.master') */
  const int lc12 = lua_gettop(L);
  lua_pushvalue(L,1);
  lua_pushliteral(L,"get");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_pushliteral(L,"spark.master");
  lua_call(L,2,LUA_MULTRET);
  return (lua_gettop(L) - lc12);
  assert(lua_gettop(L) == 1);
}


/* name: M.new
 * function(...) */
static int stuart_SparkConf_new (lua_State * L) {
  return stuart_class_shared_new(L, "stuart.SparkConf");
}


/* name: SparkConf:remove
 * function(key) */
static int stuart_SparkConf_remove (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* self.settings[key] = nil */
  lua_pushnil(L);
  lua_pushliteral(L,"settings");
  lua_gettable(L,1);
  lua_insert(L,-2);
  lua_pushvalue(L,2);
  lua_insert(L,-2);
  lua_settable(L,-3);
  lua_pop(L,1);
  assert(lua_gettop(L) == 2);

  /* return self */
  lua_pushvalue(L,1);
  return 1;
  assert(lua_gettop(L) == 2);
}


/* name: SparkConf:set
 * function(key, value) */
static int stuart_SparkConf_set (lua_State * L) {
  enum { lc_nformalargs = 3 };
  lua_settop(L,3);

  /* assert(key ~= nil) */
  lua_getfield(L,LUA_ENVIRONINDEX,"assert");
  lua_pushnil(L);
  const int lc13 = lua_equal(L,2,-1);
  lua_pop(L,1);
  lua_pushboolean(L,lc13);
  lua_pushboolean(L,!(lua_toboolean(L,-1)));
  lua_remove(L,-2);
  lua_call(L,1,0);
  assert(lua_gettop(L) == 3);

  /* assert(value ~= nil) */
  lua_getfield(L,LUA_ENVIRONINDEX,"assert");
  lua_pushnil(L);
  const int lc14 = lua_equal(L,3,-1);
  lua_pop(L,1);
  lua_pushboolean(L,lc14);
  lua_pushboolean(L,!(lua_toboolean(L,-1)));
  lua_remove(L,-2);
  lua_call(L,1,0);
  assert(lua_gettop(L) == 3);

  /* self.settings[key] = value */
  lua_pushvalue(L,3);
  lua_pushliteral(L,"settings");
  lua_gettable(L,1);
  lua_insert(L,-2);
  lua_pushvalue(L,2);
  lua_insert(L,-2);
  lua_settable(L,-3);
  lua_pop(L,1);
  assert(lua_gettop(L) == 3);

  /* return self */
  lua_pushvalue(L,1);
  return 1;
  assert(lua_gettop(L) == 3);
}


/* name: SparkConf:setAll
 * function(settings) */
static int stuart_SparkConf_setAll (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* for _,setting in ipairs(settings) do
   * internal: local f, s, var = explist */
  enum { lc15 = 2 };
  lua_getfield(L,LUA_ENVIRONINDEX,"ipairs");
  lua_pushvalue(L,2);
  lua_call(L,1,3);
  while (1) {

    /* internal: local var_1, ..., var_n = f(s, var)
     *           if var_1 == nil then break end
     *           var = var_1 */
    lua_pushvalue(L,-3);
    lua_pushvalue(L,-3);
    lua_pushvalue(L,-3);
    lua_call(L,2,2);
    if (lua_isnil(L,-2)) {
      break;
    }
    lua_pushvalue(L,-2);
    lua_replace(L,-4);

    /* internal: local _ with idx 6
     * internal: local setting with idx 7 */


    /* local k,v = setting[1], setting[2] */
    lua_pushnumber(L,1);
    lua_gettable(L,7);
    lua_pushnumber(L,2);
    lua_gettable(L,7);
    assert(lua_gettop(L) == 9);

    /* self:set(k,v) */
    lua_pushvalue(L,1);
    lua_pushliteral(L,"set");
    lua_gettable(L,-2);
    lua_insert(L,-2);
    lua_pushvalue(L,8);
    lua_pushvalue(L,9);
    lua_call(L,3,0);
    assert(lua_gettop(L) == 9);

    /* internal: stack cleanup on scope exit */
    lua_pop(L,4);
  }
  lua_settop(L,lc15);
  assert(lua_gettop(L) == 2);

  /* return self */
  lua_pushvalue(L,1);
  return 1;
  assert(lua_gettop(L) == 2);
}


/* name: SparkConf:setAppName
 * function(name) */
static int stuart_SparkConf_setAppName (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* self:set('spark.app.name', name) */
  lua_pushvalue(L,1);
  lua_pushliteral(L,"set");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_pushliteral(L,"spark.app.name");
  lua_pushvalue(L,2);
  lua_call(L,3,0);
  assert(lua_gettop(L) == 2);

  /* return self */
  lua_pushvalue(L,1);
  return 1;
  assert(lua_gettop(L) == 2);
}


/* name: SparkConf:setIfMissing
 * function(key, value) */
static int stuart_SparkConf_setIfMissing (lua_State * L) {
  enum { lc_nformalargs = 3 };
  lua_settop(L,3);

  /* if self.settings[key] == nil then */
  enum { lc16 = 3 };
  lua_pushliteral(L,"settings");
  lua_gettable(L,1);
  lua_pushvalue(L,2);
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_pushnil(L);
  const int lc17 = lua_equal(L,-2,-1);
  lua_pop(L,2);
  lua_pushboolean(L,lc17);
  const int lc18 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc18) {

    /* self.settings[key] = value */
    lua_pushvalue(L,3);
    lua_pushliteral(L,"settings");
    lua_gettable(L,1);
    lua_insert(L,-2);
    lua_pushvalue(L,2);
    lua_insert(L,-2);
    lua_settable(L,-3);
    lua_pop(L,1);
    assert(lua_gettop(L) == 3);
  }
  lua_settop(L,lc16);
  assert(lua_gettop(L) == 3);

  /* return self */
  lua_pushvalue(L,1);
  return 1;
  assert(lua_gettop(L) == 3);
}


/* name: SparkConf:setMaster
 * function(master) */
static int stuart_SparkConf_setMaster (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* self:set('spark.master', master) */
  lua_pushvalue(L,1);
  lua_pushliteral(L,"set");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_pushliteral(L,"spark.master");
  lua_pushvalue(L,2);
  lua_call(L,3,0);
  assert(lua_gettop(L) == 2);

  /* return self */
  lua_pushvalue(L,1);
  return 1;
  assert(lua_gettop(L) == 2);
}


/* name: SparkConf:setSparkHome
 * function(home) */
static int stuart_SparkConf_setSparkHome (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* self:set('spark.home', home) */
  lua_pushvalue(L,1);
  lua_pushliteral(L,"set");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_pushliteral(L,"spark.home");
  lua_pushvalue(L,2);
  lua_call(L,3,0);
  assert(lua_gettop(L) == 2);

  /* return self */
  lua_pushvalue(L,1);
  return 1;
  assert(lua_gettop(L) == 2);
}


/* name: SparkConf:toDebugString
 * function() */
static int stuart_SparkConf_toDebugString (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* local s = '' */
  lua_pushliteral(L,"");
  assert(lua_gettop(L) == 2);

  /* for k,v in pairs(self.settings) do
   * internal: local f, s, var = explist */
  enum { lc19 = 2 };
  lua_getfield(L,LUA_ENVIRONINDEX,"pairs");
  lua_pushliteral(L,"settings");
  lua_gettable(L,1);
  lua_call(L,1,3);
  while (1) {

    /* internal: local var_1, ..., var_n = f(s, var)
     *           if var_1 == nil then break end
     *           var = var_1 */
    lua_pushvalue(L,-3);
    lua_pushvalue(L,-3);
    lua_pushvalue(L,-3);
    lua_call(L,2,2);
    if (lua_isnil(L,-2)) {
      break;
    }
    lua_pushvalue(L,-2);
    lua_replace(L,-4);

    /* internal: local k with idx 6
     * internal: local v with idx 7 */


    /* s = s .. k .. '=' .. v .. '\n' */
    lua_pushvalue(L,2);
    lua_pushvalue(L,6);
    lua_pushliteral(L,"=");
    lua_pushvalue(L,7);
    lua_pushliteral(L,"\n");
    lua_concat(L,2);
    lua_concat(L,2);
    lua_concat(L,2);
    lua_concat(L,2);
    lua_replace(L,2);
    assert(lua_gettop(L) == 7);

    /* internal: stack cleanup on scope exit */
    lua_pop(L,2);
  }
  lua_settop(L,lc19);
  assert(lua_gettop(L) == 2);

  /* return s */
  lua_pushvalue(L,2);
  return 1;
  assert(lua_gettop(L) == 2);
}


// ============================================================================
// Definition of the module Romtable.
// ============================================================================

const LUA_REG_TYPE stuart_sparkconf_map[] = {
  { LSTRKEY("_init")           , LFUNCVAL(stuart_SparkConf__init) },
  { LSTRKEY("appName")         , LFUNCVAL(stuart_SparkConf_appName) },
  { LSTRKEY("clone")           , LFUNCVAL(stuart_SparkConf_clone) },
  { LSTRKEY("contains")        , LFUNCVAL(stuart_SparkConf_contains) },
  { LSTRKEY("get")             , LFUNCVAL(stuart_SparkConf_get) },
  { LSTRKEY("getAll")          , LFUNCVAL(stuart_SparkConf_getAll) },
  { LSTRKEY("getAppId")        , LFUNCVAL(stuart_SparkConf_getAppId) },
  { LSTRKEY("getBoolean")      , LFUNCVAL(stuart_SparkConf_getBoolean) },
  { LSTRKEY("master" )         , LFUNCVAL(stuart_SparkConf_master) },
  { LSTRKEY("remove" )         , LFUNCVAL(stuart_SparkConf_remove) },
  { LSTRKEY("set" )            , LFUNCVAL(stuart_SparkConf_set) },
  { LSTRKEY("setAll" )         , LFUNCVAL(stuart_SparkConf_setAll) },
  { LSTRKEY("setAppName" )     , LFUNCVAL(stuart_SparkConf_setAppName) },
  { LSTRKEY("setIfMissing" )   , LFUNCVAL(stuart_SparkConf_setIfMissing) },
  { LSTRKEY("setMaster" )      , LFUNCVAL(stuart_SparkConf_setMaster) },
  { LSTRKEY("setSparkHome" )   , LFUNCVAL(stuart_SparkConf_setSparkHome) },
  { LSTRKEY("toDebugString" )  , LFUNCVAL(stuart_SparkConf_toDebugString) },

  // class framework
  { LSTRKEY("__index")         , LRO_ROVAL(stuart_sparkconf_map) },
  { LSTRKEY("_class")          , LRO_ROVAL(stuart_sparkconf_map) },
  { LSTRKEY("classof")         , LFUNCVAL(stuart_SparkConf_classof) },
  { LSTRKEY("new" )            , LFUNCVAL(stuart_SparkConf_new) },

  { LNILKEY, LNILVAL }
};


// ============================================================================
// Registration of the module.
// ============================================================================
LUALIB_API int luaopen_stuart_sparkconf( lua_State *L ){
#if LUA_OPTIMIZE_MEMORY > 0
  return 0;
#else
  LREGISTER( L, AUXLIB_STUART_SPARKCONF, stuart_sparkconf_map );
  return 1;
#endif
}
