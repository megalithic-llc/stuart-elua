#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
#include "common.h"
#include "auxmods.h"
#include "lrotable.h"
#include "platform_conf.h"
#define MIN_OPT_LEVEL 2
#include "lrodefs.h"
#include <assert.h>
#include "lua2c.h"
#include "stuart_class.h"


/* name: DStream:_init
 * function(ssc) */
int stuart_streaming_DStream__init (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* self.ssc = ssc */
  lua_pushvalue(L,2);
  lua_pushliteral(L,"ssc");
  lua_insert(L,-2);
  lua_settable(L,1);
  assert(lua_gettop(L) == 2);

  /* self.inputs = {} */
  lua_newtable(L);
  lua_pushliteral(L,"inputs");
  lua_insert(L,-2);
  lua_settable(L,1);
  assert(lua_gettop(L) == 2);

  /* self.outputs = {} */
  lua_newtable(L);
  lua_pushliteral(L,"outputs");
  lua_insert(L,-2);
  lua_settable(L,1);
  assert(lua_gettop(L) == 2);
  return 0;
}


/* name: DStream:_notify
 * function(validTime, rdd) */
int stuart_streaming_DStream__notify (lua_State * L) {
  enum { lc_nformalargs = 3 };
  lua_settop(L,3);

  /* for _, dstream in ipairs(self.inputs) do
   * internal: local f, s, var = explist */
  enum { lc1 = 3 };
  lua_getfield(L,LUA_ENVIRONINDEX,"ipairs");
  lua_pushliteral(L,"inputs");
  lua_gettable(L,1);
  lua_call(L,1,3);
  while (1) {

    /* internal: local var_1, ..., var_n = f(s, var)
     *           if var_1 == nil then break end
     *           var = var_1 */
    lua_pushvalue(L,-3);
    lua_pushvalue(L,-3);
    lua_pushvalue(L,-3);
    lua_call(L,2,2);
    if (lua_isnil(L,-2)) {
      break;
    }
    lua_pushvalue(L,-2);
    lua_replace(L,-4);

    /* internal: local _ with idx 7
     * internal: local dstream with idx 8 */


    /* rdd = dstream:_notify(validTime, rdd) */
    lua_pushvalue(L,8);
    lua_pushliteral(L,"_notify");
    lua_gettable(L,-2);
    lua_insert(L,-2);
    lua_pushvalue(L,2);
    lua_pushvalue(L,3);
    lua_call(L,3,1);
    lua_replace(L,3);
    assert(lua_gettop(L) == 8);

    /* internal: stack cleanup on scope exit */
    lua_pop(L,2);
  }
  lua_settop(L,lc1);
  assert(lua_gettop(L) == 3);

  /* for _, dstream in ipairs(self.outputs) do
   * internal: local f, s, var = explist */
  enum { lc2 = 3 };
  lua_getfield(L,LUA_ENVIRONINDEX,"ipairs");
  lua_pushliteral(L,"outputs");
  lua_gettable(L,1);
  lua_call(L,1,3);
  while (1) {

    /* internal: local var_1, ..., var_n = f(s, var)
     *           if var_1 == nil then break end
     *           var = var_1 */
    lua_pushvalue(L,-3);
    lua_pushvalue(L,-3);
    lua_pushvalue(L,-3);
    lua_call(L,2,2);
    if (lua_isnil(L,-2)) {
      break;
    }
    lua_pushvalue(L,-2);
    lua_replace(L,-4);

    /* internal: local _ with idx 7
     * internal: local dstream with idx 8 */


    /* dstream:_notify(validTime, rdd) */
    lua_pushvalue(L,8);
    lua_pushliteral(L,"_notify");
    lua_gettable(L,-2);
    lua_insert(L,-2);
    lua_pushvalue(L,2);
    lua_pushvalue(L,3);
    lua_call(L,3,0);
    assert(lua_gettop(L) == 8);

    /* internal: stack cleanup on scope exit */
    lua_pop(L,2);
  }
  lua_settop(L,lc2);
  assert(lua_gettop(L) == 3);
  return 0;
}


/* name: M.classof
 * function(obj) */
int stuart_streaming_DStream_classof (lua_State * L) {
  return stuart_class_shared_classof(L, "stuart.streaming.DStream");
}


/* name: transformFunc
 * function(rdd) */
int stuart_streaming_DStream_count_transformFunc (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* return self.ssc.sc:makeRDD({rdd:count()}) */
  const int lc4 = lua_gettop(L);
  lc_getupvalue(L,lua_upvalueindex(1),0,1);
  lua_pushliteral(L,"ssc");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_pushliteral(L,"sc");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_pushliteral(L,"makeRDD");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_createtable(L,1,0);
  const int lc5 = lua_gettop(L);
  lua_pushvalue(L,1);
  lua_pushliteral(L,"count");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,LUA_MULTRET);
  while ((lua_gettop(L) > lc5)) {
    lua_rawseti(L,lc5,(0 + (lua_gettop(L) - lc5)));
  }
  lua_call(L,2,LUA_MULTRET);
  return (lua_gettop(L) - lc4);
  assert(lua_gettop(L) == 1);
}


/* name: DStream:count
 * function() */
int stuart_streaming_DStream_count (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);
  lc_newclosuretable(L,lua_upvalueindex(1));
  enum { lc3 = 2 };
  assert((lua_gettop(L) == lc3));
  lua_pushvalue(L,1);
  lua_rawseti(L,-2,1);

  /* local transformFunc = function(rdd)
   *     return self.ssc.sc:makeRDD({rdd:count()})
   *   end */
  lua_pushvalue(L,lc3);
  lua_pushcclosure(L,stuart_streaming_DStream_count_transformFunc,1);
  assert(lua_gettop(L) == 3);

  /* return self:transform(transformFunc) */
  const int lc6 = lua_gettop(L);
  lc_getupvalue(L,lc3,0,1);
  lua_pushliteral(L,"transform");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_pushvalue(L,3);
  lua_call(L,2,LUA_MULTRET);
  return (lua_gettop(L) - lc6);
  assert(lua_gettop(L) == 3);
}


/* name: DStream:countByWindow
 * function(windowDuration) */
int stuart_streaming_DStream_countByWindow (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* return self:window(windowDuration):count() */
  const int lc7 = lua_gettop(L);
  lua_pushvalue(L,1);
  lua_pushliteral(L,"window");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_pushvalue(L,2);
  lua_call(L,2,1);
  lua_pushliteral(L,"count");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,LUA_MULTRET);
  return (lua_gettop(L) - lc7);
  assert(lua_gettop(L) == 2);
}


/* name: DStream:foreachRDD
 * function(foreachFunc) */
int stuart_streaming_DStream_foreachRDD (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* local TransformedDStream = require 'stuart.streaming.TransformedDStream' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart.streaming.TransformedDStream");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 3);

  /* local dstream = TransformedDStream.new(self.ssc, foreachFunc) */
  lua_pushliteral(L,"new");
  lua_gettable(L,3);
  lua_pushliteral(L,"ssc");
  lua_gettable(L,1);
  lua_pushvalue(L,2);
  lua_call(L,2,1);
  assert(lua_gettop(L) == 4);

  /* self.outputs[#self.outputs+1] = dstream */
  lua_pushvalue(L,4);
  lua_pushliteral(L,"outputs");
  lua_gettable(L,1);
  lua_insert(L,-2);
  lua_pushliteral(L,"outputs");
  lua_gettable(L,1);
  const double lc8 = lua_objlen(L,-1);
  lua_pop(L,1);
  lua_pushnumber(L,lc8);
  lua_pushnumber(L,1);
  lc_add(L,-2,-1);
  lua_remove(L,-2);
  lua_remove(L,-2);
  lua_insert(L,-2);
  lua_settable(L,-3);
  lua_pop(L,1);
  assert(lua_gettop(L) == 4);
  return 0;
}


/* name: transformFunc
 * function(rdd) */
int stuart_streaming_DStream_groupByKey_transformFunc (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* return rdd:groupByKey() */
  const int lc9 = lua_gettop(L);
  lua_pushvalue(L,1);
  lua_pushliteral(L,"groupByKey");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,LUA_MULTRET);
  return (lua_gettop(L) - lc9);
  assert(lua_gettop(L) == 1);
}


/* name: DStream:groupByKey
 * function() */
int stuart_streaming_DStream_groupByKey (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* local transformFunc = function(rdd) return rdd:groupByKey() end */
  lua_pushcfunction(L,stuart_streaming_DStream_groupByKey_transformFunc);
  assert(lua_gettop(L) == 2);

  /* return self:transform(transformFunc) */
  const int lc10 = lua_gettop(L);
  lua_pushvalue(L,1);
  lua_pushliteral(L,"transform");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_pushvalue(L,2);
  lua_call(L,2,LUA_MULTRET);
  return (lua_gettop(L) - lc10);
  assert(lua_gettop(L) == 2);
}


/* name: transformFunc
 * function(rdd) */
int stuart_streaming_DStream_map_transformFunc (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* return rdd:map(f) */
  const int lc12 = lua_gettop(L);
  lua_pushvalue(L,1);
  lua_pushliteral(L,"map");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lc_getupvalue(L,lua_upvalueindex(1),0,2);
  lua_call(L,2,LUA_MULTRET);
  return (lua_gettop(L) - lc12);
  assert(lua_gettop(L) == 1);
}


/* name: DStream:map
 * function(f) */
int stuart_streaming_DStream_map (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);
  lc_newclosuretable(L,lua_upvalueindex(1));
  enum { lc11 = 3 };
  assert((lua_gettop(L) == lc11));
  lua_pushvalue(L,2);
  lua_rawseti(L,-2,2);

  /* local transformFunc = function(rdd)
   *     return rdd:map(f)
   *   end */
  lua_pushvalue(L,lc11);
  lua_pushcclosure(L,stuart_streaming_DStream_map_transformFunc,1);
  assert(lua_gettop(L) == 4);

  /* return self:transform(transformFunc) */
  const int lc13 = lua_gettop(L);
  lua_pushvalue(L,1);
  lua_pushliteral(L,"transform");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_pushvalue(L,4);
  lua_call(L,2,LUA_MULTRET);
  return (lua_gettop(L) - lc13);
  assert(lua_gettop(L) == 4);
}


/* name: transformFunc
 * function(rdd) */
int stuart_streaming_DStream_mapValues_transformFunc (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* return rdd:mapValues(f) */
  const int lc15 = lua_gettop(L);
  lua_pushvalue(L,1);
  lua_pushliteral(L,"mapValues");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lc_getupvalue(L,lua_upvalueindex(1),0,3);
  lua_call(L,2,LUA_MULTRET);
  return (lua_gettop(L) - lc15);
  assert(lua_gettop(L) == 1);
}


/* name: DStream:mapValues
 * function(f) */
int stuart_streaming_DStream_mapValues (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);
  lc_newclosuretable(L,lua_upvalueindex(1));
  enum { lc14 = 3 };
  assert((lua_gettop(L) == lc14));
  lua_pushvalue(L,2);
  lua_rawseti(L,-2,3);

  /* local transformFunc = function(rdd)
   *     return rdd:mapValues(f)
   *   end */
  lua_pushvalue(L,lc14);
  lua_pushcclosure(L,stuart_streaming_DStream_mapValues_transformFunc,1);
  assert(lua_gettop(L) == 4);

  /* return self:transform(transformFunc) */
  const int lc16 = lua_gettop(L);
  lua_pushvalue(L,1);
  lua_pushliteral(L,"transform");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_pushvalue(L,4);
  lua_call(L,2,LUA_MULTRET);
  return (lua_gettop(L) - lc16);
  assert(lua_gettop(L) == 4);
}


/* name: M.new
 * function(...) */
int stuart_streaming_DStream_new (lua_State * L) {
  return stuart_class_shared_new(L, "stuart.streaming.DStream");
}


/* name: DStream:poll
 * function() */
int stuart_streaming_DStream_poll (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);
  return 0;
}


/* function(x) */
int stuart_streaming_DStream_reduce_transformFunc_1 (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* return {0, x} */
  lua_createtable(L,2,0);
  lua_pushnumber(L,0);
  lua_rawseti(L,-2,1);
  lua_pushvalue(L,1);
  lua_rawseti(L,-2,2);
  return 1;
  assert(lua_gettop(L) == 1);
}


/* function(e) */
int stuart_streaming_DStream_reduce_transformFunc_2 (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* return e[2] */
  lua_pushnumber(L,2);
  lua_gettable(L,1);
  return 1;
  assert(lua_gettop(L) == 1);
}


/* name: transformFunc
 * function(rdd) */
int stuart_streaming_DStream_reduce_transformFunc (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* return rdd:map(function(x) return {0, x} end)
   *       :reduceByKey(f)
   *       :map(function(e) return e[2] end) */
  const int lc18 = lua_gettop(L);
  lua_pushvalue(L,1);
  lua_pushliteral(L,"map");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_pushcfunction(L,stuart_streaming_DStream_reduce_transformFunc_1);
  lua_call(L,2,1);
  lua_pushliteral(L,"reduceByKey");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lc_getupvalue(L,lua_upvalueindex(1),0,4);
  lua_call(L,2,1);
  lua_pushliteral(L,"map");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_pushcfunction(L,stuart_streaming_DStream_reduce_transformFunc_2);
  lua_call(L,2,LUA_MULTRET);
  return (lua_gettop(L) - lc18);
  assert(lua_gettop(L) == 1);
}


/* name: DStream:reduce
 * function(f) */
int stuart_streaming_DStream_reduce (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);
  lc_newclosuretable(L,lua_upvalueindex(1));
  enum { lc17 = 3 };
  assert((lua_gettop(L) == lc17));
  lua_pushvalue(L,2);
  lua_rawseti(L,-2,4);

  /* local transformFunc = function(rdd)
   *     return rdd:map(function(x) return {0, x} end)
   *       :reduceByKey(f)
   *       :map(function(e) return e[2] end)
   *   end */
  lua_pushvalue(L,lc17);
  lua_pushcclosure(L,stuart_streaming_DStream_reduce_transformFunc,1);
  assert(lua_gettop(L) == 4);

  /* return self:transform(transformFunc) */
  const int lc21 = lua_gettop(L);
  lua_pushvalue(L,1);
  lua_pushliteral(L,"transform");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_pushvalue(L,4);
  lua_call(L,2,LUA_MULTRET);
  return (lua_gettop(L) - lc21);
  assert(lua_gettop(L) == 4);
}


/* name: DStream:start
 * function() */
int stuart_streaming_DStream_start (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);
  return 0;
}


/* name: DStream:stop
 * function() */
int stuart_streaming_DStream_stop (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);
  return 0;
}


/* name: DStream:transform
 * function(transformFunc) */
int stuart_streaming_DStream_transform (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* local TransformedDStream = require 'stuart.streaming.TransformedDStream' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart.streaming.TransformedDStream");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 3);

  /* local dstream = TransformedDStream.new(self.ssc, transformFunc) */
  lua_pushliteral(L,"new");
  lua_gettable(L,3);
  lua_pushliteral(L,"ssc");
  lua_gettable(L,1);
  lua_pushvalue(L,2);
  lua_call(L,2,1);
  assert(lua_gettop(L) == 4);

  /* self.inputs[#self.inputs+1] = dstream */
  lua_pushvalue(L,4);
  lua_pushliteral(L,"inputs");
  lua_gettable(L,1);
  lua_insert(L,-2);
  lua_pushliteral(L,"inputs");
  lua_gettable(L,1);
  const double lc22 = lua_objlen(L,-1);
  lua_pop(L,1);
  lua_pushnumber(L,lc22);
  lua_pushnumber(L,1);
  lc_add(L,-2,-1);
  lua_remove(L,-2);
  lua_remove(L,-2);
  lua_insert(L,-2);
  lua_settable(L,-3);
  lua_pop(L,1);
  assert(lua_gettop(L) == 4);

  /* return dstream */
  lua_pushvalue(L,4);
  return 1;
  assert(lua_gettop(L) == 4);
}


/* name: DStream:window
 * function(windowDuration) */
int stuart_streaming_DStream_window (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* local WindowedDStream = require 'stuart.streaming.WindowedDStream' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart.streaming.WindowedDStream");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 3);

  /* local dstream = WindowedDStream.new(self.ssc, windowDuration) */
  lua_pushliteral(L,"new");
  lua_gettable(L,3);
  lua_pushliteral(L,"ssc");
  lua_gettable(L,1);
  lua_pushvalue(L,2);
  lua_call(L,2,1);
  assert(lua_gettop(L) == 4);

  /* self.inputs[#self.inputs+1] = dstream */
  lua_pushvalue(L,4);
  lua_pushliteral(L,"inputs");
  lua_gettable(L,1);
  lua_insert(L,-2);
  lua_pushliteral(L,"inputs");
  lua_gettable(L,1);
  const double lc23 = lua_objlen(L,-1);
  lua_pop(L,1);
  lua_pushnumber(L,lc23);
  lua_pushnumber(L,1);
  lc_add(L,-2,-1);
  lua_remove(L,-2);
  lua_remove(L,-2);
  lua_insert(L,-2);
  lua_settable(L,-3);
  lua_pop(L,1);
  assert(lua_gettop(L) == 4);

  /* return dstream */
  lua_pushvalue(L,4);
  return 1;
  assert(lua_gettop(L) == 4);
}


// ============================================================================
// Definition of the module Romtable.
// ============================================================================

const LUA_REG_TYPE stuart_dstream_map[] = {
  { LSTRKEY("_init")           , LFUNCVAL (stuart_streaming_DStream__init) },
  { LSTRKEY("_notify")         , LFUNCVAL (stuart_streaming_DStream__notify) },
  { LSTRKEY("count")           , LFUNCVAL (stuart_streaming_DStream_count) },
  { LSTRKEY("countByWindow")   , LFUNCVAL (stuart_streaming_DStream_countByWindow) },
  { LSTRKEY("foreachRDD")      , LFUNCVAL (stuart_streaming_DStream_foreachRDD) },
  { LSTRKEY("groupByKey")      , LFUNCVAL (stuart_streaming_DStream_groupByKey) },
  { LSTRKEY("map")             , LFUNCVAL (stuart_streaming_DStream_map) },
  { LSTRKEY("mapValues")       , LFUNCVAL (stuart_streaming_DStream_mapValues) },
  { LSTRKEY("poll")            , LFUNCVAL (stuart_streaming_DStream_poll) },
  { LSTRKEY("reduce")          , LFUNCVAL (stuart_streaming_DStream_reduce) },
  { LSTRKEY("start")           , LFUNCVAL (stuart_streaming_DStream_start) },
  { LSTRKEY("stop")            , LFUNCVAL (stuart_streaming_DStream_stop) },
  { LSTRKEY("transform")       , LFUNCVAL (stuart_streaming_DStream_transform) },
  { LSTRKEY("window")          , LFUNCVAL (stuart_streaming_DStream_window) },

  // class framework
  { LSTRKEY("__index")         , LRO_ROVAL(stuart_dstream_map) },
  { LSTRKEY("_class")          , LRO_ROVAL(stuart_dstream_map) },
  { LSTRKEY("classof")         , LFUNCVAL (stuart_streaming_DStream_classof) },
  { LSTRKEY("new")             , LFUNCVAL (stuart_streaming_DStream_new) },

  { LNILKEY, LNILVAL }
};


// ============================================================================
// Registration of the module.
// ============================================================================
LUALIB_API int luaopen_stuart_dstream( lua_State *L ){
#if LUA_OPTIMIZE_MEMORY > 0
  return 0;
#else
  LREGISTER( L, AUXLIB_STUART_DSTREAM, stuart_dstream_map );
  return 1;
#endif
}
