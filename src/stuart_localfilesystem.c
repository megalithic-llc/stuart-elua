#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
#include "common.h"
#include "auxmods.h"
#include "lrotable.h"
#include "platform_conf.h"
#define MIN_OPT_LEVEL 2
#include "lrodefs.h"
#include <assert.h>
#include "lua2c.h"
#include "stuart_class.h"
#include "stuart_filesystem.h"

#define MODULE      "stuart.LocalFileSystem"
#define SUPERMODULE "stuart.FileSystem"


/* name: LocalFileSystem:_init
 * function(uri) */
int stuart_LocalFileSystem__init (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* FileSystem._init(self, uri) */
  lc_getupvalue(L,lua_upvalueindex(1),0,1);
  lua_pushliteral(L,"_init");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_pushvalue(L,1);
  lua_pushvalue(L,2);
  lua_call(L,2,0);
  assert(lua_gettop(L) == 2);
  return 0;
}


/* name: M.classof
 * function(obj) */
static int stuart_LocalFileSystem_classof (lua_State * L) {
  return stuart_class_shared_classof(L, MODULE);
}


/* name: LocalFileSystem:isDirectory
 * function(path) */
int stuart_LocalFileSystem_isDirectory (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* local f = io.open(self.uri .. '/' .. (path or ''), 'r') */
  lua_getfield(L,LUA_ENVIRONINDEX,"io");
  lua_pushliteral(L,"open");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_pushliteral(L,"uri");
  lua_gettable(L,1);
  lua_pushliteral(L,"/");
  lua_pushvalue(L,2);
  if (!(lua_toboolean(L,-1))) {
    lua_pop(L,1);
    lua_pushliteral(L,"");
  }
  lua_concat(L,2);
  lua_concat(L,2);
  lua_pushliteral(L,"r");
  lua_call(L,2,1);
  assert(lua_gettop(L) == 3);

  /* local isDir = not f:read(0) and f:seek('end') ~= 0 */
  lua_pushvalue(L,3);
  lua_pushliteral(L,"read");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_pushnumber(L,0);
  lua_call(L,2,1);
  lua_pushboolean(L,!(lua_toboolean(L,-1)));
  lua_remove(L,-2);
  if (lua_toboolean(L,-1)) {
    lua_pop(L,1);
    lua_pushvalue(L,3);
    lua_pushliteral(L,"seek");
    lua_gettable(L,-2);
    lua_insert(L,-2);
    lua_pushliteral(L,"end");
    lua_call(L,2,1);
    lua_pushnumber(L,0);
    const int lc1 = lua_equal(L,-2,-1);
    lua_pop(L,2);
    lua_pushboolean(L,lc1);
    lua_pushboolean(L,!(lua_toboolean(L,-1)));
    lua_remove(L,-2);
  }
  assert(lua_gettop(L) == 4);

  /* f:close() */
  lua_pushvalue(L,3);
  lua_pushliteral(L,"close");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,0);
  assert(lua_gettop(L) == 4);

  /* return isDir */
  lua_pushvalue(L,4);
  return 1;
  assert(lua_gettop(L) == 4);
}


/* name: LocalFileSystem:listStatus
 * function(path) */
int stuart_LocalFileSystem_listStatus (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* error('list directory capability not present') */
  lua_getfield(L,LUA_ENVIRONINDEX,"error");
  lua_pushliteral(L,"list directory capability not present");
  lua_call(L,1,0);
  assert(lua_gettop(L) == 2);
  return 0;
}


/* name: M.new
 * function(...) */
static int stuart_LocalFileSystem_new (lua_State * L) {
  return stuart_class_shared_new_with_super(L, MODULE, SUPERMODULE);
}


/* name: LocalFileSystem:open
 * function(path) */
int stuart_LocalFileSystem_open (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* local f = assert(io.open(self.uri .. '/' .. path, 'r')) */
  lua_getfield(L,LUA_ENVIRONINDEX,"assert");
  const int lc2 = lua_gettop(L);
  lua_getfield(L,LUA_ENVIRONINDEX,"io");
  lua_pushliteral(L,"open");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_pushliteral(L,"uri");
  lua_gettable(L,1);
  lua_pushliteral(L,"/");
  lua_pushvalue(L,2);
  lua_concat(L,2);
  lua_concat(L,2);
  lua_pushliteral(L,"r");
  lua_call(L,2,LUA_MULTRET);
  lua_call(L,(lua_gettop(L) - lc2),1);
  assert(lua_gettop(L) == 3);

  /* local data = f:read '*all' */
  lua_pushvalue(L,3);
  lua_pushliteral(L,"read");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_pushliteral(L,"*all");
  lua_call(L,2,1);
  assert(lua_gettop(L) == 4);

  /* return data */
  lua_pushvalue(L,4);
  return 1;
  assert(lua_gettop(L) == 4);
}


// ============================================================================
// Definition of the module Romtable.
// ============================================================================

const LUA_REG_TYPE stuart_filesysteml_map[] = {
  { LSTRKEY("_init")           , LFUNCVAL(stuart_LocalFileSystem__init) },
  { LSTRKEY("isDirectory")     , LFUNCVAL(stuart_LocalFileSystem_isDirectory) },
  { LSTRKEY("listStatus")      , LFUNCVAL(stuart_LocalFileSystem_listStatus) },
  { LSTRKEY("open")            , LFUNCVAL(stuart_LocalFileSystem_open) },

  // class framework
  { LSTRKEY("__index")         , LRO_ROVAL(stuart_filesysteml_map) },
  { LSTRKEY("_base")           , LRO_ROVAL(stuart_filesystem_map) },
  { LSTRKEY("_class")          , LRO_ROVAL(stuart_filesysteml_map) },
  { LSTRKEY("classof")         , LFUNCVAL (stuart_LocalFileSystem_classof) },
  { LSTRKEY("new" )            , LFUNCVAL (stuart_LocalFileSystem_new) },

  // inherited (not including __index, _class, _init, classof, and new)
  { LSTRKEY("getUri")          , LFUNCVAL (stuart_FileSystem_getUri) },

  { LNILKEY, LNILVAL }
};


// ============================================================================
// Registration of the module.
// ============================================================================
LUALIB_API int luaopen_stuart_filesysteml( lua_State *L ){
#if LUA_OPTIMIZE_MEMORY > 0
  return 0;
#else
  LREGISTER( L, AUXLIB_STUART_FILESYSTEML, stuart_filesysteml_map );
  return 1;
#endif
}
