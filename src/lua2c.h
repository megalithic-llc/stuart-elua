#ifndef __LUA2C_H__
#define __LUA2C_H__

#include "lua.h"

void lc_add(lua_State * L, int idxa, int idxb);
void lc_div(lua_State * L, int idxa, int idxb);
void lc_getupvalue(lua_State * L, int tidx, int level, int varid);
int lc_le(lua_State * L, int idxa, int idxb);
void lc_mod(lua_State * L, int idxa, int idxb);
void lc_mul(lua_State * L, int idxa, int idxb);
void lc_newclosuretable(lua_State * L, int idx);
void lc_pow(lua_State * L, int idxa, int idxb);
void lc_setupvalue(lua_State * L, int tidx, int level, int varid);
void lc_sub(lua_State * L, int idxa, int idxb);
void lc_unm(lua_State * L, int idxa);

#endif
