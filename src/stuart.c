#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
#include "common.h"
#include "auxmods.h"
#include "lrotable.h"
#include "platform_conf.h"
#define MIN_OPT_LEVEL 2
#include "lrodefs.h"
#include <assert.h>


/* name: M.class
 * function(super) */
static int stuart_class (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* local class = require 'stuart.class' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart.class");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 2);

  /* return class.new(super) */
  const int lc1 = lua_gettop(L);
  lua_pushliteral(L,"new");
  lua_gettable(L,2);
  lua_pushvalue(L,1);
  lua_call(L,1,LUA_MULTRET);
  return (lua_gettop(L) - lc1);
  assert(lua_gettop(L) == 2);
}


/* name: M.istype
 * function(obj, super) */
static int stuart_istype (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* local class = require 'stuart.class' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart.class");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 3);

  /* return class.istype(obj, super) */
  const int lc2 = lua_gettop(L);
  lua_pushliteral(L,"istype");
  lua_gettable(L,3);
  lua_pushvalue(L,1);
  lua_pushvalue(L,2);
  lua_call(L,2,LUA_MULTRET);
  return (lua_gettop(L) - lc2);
  assert(lua_gettop(L) == 3);
}


/* name: M.NewContext
 * function(master, appName) */
static int stuart_NewContext (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* local Context = require 'stuart.Context' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart.Context");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 3);

  /* return Context.new(master, appName) */
  const int lc2 = lua_gettop(L);
  lua_pushliteral(L,"new");
  lua_gettable(L,3);
  lua_pushvalue(L,1);
  lua_pushvalue(L,2);
  lua_call(L,2,LUA_MULTRET);
  return (lua_gettop(L) - lc2);
  assert(lua_gettop(L) == 3);
}


/* name: M.NewStreamingContext
 * function(arg1, arg2, arg3, arg4) */
static int stuart_NewStreamingContext (lua_State * L) {
  lua_checkstack(L,20);
  enum { lc_nformalargs = 4 };
  lua_settop(L,4);

  /* local Context = require 'stuart.Context' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart.Context");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 5);

  /* local moses = require 'moses' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 6);

  /* local StreamingContext = require 'stuart.streaming.StreamingContext' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart.streaming.StreamingContext");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 7);

  /* if moses.isString(arg1) and (moses.isString(arg2) or arg2 == nil) and moses.isNumber(arg3) then */
  enum { lc4 = 7 };
  lua_pushliteral(L,"isString");
  lua_gettable(L,6);
  lua_pushvalue(L,1);
  lua_call(L,1,1);
  if (lua_toboolean(L,-1)) {
    lua_pop(L,1);
    lua_pushliteral(L,"isString");
    lua_gettable(L,6);
    lua_pushvalue(L,2);
    lua_call(L,1,1);
    if (!(lua_toboolean(L,-1))) {
      lua_pop(L,1);
      lua_pushvalue(L,2);
      lua_pushnil(L);
      const int lc5 = lua_equal(L,-2,-1);
      lua_pop(L,2);
      lua_pushboolean(L,lc5);
    }
  }
  if (lua_toboolean(L,-1)) {
    lua_pop(L,1);
    lua_pushliteral(L,"isNumber");
    lua_gettable(L,6);
    lua_pushvalue(L,3);
    lua_call(L,1,1);
  }
  const int lc6 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc6) {

    /* local sc = Context.new(arg1, arg2, arg4) */
    lua_pushliteral(L,"new");
    lua_gettable(L,5);
    lua_pushvalue(L,1);
    lua_pushvalue(L,2);
    lua_pushvalue(L,4);
    lua_call(L,3,1);
    assert(lua_gettop(L) == 8);

    /* return StreamingContext.new(sc, arg3) */
    const int lc7 = lua_gettop(L);
    lua_pushliteral(L,"new");
    lua_gettable(L,7);
    lua_pushvalue(L,8);
    lua_pushvalue(L,3);
    lua_call(L,2,LUA_MULTRET);
    return (lua_gettop(L) - lc7);
    assert(lua_gettop(L) == 8);
  }
  lua_settop(L,lc4);
  assert(lua_gettop(L) == 7);

  /* local SparkConf = require 'stuart.SparkConf' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart.SparkConf");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 8);

  /* local istype = require 'stuart.class'.istype */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart.class");
  lua_call(L,1,1);
  lua_pushliteral(L,"istype");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  assert(lua_gettop(L) == 9);

  /* if (moses.isString(arg1) or istype(arg1, SparkConf)) and moses.isNumber(arg2) and arg3 == nil then */
  enum { lc8 = 9 };
  lua_pushliteral(L,"isString");
  lua_gettable(L,6);
  lua_pushvalue(L,1);
  lua_call(L,1,1);
  if (!(lua_toboolean(L,-1))) {
    lua_pop(L,1);
    lua_pushvalue(L,9);
    lua_pushvalue(L,1);
    lua_pushvalue(L,8);
    lua_call(L,2,1);
  }
  if (lua_toboolean(L,-1)) {
    lua_pop(L,1);
    lua_pushliteral(L,"isNumber");
    lua_gettable(L,6);
    lua_pushvalue(L,2);
    lua_call(L,1,1);
  }
  if (lua_toboolean(L,-1)) {
    lua_pop(L,1);
    lua_pushvalue(L,3);
    lua_pushnil(L);
    const int lc9 = lua_equal(L,-2,-1);
    lua_pop(L,2);
    lua_pushboolean(L,lc9);
  }
  const int lc10 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc10) {

    /* local sc = Context.new(arg1) */
    lua_pushliteral(L,"new");
    lua_gettable(L,5);
    lua_pushvalue(L,1);
    lua_call(L,1,1);
    assert(lua_gettop(L) == 10);

    /* return StreamingContext.new(sc, arg2) */
    const int lc11 = lua_gettop(L);
    lua_pushliteral(L,"new");
    lua_gettable(L,7);
    lua_pushvalue(L,10);
    lua_pushvalue(L,2);
    lua_call(L,2,LUA_MULTRET);
    return (lua_gettop(L) - lc11);
    assert(lua_gettop(L) == 10);
  }
  lua_settop(L,lc8);
  assert(lua_gettop(L) == 9);

  /* if moses.isTable(arg1) then */
  enum { lc12 = 9 };
  lua_pushliteral(L,"isTable");
  lua_gettable(L,6);
  lua_pushvalue(L,1);
  lua_call(L,1,1);
  const int lc13 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc13) {

    /* if moses.isNumber(arg2) then */
    enum { lc14 = 9 };
    lua_pushliteral(L,"isNumber");
    lua_gettable(L,6);
    lua_pushvalue(L,2);
    lua_call(L,1,1);
    const int lc15 = lua_toboolean(L,-1);
    lua_pop(L,1);
    if (lc15) {

      /* return StreamingContext.new(arg1, arg2) */
      const int lc16 = lua_gettop(L);
      lua_pushliteral(L,"new");
      lua_gettable(L,7);
      lua_pushvalue(L,1);
      lua_pushvalue(L,2);
      lua_call(L,2,LUA_MULTRET);
      return (lua_gettop(L) - lc16);
      assert(lua_gettop(L) == 9);
    }
    lua_settop(L,lc14);
    assert(lua_gettop(L) == 9);

    /* return StreamingContext.new(arg1) */
    const int lc17 = lua_gettop(L);
    lua_pushliteral(L,"new");
    lua_gettable(L,7);
    lua_pushvalue(L,1);
    lua_call(L,1,LUA_MULTRET);
    return (lua_gettop(L) - lc17);
    assert(lua_gettop(L) == 9);
  }
  lua_settop(L,lc12);
  assert(lua_gettop(L) == 9);

  /* error('Failed detecting NewStreamingContext parameters') */
  lua_getfield(L,LUA_ENVIRONINDEX,"error");
  lua_pushliteral(L,"Failed detecting NewStreamingContext parameters");
  lua_call(L,1,0);
  assert(lua_gettop(L) == 9);
  return 0;
}


// ============================================================================
// Definition of the module Romtable.
// ============================================================================

const LUA_REG_TYPE stuart_map[] = {
  { LSTRKEY("class")              , LFUNCVAL(stuart_class) },
  { LSTRKEY("istype")             , LFUNCVAL(stuart_istype) },
  { LSTRKEY("NewContext")         , LFUNCVAL(stuart_NewContext) },
  { LSTRKEY("NewStreamingContext"), LFUNCVAL(stuart_NewStreamingContext) },
  { LNILKEY, LNILVAL }
};


// ============================================================================
// Registration of the module.
// ============================================================================
LUALIB_API int luaopen_stuart( lua_State *L ){
#if LUA_OPTIMIZE_MEMORY > 0
  return 0;
#else
  LREGISTER( L, AUXLIB_STUART, stuart_map );
  return 1;
#endif
}
