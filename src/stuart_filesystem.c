#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
#include "common.h"
#include "auxmods.h"
#include "lrotable.h"
#include "platform_conf.h"
#define MIN_OPT_LEVEL 2
#include "lrodefs.h"
#include <assert.h>
#include "lua2c.h"
#include "stuart_class.h"

#define MODULE "stuart.FileSystem"


/* name: FileSystem:_init
 * function(uri) */
int stuart_FileSystem__init (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* self.uri = uri */
  lua_pushvalue(L,2);
  lua_pushliteral(L,"uri");
  lua_insert(L,-2);
  lua_settable(L,1);
  assert(lua_gettop(L) == 2);
  return 0;
}


/* name: FileSystem:getUri
 * function() */
int stuart_FileSystem_getUri (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* return self.uri */
  lua_pushliteral(L,"uri");
  lua_gettable(L,1);
  return 1;
  assert(lua_gettop(L) == 1);
}


/* name: M.classof
 * function(obj) */
int stuart_FileSystem_classof (lua_State * L) {
  return stuart_class_shared_classof(L, MODULE);
}


/* name: M.new
 * function(...) */
int stuart_FileSystem_new (lua_State * L) {
  return stuart_class_shared_new(L, MODULE);
}


// ============================================================================
// Definition of the module Romtable.
// ============================================================================

const LUA_REG_TYPE stuart_filesystem_map[] = {
  { LSTRKEY("_init")           , LFUNCVAL(stuart_FileSystem__init) },
  { LSTRKEY("getUri")          , LFUNCVAL(stuart_FileSystem_getUri) },

  // class framework
  { LSTRKEY("__index")         , LRO_ROVAL(stuart_filesystem_map) },
  { LSTRKEY("_class")          , LRO_ROVAL(stuart_filesystem_map) },
  { LSTRKEY("classof")         , LFUNCVAL(stuart_FileSystem_classof) },
  { LSTRKEY("new" )            , LFUNCVAL(stuart_FileSystem_new) },

  { LNILKEY, LNILVAL }
};


// ============================================================================
// Registration of the module.
// ============================================================================
LUALIB_API int luaopen_stuart_filesystem( lua_State *L ){
#if LUA_OPTIMIZE_MEMORY > 0
  return 0;
#else
  LREGISTER( L, AUXLIB_STUART_FILESYSTEM, stuart_filesystem_map );
  return 1;
#endif
}
