#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
#include "common.h"
#include "auxmods.h"
#include "lrotable.h"
#include "platform_conf.h"
#define MIN_OPT_LEVEL 2
#include "lrodefs.h"
#include <assert.h>
#include "lua2c.h"
#include "stuart_class.h"

#define MODULE "stuart.fileSystemFactory"


/* name: M._createForLocalOpenPath
 * function(parsedUri) */
int stuart_fileSystemFactory__createForLocalOpenPath (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* local moses = require 'moses' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 2);

  /* local split = require 'stuart.util'.split */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart.util");
  lua_call(L,1,1);
  lua_pushliteral(L,"split");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  assert(lua_gettop(L) == 3);

  /* local segments = split(parsedUri.path, '/') */
  lua_pushvalue(L,3);
  lua_pushliteral(L,"path");
  lua_gettable(L,1);
  lua_pushliteral(L,"/");
  lua_call(L,2,1);
  assert(lua_gettop(L) == 4);

  /* local constructorUri, openPath */
  lua_settop(L,(lua_gettop(L) + 2));
  assert(lua_gettop(L) == 6);

  /* if #segments == 1 or (#segments == 2 and segments[2] == '') then */
  enum { lc1 = 6 };
  const double lc2 = lua_objlen(L,4);
  lua_pushnumber(L,lc2);
  lua_pushnumber(L,1);
  const int lc3 = lua_equal(L,-2,-1);
  lua_pop(L,2);
  lua_pushboolean(L,lc3);
  if (!(lua_toboolean(L,-1))) {
    lua_pop(L,1);
    const double lc4 = lua_objlen(L,4);
    lua_pushnumber(L,lc4);
    lua_pushnumber(L,2);
    const int lc5 = lua_equal(L,-2,-1);
    lua_pop(L,2);
    lua_pushboolean(L,lc5);
    if (lua_toboolean(L,-1)) {
      lua_pop(L,1);
      lua_pushnumber(L,2);
      lua_gettable(L,4);
      lua_pushliteral(L,"");
      const int lc6 = lua_equal(L,-2,-1);
      lua_pop(L,2);
      lua_pushboolean(L,lc6);
    }
  }
  const int lc7 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc7) {

    /* constructorUri = '.' */
    lua_pushliteral(L,".");
    lua_replace(L,5);
    assert(lua_gettop(L) == 6);

    /* openPath = segments[1] */
    lua_pushnumber(L,1);
    lua_gettable(L,4);
    lua_replace(L,6);
    assert(lua_gettop(L) == 6);
  }
  else {

    /* else
     * constructorUri = table.concat(moses.first(segments, #segments - 1), '/') */
    lua_getfield(L,LUA_ENVIRONINDEX,"table");
    lua_pushliteral(L,"concat");
    lua_gettable(L,-2);
    lua_remove(L,-2);
    lua_pushliteral(L,"first");
    lua_gettable(L,2);
    lua_pushvalue(L,4);
    const double lc8 = lua_objlen(L,4);
    lua_pushnumber(L,lc8);
    lua_pushnumber(L,1);
    lc_sub(L,-2,-1);
    lua_remove(L,-2);
    lua_remove(L,-2);
    lua_call(L,2,1);
    lua_pushliteral(L,"/");
    lua_call(L,2,1);
    lua_replace(L,5);
    assert(lua_gettop(L) == 6);

    /* openPath = segments[#segments] */
    const double lc9 = lua_objlen(L,4);
    lua_pushnumber(L,lc9);
    lua_gettable(L,4);
    lua_replace(L,6);
    assert(lua_gettop(L) == 6);
  }
  lua_settop(L,lc1);
  assert(lua_gettop(L) == 6);

  /* local LocalFileSystem = require 'stuart.LocalFileSystem' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart.LocalFileSystem");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 7);

  /* local fs = LocalFileSystem.new(constructorUri) */
  lua_pushliteral(L,"new");
  lua_gettable(L,7);
  lua_pushvalue(L,5);
  lua_call(L,1,1);
  assert(lua_gettop(L) == 8);

  /* return fs, openPath */
  lua_pushvalue(L,8);
  lua_pushvalue(L,6);
  return 2;
  assert(lua_gettop(L) == 8);
}


/* name: M._createForWebHdfsOpenPath
 * function(parsedUri) */
int stuart_fileSystemFactory__createForWebHdfsOpenPath (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* local moses = require 'moses' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 2);

  /* local split = require 'stuart.util'.split */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart.util");
  lua_call(L,1,1);
  lua_pushliteral(L,"split");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  assert(lua_gettop(L) == 3);

  /* local segments = split(parsedUri.path, '/') */
  lua_pushvalue(L,3);
  lua_pushliteral(L,"path");
  lua_gettable(L,1);
  lua_pushliteral(L,"/");
  lua_call(L,2,1);
  assert(lua_gettop(L) == 4);

  /* local constructorUri, openPath */
  lua_settop(L,(lua_gettop(L) + 2));
  assert(lua_gettop(L) == 6);

  /* local uriSegments = moses.clone(parsedUri) */
  lua_pushliteral(L,"clone");
  lua_gettable(L,2);
  lua_pushvalue(L,1);
  lua_call(L,1,1);
  assert(lua_gettop(L) == 7);

  /* if #segments > 3 and segments[2] == 'webhdfs' and segments[3] == 'v1' then */
  enum { lc10 = 7 };
  lua_pushnumber(L,3);
  const double lc11 = lua_objlen(L,4);
  lua_pushnumber(L,lc11);
  const int lc12 = lua_lessthan(L,-2,-1);
  lua_pop(L,2);
  lua_pushboolean(L,lc12);
  if (lua_toboolean(L,-1)) {
    lua_pop(L,1);
    lua_pushnumber(L,2);
    lua_gettable(L,4);
    lua_pushliteral(L,"webhdfs");
    const int lc13 = lua_equal(L,-2,-1);
    lua_pop(L,2);
    lua_pushboolean(L,lc13);
  }
  if (lua_toboolean(L,-1)) {
    lua_pop(L,1);
    lua_pushnumber(L,3);
    lua_gettable(L,4);
    lua_pushliteral(L,"v1");
    const int lc14 = lua_equal(L,-2,-1);
    lua_pop(L,2);
    lua_pushboolean(L,lc14);
  }
  const int lc15 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc15) {

    /* -- split /webhdfs/v1/path/file into constructorUri=/webhdfs/v1/ and openPath=path/file
     * constructorUri = string.format('%s://%s/%s/%s', uriSegments.scheme, uriSegments.host, segments[2], segments[3]) */
    lua_getfield(L,LUA_ENVIRONINDEX,"string");
    lua_pushliteral(L,"format");
    lua_gettable(L,-2);
    lua_remove(L,-2);
    lua_pushliteral(L,"%s://%s/%s/%s");
    lua_pushliteral(L,"scheme");
    lua_gettable(L,7);
    lua_pushliteral(L,"host");
    lua_gettable(L,7);
    lua_pushnumber(L,2);
    lua_gettable(L,4);
    lua_pushnumber(L,3);
    lua_gettable(L,4);
    lua_call(L,5,1);
    lua_replace(L,5);
    assert(lua_gettop(L) == 7);

    /* openPath = '/' .. table.concat(moses.rest(segments, 4), '/') */
    lua_pushliteral(L,"/");
    lua_getfield(L,LUA_ENVIRONINDEX,"table");
    lua_pushliteral(L,"concat");
    lua_gettable(L,-2);
    lua_remove(L,-2);
    lua_pushliteral(L,"rest");
    lua_gettable(L,2);
    lua_pushvalue(L,4);
    lua_pushnumber(L,4);
    lua_call(L,2,1);
    lua_pushliteral(L,"/");
    lua_call(L,2,1);
    lua_concat(L,2);
    lua_replace(L,6);
    assert(lua_gettop(L) == 7);
  }
  else {

    /* elseif #segments > 2 and segments[2] == 'v1' then */
    enum { lc16 = 7 };
    lua_pushnumber(L,2);
    const double lc17 = lua_objlen(L,4);
    lua_pushnumber(L,lc17);
    const int lc18 = lua_lessthan(L,-2,-1);
    lua_pop(L,2);
    lua_pushboolean(L,lc18);
    if (lua_toboolean(L,-1)) {
      lua_pop(L,1);
      lua_pushnumber(L,2);
      lua_gettable(L,4);
      lua_pushliteral(L,"v1");
      const int lc19 = lua_equal(L,-2,-1);
      lua_pop(L,2);
      lua_pushboolean(L,lc19);
    }
    const int lc20 = lua_toboolean(L,-1);
    lua_pop(L,1);
    if (lc20) {

      /* -- split /v1/path/file into constructorUri=/v1/ and openPath=path/file
       * constructorUri = string.format('%s://%s/%s', uriSegments.scheme, uriSegments.host, segments[2]) */
      lua_getfield(L,LUA_ENVIRONINDEX,"string");
      lua_pushliteral(L,"format");
      lua_gettable(L,-2);
      lua_remove(L,-2);
      lua_pushliteral(L,"%s://%s/%s");
      lua_pushliteral(L,"scheme");
      lua_gettable(L,7);
      lua_pushliteral(L,"host");
      lua_gettable(L,7);
      lua_pushnumber(L,2);
      lua_gettable(L,4);
      lua_call(L,4,1);
      lua_replace(L,5);
      assert(lua_gettop(L) == 7);

      /* openPath = '/' .. table.concat(moses.rest(segments, 2), '/') */
      lua_pushliteral(L,"/");
      lua_getfield(L,LUA_ENVIRONINDEX,"table");
      lua_pushliteral(L,"concat");
      lua_gettable(L,-2);
      lua_remove(L,-2);
      lua_pushliteral(L,"rest");
      lua_gettable(L,2);
      lua_pushvalue(L,4);
      lua_pushnumber(L,2);
      lua_call(L,2,1);
      lua_pushliteral(L,"/");
      lua_call(L,2,1);
      lua_concat(L,2);
      lua_replace(L,6);
      assert(lua_gettop(L) == 7);
    }
    else {

      /* else
       * -- provide /webhdfs when absent
       * constructorUri = string.format('%s://%s/webhdfs', uriSegments.scheme, uriSegments.host) */
      lua_getfield(L,LUA_ENVIRONINDEX,"string");
      lua_pushliteral(L,"format");
      lua_gettable(L,-2);
      lua_remove(L,-2);
      lua_pushliteral(L,"%s://%s/webhdfs");
      lua_pushliteral(L,"scheme");
      lua_gettable(L,7);
      lua_pushliteral(L,"host");
      lua_gettable(L,7);
      lua_call(L,3,1);
      lua_replace(L,5);
      assert(lua_gettop(L) == 7);

      /* openPath = table.concat(segments, '/') */
      lua_getfield(L,LUA_ENVIRONINDEX,"table");
      lua_pushliteral(L,"concat");
      lua_gettable(L,-2);
      lua_remove(L,-2);
      lua_pushvalue(L,4);
      lua_pushliteral(L,"/");
      lua_call(L,2,1);
      lua_replace(L,6);
      assert(lua_gettop(L) == 7);
    }
    lua_settop(L,lc16);
  }
  lua_settop(L,lc10);
  assert(lua_gettop(L) == 7);

  /* local WebHdfsFileSystem = require 'stuart.WebHdfsFileSystem' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart.WebHdfsFileSystem");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 8);

  /* local fs = WebHdfsFileSystem.new(constructorUri) */
  lua_pushliteral(L,"new");
  lua_gettable(L,8);
  lua_pushvalue(L,5);
  lua_call(L,1,1);
  assert(lua_gettop(L) == 9);

  /* return fs, openPath */
  lua_pushvalue(L,9);
  lua_pushvalue(L,6);
  return 2;
  assert(lua_gettop(L) == 9);
}


/* name: M.createForOpenPath
 * function(path) */
int stuart_fileSystemFactory_createForOpenPath (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* local urlParse = require 'stuart.util'.urlParse */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart.util");
  lua_call(L,1,1);
  lua_pushliteral(L,"urlParse");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  assert(lua_gettop(L) == 2);

  /* local parsedUri = urlParse(path) */
  lua_pushvalue(L,2);
  lua_pushvalue(L,1);
  lua_call(L,1,1);
  assert(lua_gettop(L) == 3);

  /* local M = require 'stuart.fileSystemFactory' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,MODULE);
  lua_call(L,1,1);
  assert(lua_gettop(L) == 4);

  /* if parsedUri.scheme == 'webhdfs' or parsedUri.scheme == 'swebhdfs' then */
  enum { lc21 = 4 };
  lua_pushliteral(L,"scheme");
  lua_gettable(L,3);
  lua_pushliteral(L,"webhdfs");
  const int lc22 = lua_equal(L,-2,-1);
  lua_pop(L,2);
  lua_pushboolean(L,lc22);
  if (!(lua_toboolean(L,-1))) {
    lua_pop(L,1);
    lua_pushliteral(L,"scheme");
    lua_gettable(L,3);
    lua_pushliteral(L,"swebhdfs");
    const int lc23 = lua_equal(L,-2,-1);
    lua_pop(L,2);
    lua_pushboolean(L,lc23);
  }
  const int lc24 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc24) {

    /* return M._createForWebHdfsOpenPath(parsedUri) */
    const int lc25 = lua_gettop(L);
    lua_pushliteral(L,"_createForWebHdfsOpenPath");
    lua_gettable(L,4);
    lua_pushvalue(L,3);
    lua_call(L,1,LUA_MULTRET);
    return (lua_gettop(L) - lc25);
    assert(lua_gettop(L) == 4);
  }
  else {

    /* elseif parsedUri.scheme ~= nil then */
    enum { lc26 = 4 };
    lua_pushliteral(L,"scheme");
    lua_gettable(L,3);
    lua_pushnil(L);
    const int lc27 = lua_equal(L,-2,-1);
    lua_pop(L,2);
    lua_pushboolean(L,lc27);
    lua_pushboolean(L,!(lua_toboolean(L,-1)));
    lua_remove(L,-2);
    const int lc28 = lua_toboolean(L,-1);
    lua_pop(L,1);
    if (lc28) {

      /* error('Unsupported URI scheme: ' .. parsedUri.scheme) */
      lua_getfield(L,LUA_ENVIRONINDEX,"error");
      lua_pushliteral(L,"Unsupported URI scheme: ");
      lua_pushliteral(L,"scheme");
      lua_gettable(L,3);
      lua_concat(L,2);
      lua_call(L,1,0);
      assert(lua_gettop(L) == 4);
    }
    else {

      /* else
       * return M._createForLocalOpenPath(parsedUri) */
      const int lc29 = lua_gettop(L);
      lua_pushliteral(L,"_createForLocalOpenPath");
      lua_gettable(L,4);
      lua_pushvalue(L,3);
      lua_call(L,1,LUA_MULTRET);
      return (lua_gettop(L) - lc29);
      assert(lua_gettop(L) == 4);
    }
    lua_settop(L,lc26);
  }
  lua_settop(L,lc21);
  assert(lua_gettop(L) == 4);
  return 0;
}


// ============================================================================
// Definition of the module Romtable.
// ============================================================================

const LUA_REG_TYPE stuart_filesystemf_map[] = {
  { LSTRKEY("_createForLocalOpenPath")   , LFUNCVAL(stuart_fileSystemFactory__createForLocalOpenPath) },
  { LSTRKEY("_createForWebHdfsOpenPath") , LFUNCVAL(stuart_fileSystemFactory__createForWebHdfsOpenPath) },
  { LSTRKEY("createForOpenPath")         , LFUNCVAL(stuart_fileSystemFactory_createForOpenPath) },
  { LNILKEY, LNILVAL }
};


// ============================================================================
// Registration of the module.
// ============================================================================
LUALIB_API int luaopen_stuart_filesystemf( lua_State *L ){
#if LUA_OPTIMIZE_MEMORY > 0
  return 0;
#else
  LREGISTER( L, AUXLIB_STUART_FILESYSTEMF, stuart_filesystemf_map );
  return 1;
#endif
}
