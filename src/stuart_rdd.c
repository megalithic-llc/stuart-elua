#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
#include "common.h"
#include "auxmods.h"
#include "lrotable.h"
#include "platform_conf.h"
#define MIN_OPT_LEVEL 2
#include "lrodefs.h"
#include <assert.h>
#include "lua2c.h"
#include "stuart_class.h"


/* name: RDD:_init
 * function(context, partitions) */
int stuart_RDD__init (lua_State * L) {
  enum { lc_nformalargs = 3 };
  lua_settop(L,3);

  /* self.context = context */
  lua_pushvalue(L,2);
  lua_pushliteral(L,"context");
  lua_insert(L,-2);
  lua_settable(L,1);
  assert(lua_gettop(L) == 3);

  /* self.id = context:getNextId() */
  lua_pushvalue(L,2);
  lua_pushliteral(L,"getNextId");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  lua_pushliteral(L,"id");
  lua_insert(L,-2);
  lua_settable(L,1);
  assert(lua_gettop(L) == 3);

  /* self.sparkContext = context */
  lua_pushvalue(L,2);
  lua_pushliteral(L,"sparkContext");
  lua_insert(L,-2);
  lua_settable(L,1);
  assert(lua_gettop(L) == 3);

  /* self.partitions = partitions */
  lua_pushvalue(L,3);
  lua_pushliteral(L,"partitions");
  lua_insert(L,-2);
  lua_settable(L,1);
  assert(lua_gettop(L) == 3);
  return 0;
}


/* name: RDD:__tostring
 * function() */
int stuart_RDD___tostring (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* return 'RDD[' .. self.id .. ']' */
  lua_pushliteral(L,"RDD[");
  lua_pushliteral(L,"id");
  lua_gettable(L,1);
  lua_pushliteral(L,"]");
  lua_concat(L,2);
  lua_concat(L,2);
  return 1;
  assert(lua_gettop(L) == 1);
}


/* function(r, e) */
int stuart_RDD__dict_1 (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* r[e[1]] = e[2] */
  lua_pushnumber(L,2);
  lua_gettable(L,2);
  lua_pushnumber(L,1);
  lua_gettable(L,2);
  lua_insert(L,-2);
  lua_settable(L,1);
  assert(lua_gettop(L) == 2);

  /* return r */
  lua_pushvalue(L,1);
  return 1;
  assert(lua_gettop(L) == 2);
}


/* name: RDD:_dict
 * function() */
int stuart_RDD__dict (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* local moses = require 'moses' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 2);

  /* return moses.reduce(self:collect(), function(r, e)
   *     r[e[1]] = e[2]
   *     return r
   *   end, {}) */
  const int lc1 = lua_gettop(L);
  lua_pushliteral(L,"reduce");
  lua_gettable(L,2);
  lua_pushvalue(L,1);
  lua_pushliteral(L,"collect");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  lua_pushcfunction(L,stuart_RDD__dict_1);
  lua_newtable(L);
  lua_call(L,3,LUA_MULTRET);
  return (lua_gettop(L) - lc1);
  assert(lua_gettop(L) == 2);
}


/* name: RDD:_flatten
 * function() */
int stuart_RDD__flatten (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* for _, p in ipairs(self.partitions) do
   * internal: local f, s, var = explist */
  enum { lc3 = 1 };
  lua_getfield(L,LUA_ENVIRONINDEX,"ipairs");
  lua_pushliteral(L,"partitions");
  lua_gettable(L,1);
  lua_call(L,1,3);
  while (1) {

    /* internal: local var_1, ..., var_n = f(s, var)
     *           if var_1 == nil then break end
     *           var = var_1 */
    lua_pushvalue(L,-3);
    lua_pushvalue(L,-3);
    lua_pushvalue(L,-3);
    lua_call(L,2,2);
    if (lua_isnil(L,-2)) {
      break;
    }
    lua_pushvalue(L,-2);
    lua_replace(L,-4);

    /* internal: local _ with idx 5
     * internal: local p with idx 6 */


    /* p:_flatten() */
    lua_pushvalue(L,6);
    lua_pushliteral(L,"_flatten");
    lua_gettable(L,-2);
    lua_insert(L,-2);
    lua_call(L,1,0);
    assert(lua_gettop(L) == 6);

    /* internal: stack cleanup on scope exit */
    lua_pop(L,2);
  }
  lua_settop(L,lc3);
  assert(lua_gettop(L) == 1);

  /* return self */
  lua_pushvalue(L,1);
  return 1;
  assert(lua_gettop(L) == 1);
}


/* name: RDD:_flattenValues
 * function() */
int stuart_RDD__flattenValues (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* for _, p in ipairs(self.partitions) do
   * internal: local f, s, var = explist */
  enum { lc4 = 1 };
  lua_getfield(L,LUA_ENVIRONINDEX,"ipairs");
  lua_pushliteral(L,"partitions");
  lua_gettable(L,1);
  lua_call(L,1,3);
  while (1) {

    /* internal: local var_1, ..., var_n = f(s, var)
     *           if var_1 == nil then break end
     *           var = var_1 */
    lua_pushvalue(L,-3);
    lua_pushvalue(L,-3);
    lua_pushvalue(L,-3);
    lua_call(L,2,2);
    if (lua_isnil(L,-2)) {
      break;
    }
    lua_pushvalue(L,-2);
    lua_replace(L,-4);

    /* internal: local _ with idx 5
     * internal: local p with idx 6 */


    /* p:_flattenValues() */
    lua_pushvalue(L,6);
    lua_pushliteral(L,"_flattenValues");
    lua_gettable(L,-2);
    lua_insert(L,-2);
    lua_call(L,1,0);
    assert(lua_gettop(L) == 6);

    /* internal: stack cleanup on scope exit */
    lua_pop(L,2);
  }
  lua_settop(L,lc4);
  assert(lua_gettop(L) == 1);

  /* return self */
  lua_pushvalue(L,1);
  return 1;
  assert(lua_gettop(L) == 1);
}


/* function(r, p) */
int stuart_RDD_aggregate_1 (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* local y = moses.reduce(p.data, seqOp, clone(zeroValue)) */
  lc_getupvalue(L,lua_upvalueindex(1),1,4);
  lua_pushliteral(L,"reduce");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  const int lc10 = lua_gettop(L);
  lua_pushliteral(L,"data");
  lua_gettable(L,2);
  lc_getupvalue(L,lua_upvalueindex(1),2,2);
  lc_getupvalue(L,lua_upvalueindex(1),0,5);
  lc_getupvalue(L,lua_upvalueindex(1),2,1);
  lua_call(L,1,LUA_MULTRET);
  lua_call(L,(lua_gettop(L) - lc10),1);
  assert(lua_gettop(L) == 3);

  /* return combOp(r, y) */
  const int lc11 = lua_gettop(L);
  lc_getupvalue(L,lua_upvalueindex(1),2,3);
  lua_pushvalue(L,1);
  lua_pushvalue(L,3);
  lua_call(L,2,LUA_MULTRET);
  return (lua_gettop(L) - lc11);
  assert(lua_gettop(L) == 3);
}


/* name: RDD:aggregate
 * function(zeroValue, seqOp, combOp) */
int stuart_RDD_aggregate (lua_State * L) {
  enum { lc_nformalargs = 4 };
  lua_settop(L,4);
  lc_newclosuretable(L,lua_upvalueindex(1));
  enum { lc5 = 5 };
  assert((lua_gettop(L) == lc5));
  lua_pushvalue(L,2);
  lua_rawseti(L,-2,1);
  lua_pushvalue(L,3);
  lua_rawseti(L,-2,2);
  lua_pushvalue(L,4);
  lua_rawseti(L,-2,3);

  /* local moses = require 'moses' */
  lc_newclosuretable(L,lc5);
  enum { lc6 = 6 };
  assert((lua_gettop(L) == lc6));
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  lua_rawseti(L,lc6,4);
  assert(lua_gettop(L) == 6);

  /* local clone = require 'stuart.util'.clone */
  lc_newclosuretable(L,lc6);
  enum { lc7 = 7 };
  assert((lua_gettop(L) == lc7));
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart.util");
  lua_call(L,1,1);
  lua_pushliteral(L,"clone");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_rawseti(L,lc7,5);
  assert(lua_gettop(L) == 7);

  /* return moses.reduce(self.partitions, function(r, p)
   *     local y = moses.reduce(p.data, seqOp, clone(zeroValue))
   *     return combOp(r, y)
   *   end, clone(zeroValue)) */
  const int lc8 = lua_gettop(L);
  lc_getupvalue(L,lc7,1,4);
  lua_pushliteral(L,"reduce");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  const int lc9 = lua_gettop(L);
  lua_pushliteral(L,"partitions");
  lua_gettable(L,1);
  lua_pushvalue(L,lc7);
  lua_pushcclosure(L,stuart_RDD_aggregate_1,1);
  lc_getupvalue(L,lc7,0,5);
  lc_getupvalue(L,lc7,2,1);
  lua_call(L,1,LUA_MULTRET);
  lua_call(L,(lua_gettop(L) - lc9),LUA_MULTRET);
  return (lua_gettop(L) - lc8);
  assert(lua_gettop(L) == 7);
}


/* function(e) */
int stuart_RDD_aggregateByKey_1 (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* return e[1] */
  lua_pushnumber(L,1);
  lua_gettable(L,1);
  return 1;
  assert(lua_gettop(L) == 1);
}


/* function(r2,e) */
int stuart_RDD_aggregateByKey_2 (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* if e[1] == key then */
  enum { lc20 = 2 };
  lua_pushnumber(L,1);
  lua_gettable(L,2);
  lc_getupvalue(L,lua_upvalueindex(1),0,11);
  const int lc21 = lua_equal(L,-2,-1);
  lua_pop(L,2);
  lua_pushboolean(L,lc21);
  const int lc22 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc22) {

    /* r2[#r2+1] = e[2] */
    lua_pushnumber(L,2);
    lua_gettable(L,2);
    const double lc23 = lua_objlen(L,1);
    lua_pushnumber(L,lc23);
    lua_pushnumber(L,1);
    lc_add(L,-2,-1);
    lua_remove(L,-2);
    lua_remove(L,-2);
    lua_insert(L,-2);
    lua_settable(L,1);
    assert(lua_gettop(L) == 2);
  }
  lua_settop(L,lc20);
  assert(lua_gettop(L) == 2);

  /* return r2 */
  lua_pushvalue(L,1);
  return 1;
  assert(lua_gettop(L) == 2);
}


/* function(r,key) */
int stuart_RDD_aggregateByKey_3 (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);
  lc_newclosuretable(L,lua_upvalueindex(1));
  enum { lc19 = 3 };
  assert((lua_gettop(L) == lc19));
  lua_pushvalue(L,2);
  lua_rawseti(L,-2,11);

  /* local valuesForKey = moses.reduce(p.data, function(r2,e)
   *         if e[1] == key then r2[#r2+1] = e[2] end
   *         return r2
   *       end, {}) */
  lc_getupvalue(L,lc19,2,9);
  lua_pushliteral(L,"reduce");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lc_getupvalue(L,lc19,1,10);
  lua_pushliteral(L,"data");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_pushvalue(L,lc19);
  lua_pushcclosure(L,stuart_RDD_aggregateByKey_2,1);
  lua_newtable(L);
  lua_call(L,3,1);
  assert(lua_gettop(L) == 4);

  /* r[key] = moses.reduce(valuesForKey, seqOp, zeroValue) */
  lc_getupvalue(L,lc19,2,9);
  lua_pushliteral(L,"reduce");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_pushvalue(L,4);
  lc_getupvalue(L,lc19,3,7);
  lc_getupvalue(L,lc19,3,6);
  lua_call(L,3,1);
  lc_getupvalue(L,lc19,0,11);
  lua_insert(L,-2);
  lua_settable(L,1);
  assert(lua_gettop(L) == 4);

  /* return r */
  lua_pushvalue(L,1);
  return 1;
  assert(lua_gettop(L) == 4);
}


/* function(p) */
int stuart_RDD_aggregateByKey_4 (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);
  lc_newclosuretable(L,lua_upvalueindex(1));
  enum { lc16 = 2 };
  assert((lua_gettop(L) == lc16));
  lua_pushvalue(L,1);
  lua_rawseti(L,-2,10);

  /* local keys = moses.uniq(moses.map(p.data, function(e) return e[1] end)) */
  lc_getupvalue(L,lc16,1,9);
  lua_pushliteral(L,"uniq");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  const int lc17 = lua_gettop(L);
  lc_getupvalue(L,lc16,1,9);
  lua_pushliteral(L,"map");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lc_getupvalue(L,lc16,0,10);
  lua_pushliteral(L,"data");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_pushcfunction(L,stuart_RDD_aggregateByKey_1);
  lua_call(L,2,LUA_MULTRET);
  lua_call(L,(lua_gettop(L) - lc17),1);
  assert(lua_gettop(L) == 3);

  /* local z = moses.reduce(keys, function(r,key)
   *       local valuesForKey = moses.reduce(p.data, function(r2,e)
   *         if e[1] == key then r2[#r2+1] = e[2] end
   *         return r2
   *       end, {})
   *       r[key] = moses.reduce(valuesForKey, seqOp, zeroValue)
   *       return r
   *     end, {}) */
  lc_getupvalue(L,lc16,1,9);
  lua_pushliteral(L,"reduce");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_pushvalue(L,3);
  lua_pushvalue(L,lc16);
  lua_pushcclosure(L,stuart_RDD_aggregateByKey_3,1);
  lua_newtable(L);
  lua_call(L,3,1);
  assert(lua_gettop(L) == 4);

  /* return z */
  lua_pushvalue(L,4);
  return 1;
  assert(lua_gettop(L) == 4);
}


/* function(r,e) */
int stuart_RDD_aggregateByKey_5 (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* return moses.append(r, moses.keys(e)) */
  const int lc28 = lua_gettop(L);
  lc_getupvalue(L,lua_upvalueindex(1),1,9);
  lua_pushliteral(L,"append");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  const int lc29 = lua_gettop(L);
  lua_pushvalue(L,1);
  lc_getupvalue(L,lua_upvalueindex(1),1,9);
  lua_pushliteral(L,"keys");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_pushvalue(L,2);
  lua_call(L,1,LUA_MULTRET);
  lua_call(L,(lua_gettop(L) - lc29),LUA_MULTRET);
  return (lua_gettop(L) - lc28);
  assert(lua_gettop(L) == 2);
}


/* function(r2,e) */
int stuart_RDD_aggregateByKey_6 (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* for k,v in pairs(e) do
   * internal: local f, s, var = explist */
  enum { lc32 = 2 };
  lua_getfield(L,LUA_ENVIRONINDEX,"pairs");
  lua_pushvalue(L,2);
  lua_call(L,1,3);
  while (1) {

    /* internal: local var_1, ..., var_n = f(s, var)
     *           if var_1 == nil then break end
     *           var = var_1 */
    lua_pushvalue(L,-3);
    lua_pushvalue(L,-3);
    lua_pushvalue(L,-3);
    lua_call(L,2,2);
    if (lua_isnil(L,-2)) {
      break;
    }
    lua_pushvalue(L,-2);
    lua_replace(L,-4);

    /* internal: local k with idx 6
     * internal: local v with idx 7 */


    /* if k == key then */
    enum { lc33 = 7 };
    lc_getupvalue(L,lua_upvalueindex(1),0,13);
    const int lc34 = lua_equal(L,6,-1);
    lua_pop(L,1);
    lua_pushboolean(L,lc34);
    const int lc35 = lua_toboolean(L,-1);
    lua_pop(L,1);
    if (lc35) {

      /* r2[#r2+1] = v */
      lua_pushvalue(L,7);
      const double lc36 = lua_objlen(L,1);
      lua_pushnumber(L,lc36);
      lua_pushnumber(L,1);
      lc_add(L,-2,-1);
      lua_remove(L,-2);
      lua_remove(L,-2);
      lua_insert(L,-2);
      lua_settable(L,1);
      assert(lua_gettop(L) == 7);
    }
    lua_settop(L,lc33);
    assert(lua_gettop(L) == 7);

    /* internal: stack cleanup on scope exit */
    lua_pop(L,2);
  }
  lua_settop(L,lc32);
  assert(lua_gettop(L) == 2);

  /* return r2 */
  lua_pushvalue(L,1);
  return 1;
  assert(lua_gettop(L) == 2);
}


/* function(r,key) */
int stuart_RDD_aggregateByKey_7 (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);
  lc_newclosuretable(L,lua_upvalueindex(1));
  enum { lc31 = 3 };
  assert((lua_gettop(L) == lc31));
  lua_pushvalue(L,2);
  lua_rawseti(L,-2,13);

  /* local valuesForKey = moses.reduce(y, function(r2,e)
   *       for k,v in pairs(e) do
   *         if k == key then r2[#r2+1] = v end
   *       end
   *       return r2
   *     end, {}) */
  lc_getupvalue(L,lc31,2,9);
  lua_pushliteral(L,"reduce");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lc_getupvalue(L,lc31,1,12);
  lua_pushvalue(L,lc31);
  lua_pushcclosure(L,stuart_RDD_aggregateByKey_6,1);
  lua_newtable(L);
  lua_call(L,3,1);
  assert(lua_gettop(L) == 4);

  /* r[#r+1] = {key, moses.reduce(valuesForKey, combOp, 0)} */
  lua_createtable(L,2,0);
  lc_getupvalue(L,lc31,0,13);
  lua_rawseti(L,-2,1);
  const int lc38 = lua_gettop(L);
  lc_getupvalue(L,lc31,2,9);
  lua_pushliteral(L,"reduce");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_pushvalue(L,4);
  lc_getupvalue(L,lc31,3,8);
  lua_pushnumber(L,0);
  lua_call(L,3,LUA_MULTRET);
  while ((lua_gettop(L) > lc38)) {
    lua_rawseti(L,lc38,(1 + (lua_gettop(L) - lc38)));
  }
  const double lc39 = lua_objlen(L,1);
  lua_pushnumber(L,lc39);
  lua_pushnumber(L,1);
  lc_add(L,-2,-1);
  lua_remove(L,-2);
  lua_remove(L,-2);
  lua_insert(L,-2);
  lua_settable(L,1);
  assert(lua_gettop(L) == 4);

  /* return r */
  lua_pushvalue(L,1);
  return 1;
  assert(lua_gettop(L) == 4);
}


/* name: RDD:aggregateByKey
 * function(zeroValue, seqOp, combOp) */
int stuart_RDD_aggregateByKey (lua_State * L) {
  enum { lc_nformalargs = 4 };
  lua_settop(L,4);
  lc_newclosuretable(L,lua_upvalueindex(1));
  enum { lc13 = 5 };
  assert((lua_gettop(L) == lc13));
  lua_pushvalue(L,2);
  lua_rawseti(L,-2,6);
  lua_pushvalue(L,3);
  lua_rawseti(L,-2,7);
  lua_pushvalue(L,4);
  lua_rawseti(L,-2,8);

  /* local moses = require 'moses' */
  lc_newclosuretable(L,lc13);
  enum { lc14 = 6 };
  assert((lua_gettop(L) == lc14));
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  lua_rawseti(L,lc14,9);
  assert(lua_gettop(L) == 6);

  /* local y = moses.map(self.partitions, function(p)
   *     local keys = moses.uniq(moses.map(p.data, function(e) return e[1] end))
   *     local z = moses.reduce(keys, function(r,key)
   *       local valuesForKey = moses.reduce(p.data, function(r2,e)
   *         if e[1] == key then r2[#r2+1] = e[2] end
   *         return r2
   *       end, {})
   *       r[key] = moses.reduce(valuesForKey, seqOp, zeroValue)
   *       return r
   *     end, {})
   *     return z
   *   end, zeroValue) */
  lc_newclosuretable(L,lc14);
  enum { lc15 = 7 };
  assert((lua_gettop(L) == lc15));
  lc_getupvalue(L,lc14,0,9);
  lua_pushliteral(L,"map");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_pushliteral(L,"partitions");
  lua_gettable(L,1);
  lua_pushvalue(L,lc14);
  lua_pushcclosure(L,stuart_RDD_aggregateByKey_4,1);
  lc_getupvalue(L,lc14,1,6);
  lua_call(L,3,1);
  lua_rawseti(L,lc15,12);
  assert(lua_gettop(L) == 7);

  /* local keys = moses.uniq(moses.reduce(y, function(r,e) return moses.append(r, moses.keys(e)) end, {})) */
  lc_getupvalue(L,lc15,1,9);
  lua_pushliteral(L,"uniq");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  const int lc27 = lua_gettop(L);
  lc_getupvalue(L,lc15,1,9);
  lua_pushliteral(L,"reduce");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lc_getupvalue(L,lc15,0,12);
  lua_pushvalue(L,lc15);
  lua_pushcclosure(L,stuart_RDD_aggregateByKey_5,1);
  lua_newtable(L);
  lua_call(L,3,LUA_MULTRET);
  lua_call(L,(lua_gettop(L) - lc27),1);
  assert(lua_gettop(L) == 8);

  /* local t = moses.reduce(keys, function(r,key)
   *     local valuesForKey = moses.reduce(y, function(r2,e)
   *       for k,v in pairs(e) do
   *         if k == key then r2[#r2+1] = v end
   *       end
   *       return r2
   *     end, {})
   *     r[#r+1] = {key, moses.reduce(valuesForKey, combOp, 0)}
   *     return r
   *   end, {}) */
  lc_getupvalue(L,lc15,1,9);
  lua_pushliteral(L,"reduce");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_pushvalue(L,8);
  lua_pushvalue(L,lc15);
  lua_pushcclosure(L,stuart_RDD_aggregateByKey_7,1);
  lua_newtable(L);
  lua_call(L,3,1);
  assert(lua_gettop(L) == 9);

  /* return self.context:parallelize(t) */
  const int lc41 = lua_gettop(L);
  lua_pushliteral(L,"context");
  lua_gettable(L,1);
  lua_pushliteral(L,"parallelize");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_pushvalue(L,9);
  lua_call(L,2,LUA_MULTRET);
  return (lua_gettop(L) - lc41);
  assert(lua_gettop(L) == 9);
}


/* function(y) */
int stuart_RDD_cartesian_1 (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* t[#t+1] = {x, y} */
  lua_createtable(L,2,0);
  lc_getupvalue(L,lua_upvalueindex(1),0,17);
  lua_rawseti(L,-2,1);
  lua_pushvalue(L,1);
  lua_rawseti(L,-2,2);
  lc_getupvalue(L,lua_upvalueindex(1),1,16);
  lua_insert(L,-2);
  lc_getupvalue(L,lua_upvalueindex(1),1,16);
  const double lc46 = lua_objlen(L,-1);
  lua_pop(L,1);
  lua_pushnumber(L,lc46);
  lua_pushnumber(L,1);
  lc_add(L,-2,-1);
  lua_remove(L,-2);
  lua_remove(L,-2);
  lua_insert(L,-2);
  lua_settable(L,-3);
  lua_pop(L,1);
  assert(lua_gettop(L) == 1);
  return 0;
}


/* function(x) */
int stuart_RDD_cartesian_2 (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);
  lc_newclosuretable(L,lua_upvalueindex(1));
  enum { lc45 = 2 };
  assert((lua_gettop(L) == lc45));
  lua_pushvalue(L,1);
  lua_rawseti(L,-2,17);

  /* moses.forEach(other:collect(), function(y)
   *       t[#t+1] = {x, y}
   *     end) */
  lc_getupvalue(L,lc45,2,15);
  lua_pushliteral(L,"forEach");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lc_getupvalue(L,lc45,3,14);
  lua_pushliteral(L,"collect");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  lua_pushvalue(L,lc45);
  lua_pushcclosure(L,stuart_RDD_cartesian_1,1);
  lua_call(L,2,0);
  assert(lua_gettop(L) == 2);
  return 0;
}


/* name: RDD:cartesian
 * function(other) */
int stuart_RDD_cartesian (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);
  lc_newclosuretable(L,lua_upvalueindex(1));
  enum { lc42 = 3 };
  assert((lua_gettop(L) == lc42));
  lua_pushvalue(L,2);
  lua_rawseti(L,-2,14);

  /* local moses = require 'moses' */
  lc_newclosuretable(L,lc42);
  enum { lc43 = 4 };
  assert((lua_gettop(L) == lc43));
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  lua_rawseti(L,lc43,15);
  assert(lua_gettop(L) == 4);

  /* local t = {} */
  lc_newclosuretable(L,lc43);
  enum { lc44 = 5 };
  assert((lua_gettop(L) == lc44));
  lua_newtable(L);
  lua_rawseti(L,lc44,16);
  assert(lua_gettop(L) == 5);

  /* moses.forEach(self:collect(), function(x)
   *     moses.forEach(other:collect(), function(y)
   *       t[#t+1] = {x, y}
   *     end)
   *   end) */
  lc_getupvalue(L,lc44,1,15);
  lua_pushliteral(L,"forEach");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_pushvalue(L,1);
  lua_pushliteral(L,"collect");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  lua_pushvalue(L,lc44);
  lua_pushcclosure(L,stuart_RDD_cartesian_2,1);
  lua_call(L,2,0);
  assert(lua_gettop(L) == 5);

  /* return self.context:parallelize(t) */
  const int lc49 = lua_gettop(L);
  lua_pushliteral(L,"context");
  lua_gettable(L,1);
  lua_pushliteral(L,"parallelize");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lc_getupvalue(L,lc44,0,16);
  lua_call(L,2,LUA_MULTRET);
  return (lua_gettop(L) - lc49);
  assert(lua_gettop(L) == 5);
}


/* name: M.classof
 * function(obj) */
int stuart_RDD_classof (lua_State * L) {
  return stuart_class_shared_classof(L, "stuart.RDD");
}


/* name: RDD:coalesce
 * function(numPartitions, shuffle) */
int stuart_RDD_coalesce (lua_State * L) {
  enum { lc_nformalargs = 3 };
  lua_settop(L,3);

  /* local moses = require 'moses' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 4);

  /* if not moses.isBoolean(shuffle) then */
  enum { lc50 = 4 };
  lua_pushliteral(L,"isBoolean");
  lua_gettable(L,4);
  lua_pushvalue(L,3);
  lua_call(L,1,1);
  lua_pushboolean(L,!(lua_toboolean(L,-1)));
  lua_remove(L,-2);
  const int lc51 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc51) {

    /* shuffle = false */
    lua_pushboolean(L,0);
    lua_replace(L,3);
    assert(lua_gettop(L) == 4);
  }
  lua_settop(L,lc50);
  assert(lua_gettop(L) == 4);

  /* return self.context:parallelize(self:collect(), numPartitions) */
  const int lc52 = lua_gettop(L);
  lua_pushliteral(L,"context");
  lua_gettable(L,1);
  lua_pushliteral(L,"parallelize");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_pushvalue(L,1);
  lua_pushliteral(L,"collect");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  lua_pushvalue(L,2);
  lua_call(L,3,LUA_MULTRET);
  return (lua_gettop(L) - lc52);
  assert(lua_gettop(L) == 4);
}


/* function(r, v) */
int stuart_RDD_collect_1 (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* local x = f(v) */
  lc_getupvalue(L,lua_upvalueindex(1),0,18);
  lua_pushvalue(L,2);
  lua_call(L,1,1);
  assert(lua_gettop(L) == 3);

  /* if x ~= nil then */
  enum { lc57 = 3 };
  lua_pushnil(L);
  const int lc58 = lua_equal(L,3,-1);
  lua_pop(L,1);
  lua_pushboolean(L,lc58);
  lua_pushboolean(L,!(lua_toboolean(L,-1)));
  lua_remove(L,-2);
  const int lc59 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc59) {

    /* r[#r+1] = x */
    lua_pushvalue(L,3);
    const double lc60 = lua_objlen(L,1);
    lua_pushnumber(L,lc60);
    lua_pushnumber(L,1);
    lc_add(L,-2,-1);
    lua_remove(L,-2);
    lua_remove(L,-2);
    lua_insert(L,-2);
    lua_settable(L,1);
    assert(lua_gettop(L) == 3);
  }
  lua_settop(L,lc57);
  assert(lua_gettop(L) == 3);

  /* return r */
  lua_pushvalue(L,1);
  return 1;
  assert(lua_gettop(L) == 3);
}


/* name: RDD:collect
 * function(f) */
int stuart_RDD_collect (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);
  lc_newclosuretable(L,lua_upvalueindex(1));
  enum { lc53 = 3 };
  assert((lua_gettop(L) == lc53));
  lua_pushvalue(L,2);
  lua_rawseti(L,-2,18);

  /* local moses = require 'moses' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 4);

  /* local t = moses.tabulate(self:toLocalIterator()) */
  lua_pushliteral(L,"tabulate");
  lua_gettable(L,4);
  const int lc54 = lua_gettop(L);
  lua_pushvalue(L,1);
  lua_pushliteral(L,"toLocalIterator");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,LUA_MULTRET);
  lua_call(L,(lua_gettop(L) - lc54),1);
  assert(lua_gettop(L) == 5);

  /* if moses.isFunction(f) then */
  enum { lc55 = 5 };
  lua_pushliteral(L,"isFunction");
  lua_gettable(L,4);
  lc_getupvalue(L,lc53,0,18);
  lua_call(L,1,1);
  const int lc56 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc56) {

    /* -- reduce, not map, because Lua arrays cannot contain nil
     * t = moses.reduce(t, function(r, v)
     *       local x = f(v)
     *       if x ~= nil then r[#r+1] = x end
     *       return r
     *     end, {}) */
    lua_pushliteral(L,"reduce");
    lua_gettable(L,4);
    lua_pushvalue(L,5);
    lua_pushvalue(L,lc53);
    lua_pushcclosure(L,stuart_RDD_collect_1,1);
    lua_newtable(L);
    lua_call(L,3,1);
    lua_replace(L,5);
    assert(lua_gettop(L) == 5);
  }
  lua_settop(L,lc55);
  assert(lua_gettop(L) == 5);

  /* return t */
  lua_pushvalue(L,5);
  return 1;
  assert(lua_gettop(L) == 5);
}


/* function(e) */
int stuart_RDD_combineByKey_1 (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* return e[1] */
  lua_pushnumber(L,1);
  lua_gettable(L,1);
  return 1;
  assert(lua_gettop(L) == 1);
}


/* function(r2,e) */
int stuart_RDD_combineByKey_2 (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* if e[1] == key then */
  enum { lc74 = 2 };
  lua_pushnumber(L,1);
  lua_gettable(L,2);
  lc_getupvalue(L,lua_upvalueindex(1),0,23);
  const int lc75 = lua_equal(L,-2,-1);
  lua_pop(L,2);
  lua_pushboolean(L,lc75);
  const int lc76 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc76) {

    /* r2[#r2+1] = e[2] */
    lua_pushnumber(L,2);
    lua_gettable(L,2);
    const double lc77 = lua_objlen(L,1);
    lua_pushnumber(L,lc77);
    lua_pushnumber(L,1);
    lc_add(L,-2,-1);
    lua_remove(L,-2);
    lua_remove(L,-2);
    lua_insert(L,-2);
    lua_settable(L,1);
    assert(lua_gettop(L) == 2);
  }
  lua_settop(L,lc74);
  assert(lua_gettop(L) == 2);

  /* return r2 */
  lua_pushvalue(L,1);
  return 1;
  assert(lua_gettop(L) == 2);
}


/* function(r,key) */
int stuart_RDD_combineByKey_3 (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);
  lc_newclosuretable(L,lua_upvalueindex(1));
  enum { lc73 = 3 };
  assert((lua_gettop(L) == lc73));
  lua_pushvalue(L,2);
  lua_rawseti(L,-2,23);

  /* local valuesForKey = moses.reduce(p.data, function(r2,e)
   *         if e[1] == key then r2[#r2+1] = e[2] end
   *         return r2
   *       end, {}) */
  lc_getupvalue(L,lc73,2,21);
  lua_pushliteral(L,"reduce");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lc_getupvalue(L,lc73,1,22);
  lua_pushliteral(L,"data");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_pushvalue(L,lc73);
  lua_pushcclosure(L,stuart_RDD_combineByKey_2,1);
  lua_newtable(L);
  lua_call(L,3,1);
  assert(lua_gettop(L) == 4);

  /* r[key] = moses.reduce(valuesForKey, mergeValue, {}) */
  lc_getupvalue(L,lc73,2,21);
  lua_pushliteral(L,"reduce");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_pushvalue(L,4);
  lc_getupvalue(L,lc73,3,19);
  lua_newtable(L);
  lua_call(L,3,1);
  lc_getupvalue(L,lc73,0,23);
  lua_insert(L,-2);
  lua_settable(L,1);
  assert(lua_gettop(L) == 4);

  /* return r */
  lua_pushvalue(L,1);
  return 1;
  assert(lua_gettop(L) == 4);
}


/* function(p) */
int stuart_RDD_combineByKey_4 (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);
  lc_newclosuretable(L,lua_upvalueindex(1));
  enum { lc70 = 2 };
  assert((lua_gettop(L) == lc70));
  lua_pushvalue(L,1);
  lua_rawseti(L,-2,22);

  /* local keys = moses.uniq(moses.map(p.data, function(e) return e[1] end)) */
  lc_getupvalue(L,lc70,1,21);
  lua_pushliteral(L,"uniq");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  const int lc71 = lua_gettop(L);
  lc_getupvalue(L,lc70,1,21);
  lua_pushliteral(L,"map");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lc_getupvalue(L,lc70,0,22);
  lua_pushliteral(L,"data");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_pushcfunction(L,stuart_RDD_combineByKey_1);
  lua_call(L,2,LUA_MULTRET);
  lua_call(L,(lua_gettop(L) - lc71),1);
  assert(lua_gettop(L) == 3);

  /* local z = moses.reduce(keys, function(r,key)
   *       local valuesForKey = moses.reduce(p.data, function(r2,e)
   *         if e[1] == key then r2[#r2+1] = e[2] end
   *         return r2
   *       end, {})
   *       r[key] = moses.reduce(valuesForKey, mergeValue, {})
   *       return r
   *     end, {}) */
  lc_getupvalue(L,lc70,1,21);
  lua_pushliteral(L,"reduce");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_pushvalue(L,3);
  lua_pushvalue(L,lc70);
  lua_pushcclosure(L,stuart_RDD_combineByKey_3,1);
  lua_newtable(L);
  lua_call(L,3,1);
  assert(lua_gettop(L) == 4);

  /* return z */
  lua_pushvalue(L,4);
  return 1;
  assert(lua_gettop(L) == 4);
}


/* function(r,e) */
int stuart_RDD_combineByKey_5 (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* return moses.append(r, moses.keys(e)) */
  const int lc82 = lua_gettop(L);
  lc_getupvalue(L,lua_upvalueindex(1),1,21);
  lua_pushliteral(L,"append");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  const int lc83 = lua_gettop(L);
  lua_pushvalue(L,1);
  lc_getupvalue(L,lua_upvalueindex(1),1,21);
  lua_pushliteral(L,"keys");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_pushvalue(L,2);
  lua_call(L,1,LUA_MULTRET);
  lua_call(L,(lua_gettop(L) - lc83),LUA_MULTRET);
  return (lua_gettop(L) - lc82);
  assert(lua_gettop(L) == 2);
}


/* function(r2,e) */
int stuart_RDD_combineByKey_6 (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* for k,v in pairs(e) do
   * internal: local f, s, var = explist */
  enum { lc86 = 2 };
  lua_getfield(L,LUA_ENVIRONINDEX,"pairs");
  lua_pushvalue(L,2);
  lua_call(L,1,3);
  while (1) {

    /* internal: local var_1, ..., var_n = f(s, var)
     *           if var_1 == nil then break end
     *           var = var_1 */
    lua_pushvalue(L,-3);
    lua_pushvalue(L,-3);
    lua_pushvalue(L,-3);
    lua_call(L,2,2);
    if (lua_isnil(L,-2)) {
      break;
    }
    lua_pushvalue(L,-2);
    lua_replace(L,-4);

    /* internal: local k with idx 6
     * internal: local v with idx 7 */


    /* if k == key then */
    enum { lc87 = 7 };
    lc_getupvalue(L,lua_upvalueindex(1),0,25);
    const int lc88 = lua_equal(L,6,-1);
    lua_pop(L,1);
    lua_pushboolean(L,lc88);
    const int lc89 = lua_toboolean(L,-1);
    lua_pop(L,1);
    if (lc89) {

      /* r2[#r2+1] = v */
      lua_pushvalue(L,7);
      const double lc90 = lua_objlen(L,1);
      lua_pushnumber(L,lc90);
      lua_pushnumber(L,1);
      lc_add(L,-2,-1);
      lua_remove(L,-2);
      lua_remove(L,-2);
      lua_insert(L,-2);
      lua_settable(L,1);
      assert(lua_gettop(L) == 7);
    }
    lua_settop(L,lc87);
    assert(lua_gettop(L) == 7);

    /* internal: stack cleanup on scope exit */
    lua_pop(L,2);
  }
  lua_settop(L,lc86);
  assert(lua_gettop(L) == 2);

  /* return r2 */
  lua_pushvalue(L,1);
  return 1;
  assert(lua_gettop(L) == 2);
}


/* function(r,key) */
int stuart_RDD_combineByKey_7 (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);
  lc_newclosuretable(L,lua_upvalueindex(1));
  enum { lc85 = 3 };
  assert((lua_gettop(L) == lc85));
  lua_pushvalue(L,2);
  lua_rawseti(L,-2,25);

  /* local valuesForKey = moses.reduce(y, function(r2,e)
   *       for k,v in pairs(e) do
   *         if k == key then r2[#r2+1] = v end
   *       end
   *       return r2
   *     end, {}) */
  lc_getupvalue(L,lc85,2,21);
  lua_pushliteral(L,"reduce");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lc_getupvalue(L,lc85,1,24);
  lua_pushvalue(L,lc85);
  lua_pushcclosure(L,stuart_RDD_combineByKey_6,1);
  lua_newtable(L);
  lua_call(L,3,1);
  assert(lua_gettop(L) == 4);

  /* r[#r+1] = {key, moses.reduce(valuesForKey, mergeCombiners, {})} */
  lua_createtable(L,2,0);
  lc_getupvalue(L,lc85,0,25);
  lua_rawseti(L,-2,1);
  const int lc92 = lua_gettop(L);
  lc_getupvalue(L,lc85,2,21);
  lua_pushliteral(L,"reduce");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_pushvalue(L,4);
  lc_getupvalue(L,lc85,3,20);
  lua_newtable(L);
  lua_call(L,3,LUA_MULTRET);
  while ((lua_gettop(L) > lc92)) {
    lua_rawseti(L,lc92,(1 + (lua_gettop(L) - lc92)));
  }
  const double lc93 = lua_objlen(L,1);
  lua_pushnumber(L,lc93);
  lua_pushnumber(L,1);
  lc_add(L,-2,-1);
  lua_remove(L,-2);
  lua_remove(L,-2);
  lua_insert(L,-2);
  lua_settable(L,1);
  assert(lua_gettop(L) == 4);

  /* return r */
  lua_pushvalue(L,1);
  return 1;
  assert(lua_gettop(L) == 4);
}


/* name: RDD:combineByKey
 * function(createCombiner, mergeValue, mergeCombiners) */
int stuart_RDD_combineByKey (lua_State * L) {
  enum { lc_nformalargs = 4 };
  lua_settop(L,4);
  lc_newclosuretable(L,lua_upvalueindex(1));
  enum { lc64 = 5 };
  assert((lua_gettop(L) == lc64));
  lua_pushvalue(L,3);
  lua_rawseti(L,-2,19);
  lua_pushvalue(L,4);
  lua_rawseti(L,-2,20);

  /* local moses = require 'moses' */
  lc_newclosuretable(L,lc64);
  enum { lc65 = 6 };
  assert((lua_gettop(L) == lc65));
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  lua_rawseti(L,lc65,21);
  assert(lua_gettop(L) == 6);

  /* assert(moses.isFunction(createCombiner)) */
  lua_getfield(L,LUA_ENVIRONINDEX,"assert");
  const int lc66 = lua_gettop(L);
  lc_getupvalue(L,lc65,0,21);
  lua_pushliteral(L,"isFunction");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_pushvalue(L,2);
  lua_call(L,1,LUA_MULTRET);
  lua_call(L,(lua_gettop(L) - lc66),0);
  assert(lua_gettop(L) == 6);

  /* assert(moses.isFunction(mergeValue)) */
  lua_getfield(L,LUA_ENVIRONINDEX,"assert");
  const int lc67 = lua_gettop(L);
  lc_getupvalue(L,lc65,0,21);
  lua_pushliteral(L,"isFunction");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lc_getupvalue(L,lc65,1,19);
  lua_call(L,1,LUA_MULTRET);
  lua_call(L,(lua_gettop(L) - lc67),0);
  assert(lua_gettop(L) == 6);

  /* assert(moses.isFunction(mergeCombiners)) */
  lua_getfield(L,LUA_ENVIRONINDEX,"assert");
  const int lc68 = lua_gettop(L);
  lc_getupvalue(L,lc65,0,21);
  lua_pushliteral(L,"isFunction");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lc_getupvalue(L,lc65,1,20);
  lua_call(L,1,LUA_MULTRET);
  lua_call(L,(lua_gettop(L) - lc68),0);
  assert(lua_gettop(L) == 6);

  /* local y = moses.map(self.partitions, function(p)
   *     local keys = moses.uniq(moses.map(p.data, function(e) return e[1] end))
   *     local z = moses.reduce(keys, function(r,key)
   *       local valuesForKey = moses.reduce(p.data, function(r2,e)
   *         if e[1] == key then r2[#r2+1] = e[2] end
   *         return r2
   *       end, {})
   *       r[key] = moses.reduce(valuesForKey, mergeValue, {})
   *       return r
   *     end, {})
   *     return z
   *   end) */
  lc_newclosuretable(L,lc65);
  enum { lc69 = 7 };
  assert((lua_gettop(L) == lc69));
  lc_getupvalue(L,lc65,0,21);
  lua_pushliteral(L,"map");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_pushliteral(L,"partitions");
  lua_gettable(L,1);
  lua_pushvalue(L,lc65);
  lua_pushcclosure(L,stuart_RDD_combineByKey_4,1);
  lua_call(L,2,1);
  lua_rawseti(L,lc69,24);
  assert(lua_gettop(L) == 7);

  /* local keys = moses.uniq(moses.reduce(y, function(r,e) return moses.append(r, moses.keys(e)) end, {})) */
  lc_getupvalue(L,lc69,1,21);
  lua_pushliteral(L,"uniq");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  const int lc81 = lua_gettop(L);
  lc_getupvalue(L,lc69,1,21);
  lua_pushliteral(L,"reduce");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lc_getupvalue(L,lc69,0,24);
  lua_pushvalue(L,lc69);
  lua_pushcclosure(L,stuart_RDD_combineByKey_5,1);
  lua_newtable(L);
  lua_call(L,3,LUA_MULTRET);
  lua_call(L,(lua_gettop(L) - lc81),1);
  assert(lua_gettop(L) == 8);

  /* local t = moses.reduce(keys, function(r,key)
   *     local valuesForKey = moses.reduce(y, function(r2,e)
   *       for k,v in pairs(e) do
   *         if k == key then r2[#r2+1] = v end
   *       end
   *       return r2
   *     end, {})
   *     r[#r+1] = {key, moses.reduce(valuesForKey, mergeCombiners, {})}
   *     return r
   *   end, {}) */
  lc_getupvalue(L,lc69,1,21);
  lua_pushliteral(L,"reduce");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_pushvalue(L,8);
  lua_pushvalue(L,lc69);
  lua_pushcclosure(L,stuart_RDD_combineByKey_7,1);
  lua_newtable(L);
  lua_call(L,3,1);
  assert(lua_gettop(L) == 9);

  /* return self.context:parallelize(t) */
  const int lc95 = lua_gettop(L);
  lua_pushliteral(L,"context");
  lua_gettable(L,1);
  lua_pushliteral(L,"parallelize");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_pushvalue(L,9);
  lua_call(L,2,LUA_MULTRET);
  return (lua_gettop(L) - lc95);
  assert(lua_gettop(L) == 9);
}


/* function(r, p) */
int stuart_RDD_count_1 (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* return r + p:_count() */
  lua_pushvalue(L,2);
  lua_pushliteral(L,"_count");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  lc_add(L,1,-1);
  lua_remove(L,-2);
  return 1;
  assert(lua_gettop(L) == 2);
}


/* name: RDD:count
 * function() */
int stuart_RDD_count (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* local moses = require 'moses' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 2);

  /* return moses.reduce(self.partitions, function(r, p) return r + p:_count() end, 0) */
  const int lc96 = lua_gettop(L);
  lua_pushliteral(L,"reduce");
  lua_gettable(L,2);
  lua_pushliteral(L,"partitions");
  lua_gettable(L,1);
  lua_pushcfunction(L,stuart_RDD_count_1);
  lua_pushnumber(L,0);
  lua_call(L,3,LUA_MULTRET);
  return (lua_gettop(L) - lc96);
  assert(lua_gettop(L) == 2);
}


/* function(r, e) */
int stuart_RDD_countByKey_1 (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* local k = e[1] */
  lua_pushnumber(L,1);
  lua_gettable(L,2);
  assert(lua_gettop(L) == 3);

  /* if r[k] == nil then */
  enum { lc100 = 3 };
  lua_pushvalue(L,3);
  lua_gettable(L,1);
  lua_pushnil(L);
  const int lc101 = lua_equal(L,-2,-1);
  lua_pop(L,2);
  lua_pushboolean(L,lc101);
  const int lc102 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc102) {

    /* r[k] = 1 */
    lua_pushnumber(L,1);
    lua_pushvalue(L,3);
    lua_insert(L,-2);
    lua_settable(L,1);
    assert(lua_gettop(L) == 3);
  }
  else {

    /* else
     * r[k] = r[k] + 1 */
    lua_pushvalue(L,3);
    lua_gettable(L,1);
    lua_pushnumber(L,1);
    lc_add(L,-2,-1);
    lua_remove(L,-2);
    lua_remove(L,-2);
    lua_pushvalue(L,3);
    lua_insert(L,-2);
    lua_settable(L,1);
    assert(lua_gettop(L) == 3);
  }
  lua_settop(L,lc100);
  assert(lua_gettop(L) == 3);

  /* return r */
  lua_pushvalue(L,1);
  return 1;
  assert(lua_gettop(L) == 3);
}


/* name: RDD:countByKey
 * function() */
int stuart_RDD_countByKey (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* local moses = require 'moses' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 2);

  /* return moses.reduce(self:collect(), function(r, e)
   *     local k = e[1]
   *     if r[k] == nil then
   *       r[k] = 1
   *     else
   *       r[k] = r[k] + 1
   *     end
   *     return r
   *   end, {}) */
  const int lc99 = lua_gettop(L);
  lua_pushliteral(L,"reduce");
  lua_gettable(L,2);
  lua_pushvalue(L,1);
  lua_pushliteral(L,"collect");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  lua_pushcfunction(L,stuart_RDD_countByKey_1);
  lua_newtable(L);
  lua_call(L,3,LUA_MULTRET);
  return (lua_gettop(L) - lc99);
  assert(lua_gettop(L) == 2);
}


/* function(r, n) */
int stuart_RDD_countByValue_1 (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* if r[n] == nil then */
  enum { lc105 = 2 };
  lua_pushvalue(L,2);
  lua_gettable(L,1);
  lua_pushnil(L);
  const int lc106 = lua_equal(L,-2,-1);
  lua_pop(L,2);
  lua_pushboolean(L,lc106);
  const int lc107 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc107) {

    /* r[n] = 1 */
    lua_pushnumber(L,1);
    lua_pushvalue(L,2);
    lua_insert(L,-2);
    lua_settable(L,1);
    assert(lua_gettop(L) == 2);
  }
  else {

    /* else
     * r[n] = r[n] + 1 */
    lua_pushvalue(L,2);
    lua_gettable(L,1);
    lua_pushnumber(L,1);
    lc_add(L,-2,-1);
    lua_remove(L,-2);
    lua_remove(L,-2);
    lua_pushvalue(L,2);
    lua_insert(L,-2);
    lua_settable(L,1);
    assert(lua_gettop(L) == 2);
  }
  lua_settop(L,lc105);
  assert(lua_gettop(L) == 2);

  /* return r */
  lua_pushvalue(L,1);
  return 1;
  assert(lua_gettop(L) == 2);
}


/* name: RDD:countByValue
 * function() */
int stuart_RDD_countByValue (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* local moses = require 'moses' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 2);

  /* return moses.reduce(self:collect(), function(r, n)
   *     if r[n] == nil then
   *       r[n] = 1
   *     else
   *       r[n] = r[n] + 1
   *     end
   *     return r
   *   end, {}) */
  const int lc104 = lua_gettop(L);
  lua_pushliteral(L,"reduce");
  lua_gettable(L,2);
  lua_pushvalue(L,1);
  lua_pushliteral(L,"collect");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  lua_pushcfunction(L,stuart_RDD_countByValue_1);
  lua_newtable(L);
  lua_call(L,3,LUA_MULTRET);
  return (lua_gettop(L) - lc104);
  assert(lua_gettop(L) == 2);
}


/* name: RDD:distinct
 * function(numPartitions) */
int stuart_RDD_distinct (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* local moses = require 'moses' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 3);

  /* local t = moses.uniq(self:collect()) */
  lua_pushliteral(L,"uniq");
  lua_gettable(L,3);
  const int lc109 = lua_gettop(L);
  lua_pushvalue(L,1);
  lua_pushliteral(L,"collect");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,LUA_MULTRET);
  lua_call(L,(lua_gettop(L) - lc109),1);
  assert(lua_gettop(L) == 4);

  /* return self.context:parallelize(t, numPartitions) */
  const int lc110 = lua_gettop(L);
  lua_pushliteral(L,"context");
  lua_gettable(L,1);
  lua_pushliteral(L,"parallelize");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_pushvalue(L,4);
  lua_pushvalue(L,2);
  lua_call(L,3,LUA_MULTRET);
  return (lua_gettop(L) - lc110);
  assert(lua_gettop(L) == 4);
}


/* function(v) */
int stuart_RDD_filter_1 (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* return f(v) */
  const int lc112 = lua_gettop(L);
  lc_getupvalue(L,lua_upvalueindex(1),0,26);
  lua_pushvalue(L,1);
  lua_call(L,1,LUA_MULTRET);
  return (lua_gettop(L) - lc112);
  assert(lua_gettop(L) == 1);
}


/* name: RDD:filter
 * function(f) */
int stuart_RDD_filter (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);
  lc_newclosuretable(L,lua_upvalueindex(1));
  enum { lc111 = 3 };
  assert((lua_gettop(L) == lc111));
  lua_pushvalue(L,2);
  lua_rawseti(L,-2,26);

  /* local moses = require 'moses' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 4);

  /* local t = moses.filter(self:collect(), function(v) return f(v) end) */
  lua_pushliteral(L,"filter");
  lua_gettable(L,4);
  lua_pushvalue(L,1);
  lua_pushliteral(L,"collect");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  lua_pushvalue(L,lc111);
  lua_pushcclosure(L,stuart_RDD_filter_1,1);
  lua_call(L,2,1);
  assert(lua_gettop(L) == 5);

  /* return self.context:parallelize(t) */
  const int lc114 = lua_gettop(L);
  lua_pushliteral(L,"context");
  lua_gettable(L,1);
  lua_pushliteral(L,"parallelize");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_pushvalue(L,5);
  lua_call(L,2,LUA_MULTRET);
  return (lua_gettop(L) - lc114);
  assert(lua_gettop(L) == 5);
}


/* name: RDD:first
 * function() */
int stuart_RDD_first (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* return self.partitions[1].data[1] */
  lua_pushliteral(L,"partitions");
  lua_gettable(L,1);
  lua_pushnumber(L,1);
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_pushliteral(L,"data");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_pushnumber(L,1);
  lua_gettable(L,-2);
  lua_remove(L,-2);
  return 1;
  assert(lua_gettop(L) == 1);
}


/* name: RDD:flatMap
 * function(f) */
int stuart_RDD_flatMap (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* return self:map(f):_flatten() */
  const int lc122 = lua_gettop(L);
  lua_pushvalue(L,1);
  lua_pushliteral(L,"map");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_pushvalue(L,2);
  lua_call(L,2,1);
  lua_pushliteral(L,"_flatten");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,LUA_MULTRET);
  return (lua_gettop(L) - lc122);
  assert(lua_gettop(L) == 2);
}


/* name: RDD:flatMapValues
 * function(f) */
int stuart_RDD_flatMapValues (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* return self:mapValues(f):_flattenValues() */
  const int lc123 = lua_gettop(L);
  lua_pushvalue(L,1);
  lua_pushliteral(L,"mapValues");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_pushvalue(L,2);
  lua_call(L,2,1);
  lua_pushliteral(L,"_flattenValues");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,LUA_MULTRET);
  return (lua_gettop(L) - lc123);
  assert(lua_gettop(L) == 2);
}


/* name: RDD:fold
 * function(zeroValue, op) */
int stuart_RDD_fold (lua_State * L) {
  enum { lc_nformalargs = 3 };
  lua_settop(L,3);

  /* local moses = require 'moses' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 4);

  /* return moses.reduce(self:collect(), op, zeroValue) */
  const int lc124 = lua_gettop(L);
  lua_pushliteral(L,"reduce");
  lua_gettable(L,4);
  lua_pushvalue(L,1);
  lua_pushliteral(L,"collect");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  lua_pushvalue(L,3);
  lua_pushvalue(L,2);
  lua_call(L,3,LUA_MULTRET);
  return (lua_gettop(L) - lc124);
  assert(lua_gettop(L) == 4);
}


/* function(e) */
int stuart_RDD_foldByKey_1 (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* return e[1] */
  lua_pushnumber(L,1);
  lua_gettable(L,1);
  return 1;
  assert(lua_gettop(L) == 1);
}


/* function(e) */
int stuart_RDD_foldByKey_2 (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* if e[1] == k then */
  enum { lc130 = 1 };
  lua_pushnumber(L,1);
  lua_gettable(L,1);
  lc_getupvalue(L,lua_upvalueindex(1),0,34);
  const int lc131 = lua_equal(L,-2,-1);
  lua_pop(L,2);
  lua_pushboolean(L,lc131);
  const int lc132 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc132) {

    /* return e[2] */
    lua_pushnumber(L,2);
    lua_gettable(L,1);
    return 1;
    assert(lua_gettop(L) == 1);
  }
  lua_settop(L,lc130);
  assert(lua_gettop(L) == 1);
  return 0;
}


/* function(k) */
int stuart_RDD_foldByKey_3 (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);
  lc_newclosuretable(L,lua_upvalueindex(1));
  enum { lc129 = 2 };
  assert((lua_gettop(L) == lc129));
  lua_pushvalue(L,1);
  lua_rawseti(L,-2,34);

  /* local c = moses.map(self:collect(), function(e)
   *       if e[1] == k then return e[2] end
   *     end) */
  lc_getupvalue(L,lc129,1,33);
  lua_pushliteral(L,"map");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lc_getupvalue(L,lc129,2,30);
  lua_pushliteral(L,"collect");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  lua_pushvalue(L,lc129);
  lua_pushcclosure(L,stuart_RDD_foldByKey_2,1);
  lua_call(L,2,1);
  assert(lua_gettop(L) == 3);

  /* return {k, moses.reduce(c, op, zeroValue)} */
  lua_createtable(L,2,0);
  lc_getupvalue(L,lc129,0,34);
  lua_rawseti(L,-2,1);
  const int lc134 = lua_gettop(L);
  lc_getupvalue(L,lc129,1,33);
  lua_pushliteral(L,"reduce");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_pushvalue(L,3);
  lc_getupvalue(L,lc129,2,32);
  lc_getupvalue(L,lc129,2,31);
  lua_call(L,3,LUA_MULTRET);
  while ((lua_gettop(L) > lc134)) {
    lua_rawseti(L,lc134,(1 + (lua_gettop(L) - lc134)));
  }
  return 1;
  assert(lua_gettop(L) == 3);
}


/* name: RDD:foldByKey
 * function(zeroValue, op) */
int stuart_RDD_foldByKey (lua_State * L) {
  enum { lc_nformalargs = 3 };
  lua_settop(L,3);
  lc_newclosuretable(L,lua_upvalueindex(1));
  enum { lc125 = 4 };
  assert((lua_gettop(L) == lc125));
  lua_pushvalue(L,1);
  lua_rawseti(L,-2,30);
  lua_pushvalue(L,2);
  lua_rawseti(L,-2,31);
  lua_pushvalue(L,3);
  lua_rawseti(L,-2,32);

  /* local moses = require 'moses' */
  lc_newclosuretable(L,lc125);
  enum { lc126 = 5 };
  assert((lua_gettop(L) == lc126));
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  lua_rawseti(L,lc126,33);
  assert(lua_gettop(L) == 5);

  /* local keys = moses.unique(moses.map(self:collect(), function(e) return e[1] end)) */
  lc_getupvalue(L,lc126,0,33);
  lua_pushliteral(L,"unique");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  const int lc127 = lua_gettop(L);
  lc_getupvalue(L,lc126,0,33);
  lua_pushliteral(L,"map");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lc_getupvalue(L,lc126,1,30);
  lua_pushliteral(L,"collect");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  lua_pushcfunction(L,stuart_RDD_foldByKey_1);
  lua_call(L,2,LUA_MULTRET);
  lua_call(L,(lua_gettop(L) - lc127),1);
  assert(lua_gettop(L) == 6);

  /* local t = moses.map(keys, function(k)
   *     local c = moses.map(self:collect(), function(e)
   *       if e[1] == k then return e[2] end
   *     end)
   *     return {k, moses.reduce(c, op, zeroValue)}
   *   end) */
  lc_getupvalue(L,lc126,0,33);
  lua_pushliteral(L,"map");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_pushvalue(L,6);
  lua_pushvalue(L,lc126);
  lua_pushcclosure(L,stuart_RDD_foldByKey_3,1);
  lua_call(L,2,1);
  assert(lua_gettop(L) == 7);

  /* return self.context:parallelize(t) */
  const int lc136 = lua_gettop(L);
  lc_getupvalue(L,lc126,1,30);
  lua_pushliteral(L,"context");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_pushliteral(L,"parallelize");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_pushvalue(L,7);
  lua_call(L,2,LUA_MULTRET);
  return (lua_gettop(L) - lc136);
  assert(lua_gettop(L) == 7);
}


/* name: RDD:foreach
 * function(f) */
int stuart_RDD_foreach (lua_State * L) {
  lua_checkstack(L,22);
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* for _, p in ipairs(self.partitions) do
   * internal: local f, s, var = explist */
  enum { lc137 = 2 };
  lua_getfield(L,LUA_ENVIRONINDEX,"ipairs");
  lua_pushliteral(L,"partitions");
  lua_gettable(L,1);
  lua_call(L,1,3);
  while (1) {

    /* internal: local var_1, ..., var_n = f(s, var)
     *           if var_1 == nil then break end
     *           var = var_1 */
    lua_pushvalue(L,-3);
    lua_pushvalue(L,-3);
    lua_pushvalue(L,-3);
    lua_call(L,2,2);
    if (lua_isnil(L,-2)) {
      break;
    }
    lua_pushvalue(L,-2);
    lua_replace(L,-4);

    /* internal: local _ with idx 6
     * internal: local p with idx 7 */


    /* for i, _ in ipairs(p.data) do
     * internal: local f, s, var = explist */
    enum { lc138 = 7 };
    lua_getfield(L,LUA_ENVIRONINDEX,"ipairs");
    lua_pushliteral(L,"data");
    lua_gettable(L,7);
    lua_call(L,1,3);
    while (1) {

      /* internal: local var_1, ..., var_n = f(s, var)
       *           if var_1 == nil then break end
       *           var = var_1 */
      lua_pushvalue(L,-3);
      lua_pushvalue(L,-3);
      lua_pushvalue(L,-3);
      lua_call(L,2,2);
      if (lua_isnil(L,-2)) {
        break;
      }
      lua_pushvalue(L,-2);
      lua_replace(L,-4);

      /* internal: local i with idx 11
       * internal: local _ with idx 12 */


      /* f(p.data[i]) */
      lua_pushvalue(L,2);
      lua_pushliteral(L,"data");
      lua_gettable(L,7);
      lua_pushvalue(L,11);
      lua_gettable(L,-2);
      lua_remove(L,-2);
      lua_call(L,1,0);
      assert(lua_gettop(L) == 12);

      /* internal: stack cleanup on scope exit */
      lua_pop(L,2);
    }
    lua_settop(L,lc138);
    assert(lua_gettop(L) == 7);

    /* internal: stack cleanup on scope exit */
    lua_pop(L,2);
  }
  lua_settop(L,lc137);
  assert(lua_gettop(L) == 2);
  return 0;
}


/* name: RDD:foreachPartition
 * function(f) */
int stuart_RDD_foreachPartition (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* for _, p in ipairs(self.partitions) do
   * internal: local f, s, var = explist */
  enum { lc139 = 2 };
  lua_getfield(L,LUA_ENVIRONINDEX,"ipairs");
  lua_pushliteral(L,"partitions");
  lua_gettable(L,1);
  lua_call(L,1,3);
  while (1) {

    /* internal: local var_1, ..., var_n = f(s, var)
     *           if var_1 == nil then break end
     *           var = var_1 */
    lua_pushvalue(L,-3);
    lua_pushvalue(L,-3);
    lua_pushvalue(L,-3);
    lua_call(L,2,2);
    if (lua_isnil(L,-2)) {
      break;
    }
    lua_pushvalue(L,-2);
    lua_replace(L,-4);

    /* internal: local _ with idx 6
     * internal: local p with idx 7 */


    /* f(p.data) */
    lua_pushvalue(L,2);
    lua_pushliteral(L,"data");
    lua_gettable(L,7);
    lua_call(L,1,0);
    assert(lua_gettop(L) == 7);

    /* internal: stack cleanup on scope exit */
    lua_pop(L,2);
  }
  lua_settop(L,lc139);
  assert(lua_gettop(L) == 2);
  return 0;
}


/* function(r, e) */
int stuart_RDD_groupByKey_1 (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* if e[1] == k then */
  enum { lc161 = 2 };
  lua_pushnumber(L,1);
  lua_gettable(L,2);
  lc_getupvalue(L,lua_upvalueindex(1),0,41);
  const int lc162 = lua_equal(L,-2,-1);
  lua_pop(L,2);
  lua_pushboolean(L,lc162);
  const int lc163 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc163) {

    /* r[#r+1] = e[2] */
    lua_pushnumber(L,2);
    lua_gettable(L,2);
    const double lc164 = lua_objlen(L,1);
    lua_pushnumber(L,lc164);
    lua_pushnumber(L,1);
    lc_add(L,-2,-1);
    lua_remove(L,-2);
    lua_remove(L,-2);
    lua_insert(L,-2);
    lua_settable(L,1);
    assert(lua_gettop(L) == 2);
  }
  lua_settop(L,lc161);
  assert(lua_gettop(L) == 2);

  /* return r */
  lua_pushvalue(L,1);
  return 1;
  assert(lua_gettop(L) == 2);
}


/* function(p) */
int stuart_RDD_glom_1 (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* return p.data */
  lua_pushliteral(L,"data");
  lua_gettable(L,1);
  return 1;
  assert(lua_gettop(L) == 1);
}


/* name: RDD:glom
 * function() */
int stuart_RDD_glom (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* local moses = require 'moses' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 2);

  /* local t = moses.map(self.partitions, function(p) return p.data end) */
  lua_pushliteral(L,"map");
  lua_gettable(L,2);
  lua_pushliteral(L,"partitions");
  lua_gettable(L,1);
  lua_pushcfunction(L,stuart_RDD_glom_1);
  lua_call(L,2,1);
  assert(lua_gettop(L) == 3);

  /* return self.context:parallelize(t) */
  const int lc141 = lua_gettop(L);
  lua_pushliteral(L,"context");
  lua_gettable(L,1);
  lua_pushliteral(L,"parallelize");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_pushvalue(L,3);
  lua_call(L,2,LUA_MULTRET);
  return (lua_gettop(L) - lc141);
  assert(lua_gettop(L) == 3);
}


/* function(v) */
int stuart_RDD_groupBy_1 (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* return f(v) */
  const int lc146 = lua_gettop(L);
  lc_getupvalue(L,lua_upvalueindex(1),2,35);
  lua_pushvalue(L,1);
  lua_call(L,1,LUA_MULTRET);
  return (lua_gettop(L) - lc146);
  assert(lua_gettop(L) == 1);
}


/* function(r, e) */
int stuart_RDD_groupBy_2 (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* if f(e) == k then */
  enum { lc149 = 2 };
  lc_getupvalue(L,lua_upvalueindex(1),3,35);
  lua_pushvalue(L,2);
  lua_call(L,1,1);
  lc_getupvalue(L,lua_upvalueindex(1),0,38);
  const int lc150 = lua_equal(L,-2,-1);
  lua_pop(L,2);
  lua_pushboolean(L,lc150);
  const int lc151 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc151) {

    /* r[#r+1] = e */
    lua_pushvalue(L,2);
    const double lc152 = lua_objlen(L,1);
    lua_pushnumber(L,lc152);
    lua_pushnumber(L,1);
    lc_add(L,-2,-1);
    lua_remove(L,-2);
    lua_remove(L,-2);
    lua_insert(L,-2);
    lua_settable(L,1);
    assert(lua_gettop(L) == 2);
  }
  lua_settop(L,lc149);
  assert(lua_gettop(L) == 2);

  /* return r */
  lua_pushvalue(L,1);
  return 1;
  assert(lua_gettop(L) == 2);
}


/* function(k) */
int stuart_RDD_groupBy_3 (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);
  lc_newclosuretable(L,lua_upvalueindex(1));
  enum { lc148 = 2 };
  assert((lua_gettop(L) == lc148));
  lua_pushvalue(L,1);
  lua_rawseti(L,-2,38);

  /* local v = moses.reduce(x, function(r, e)
   *       if f(e) == k then r[#r+1] = e end
   *       return r
   *     end, {}) */
  lc_getupvalue(L,lc148,1,37);
  lua_pushliteral(L,"reduce");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lc_getupvalue(L,lc148,2,36);
  lua_pushvalue(L,lc148);
  lua_pushcclosure(L,stuart_RDD_groupBy_2,1);
  lua_newtable(L);
  lua_call(L,3,1);
  assert(lua_gettop(L) == 3);

  /* return {k, v} */
  lua_createtable(L,2,0);
  lc_getupvalue(L,lc148,0,38);
  lua_rawseti(L,-2,1);
  lua_pushvalue(L,3);
  lua_rawseti(L,-2,2);
  return 1;
  assert(lua_gettop(L) == 3);
}


/* name: RDD:groupBy
 * function(f) */
int stuart_RDD_groupBy (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);
  lc_newclosuretable(L,lua_upvalueindex(1));
  enum { lc142 = 3 };
  assert((lua_gettop(L) == lc142));
  lua_pushvalue(L,2);
  lua_rawseti(L,-2,35);

  /* local x = self:collect() */
  lc_newclosuretable(L,lc142);
  enum { lc143 = 4 };
  assert((lua_gettop(L) == lc143));
  lua_pushvalue(L,1);
  lua_pushliteral(L,"collect");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  lua_rawseti(L,lc143,36);
  assert(lua_gettop(L) == 4);

  /* local moses = require 'moses' */
  lc_newclosuretable(L,lc143);
  enum { lc144 = 5 };
  assert((lua_gettop(L) == lc144));
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  lua_rawseti(L,lc144,37);
  assert(lua_gettop(L) == 5);

  /* local keys = moses.unique(moses.map(x, function(v) return f(v) end)) */
  lc_getupvalue(L,lc144,0,37);
  lua_pushliteral(L,"unique");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  const int lc145 = lua_gettop(L);
  lc_getupvalue(L,lc144,0,37);
  lua_pushliteral(L,"map");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lc_getupvalue(L,lc144,1,36);
  lua_pushvalue(L,lc144);
  lua_pushcclosure(L,stuart_RDD_groupBy_1,1);
  lua_call(L,2,LUA_MULTRET);
  lua_call(L,(lua_gettop(L) - lc145),1);
  assert(lua_gettop(L) == 6);

  /* local t = moses.map(keys, function(k)
   *     local v = moses.reduce(x, function(r, e)
   *       if f(e) == k then r[#r+1] = e end
   *       return r
   *     end, {})
   *     return {k, v}
   *   end) */
  lc_getupvalue(L,lc144,0,37);
  lua_pushliteral(L,"map");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_pushvalue(L,6);
  lua_pushvalue(L,lc144);
  lua_pushcclosure(L,stuart_RDD_groupBy_3,1);
  lua_call(L,2,1);
  assert(lua_gettop(L) == 7);

  /* return self.context:parallelize(t) */
  const int lc155 = lua_gettop(L);
  lua_pushliteral(L,"context");
  lua_gettable(L,1);
  lua_pushliteral(L,"parallelize");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_pushvalue(L,7);
  lua_call(L,2,LUA_MULTRET);
  return (lua_gettop(L) - lc155);
  assert(lua_gettop(L) == 7);
}


/* function(k) */
int stuart_RDD_groupByKey_2 (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);
  lc_newclosuretable(L,lua_upvalueindex(1));
  enum { lc160 = 2 };
  assert((lua_gettop(L) == lc160));
  lua_pushvalue(L,1);
  lua_rawseti(L,-2,41);

  /* local v = moses.reduce(self:collect(), function(r, e)
   *       if e[1] == k then r[#r+1] = e[2] end
   *       return r
   *     end, {}) */
  lc_getupvalue(L,lc160,1,40);
  lua_pushliteral(L,"reduce");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lc_getupvalue(L,lc160,2,39);
  lua_pushliteral(L,"collect");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  lua_pushvalue(L,lc160);
  lua_pushcclosure(L,stuart_RDD_groupByKey_1,1);
  lua_newtable(L);
  lua_call(L,3,1);
  assert(lua_gettop(L) == 3);

  /* return {k, v} */
  lua_createtable(L,2,0);
  lc_getupvalue(L,lc160,0,41);
  lua_rawseti(L,-2,1);
  lua_pushvalue(L,3);
  lua_rawseti(L,-2,2);
  return 1;
  assert(lua_gettop(L) == 3);
}


/* name: RDD:groupByKey
 * function(numPartitions) */
int stuart_RDD_groupByKey (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);
  lc_newclosuretable(L,lua_upvalueindex(1));
  enum { lc156 = 3 };
  assert((lua_gettop(L) == lc156));
  lua_pushvalue(L,1);
  lua_rawseti(L,-2,39);

  /* numPartitions = numPartitions or #self.partitions */
  lua_pushvalue(L,2);
  if (!(lua_toboolean(L,-1))) {
    lua_pop(L,1);
    lc_getupvalue(L,lc156,0,39);
    lua_pushliteral(L,"partitions");
    lua_gettable(L,-2);
    lua_remove(L,-2);
    const double lc157 = lua_objlen(L,-1);
    lua_pop(L,1);
    lua_pushnumber(L,lc157);
  }
  lua_replace(L,2);
  assert(lua_gettop(L) == 3);

  /* local moses = require 'moses' */
  lc_newclosuretable(L,lc156);
  enum { lc158 = 4 };
  assert((lua_gettop(L) == lc158));
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  lua_rawseti(L,lc158,40);
  assert(lua_gettop(L) == 4);

  /* local keys = moses.keys(self:_dict()) */
  lc_getupvalue(L,lc158,0,40);
  lua_pushliteral(L,"keys");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  const int lc159 = lua_gettop(L);
  lc_getupvalue(L,lc158,1,39);
  lua_pushliteral(L,"_dict");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,LUA_MULTRET);
  lua_call(L,(lua_gettop(L) - lc159),1);
  assert(lua_gettop(L) == 5);

  /* local t = moses.map(keys, function(k)
   *     local v = moses.reduce(self:collect(), function(r, e)
   *       if e[1] == k then r[#r+1] = e[2] end
   *       return r
   *     end, {})
   *     return {k, v}
   *   end) */
  lc_getupvalue(L,lc158,0,40);
  lua_pushliteral(L,"map");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_pushvalue(L,5);
  lua_pushvalue(L,lc158);
  lua_pushcclosure(L,stuart_RDD_groupByKey_2,1);
  lua_call(L,2,1);
  assert(lua_gettop(L) == 6);

  /* return self.context:parallelize(t, numPartitions) */
  const int lc167 = lua_gettop(L);
  lc_getupvalue(L,lc158,1,39);
  lua_pushliteral(L,"context");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_pushliteral(L,"parallelize");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_pushvalue(L,6);
  lua_pushvalue(L,2);
  lua_call(L,3,LUA_MULTRET);
  return (lua_gettop(L) - lc167);
  assert(lua_gettop(L) == 6);
}


/* function(x) */
int stuart_RDD__histogram_1 (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* for i = 1, num_buckets, 1 do */
  lua_pushnumber(L,1);
  lc_getupvalue(L,lua_upvalueindex(1),1,46);
  lua_pushnumber(L,1);
  if (!(((lua_isnumber(L,-3) && lua_isnumber(L,-2)) && lua_isnumber(L,-1)))) {
    luaL_error(L,"'for' limit must be a number");
  }
  double lc178_var = lua_tonumber(L,-3);
  const double lc179_limit = lua_tonumber(L,-2);
  const double lc180_step = lua_tonumber(L,-1);
  lua_pop(L,3);
  enum { lc181 = 1 };
  while ((((lc180_step > 0) && (lc178_var <= lc179_limit)) || ((lc180_step <= 0) && (lc178_var >= lc179_limit)))) {

    /* internal: local i at index 2 */
    lua_pushnumber(L,lc178_var);

    /* local shouldAdd */
    lua_settop(L,(lua_gettop(L) + 1));
    assert(lua_gettop(L) == 3);

    /* local lastBucket = i == num_buckets */
    lua_pushvalue(L,2);
    lc_getupvalue(L,lua_upvalueindex(1),1,46);
    const int lc182 = lua_equal(L,-2,-1);
    lua_pop(L,2);
    lua_pushboolean(L,lc182);
    assert(lua_gettop(L) == 4);

    /* if lastBucket then */
    enum { lc183 = 4 };
    if (lua_toboolean(L,4)) {

      /* -- last bucket is inclusive
       * shouldAdd = x >= buckets[i] and x <= buckets[i+1] */
      lc_getupvalue(L,lua_upvalueindex(1),2,45);
      lua_pushvalue(L,2);
      lua_gettable(L,-2);
      lua_remove(L,-2);
      const int lc184 = lc_le(L,-1,1);
      lua_pop(L,1);
      lua_pushboolean(L,lc184);
      if (lua_toboolean(L,-1)) {
        lua_pop(L,1);
        lc_getupvalue(L,lua_upvalueindex(1),2,45);
        lua_pushnumber(L,1);
        lc_add(L,2,-1);
        lua_remove(L,-2);
        lua_gettable(L,-2);
        lua_remove(L,-2);
        const int lc185 = lc_le(L,1,-1);
        lua_pop(L,1);
        lua_pushboolean(L,lc185);
      }
      lua_replace(L,3);
      assert(lua_gettop(L) == 4);
    }
    else {

      /* else
       * shouldAdd = x >= buckets[i] and x < buckets[i+1] */
      lc_getupvalue(L,lua_upvalueindex(1),2,45);
      lua_pushvalue(L,2);
      lua_gettable(L,-2);
      lua_remove(L,-2);
      const int lc186 = lc_le(L,-1,1);
      lua_pop(L,1);
      lua_pushboolean(L,lc186);
      if (lua_toboolean(L,-1)) {
        lua_pop(L,1);
        lc_getupvalue(L,lua_upvalueindex(1),2,45);
        lua_pushnumber(L,1);
        lc_add(L,2,-1);
        lua_remove(L,-2);
        lua_gettable(L,-2);
        lua_remove(L,-2);
        const int lc187 = lua_lessthan(L,1,-1);
        lua_pop(L,1);
        lua_pushboolean(L,lc187);
      }
      lua_replace(L,3);
      assert(lua_gettop(L) == 4);
    }
    lua_settop(L,lc183);
    assert(lua_gettop(L) == 4);

    /* if shouldAdd then */
    enum { lc188 = 4 };
    if (lua_toboolean(L,3)) {

      /* h[i] = h[i] + 1 */
      lc_getupvalue(L,lua_upvalueindex(1),0,47);
      lua_pushvalue(L,2);
      lua_gettable(L,-2);
      lua_remove(L,-2);
      lua_pushnumber(L,1);
      lc_add(L,-2,-1);
      lua_remove(L,-2);
      lua_remove(L,-2);
      lc_getupvalue(L,lua_upvalueindex(1),0,47);
      lua_insert(L,-2);
      lua_pushvalue(L,2);
      lua_insert(L,-2);
      lua_settable(L,-3);
      lua_pop(L,1);
      assert(lua_gettop(L) == 4);
    }
    lua_settop(L,lc188);
    assert(lua_gettop(L) == 4);

    /* internal: stack cleanup on scope exit */
    lua_pop(L,3);
    lc178_var += lc180_step;
  }
  lua_settop(L,lc181);
  assert(lua_gettop(L) == 1);
  return 0;
}


/* name: RDD:_histogram
 * function(buckets) */
int stuart_RDD__histogram (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);
  lc_newclosuretable(L,lua_upvalueindex(1));
  enum { lc174 = 3 };
  assert((lua_gettop(L) == lc174));
  lua_pushvalue(L,2);
  lua_rawseti(L,-2,45);

  /* local num_buckets = #buckets - 1 */
  lc_newclosuretable(L,lc174);
  enum { lc175 = 4 };
  assert((lua_gettop(L) == lc175));
  lc_getupvalue(L,lc174,0,45);
  const double lc176 = lua_objlen(L,-1);
  lua_pop(L,1);
  lua_pushnumber(L,lc176);
  lua_pushnumber(L,1);
  lc_sub(L,-2,-1);
  lua_remove(L,-2);
  lua_remove(L,-2);
  lua_rawseti(L,lc175,46);
  assert(lua_gettop(L) == 4);

  /* local moses = require 'moses' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 5);

  /* local h = {} */
  lc_newclosuretable(L,lc175);
  enum { lc177 = 6 };
  assert((lua_gettop(L) == lc177));
  lua_newtable(L);
  lua_rawseti(L,lc177,47);
  assert(lua_gettop(L) == 6);

  /* moses.fill(h, 0, 1, num_buckets) */
  lua_pushliteral(L,"fill");
  lua_gettable(L,5);
  lc_getupvalue(L,lc177,0,47);
  lua_pushnumber(L,0);
  lua_pushnumber(L,1);
  lc_getupvalue(L,lc177,1,46);
  lua_call(L,4,0);
  assert(lua_gettop(L) == 6);

  /* moses.forEach(self:collect(), function(x)
   *     for i = 1, num_buckets, 1 do
   *       local shouldAdd
   *       local lastBucket = i == num_buckets
   *       if lastBucket then -- last bucket is inclusive
   *         shouldAdd = x >= buckets[i] and x <= buckets[i+1]
   *       else
   *         shouldAdd = x >= buckets[i] and x < buckets[i+1]
   *       end
   *       if shouldAdd then h[i] = h[i] + 1 end
   *     end
   *   end) */
  lua_pushliteral(L,"forEach");
  lua_gettable(L,5);
  lua_pushvalue(L,1);
  lua_pushliteral(L,"collect");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  lua_pushvalue(L,lc177);
  lua_pushcclosure(L,stuart_RDD__histogram_1,1);
  lua_call(L,2,0);
  assert(lua_gettop(L) == 6);

  /* return h */
  lc_getupvalue(L,lc177,0,47);
  return 1;
  assert(lua_gettop(L) == 6);
}


/* function(v) */
int stuart_RDD_histogram_1 (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* return min_v + v*(max_v-min_v)/num_buckets */
  lc_getupvalue(L,lua_upvalueindex(1),1,43);
  lc_getupvalue(L,lua_upvalueindex(1),0,44);
  lc_getupvalue(L,lua_upvalueindex(1),1,43);
  lc_sub(L,-2,-1);
  lua_remove(L,-2);
  lua_remove(L,-2);
  lc_mul(L,1,-1);
  lua_remove(L,-2);
  lc_getupvalue(L,lua_upvalueindex(1),2,42);
  lc_div(L,-2,-1);
  lua_remove(L,-2);
  lua_remove(L,-2);
  lc_add(L,-2,-1);
  lua_remove(L,-2);
  lua_remove(L,-2);
  return 1;
  assert(lua_gettop(L) == 1);
}


/* name: RDD:histogram
 * function(buckets) */
int stuart_RDD_histogram (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* local moses = require 'moses' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 3);

  /* if moses.isNumber(buckets) then */
  enum { lc168 = 3 };
  lua_pushliteral(L,"isNumber");
  lua_gettable(L,3);
  lua_pushvalue(L,2);
  lua_call(L,1,1);
  const int lc169 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc169) {

    /* local num_buckets = buckets */
    lc_newclosuretable(L,lua_upvalueindex(1));
    enum { lc170 = 4 };
    assert((lua_gettop(L) == lc170));
    lua_pushvalue(L,2);
    lua_rawseti(L,lc170,42);
    assert(lua_gettop(L) == 4);

    /* local min_v = self:min() */
    lc_newclosuretable(L,lc170);
    enum { lc171 = 5 };
    assert((lua_gettop(L) == lc171));
    lua_pushvalue(L,1);
    lua_pushliteral(L,"min");
    lua_gettable(L,-2);
    lua_insert(L,-2);
    lua_call(L,1,1);
    lua_rawseti(L,lc171,43);
    assert(lua_gettop(L) == 5);

    /* local max_v = self:max() */
    lc_newclosuretable(L,lc171);
    enum { lc172 = 6 };
    assert((lua_gettop(L) == lc172));
    lua_pushvalue(L,1);
    lua_pushliteral(L,"max");
    lua_gettable(L,-2);
    lua_insert(L,-2);
    lua_call(L,1,1);
    lua_rawseti(L,lc172,44);
    assert(lua_gettop(L) == 6);

    /* buckets = moses.map(moses.range(0, num_buckets), function(v)
     *       return min_v + v*(max_v-min_v)/num_buckets
     *     end) */
    lua_pushliteral(L,"map");
    lua_gettable(L,3);
    lua_pushliteral(L,"range");
    lua_gettable(L,3);
    lua_pushnumber(L,0);
    lc_getupvalue(L,lc172,2,42);
    lua_call(L,2,1);
    lua_pushvalue(L,lc172);
    lua_pushcclosure(L,stuart_RDD_histogram_1,1);
    lua_call(L,2,1);
    lua_replace(L,2);
    assert(lua_gettop(L) == 6);

    /* local h = self:_histogram(buckets) */
    lua_pushvalue(L,1);
    lua_pushliteral(L,"_histogram");
    lua_gettable(L,-2);
    lua_insert(L,-2);
    lua_pushvalue(L,2);
    lua_call(L,2,1);
    assert(lua_gettop(L) == 7);

    /* return buckets, h */
    lua_pushvalue(L,2);
    lua_pushvalue(L,7);
    return 2;
    assert(lua_gettop(L) == 7);
  }
  lua_settop(L,lc168);
  assert(lua_gettop(L) == 3);

  /* local h = self:_histogram(buckets) */
  lua_pushvalue(L,1);
  lua_pushliteral(L,"_histogram");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_pushvalue(L,2);
  lua_call(L,2,1);
  assert(lua_gettop(L) == 4);

  /* return h */
  lua_pushvalue(L,4);
  return 1;
  assert(lua_gettop(L) == 4);
}


/* name: RDD:intersection
 * function(other) */
int stuart_RDD_intersection (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* local moses = require 'moses' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 3);

  /* return self.context:parallelize(moses.intersection(moses.unique(self:collect()), moses.unique(other:collect()))) */
  const int lc190 = lua_gettop(L);
  lua_pushliteral(L,"context");
  lua_gettable(L,1);
  const int lc191 = lua_gettop(L);
  lua_pushliteral(L,"parallelize");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_pushliteral(L,"intersection");
  lua_gettable(L,3);
  const int lc192 = lua_gettop(L);
  lua_pushliteral(L,"unique");
  lua_gettable(L,3);
  const int lc193 = lua_gettop(L);
  lua_pushvalue(L,1);
  lua_pushliteral(L,"collect");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,LUA_MULTRET);
  lua_call(L,(lua_gettop(L) - lc193),1);
  lua_pushliteral(L,"unique");
  lua_gettable(L,3);
  const int lc194 = lua_gettop(L);
  lua_pushvalue(L,2);
  lua_pushliteral(L,"collect");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,LUA_MULTRET);
  lua_call(L,(lua_gettop(L) - lc194),LUA_MULTRET);
  lua_call(L,(lua_gettop(L) - lc192),LUA_MULTRET);
  lua_call(L,(lua_gettop(L) - lc191),LUA_MULTRET);
  return (lua_gettop(L) - lc190);
  assert(lua_gettop(L) == 3);
}


/* name: RDD:isEmpty
 * function() */
int stuart_RDD_isEmpty (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* return self:count() <= 0 */
  lua_pushvalue(L,1);
  lua_pushliteral(L,"count");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  lua_pushnumber(L,0);
  const int lc195 = lc_le(L,-2,-1);
  lua_pop(L,2);
  lua_pushboolean(L,lc195);
  return 1;
  assert(lua_gettop(L) == 1);
}


/* function(y) */
int stuart_RDD_join_1 (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* if y[1] == key then */
  enum { lc206 = 1 };
  lua_pushnumber(L,1);
  lua_gettable(L,1);
  lc_getupvalue(L,lua_upvalueindex(1),1,52);
  const int lc207 = lua_equal(L,-2,-1);
  lua_pop(L,2);
  lua_pushboolean(L,lc207);
  const int lc208 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc208) {

    /* r[#r+1] = {key, {x[2], y[2]}} */
    lua_createtable(L,2,0);
    lc_getupvalue(L,lua_upvalueindex(1),1,52);
    lua_rawseti(L,-2,1);
    lua_createtable(L,2,0);
    lc_getupvalue(L,lua_upvalueindex(1),0,53);
    lua_pushnumber(L,2);
    lua_gettable(L,-2);
    lua_remove(L,-2);
    lua_rawseti(L,-2,1);
    lua_pushnumber(L,2);
    lua_gettable(L,1);
    lua_rawseti(L,-2,2);
    lua_rawseti(L,-2,2);
    lc_getupvalue(L,lua_upvalueindex(1),1,51);
    lua_insert(L,-2);
    lc_getupvalue(L,lua_upvalueindex(1),1,51);
    const double lc209 = lua_objlen(L,-1);
    lua_pop(L,1);
    lua_pushnumber(L,lc209);
    lua_pushnumber(L,1);
    lc_add(L,-2,-1);
    lua_remove(L,-2);
    lua_remove(L,-2);
    lua_insert(L,-2);
    lua_settable(L,-3);
    lua_pop(L,1);
    assert(lua_gettop(L) == 1);
  }
  lua_settop(L,lc206);
  assert(lua_gettop(L) == 1);
  return 0;
}


/* function(x) */
int stuart_RDD_join_2 (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);
  lc_newclosuretable(L,lua_upvalueindex(1));
  enum { lc202 = 2 };
  assert((lua_gettop(L) == lc202));
  lua_pushvalue(L,1);
  lua_rawseti(L,-2,53);

  /* if x[1] == key then */
  enum { lc203 = 2 };
  lc_getupvalue(L,lc202,0,53);
  lua_pushnumber(L,1);
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lc_getupvalue(L,lc202,1,52);
  const int lc204 = lua_equal(L,-2,-1);
  lua_pop(L,2);
  lua_pushboolean(L,lc204);
  const int lc205 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc205) {

    /* moses.forEach(other:collect(), function(y)
     *           if y[1] == key then
     *             r[#r+1] = {key, {x[2], y[2]}}
     *           end
     *         end) */
    lc_getupvalue(L,lc202,2,50);
    lua_pushliteral(L,"forEach");
    lua_gettable(L,-2);
    lua_remove(L,-2);
    lc_getupvalue(L,lc202,3,49);
    lua_pushliteral(L,"collect");
    lua_gettable(L,-2);
    lua_insert(L,-2);
    lua_call(L,1,1);
    lua_pushvalue(L,lc202);
    lua_pushcclosure(L,stuart_RDD_join_1,1);
    lua_call(L,2,0);
    assert(lua_gettop(L) == 2);
  }
  lua_settop(L,lc203);
  assert(lua_gettop(L) == 2);
  return 0;
}


/* function(r, key) */
int stuart_RDD_join_3 (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);
  lc_newclosuretable(L,lua_upvalueindex(1));
  enum { lc201 = 3 };
  assert((lua_gettop(L) == lc201));
  lua_pushvalue(L,1);
  lua_rawseti(L,-2,51);
  lua_pushvalue(L,2);
  lua_rawseti(L,-2,52);

  /* moses.forEach(self:collect(), function(x)
   *       if x[1] == key then
   *         moses.forEach(other:collect(), function(y)
   *           if y[1] == key then
   *             r[#r+1] = {key, {x[2], y[2]}}
   *           end
   *         end)
   *       end
   *     end) */
  lc_getupvalue(L,lc201,1,50);
  lua_pushliteral(L,"forEach");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lc_getupvalue(L,lc201,2,48);
  lua_pushliteral(L,"collect");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  lua_pushvalue(L,lc201);
  lua_pushcclosure(L,stuart_RDD_join_2,1);
  lua_call(L,2,0);
  assert(lua_gettop(L) == 3);

  /* return r */
  lc_getupvalue(L,lc201,0,51);
  return 1;
  assert(lua_gettop(L) == 3);
}


/* name: RDD:join
 * function(other) */
int stuart_RDD_join (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);
  lc_newclosuretable(L,lua_upvalueindex(1));
  enum { lc196 = 3 };
  assert((lua_gettop(L) == lc196));
  lua_pushvalue(L,1);
  lua_rawseti(L,-2,48);
  lua_pushvalue(L,2);
  lua_rawseti(L,-2,49);

  /* local moses = require 'moses' */
  lc_newclosuretable(L,lc196);
  enum { lc197 = 4 };
  assert((lua_gettop(L) == lc197));
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  lua_rawseti(L,lc197,50);
  assert(lua_gettop(L) == 4);

  /* local keys = moses.intersection(moses.keys(self:_dict()), moses.keys(other:_dict())) */
  lc_getupvalue(L,lc197,0,50);
  lua_pushliteral(L,"intersection");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  const int lc198 = lua_gettop(L);
  lc_getupvalue(L,lc197,0,50);
  lua_pushliteral(L,"keys");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  const int lc199 = lua_gettop(L);
  lc_getupvalue(L,lc197,1,48);
  lua_pushliteral(L,"_dict");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,LUA_MULTRET);
  lua_call(L,(lua_gettop(L) - lc199),1);
  lc_getupvalue(L,lc197,0,50);
  lua_pushliteral(L,"keys");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  const int lc200 = lua_gettop(L);
  lc_getupvalue(L,lc197,1,49);
  lua_pushliteral(L,"_dict");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,LUA_MULTRET);
  lua_call(L,(lua_gettop(L) - lc200),LUA_MULTRET);
  lua_call(L,(lua_gettop(L) - lc198),1);
  assert(lua_gettop(L) == 5);

  /* local t = moses.reduce(keys, function(r, key)
   *     moses.forEach(self:collect(), function(x)
   *       if x[1] == key then
   *         moses.forEach(other:collect(), function(y)
   *           if y[1] == key then
   *             r[#r+1] = {key, {x[2], y[2]}}
   *           end
   *         end)
   *       end
   *     end)
   *     return r
   *   end, {}) */
  lc_getupvalue(L,lc197,0,50);
  lua_pushliteral(L,"reduce");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_pushvalue(L,5);
  lua_pushvalue(L,lc197);
  lua_pushcclosure(L,stuart_RDD_join_3,1);
  lua_newtable(L);
  lua_call(L,3,1);
  assert(lua_gettop(L) == 6);

  /* return self.context:parallelize(t, math.max(#self.partitions, #other.partitions)) */
  const int lc213 = lua_gettop(L);
  lc_getupvalue(L,lc197,1,48);
  lua_pushliteral(L,"context");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  const int lc214 = lua_gettop(L);
  lua_pushliteral(L,"parallelize");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_pushvalue(L,6);
  lua_getfield(L,LUA_ENVIRONINDEX,"math");
  lua_pushliteral(L,"max");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lc_getupvalue(L,lc197,1,48);
  lua_pushliteral(L,"partitions");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  const double lc215 = lua_objlen(L,-1);
  lua_pop(L,1);
  lua_pushnumber(L,lc215);
  lc_getupvalue(L,lc197,1,49);
  lua_pushliteral(L,"partitions");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  const double lc216 = lua_objlen(L,-1);
  lua_pop(L,1);
  lua_pushnumber(L,lc216);
  lua_call(L,2,LUA_MULTRET);
  lua_call(L,(lua_gettop(L) - lc214),LUA_MULTRET);
  return (lua_gettop(L) - lc213);
  assert(lua_gettop(L) == 6);
}


/* function(e) */
int stuart_RDD_keyBy_1 (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* return {f(e), e} */
  lua_createtable(L,2,0);
  lc_getupvalue(L,lua_upvalueindex(1),0,54);
  lua_pushvalue(L,1);
  lua_call(L,1,1);
  lua_rawseti(L,-2,1);
  lua_pushvalue(L,1);
  lua_rawseti(L,-2,2);
  return 1;
  assert(lua_gettop(L) == 1);
}


/* name: RDD:keyBy
 * function(f) */
int stuart_RDD_keyBy (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);
  lc_newclosuretable(L,lua_upvalueindex(1));
  enum { lc217 = 3 };
  assert((lua_gettop(L) == lc217));
  lua_pushvalue(L,2);
  lua_rawseti(L,-2,54);

  /* local moses = require 'moses' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 4);

  /* local t = moses.map(self:collect(), function(e) return {f(e), e} end) */
  lua_pushliteral(L,"map");
  lua_gettable(L,4);
  lua_pushvalue(L,1);
  lua_pushliteral(L,"collect");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  lua_pushvalue(L,lc217);
  lua_pushcclosure(L,stuart_RDD_keyBy_1,1);
  lua_call(L,2,1);
  assert(lua_gettop(L) == 5);

  /* return self.context:parallelize(t) */
  const int lc219 = lua_gettop(L);
  lua_pushliteral(L,"context");
  lua_gettable(L,1);
  lua_pushliteral(L,"parallelize");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_pushvalue(L,5);
  lua_call(L,2,LUA_MULTRET);
  return (lua_gettop(L) - lc219);
  assert(lua_gettop(L) == 5);
}


/* function(e) */
int stuart_RDD_keys_1 (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* return e[1] */
  lua_pushnumber(L,1);
  lua_gettable(L,1);
  return 1;
  assert(lua_gettop(L) == 1);
}


/* name: RDD:keys
 * function() */
int stuart_RDD_keys (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* local moses = require 'moses' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 2);

  /* local t = moses.map(self:collect(), function(e) return e[1] end) */
  lua_pushliteral(L,"map");
  lua_gettable(L,2);
  lua_pushvalue(L,1);
  lua_pushliteral(L,"collect");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  lua_pushcfunction(L,stuart_RDD_keys_1);
  lua_call(L,2,1);
  assert(lua_gettop(L) == 3);

  /* return self.context:parallelize(t) */
  const int lc221 = lua_gettop(L);
  lua_pushliteral(L,"context");
  lua_gettable(L,1);
  lua_pushliteral(L,"parallelize");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_pushvalue(L,3);
  lua_call(L,2,LUA_MULTRET);
  return (lua_gettop(L) - lc221);
  assert(lua_gettop(L) == 3);
}


/* function(y) */
int stuart_RDD_leftOuterJoin_1 (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* if y[1] == e[1] then */
  enum { lc226 = 1 };
  lua_pushnumber(L,1);
  lua_gettable(L,1);
  lc_getupvalue(L,lua_upvalueindex(1),1,58);
  lua_pushnumber(L,1);
  lua_gettable(L,-2);
  lua_remove(L,-2);
  const int lc227 = lua_equal(L,-2,-1);
  lua_pop(L,2);
  lua_pushboolean(L,lc227);
  const int lc228 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc228) {

    /* right[#right+1] = y[2] */
    lua_pushnumber(L,2);
    lua_gettable(L,1);
    lc_getupvalue(L,lua_upvalueindex(1),0,59);
    lua_insert(L,-2);
    lc_getupvalue(L,lua_upvalueindex(1),0,59);
    const double lc229 = lua_objlen(L,-1);
    lua_pop(L,1);
    lua_pushnumber(L,lc229);
    lua_pushnumber(L,1);
    lc_add(L,-2,-1);
    lua_remove(L,-2);
    lua_remove(L,-2);
    lua_insert(L,-2);
    lua_settable(L,-3);
    lua_pop(L,1);
    assert(lua_gettop(L) == 1);
  }
  lua_settop(L,lc226);
  assert(lua_gettop(L) == 1);
  return 0;
}


/* function(z) */
int stuart_RDD_leftOuterJoin_2 (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* r[#r+1] = {e[1], {e[2], z}} */
  lua_createtable(L,2,0);
  lc_getupvalue(L,lua_upvalueindex(1),1,58);
  lua_pushnumber(L,1);
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_rawseti(L,-2,1);
  lua_createtable(L,2,0);
  lc_getupvalue(L,lua_upvalueindex(1),1,58);
  lua_pushnumber(L,2);
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_rawseti(L,-2,1);
  lua_pushvalue(L,1);
  lua_rawseti(L,-2,2);
  lua_rawseti(L,-2,2);
  lc_getupvalue(L,lua_upvalueindex(1),1,57);
  lua_insert(L,-2);
  lc_getupvalue(L,lua_upvalueindex(1),1,57);
  const double lc236 = lua_objlen(L,-1);
  lua_pop(L,1);
  lua_pushnumber(L,lc236);
  lua_pushnumber(L,1);
  lc_add(L,-2,-1);
  lua_remove(L,-2);
  lua_remove(L,-2);
  lua_insert(L,-2);
  lua_settable(L,-3);
  lua_pop(L,1);
  assert(lua_gettop(L) == 1);
  return 0;
}


/* function(r, e) */
int stuart_RDD_leftOuterJoin_3 (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);
  lc_newclosuretable(L,lua_upvalueindex(1));
  enum { lc224 = 3 };
  assert((lua_gettop(L) == lc224));
  lua_pushvalue(L,1);
  lua_rawseti(L,-2,57);
  lua_pushvalue(L,2);
  lua_rawseti(L,-2,58);

  /* local right = {} */
  lc_newclosuretable(L,lc224);
  enum { lc225 = 4 };
  assert((lua_gettop(L) == lc225));
  lua_newtable(L);
  lua_rawseti(L,lc225,59);
  assert(lua_gettop(L) == 4);

  /* moses.forEach(other:collect(), function(y)
   *         if y[1] == e[1] then right[#right+1] = y[2] end
   *       end) */
  lc_getupvalue(L,lc225,2,56);
  lua_pushliteral(L,"forEach");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lc_getupvalue(L,lc225,3,55);
  lua_pushliteral(L,"collect");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  lua_pushvalue(L,lc225);
  lua_pushcclosure(L,stuart_RDD_leftOuterJoin_1,1);
  lua_call(L,2,0);
  assert(lua_gettop(L) == 4);

  /* if #right == 0 then */
  enum { lc231 = 4 };
  lc_getupvalue(L,lc225,0,59);
  const double lc232 = lua_objlen(L,-1);
  lua_pop(L,1);
  lua_pushnumber(L,lc232);
  lua_pushnumber(L,0);
  const int lc233 = lua_equal(L,-2,-1);
  lua_pop(L,2);
  lua_pushboolean(L,lc233);
  const int lc234 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc234) {

    /* r[#r+1] = {e[1], {e[2], nil}} */
    lua_createtable(L,2,0);
    lc_getupvalue(L,lc225,1,58);
    lua_pushnumber(L,1);
    lua_gettable(L,-2);
    lua_remove(L,-2);
    lua_rawseti(L,-2,1);
    lua_createtable(L,2,0);
    lc_getupvalue(L,lc225,1,58);
    lua_pushnumber(L,2);
    lua_gettable(L,-2);
    lua_remove(L,-2);
    lua_rawseti(L,-2,1);
    lua_pushnil(L);
    lua_rawseti(L,-2,2);
    lua_rawseti(L,-2,2);
    lc_getupvalue(L,lc225,1,57);
    lua_insert(L,-2);
    lc_getupvalue(L,lc225,1,57);
    const double lc235 = lua_objlen(L,-1);
    lua_pop(L,1);
    lua_pushnumber(L,lc235);
    lua_pushnumber(L,1);
    lc_add(L,-2,-1);
    lua_remove(L,-2);
    lua_remove(L,-2);
    lua_insert(L,-2);
    lua_settable(L,-3);
    lua_pop(L,1);
    assert(lua_gettop(L) == 4);
  }
  else {

    /* else
     * moses.forEach(right, function(z)
     *           r[#r+1] = {e[1], {e[2], z}}
     *         end) */
    lc_getupvalue(L,lc225,2,56);
    lua_pushliteral(L,"forEach");
    lua_gettable(L,-2);
    lua_remove(L,-2);
    lc_getupvalue(L,lc225,0,59);
    lua_pushvalue(L,lc225);
    lua_pushcclosure(L,stuart_RDD_leftOuterJoin_2,1);
    lua_call(L,2,0);
    assert(lua_gettop(L) == 4);
  }
  lua_settop(L,lc231);
  assert(lua_gettop(L) == 4);

  /* return r */
  lc_getupvalue(L,lc225,1,57);
  return 1;
  assert(lua_gettop(L) == 4);
}


/* name: RDD:leftOuterJoin
 * function(other) */
int stuart_RDD_leftOuterJoin (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);
  lc_newclosuretable(L,lua_upvalueindex(1));
  enum { lc222 = 3 };
  assert((lua_gettop(L) == lc222));
  lua_pushvalue(L,2);
  lua_rawseti(L,-2,55);

  /* local moses = require 'moses' */
  lc_newclosuretable(L,lc222);
  enum { lc223 = 4 };
  assert((lua_gettop(L) == lc223));
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  lua_rawseti(L,lc223,56);
  assert(lua_gettop(L) == 4);

  /* local t = moses.reduce(self:collect(), function(r, e)
   *       local right = {}
   *       moses.forEach(other:collect(), function(y)
   *         if y[1] == e[1] then right[#right+1] = y[2] end
   *       end)
   *       if #right == 0 then
   *         r[#r+1] = {e[1], {e[2], nil}}
   *       else
   *         moses.forEach(right, function(z)
   *           r[#r+1] = {e[1], {e[2], z}}
   *         end)
   *       end
   *     return r
   *   end, {}) */
  lc_getupvalue(L,lc223,0,56);
  lua_pushliteral(L,"reduce");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_pushvalue(L,1);
  lua_pushliteral(L,"collect");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  lua_pushvalue(L,lc223);
  lua_pushcclosure(L,stuart_RDD_leftOuterJoin_3,1);
  lua_newtable(L);
  lua_call(L,3,1);
  assert(lua_gettop(L) == 5);

  /* return self.context:parallelize(t) */
  const int lc239 = lua_gettop(L);
  lua_pushliteral(L,"context");
  lua_gettable(L,1);
  lua_pushliteral(L,"parallelize");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_pushvalue(L,5);
  lua_call(L,2,LUA_MULTRET);
  return (lua_gettop(L) - lc239);
  assert(lua_gettop(L) == 5);
}


/* function(r, e) */
int stuart_RDD_lookup_1 (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* if e[1] == key then */
  enum { lc242 = 2 };
  lua_pushnumber(L,1);
  lua_gettable(L,2);
  lc_getupvalue(L,lua_upvalueindex(1),0,60);
  const int lc243 = lua_equal(L,-2,-1);
  lua_pop(L,2);
  lua_pushboolean(L,lc243);
  const int lc244 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc244) {

    /* r[#r+1] = e[2] */
    lua_pushnumber(L,2);
    lua_gettable(L,2);
    const double lc245 = lua_objlen(L,1);
    lua_pushnumber(L,lc245);
    lua_pushnumber(L,1);
    lc_add(L,-2,-1);
    lua_remove(L,-2);
    lua_remove(L,-2);
    lua_insert(L,-2);
    lua_settable(L,1);
    assert(lua_gettop(L) == 2);
  }
  lua_settop(L,lc242);
  assert(lua_gettop(L) == 2);

  /* return r */
  lua_pushvalue(L,1);
  return 1;
  assert(lua_gettop(L) == 2);
}


/* name: RDD:lookup
 * function(key) */
int stuart_RDD_lookup (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);
  lc_newclosuretable(L,lua_upvalueindex(1));
  enum { lc240 = 3 };
  assert((lua_gettop(L) == lc240));
  lua_pushvalue(L,2);
  lua_rawseti(L,-2,60);

  /* local moses = require 'moses' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 4);

  /* return moses.reduce(self:collect(), function(r, e)
   *     if e[1] == key then r[#r+1] = e[2] end
   *     return r
   *   end, {}) */
  const int lc241 = lua_gettop(L);
  lua_pushliteral(L,"reduce");
  lua_gettable(L,4);
  lua_pushvalue(L,1);
  lua_pushliteral(L,"collect");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  lua_pushvalue(L,lc240);
  lua_pushcclosure(L,stuart_RDD_lookup_1,1);
  lua_newtable(L);
  lua_call(L,3,LUA_MULTRET);
  return (lua_gettop(L) - lc241);
  assert(lua_gettop(L) == 4);
}


/* name: RDD:map
 * function(f) */
int stuart_RDD_map (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* local t = {} */
  lua_newtable(L);
  assert(lua_gettop(L) == 3);

  /* for e in self:toLocalIterator() do
   * internal: local f, s, var = explist */
  enum { lc247 = 3 };
  lua_pushvalue(L,1);
  lua_pushliteral(L,"toLocalIterator");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,3);
  while (1) {

    /* internal: local var_1, ..., var_n = f(s, var)
     *           if var_1 == nil then break end
     *           var = var_1 */
    lua_pushvalue(L,-3);
    lua_pushvalue(L,-3);
    lua_pushvalue(L,-3);
    lua_call(L,2,1);
    if (lua_isnil(L,-1)) {
      break;
    }
    lua_pushvalue(L,-1);
    lua_replace(L,-3);

    /* internal: local e with idx 7 */


    /* t[#t+1] = f(e) */
    lua_pushvalue(L,2);
    lua_pushvalue(L,7);
    lua_call(L,1,1);
    const double lc248 = lua_objlen(L,3);
    lua_pushnumber(L,lc248);
    lua_pushnumber(L,1);
    lc_add(L,-2,-1);
    lua_remove(L,-2);
    lua_remove(L,-2);
    lua_insert(L,-2);
    lua_settable(L,3);
    assert(lua_gettop(L) == 7);

    /* internal: stack cleanup on scope exit */
    lua_pop(L,1);
  }
  lua_settop(L,lc247);
  assert(lua_gettop(L) == 3);

  /* return self.context:parallelize(t, #self.partitions) */
  const int lc249 = lua_gettop(L);
  lua_pushliteral(L,"context");
  lua_gettable(L,1);
  lua_pushliteral(L,"parallelize");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_pushvalue(L,3);
  lua_pushliteral(L,"partitions");
  lua_gettable(L,1);
  const double lc250 = lua_objlen(L,-1);
  lua_pop(L,1);
  lua_pushnumber(L,lc250);
  lua_call(L,3,LUA_MULTRET);
  return (lua_gettop(L) - lc249);
  assert(lua_gettop(L) == 3);
}


/* function(r,p) */
int stuart_RDD_mapPartitions_1 (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* for e in iter(p:_toLocalIterator()) do
   * internal: local f, s, var = explist */
  enum { lc252 = 2 };
  lc_getupvalue(L,lua_upvalueindex(1),0,61);
  const int lc253 = lua_gettop(L);
  lua_pushvalue(L,2);
  lua_pushliteral(L,"_toLocalIterator");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,LUA_MULTRET);
  lua_call(L,(lua_gettop(L) - lc253),3);
  while (1) {

    /* internal: local var_1, ..., var_n = f(s, var)
     *           if var_1 == nil then break end
     *           var = var_1 */
    lua_pushvalue(L,-3);
    lua_pushvalue(L,-3);
    lua_pushvalue(L,-3);
    lua_call(L,2,1);
    if (lua_isnil(L,-1)) {
      break;
    }
    lua_pushvalue(L,-1);
    lua_replace(L,-3);

    /* internal: local e with idx 6 */


    /* r[#r+1] = e */
    lua_pushvalue(L,6);
    const double lc254 = lua_objlen(L,1);
    lua_pushnumber(L,lc254);
    lua_pushnumber(L,1);
    lc_add(L,-2,-1);
    lua_remove(L,-2);
    lua_remove(L,-2);
    lua_insert(L,-2);
    lua_settable(L,1);
    assert(lua_gettop(L) == 6);

    /* internal: stack cleanup on scope exit */
    lua_pop(L,1);
  }
  lua_settop(L,lc252);
  assert(lua_gettop(L) == 2);

  /* return r */
  lua_pushvalue(L,1);
  return 1;
  assert(lua_gettop(L) == 2);
}


/* name: RDD:mapPartitions
 * function(iter) */
int stuart_RDD_mapPartitions (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);
  lc_newclosuretable(L,lua_upvalueindex(1));
  enum { lc251 = 3 };
  assert((lua_gettop(L) == lc251));
  lua_pushvalue(L,2);
  lua_rawseti(L,-2,61);

  /* local moses = require 'moses' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 4);

  /* local t = moses.reduce(self.partitions, function(r,p)
   *     for e in iter(p:_toLocalIterator()) do
   *       r[#r+1] = e
   *     end
   *     return r
   *   end, {}) */
  lua_pushliteral(L,"reduce");
  lua_gettable(L,4);
  lua_pushliteral(L,"partitions");
  lua_gettable(L,1);
  lua_pushvalue(L,lc251);
  lua_pushcclosure(L,stuart_RDD_mapPartitions_1,1);
  lua_newtable(L);
  lua_call(L,3,1);
  assert(lua_gettop(L) == 5);

  /* return self.context:parallelize(t) */
  const int lc256 = lua_gettop(L);
  lua_pushliteral(L,"context");
  lua_gettable(L,1);
  lua_pushliteral(L,"parallelize");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_pushvalue(L,5);
  lua_call(L,2,LUA_MULTRET);
  return (lua_gettop(L) - lc256);
  assert(lua_gettop(L) == 5);
}


/* function(r,p) */
int stuart_RDD_mapPartitionsWithIndex_1 (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* for e in iter(index, p:_toLocalIterator()) do
   * internal: local f, s, var = explist */
  enum { lc259 = 2 };
  lc_getupvalue(L,lua_upvalueindex(1),1,62);
  const int lc260 = lua_gettop(L);
  lc_getupvalue(L,lua_upvalueindex(1),0,63);
  lua_pushvalue(L,2);
  lua_pushliteral(L,"_toLocalIterator");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,LUA_MULTRET);
  lua_call(L,(lua_gettop(L) - lc260),3);
  while (1) {

    /* internal: local var_1, ..., var_n = f(s, var)
     *           if var_1 == nil then break end
     *           var = var_1 */
    lua_pushvalue(L,-3);
    lua_pushvalue(L,-3);
    lua_pushvalue(L,-3);
    lua_call(L,2,1);
    if (lua_isnil(L,-1)) {
      break;
    }
    lua_pushvalue(L,-1);
    lua_replace(L,-3);

    /* internal: local e with idx 6 */


    /* r[#r+1] = e */
    lua_pushvalue(L,6);
    const double lc261 = lua_objlen(L,1);
    lua_pushnumber(L,lc261);
    lua_pushnumber(L,1);
    lc_add(L,-2,-1);
    lua_remove(L,-2);
    lua_remove(L,-2);
    lua_insert(L,-2);
    lua_settable(L,1);
    assert(lua_gettop(L) == 6);

    /* internal: stack cleanup on scope exit */
    lua_pop(L,1);
  }
  lua_settop(L,lc259);
  assert(lua_gettop(L) == 2);

  /* index = index + 1 */
  lc_getupvalue(L,lua_upvalueindex(1),0,63);
  lua_pushnumber(L,1);
  lc_add(L,-2,-1);
  lua_remove(L,-2);
  lua_remove(L,-2);
  lc_setupvalue(L,lua_upvalueindex(1),0,63);
  assert(lua_gettop(L) == 2);

  /* return r */
  lua_pushvalue(L,1);
  return 1;
  assert(lua_gettop(L) == 2);
}


/* name: RDD:mapPartitionsWithIndex
 * function(iter) */
int stuart_RDD_mapPartitionsWithIndex (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);
  lc_newclosuretable(L,lua_upvalueindex(1));
  enum { lc257 = 3 };
  assert((lua_gettop(L) == lc257));
  lua_pushvalue(L,2);
  lua_rawseti(L,-2,62);

  /* local index = 0 */
  lc_newclosuretable(L,lc257);
  enum { lc258 = 4 };
  assert((lua_gettop(L) == lc258));
  lua_pushnumber(L,0);
  lua_rawseti(L,lc258,63);
  assert(lua_gettop(L) == 4);

  /* local moses = require 'moses' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 5);

  /* local t = moses.reduce(self.partitions, function(r,p)
   *     for e in iter(index, p:_toLocalIterator()) do
   *       r[#r+1] = e
   *     end
   *     index = index + 1
   *     return r
   *   end, {}) */
  lua_pushliteral(L,"reduce");
  lua_gettable(L,5);
  lua_pushliteral(L,"partitions");
  lua_gettable(L,1);
  lua_pushvalue(L,lc258);
  lua_pushcclosure(L,stuart_RDD_mapPartitionsWithIndex_1,1);
  lua_newtable(L);
  lua_call(L,3,1);
  assert(lua_gettop(L) == 6);

  /* return self.context:parallelize(t) */
  const int lc263 = lua_gettop(L);
  lua_pushliteral(L,"context");
  lua_gettable(L,1);
  lua_pushliteral(L,"parallelize");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_pushvalue(L,6);
  lua_call(L,2,LUA_MULTRET);
  return (lua_gettop(L) - lc263);
  assert(lua_gettop(L) == 6);
}


/* function(e) */
int stuart_RDD_mapValues_1 (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* return {e[1], f(e[2])} */
  lua_createtable(L,2,0);
  lua_pushnumber(L,1);
  lua_gettable(L,1);
  lua_rawseti(L,-2,1);
  const int lc265 = lua_gettop(L);
  lc_getupvalue(L,lua_upvalueindex(1),0,64);
  lua_pushnumber(L,2);
  lua_gettable(L,1);
  lua_call(L,1,LUA_MULTRET);
  while ((lua_gettop(L) > lc265)) {
    lua_rawseti(L,lc265,(1 + (lua_gettop(L) - lc265)));
  }
  return 1;
  assert(lua_gettop(L) == 1);
}


/* name: RDD:mapValues
 * function(f) */
int stuart_RDD_mapValues (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);
  lc_newclosuretable(L,lua_upvalueindex(1));
  enum { lc264 = 3 };
  assert((lua_gettop(L) == lc264));
  lua_pushvalue(L,2);
  lua_rawseti(L,-2,64);

  /* local moses = require 'moses' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 4);

  /* local t = moses.map(self:collect(), function(e) return {e[1], f(e[2])} end) */
  lua_pushliteral(L,"map");
  lua_gettable(L,4);
  lua_pushvalue(L,1);
  lua_pushliteral(L,"collect");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  lua_pushvalue(L,lc264);
  lua_pushcclosure(L,stuart_RDD_mapValues_1,1);
  lua_call(L,2,1);
  assert(lua_gettop(L) == 5);

  /* return self.context:parallelize(t) */
  const int lc267 = lua_gettop(L);
  lua_pushliteral(L,"context");
  lua_gettable(L,1);
  lua_pushliteral(L,"parallelize");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_pushvalue(L,5);
  lua_call(L,2,LUA_MULTRET);
  return (lua_gettop(L) - lc267);
  assert(lua_gettop(L) == 5);
}


/* name: RDD:max
 * function() */
int stuart_RDD_max (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* local r = self:first() */
  lua_pushvalue(L,1);
  lua_pushliteral(L,"first");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  assert(lua_gettop(L) == 2);

  /* for n in self:toLocalIterator() do
   * internal: local f, s, var = explist */
  enum { lc268 = 2 };
  lua_pushvalue(L,1);
  lua_pushliteral(L,"toLocalIterator");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,3);
  while (1) {

    /* internal: local var_1, ..., var_n = f(s, var)
     *           if var_1 == nil then break end
     *           var = var_1 */
    lua_pushvalue(L,-3);
    lua_pushvalue(L,-3);
    lua_pushvalue(L,-3);
    lua_call(L,2,1);
    if (lua_isnil(L,-1)) {
      break;
    }
    lua_pushvalue(L,-1);
    lua_replace(L,-3);

    /* internal: local n with idx 6 */


    /* if n > r then */
    enum { lc269 = 6 };
    const int lc270 = lua_lessthan(L,2,6);
    lua_pushboolean(L,lc270);
    const int lc271 = lua_toboolean(L,-1);
    lua_pop(L,1);
    if (lc271) {

      /* r = n */
      lua_pushvalue(L,6);
      lua_replace(L,2);
      assert(lua_gettop(L) == 6);
    }
    lua_settop(L,lc269);
    assert(lua_gettop(L) == 6);

    /* internal: stack cleanup on scope exit */
    lua_pop(L,1);
  }
  lua_settop(L,lc268);
  assert(lua_gettop(L) == 2);

  /* return r */
  lua_pushvalue(L,2);
  return 1;
  assert(lua_gettop(L) == 2);
}


/* name: RDD:mean
 * function() */
int stuart_RDD_mean (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* return self:stats().mean */
  lua_pushvalue(L,1);
  lua_pushliteral(L,"stats");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  lua_pushliteral(L,"mean");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  return 1;
  assert(lua_gettop(L) == 1);
}


/* name: RDD:min
 * function() */
int stuart_RDD_min (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* local r = self:first() */
  lua_pushvalue(L,1);
  lua_pushliteral(L,"first");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  assert(lua_gettop(L) == 2);

  /* for n in self:toLocalIterator() do
   * internal: local f, s, var = explist */
  enum { lc272 = 2 };
  lua_pushvalue(L,1);
  lua_pushliteral(L,"toLocalIterator");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,3);
  while (1) {

    /* internal: local var_1, ..., var_n = f(s, var)
     *           if var_1 == nil then break end
     *           var = var_1 */
    lua_pushvalue(L,-3);
    lua_pushvalue(L,-3);
    lua_pushvalue(L,-3);
    lua_call(L,2,1);
    if (lua_isnil(L,-1)) {
      break;
    }
    lua_pushvalue(L,-1);
    lua_replace(L,-3);

    /* internal: local n with idx 6 */


    /* if n < r then */
    enum { lc273 = 6 };
    const int lc274 = lua_lessthan(L,6,2);
    lua_pushboolean(L,lc274);
    const int lc275 = lua_toboolean(L,-1);
    lua_pop(L,1);
    if (lc275) {

      /* r = n */
      lua_pushvalue(L,6);
      lua_replace(L,2);
      assert(lua_gettop(L) == 6);
    }
    lua_settop(L,lc273);
    assert(lua_gettop(L) == 6);

    /* internal: stack cleanup on scope exit */
    lua_pop(L,1);
  }
  lua_settop(L,lc272);
  assert(lua_gettop(L) == 2);

  /* return r */
  lua_pushvalue(L,2);
  return 1;
  assert(lua_gettop(L) == 2);
}


/* name: M.new
 * function(...) */
int stuart_RDD_new (lua_State * L) {
  return stuart_class_shared_new(L, "stuart.RDD");
}


/* name: RDD:popStdev
 * function() */
int stuart_RDD_popStdev (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* return self:stats().popStdev */
  lua_pushvalue(L,1);
  lua_pushliteral(L,"stats");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  lua_pushliteral(L,"popStdev");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  return 1;
  assert(lua_gettop(L) == 1);
}


/* name: RDD:popVariance
 * function() */
int stuart_RDD_popVariance (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* return self:stats().popVariance */
  lua_pushvalue(L,1);
  lua_pushliteral(L,"stats");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  lua_pushliteral(L,"popVariance");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  return 1;
  assert(lua_gettop(L) == 1);
}


/* name: RDD:reduce
 * function(f) */
int stuart_RDD_reduce (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* local moses = require 'moses' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 3);

  /* return moses.reduce(self:collect(), f) */
  const int lc276 = lua_gettop(L);
  lua_pushliteral(L,"reduce");
  lua_gettable(L,3);
  lua_pushvalue(L,1);
  lua_pushliteral(L,"collect");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  lua_pushvalue(L,2);
  lua_call(L,2,LUA_MULTRET);
  return (lua_gettop(L) - lc276);
  assert(lua_gettop(L) == 3);
}


/* function(e) */
int stuart_RDD_reduceByKey_1 (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* return moses.reduce(e, f) */
  const int lc280 = lua_gettop(L);
  lc_getupvalue(L,lua_upvalueindex(1),0,66);
  lua_pushliteral(L,"reduce");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_pushvalue(L,1);
  lc_getupvalue(L,lua_upvalueindex(1),1,65);
  lua_call(L,2,LUA_MULTRET);
  return (lua_gettop(L) - lc280);
  assert(lua_gettop(L) == 1);
}


/* name: RDD:reduceByKey
 * function(f) */
int stuart_RDD_reduceByKey (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);
  lc_newclosuretable(L,lua_upvalueindex(1));
  enum { lc277 = 3 };
  assert((lua_gettop(L) == lc277));
  lua_pushvalue(L,2);
  lua_rawseti(L,-2,65);

  /* local moses = require 'moses' */
  lc_newclosuretable(L,lc277);
  enum { lc278 = 4 };
  assert((lua_gettop(L) == lc278));
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  lua_rawseti(L,lc278,66);
  assert(lua_gettop(L) == 4);

  /* return self:groupByKey():mapValues(function(e) return moses.reduce(e, f) end) */
  const int lc279 = lua_gettop(L);
  lua_pushvalue(L,1);
  lua_pushliteral(L,"groupByKey");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  lua_pushliteral(L,"mapValues");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_pushvalue(L,lc278);
  lua_pushcclosure(L,stuart_RDD_reduceByKey_1,1);
  lua_call(L,2,LUA_MULTRET);
  return (lua_gettop(L) - lc279);
  assert(lua_gettop(L) == 4);
}


/* name: RDD:repartition
 * function(numPartitions) */
int stuart_RDD_repartition (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* return self:coalesce(numPartitions, true) */
  const int lc282 = lua_gettop(L);
  lua_pushvalue(L,1);
  lua_pushliteral(L,"coalesce");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_pushvalue(L,2);
  lua_pushboolean(L,1);
  lua_call(L,3,LUA_MULTRET);
  return (lua_gettop(L) - lc282);
  assert(lua_gettop(L) == 2);
}


/* name: RDD:sampleStdev
 * function() */
int stuart_RDD_sampleStdev (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* return self:stats().stdev */
  lua_pushvalue(L,1);
  lua_pushliteral(L,"stats");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  lua_pushliteral(L,"stdev");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  return 1;
  assert(lua_gettop(L) == 1);
}


/* name: RDD:sampleVariance
 * function() */
int stuart_RDD_sampleVariance (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* return self:stats().variance */
  lua_pushvalue(L,1);
  lua_pushliteral(L,"stats");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  lua_pushliteral(L,"variance");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  return 1;
  assert(lua_gettop(L) == 1);
}


/* name: RDD:setName
 * function(name) */
int stuart_RDD_setName (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* self.name = name */
  lua_pushvalue(L,2);
  lua_pushliteral(L,"name");
  lua_insert(L,-2);
  lua_settable(L,1);
  assert(lua_gettop(L) == 2);
  return 0;
}


/* name: comp
 * function(a,b) */
int stuart_RDD_sortBy_1 (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* return a<b */
  const int lc311 = lua_lessthan(L,1,2);
  lua_pushboolean(L,lc311);
  return 1;
  assert(lua_gettop(L) == 2);
}


/* name: comp
 * function(a,b) */
int stuart_RDD_sortBy_2 (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* return a>b */
  const int lc312 = lua_lessthan(L,2,1);
  lua_pushboolean(L,lc312);
  return 1;
  assert(lua_gettop(L) == 2);
}


/* name: RDD:sortBy
 * function(f, ascending, numPartitions) */
int stuart_RDD_sortBy (lua_State * L) {
  enum { lc_nformalargs = 4 };
  lua_settop(L,4);

  /* local moses = require 'moses' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 5);

  /* if not moses.isBoolean(ascending) then */
  enum { lc308 = 5 };
  lua_pushliteral(L,"isBoolean");
  lua_gettable(L,5);
  lua_pushvalue(L,3);
  lua_call(L,1,1);
  lua_pushboolean(L,!(lua_toboolean(L,-1)));
  lua_remove(L,-2);
  const int lc309 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc309) {

    /* ascending = true */
    lua_pushboolean(L,1);
    lua_replace(L,3);
    assert(lua_gettop(L) == 5);
  }
  lua_settop(L,lc308);
  assert(lua_gettop(L) == 5);

  /* local t = self:collect() */
  lua_pushvalue(L,1);
  lua_pushliteral(L,"collect");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  assert(lua_gettop(L) == 6);

  /* local comp */
  lua_settop(L,(lua_gettop(L) + 1));
  assert(lua_gettop(L) == 7);

  /* if ascending then */
  enum { lc310 = 7 };
  if (lua_toboolean(L,3)) {

    /* comp = function(a,b) return a<b end */
    lua_pushcfunction(L,stuart_RDD_sortBy_1);
    lua_replace(L,7);
    assert(lua_gettop(L) == 7);
  }
  else {

    /* else
     * comp = function(a,b) return a>b end */
    lua_pushcfunction(L,stuart_RDD_sortBy_2);
    lua_replace(L,7);
    assert(lua_gettop(L) == 7);
  }
  lua_settop(L,lc310);
  assert(lua_gettop(L) == 7);

  /* t = moses.sortBy(t, f, comp) */
  lua_pushliteral(L,"sortBy");
  lua_gettable(L,5);
  lua_pushvalue(L,6);
  lua_pushvalue(L,2);
  lua_pushvalue(L,7);
  lua_call(L,3,1);
  lua_replace(L,6);
  assert(lua_gettop(L) == 7);

  /* return self.context:parallelize(t, numPartitions) */
  const int lc313 = lua_gettop(L);
  lua_pushliteral(L,"context");
  lua_gettable(L,1);
  lua_pushliteral(L,"parallelize");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_pushvalue(L,6);
  lua_pushvalue(L,4);
  lua_call(L,3,LUA_MULTRET);
  return (lua_gettop(L) - lc313);
  assert(lua_gettop(L) == 7);
}


/* function(r, v) */
int stuart_RDD_stats_1 (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* r.count = r.count + 1 */
  lua_pushliteral(L,"count");
  lua_gettable(L,1);
  lua_pushnumber(L,1);
  lc_add(L,-2,-1);
  lua_remove(L,-2);
  lua_remove(L,-2);
  lua_pushliteral(L,"count");
  lua_insert(L,-2);
  lua_settable(L,1);
  assert(lua_gettop(L) == 2);

  /* r.sum = r.sum + v */
  lua_pushliteral(L,"sum");
  lua_gettable(L,1);
  lc_add(L,-1,2);
  lua_remove(L,-2);
  lua_pushliteral(L,"sum");
  lua_insert(L,-2);
  lua_settable(L,1);
  assert(lua_gettop(L) == 2);

  /* return r */
  lua_pushvalue(L,1);
  return 1;
  assert(lua_gettop(L) == 2);
}


/* name: f
 * function(a,b) */
int stuart_RDD_sortByKey_1 (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* if not moses.isTable(a) or not moses.isTable(b) then */
  enum { lc321 = 2 };
  lc_getupvalue(L,lua_upvalueindex(1),0,73);
  lua_pushliteral(L,"isTable");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_pushvalue(L,1);
  lua_call(L,1,1);
  lua_pushboolean(L,!(lua_toboolean(L,-1)));
  lua_remove(L,-2);
  if (!(lua_toboolean(L,-1))) {
    lua_pop(L,1);
    lc_getupvalue(L,lua_upvalueindex(1),0,73);
    lua_pushliteral(L,"isTable");
    lua_gettable(L,-2);
    lua_remove(L,-2);
    lua_pushvalue(L,2);
    lua_call(L,1,1);
    lua_pushboolean(L,!(lua_toboolean(L,-1)));
    lua_remove(L,-2);
  }
  const int lc322 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc322) {

    /* return 0 */
    lua_pushnumber(L,0);
    return 1;
    assert(lua_gettop(L) == 2);
  }
  lua_settop(L,lc321);
  assert(lua_gettop(L) == 2);

  /* if ascending then */
  enum { lc323 = 2 };
  lc_getupvalue(L,lua_upvalueindex(1),1,72);
  const int lc324 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc324) {

    /* return a[1] < b[1] */
    lua_pushnumber(L,1);
    lua_gettable(L,1);
    lua_pushnumber(L,1);
    lua_gettable(L,2);
    const int lc325 = lua_lessthan(L,-2,-1);
    lua_pop(L,2);
    lua_pushboolean(L,lc325);
    return 1;
    assert(lua_gettop(L) == 2);
  }
  else {

    /* else
     * return a[1] > b[1] */
    lua_pushnumber(L,1);
    lua_gettable(L,2);
    lua_pushnumber(L,1);
    lua_gettable(L,1);
    const int lc326 = lua_lessthan(L,-2,-1);
    lua_pop(L,2);
    lua_pushboolean(L,lc326);
    return 1;
    assert(lua_gettop(L) == 2);
  }
  lua_settop(L,lc323);
  assert(lua_gettop(L) == 2);
  return 0;
}


/* name: RDD:sortByKey
 * function(ascending, numPartitions) */
int stuart_RDD_sortByKey (lua_State * L) {
  enum { lc_nformalargs = 3 };
  lua_settop(L,3);
  lc_newclosuretable(L,lua_upvalueindex(1));
  enum { lc314 = 4 };
  assert((lua_gettop(L) == lc314));
  lua_pushvalue(L,2);
  lua_rawseti(L,-2,72);

  /* local moses = require 'moses' */
  lc_newclosuretable(L,lc314);
  enum { lc315 = 5 };
  assert((lua_gettop(L) == lc315));
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  lua_rawseti(L,lc315,73);
  assert(lua_gettop(L) == 5);

  /* if not moses.isBoolean(ascending) then */
  enum { lc316 = 5 };
  lc_getupvalue(L,lc315,0,73);
  lua_pushliteral(L,"isBoolean");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lc_getupvalue(L,lc315,1,72);
  lua_call(L,1,1);
  lua_pushboolean(L,!(lua_toboolean(L,-1)));
  lua_remove(L,-2);
  const int lc317 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc317) {

    /* ascending = true */
    lua_pushboolean(L,1);
    lc_setupvalue(L,lc315,1,72);
    assert(lua_gettop(L) == 5);
  }
  lua_settop(L,lc316);
  assert(lua_gettop(L) == 5);

  /* if not moses.isNumber(numPartitions) then */
  enum { lc318 = 5 };
  lc_getupvalue(L,lc315,0,73);
  lua_pushliteral(L,"isNumber");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_pushvalue(L,3);
  lua_call(L,1,1);
  lua_pushboolean(L,!(lua_toboolean(L,-1)));
  lua_remove(L,-2);
  const int lc319 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc319) {

    /* numPartitions = #self.partitions */
    lua_pushliteral(L,"partitions");
    lua_gettable(L,1);
    const double lc320 = lua_objlen(L,-1);
    lua_pop(L,1);
    lua_pushnumber(L,lc320);
    lua_replace(L,3);
    assert(lua_gettop(L) == 5);
  }
  lua_settop(L,lc318);
  assert(lua_gettop(L) == 5);

  /* local f = function(a,b)
   *     if not moses.isTable(a) or not moses.isTable(b) then return 0 end
   *     if ascending then
   *       return a[1] < b[1]
   *     else
   *       return a[1] > b[1]
   *     end
   *   end */
  lua_pushvalue(L,lc315);
  lua_pushcclosure(L,stuart_RDD_sortByKey_1,1);
  assert(lua_gettop(L) == 6);

  /* local t = moses.sort(self:collect(), f) */
  lc_getupvalue(L,lc315,0,73);
  lua_pushliteral(L,"sort");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_pushvalue(L,1);
  lua_pushliteral(L,"collect");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  lua_pushvalue(L,6);
  lua_call(L,2,1);
  assert(lua_gettop(L) == 7);

  /* return self.context:parallelize(t, numPartitions) */
  const int lc327 = lua_gettop(L);
  lua_pushliteral(L,"context");
  lua_gettable(L,1);
  lua_pushliteral(L,"parallelize");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_pushvalue(L,7);
  lua_pushvalue(L,3);
  lua_call(L,3,LUA_MULTRET);
  return (lua_gettop(L) - lc327);
  assert(lua_gettop(L) == 7);
}


/* function(p, v) */
int stuart_RDD_stats_2 (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* local distance = v - r.mean */
  lc_getupvalue(L,lua_upvalueindex(1),0,74);
  lua_pushliteral(L,"mean");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lc_sub(L,2,-1);
  lua_remove(L,-2);
  assert(lua_gettop(L) == 3);

  /* p = p + distance * distance */
  lc_mul(L,3,3);
  lc_add(L,1,-1);
  lua_remove(L,-2);
  lua_replace(L,1);
  assert(lua_gettop(L) == 3);

  /* return p */
  lua_pushvalue(L,1);
  return 1;
  assert(lua_gettop(L) == 3);
}


/* name: RDD:stats
 * function() */
int stuart_RDD_stats (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* local x = self:collect() */
  lua_pushvalue(L,1);
  lua_pushliteral(L,"collect");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  assert(lua_gettop(L) == 2);

  /* -- calculate mean
   * local moses = require 'moses' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 3);

  /* local r = moses.reduce(x, function(r, v)
   *     r.count = r.count + 1
   *     r.sum = r.sum + v
   *     return r
   *   end, {count=0, sum=0}) */
  lc_newclosuretable(L,lua_upvalueindex(1));
  enum { lc328 = 4 };
  assert((lua_gettop(L) == lc328));
  lua_pushliteral(L,"reduce");
  lua_gettable(L,3);
  lua_pushvalue(L,2);
  lua_pushcfunction(L,stuart_RDD_stats_1);
  lua_createtable(L,0,2);
  lua_pushliteral(L,"count");
  lua_pushnumber(L,0);
  lua_rawset(L,-3);
  lua_pushliteral(L,"sum");
  lua_pushnumber(L,0);
  lua_rawset(L,-3);
  lua_call(L,3,1);
  lua_rawseti(L,lc328,74);
  assert(lua_gettop(L) == 4);

  /* r.mean = r.sum / r.count */
  lc_getupvalue(L,lc328,0,74);
  lua_pushliteral(L,"sum");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lc_getupvalue(L,lc328,0,74);
  lua_pushliteral(L,"count");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lc_div(L,-2,-1);
  lua_remove(L,-2);
  lua_remove(L,-2);
  lc_getupvalue(L,lc328,0,74);
  lua_insert(L,-2);
  lua_pushliteral(L,"mean");
  lua_insert(L,-2);
  lua_settable(L,-3);
  lua_pop(L,1);
  assert(lua_gettop(L) == 4);

  /* r.sum = nil */
  lua_pushnil(L);
  lc_getupvalue(L,lc328,0,74);
  lua_insert(L,-2);
  lua_pushliteral(L,"sum");
  lua_insert(L,-2);
  lua_settable(L,-3);
  lua_pop(L,1);
  assert(lua_gettop(L) == 4);

  /* -- calculate sum of distances from mean squared
   * local sumDistances = moses.reduce(x, function(p, v)
   *     local distance = v - r.mean
   *     p = p + distance * distance
   *     return p
   *   end, 0) */
  lua_pushliteral(L,"reduce");
  lua_gettable(L,3);
  lua_pushvalue(L,2);
  lua_pushvalue(L,lc328);
  lua_pushcclosure(L,stuart_RDD_stats_2,1);
  lua_pushnumber(L,0);
  lua_call(L,3,1);
  assert(lua_gettop(L) == 5);

  /* if r.count < 2 then */
  enum { lc331 = 5 };
  lc_getupvalue(L,lc328,0,74);
  lua_pushliteral(L,"count");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_pushnumber(L,2);
  const int lc332 = lua_lessthan(L,-2,-1);
  lua_pop(L,2);
  lua_pushboolean(L,lc332);
  const int lc333 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc333) {

    /* -- avoid divide by zero and answer is zero for 1 item
     * r.popStdev = 0 */
    lua_pushnumber(L,0);
    lc_getupvalue(L,lc328,0,74);
    lua_insert(L,-2);
    lua_pushliteral(L,"popStdev");
    lua_insert(L,-2);
    lua_settable(L,-3);
    lua_pop(L,1);
    assert(lua_gettop(L) == 5);

    /* r.popVariance = 0 */
    lua_pushnumber(L,0);
    lc_getupvalue(L,lc328,0,74);
    lua_insert(L,-2);
    lua_pushliteral(L,"popVariance");
    lua_insert(L,-2);
    lua_settable(L,-3);
    lua_pop(L,1);
    assert(lua_gettop(L) == 5);

    /* r.stdev = 0 */
    lua_pushnumber(L,0);
    lc_getupvalue(L,lc328,0,74);
    lua_insert(L,-2);
    lua_pushliteral(L,"stdev");
    lua_insert(L,-2);
    lua_settable(L,-3);
    lua_pop(L,1);
    assert(lua_gettop(L) == 5);

    /* r.variance = 0 */
    lua_pushnumber(L,0);
    lc_getupvalue(L,lc328,0,74);
    lua_insert(L,-2);
    lua_pushliteral(L,"variance");
    lua_insert(L,-2);
    lua_settable(L,-3);
    lua_pop(L,1);
    assert(lua_gettop(L) == 5);

    /* return r */
    lc_getupvalue(L,lc328,0,74);
    return 1;
    assert(lua_gettop(L) == 5);
  }
  lua_settop(L,lc331);
  assert(lua_gettop(L) == 5);

  /* -- 'Sample' variance/stdev divide sum by N - 1 (Bessel's correction)
   * r.variance = sumDistances/(r.count - 1) */
  lc_getupvalue(L,lc328,0,74);
  lua_pushliteral(L,"count");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_pushnumber(L,1);
  lc_sub(L,-2,-1);
  lua_remove(L,-2);
  lua_remove(L,-2);
  lc_div(L,5,-1);
  lua_remove(L,-2);
  lc_getupvalue(L,lc328,0,74);
  lua_insert(L,-2);
  lua_pushliteral(L,"variance");
  lua_insert(L,-2);
  lua_settable(L,-3);
  lua_pop(L,1);
  assert(lua_gettop(L) == 5);

  /* r.stdev = math.sqrt(r.variance) */
  lua_getfield(L,LUA_ENVIRONINDEX,"math");
  lua_pushliteral(L,"sqrt");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lc_getupvalue(L,lc328,0,74);
  lua_pushliteral(L,"variance");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_call(L,1,1);
  lc_getupvalue(L,lc328,0,74);
  lua_insert(L,-2);
  lua_pushliteral(L,"stdev");
  lua_insert(L,-2);
  lua_settable(L,-3);
  lua_pop(L,1);
  assert(lua_gettop(L) == 5);

  /* -- 'Population' variance/stdev divide sum by number of data points
   * r.popVariance = sumDistances/r.count */
  lc_getupvalue(L,lc328,0,74);
  lua_pushliteral(L,"count");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lc_div(L,5,-1);
  lua_remove(L,-2);
  lc_getupvalue(L,lc328,0,74);
  lua_insert(L,-2);
  lua_pushliteral(L,"popVariance");
  lua_insert(L,-2);
  lua_settable(L,-3);
  lua_pop(L,1);
  assert(lua_gettop(L) == 5);

  /* r.popStdev = math.sqrt(r.popVariance) */
  lua_getfield(L,LUA_ENVIRONINDEX,"math");
  lua_pushliteral(L,"sqrt");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lc_getupvalue(L,lc328,0,74);
  lua_pushliteral(L,"popVariance");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_call(L,1,1);
  lc_getupvalue(L,lc328,0,74);
  lua_insert(L,-2);
  lua_pushliteral(L,"popStdev");
  lua_insert(L,-2);
  lua_settable(L,-3);
  lua_pop(L,1);
  assert(lua_gettop(L) == 5);

  /* return r */
  lc_getupvalue(L,lc328,0,74);
  return 1;
  assert(lua_gettop(L) == 5);
}


/* name: RDD:subtract
 * function(other) */
int stuart_RDD_subtract (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* local moses = require 'moses' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 3);

  /* local t = moses.without(self:collect(), other:collect()) */
  lua_pushliteral(L,"without");
  lua_gettable(L,3);
  const int lc334 = lua_gettop(L);
  lua_pushvalue(L,1);
  lua_pushliteral(L,"collect");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  lua_pushvalue(L,2);
  lua_pushliteral(L,"collect");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,LUA_MULTRET);
  lua_call(L,(lua_gettop(L) - lc334),1);
  assert(lua_gettop(L) == 4);

  /* return self.context:parallelize(t, #self.partitions) */
  const int lc335 = lua_gettop(L);
  lua_pushliteral(L,"context");
  lua_gettable(L,1);
  lua_pushliteral(L,"parallelize");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_pushvalue(L,4);
  lua_pushliteral(L,"partitions");
  lua_gettable(L,1);
  const double lc336 = lua_objlen(L,-1);
  lua_pop(L,1);
  lua_pushnumber(L,lc336);
  lua_call(L,3,LUA_MULTRET);
  return (lua_gettop(L) - lc335);
  assert(lua_gettop(L) == 4);
}


/* function(r, e) */
int stuart_RDD_subtractByKey_1 (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* if moses.detect(keys, e[1]) ~= nil then */
  enum { lc339 = 2 };
  lc_getupvalue(L,lua_upvalueindex(1),1,75);
  lua_pushliteral(L,"detect");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lc_getupvalue(L,lua_upvalueindex(1),0,76);
  lua_pushnumber(L,1);
  lua_gettable(L,2);
  lua_call(L,2,1);
  lua_pushnil(L);
  const int lc340 = lua_equal(L,-2,-1);
  lua_pop(L,2);
  lua_pushboolean(L,lc340);
  lua_pushboolean(L,!(lua_toboolean(L,-1)));
  lua_remove(L,-2);
  const int lc341 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc341) {

    /* r[#r+1] = e */
    lua_pushvalue(L,2);
    const double lc342 = lua_objlen(L,1);
    lua_pushnumber(L,lc342);
    lua_pushnumber(L,1);
    lc_add(L,-2,-1);
    lua_remove(L,-2);
    lua_remove(L,-2);
    lua_insert(L,-2);
    lua_settable(L,1);
    assert(lua_gettop(L) == 2);
  }
  lua_settop(L,lc339);
  assert(lua_gettop(L) == 2);

  /* return r */
  lua_pushvalue(L,1);
  return 1;
  assert(lua_gettop(L) == 2);
}


/* name: RDD:subtractByKey
 * function(other) */
int stuart_RDD_subtractByKey (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* local selfKeys = self:keys():collect() */
  lua_pushvalue(L,1);
  lua_pushliteral(L,"keys");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  lua_pushliteral(L,"collect");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  assert(lua_gettop(L) == 3);

  /* local otherKeys = other:keys():collect() */
  lua_pushvalue(L,2);
  lua_pushliteral(L,"keys");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  lua_pushliteral(L,"collect");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  assert(lua_gettop(L) == 4);

  /* local moses = require 'moses' */
  lc_newclosuretable(L,lua_upvalueindex(1));
  enum { lc337 = 5 };
  assert((lua_gettop(L) == lc337));
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  lua_rawseti(L,lc337,75);
  assert(lua_gettop(L) == 5);

  /* local keys = moses.without(selfKeys, otherKeys) */
  lc_newclosuretable(L,lc337);
  enum { lc338 = 6 };
  assert((lua_gettop(L) == lc338));
  lc_getupvalue(L,lc337,0,75);
  lua_pushliteral(L,"without");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_pushvalue(L,3);
  lua_pushvalue(L,4);
  lua_call(L,2,1);
  lua_rawseti(L,lc338,76);
  assert(lua_gettop(L) == 6);

  /* local t = moses.reduce(self:collect(), function(r, e)
   *     if moses.detect(keys, e[1]) ~= nil then r[#r+1] = e end
   *     return r
   *   end, {}) */
  lc_getupvalue(L,lc338,1,75);
  lua_pushliteral(L,"reduce");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_pushvalue(L,1);
  lua_pushliteral(L,"collect");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  lua_pushvalue(L,lc338);
  lua_pushcclosure(L,stuart_RDD_subtractByKey_1,1);
  lua_newtable(L);
  lua_call(L,3,1);
  assert(lua_gettop(L) == 7);

  /* return self.context:parallelize(t, #self.partitions) */
  const int lc344 = lua_gettop(L);
  lua_pushliteral(L,"context");
  lua_gettable(L,1);
  lua_pushliteral(L,"parallelize");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_pushvalue(L,7);
  lua_pushliteral(L,"partitions");
  lua_gettable(L,1);
  const double lc345 = lua_objlen(L,-1);
  lua_pop(L,1);
  lua_pushnumber(L,lc345);
  lua_call(L,3,LUA_MULTRET);
  return (lua_gettop(L) - lc344);
  assert(lua_gettop(L) == 7);
}


/* function(r, v) */
int stuart_RDD_sum_1 (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* return r+v */
  lc_add(L,1,2);
  return 1;
  assert(lua_gettop(L) == 2);
}


/* name: RDD:sum
 * function() */
int stuart_RDD_sum (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* local moses = require 'moses' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 2);

  /* return moses.reduce(self:collect(), function(r, v) return r+v end, 0) */
  const int lc346 = lua_gettop(L);
  lua_pushliteral(L,"reduce");
  lua_gettable(L,2);
  lua_pushvalue(L,1);
  lua_pushliteral(L,"collect");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  lua_pushcfunction(L,stuart_RDD_sum_1);
  lua_pushnumber(L,0);
  lua_call(L,3,LUA_MULTRET);
  return (lua_gettop(L) - lc346);
  assert(lua_gettop(L) == 2);
}


/* name: RDD:take
 * function(n) */
int stuart_RDD_take (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* local iter = self:toLocalIterator() */
  lua_pushvalue(L,1);
  lua_pushliteral(L,"toLocalIterator");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  assert(lua_gettop(L) == 3);

  /* local t = {} */
  lua_newtable(L);
  assert(lua_gettop(L) == 4);

  /* for i = 1, n, 1 do */
  lua_pushnumber(L,1);
  lua_pushnumber(L,1);
  if (!(((lua_isnumber(L,-2) && lua_isnumber(L,2)) && lua_isnumber(L,-1)))) {
    luaL_error(L,"'for' limit must be a number");
  }
  double lc349_var = lua_tonumber(L,-2);
  const double lc350_limit = lua_tonumber(L,2);
  const double lc351_step = lua_tonumber(L,-1);
  lua_pop(L,2);
  enum { lc352 = 4 };
  while ((((lc351_step > 0) && (lc349_var <= lc350_limit)) || ((lc351_step <= 0) && (lc349_var >= lc350_limit)))) {

    /* internal: local i at index 5 */
    lua_pushnumber(L,lc349_var);

    /* local x = iter() */
    lua_pushvalue(L,3);
    lua_call(L,0,1);
    assert(lua_gettop(L) == 6);

    /* if x == nil then */
    enum { lc353 = 6 };
    lua_pushnil(L);
    const int lc354 = lua_equal(L,6,-1);
    lua_pop(L,1);
    lua_pushboolean(L,lc354);
    const int lc355 = lua_toboolean(L,-1);
    lua_pop(L,1);
    if (lc355) {

      /* break */
      break;
      assert(lua_gettop(L) == 6);
    }
    lua_settop(L,lc353);
    assert(lua_gettop(L) == 6);

    /* t[#t+1] = x */
    lua_pushvalue(L,6);
    const double lc356 = lua_objlen(L,4);
    lua_pushnumber(L,lc356);
    lua_pushnumber(L,1);
    lc_add(L,-2,-1);
    lua_remove(L,-2);
    lua_remove(L,-2);
    lua_insert(L,-2);
    lua_settable(L,4);
    assert(lua_gettop(L) == 6);

    /* internal: stack cleanup on scope exit */
    lua_pop(L,2);
    lc349_var += lc351_step;
  }
  lua_settop(L,lc352);
  assert(lua_gettop(L) == 4);

  /* return t */
  lua_pushvalue(L,4);
  return 1;
  assert(lua_gettop(L) == 4);
}


/* function() */
int stuart_RDD_toLocalIterator_1 (lua_State * L) {
  enum { lc_nformalargs = 0 };
  lua_settop(L,0);

  /* if pIndex > #self.partitions then */
  enum { lc381 = 0 };
  lc_getupvalue(L,lua_upvalueindex(1),3,77);
  lua_pushliteral(L,"partitions");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  const double lc382 = lua_objlen(L,-1);
  lua_pop(L,1);
  lua_pushnumber(L,lc382);
  lc_getupvalue(L,lua_upvalueindex(1),2,78);
  const int lc383 = lua_lessthan(L,-2,-1);
  lua_pop(L,2);
  lua_pushboolean(L,lc383);
  const int lc384 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc384) {

    /* return nil */
    lua_pushnil(L);
    return 1;
    assert(lua_gettop(L) == 0);
  }
  lua_settop(L,lc381);
  assert(lua_gettop(L) == 0);

  /* local partitionData = self.partitions[pIndex].data */
  lc_getupvalue(L,lua_upvalueindex(1),3,77);
  lua_pushliteral(L,"partitions");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lc_getupvalue(L,lua_upvalueindex(1),2,78);
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_pushliteral(L,"data");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  assert(lua_gettop(L) == 1);

  /* if not moses.isTable(partitionData) then */
  enum { lc385 = 1 };
  lc_getupvalue(L,lua_upvalueindex(1),0,80);
  lua_pushliteral(L,"isTable");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_pushvalue(L,1);
  lua_call(L,1,1);
  lua_pushboolean(L,!(lua_toboolean(L,-1)));
  lua_remove(L,-2);
  const int lc386 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc386) {

    /* return nil */
    lua_pushnil(L);
    return 1;
    assert(lua_gettop(L) == 1);
  }
  lua_settop(L,lc385);
  assert(lua_gettop(L) == 1);

  /* i = i + 1 */
  lc_getupvalue(L,lua_upvalueindex(1),1,79);
  lua_pushnumber(L,1);
  lc_add(L,-2,-1);
  lua_remove(L,-2);
  lua_remove(L,-2);
  lc_setupvalue(L,lua_upvalueindex(1),1,79);
  assert(lua_gettop(L) == 1);

  /* if i > #partitionData then */
  enum { lc387 = 1 };
  const double lc388 = lua_objlen(L,1);
  lua_pushnumber(L,lc388);
  lc_getupvalue(L,lua_upvalueindex(1),1,79);
  const int lc389 = lua_lessthan(L,-2,-1);
  lua_pop(L,2);
  lua_pushboolean(L,lc389);
  const int lc390 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc390) {

    /* pIndex = pIndex + 1 */
    lc_getupvalue(L,lua_upvalueindex(1),2,78);
    lua_pushnumber(L,1);
    lc_add(L,-2,-1);
    lua_remove(L,-2);
    lua_remove(L,-2);
    lc_setupvalue(L,lua_upvalueindex(1),2,78);
    assert(lua_gettop(L) == 1);

    /* i = 1 */
    lua_pushnumber(L,1);
    lc_setupvalue(L,lua_upvalueindex(1),1,79);
    assert(lua_gettop(L) == 1);

    /* if pIndex > #self.partitions then */
    enum { lc391 = 1 };
    lc_getupvalue(L,lua_upvalueindex(1),3,77);
    lua_pushliteral(L,"partitions");
    lua_gettable(L,-2);
    lua_remove(L,-2);
    const double lc392 = lua_objlen(L,-1);
    lua_pop(L,1);
    lua_pushnumber(L,lc392);
    lc_getupvalue(L,lua_upvalueindex(1),2,78);
    const int lc393 = lua_lessthan(L,-2,-1);
    lua_pop(L,2);
    lua_pushboolean(L,lc393);
    const int lc394 = lua_toboolean(L,-1);
    lua_pop(L,1);
    if (lc394) {

      /* return nil */
      lua_pushnil(L);
      return 1;
      assert(lua_gettop(L) == 1);
    }
    lua_settop(L,lc391);
    assert(lua_gettop(L) == 1);

    /* partitionData = self.partitions[pIndex].data */
    lc_getupvalue(L,lua_upvalueindex(1),3,77);
    lua_pushliteral(L,"partitions");
    lua_gettable(L,-2);
    lua_remove(L,-2);
    lc_getupvalue(L,lua_upvalueindex(1),2,78);
    lua_gettable(L,-2);
    lua_remove(L,-2);
    lua_pushliteral(L,"data");
    lua_gettable(L,-2);
    lua_remove(L,-2);
    lua_replace(L,1);
    assert(lua_gettop(L) == 1);
  }
  lua_settop(L,lc387);
  assert(lua_gettop(L) == 1);

  /* if pIndex <= #self.partitions and i <= #partitionData then */
  enum { lc395 = 1 };
  lc_getupvalue(L,lua_upvalueindex(1),2,78);
  lc_getupvalue(L,lua_upvalueindex(1),3,77);
  lua_pushliteral(L,"partitions");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  const double lc396 = lua_objlen(L,-1);
  lua_pop(L,1);
  lua_pushnumber(L,lc396);
  const int lc397 = lc_le(L,-2,-1);
  lua_pop(L,2);
  lua_pushboolean(L,lc397);
  if (lua_toboolean(L,-1)) {
    lua_pop(L,1);
    lc_getupvalue(L,lua_upvalueindex(1),1,79);
    const double lc398 = lua_objlen(L,1);
    lua_pushnumber(L,lc398);
    const int lc399 = lc_le(L,-2,-1);
    lua_pop(L,2);
    lua_pushboolean(L,lc399);
  }
  const int lc400 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc400) {

    /* return partitionData[i] */
    lc_getupvalue(L,lua_upvalueindex(1),1,79);
    lua_gettable(L,1);
    return 1;
    assert(lua_gettop(L) == 1);
  }
  lua_settop(L,lc395);
  assert(lua_gettop(L) == 1);
  return 0;
}


/* name: RDD:toLocalIterator
 * function() */
int stuart_RDD_toLocalIterator (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);
  lc_newclosuretable(L,lua_upvalueindex(1));
  enum { lc377 = 2 };
  assert((lua_gettop(L) == lc377));
  lua_pushvalue(L,1);
  lua_rawseti(L,-2,77);

  /* local pIndex = 1 */
  lc_newclosuretable(L,lc377);
  enum { lc378 = 3 };
  assert((lua_gettop(L) == lc378));
  lua_pushnumber(L,1);
  lua_rawseti(L,lc378,78);
  assert(lua_gettop(L) == 3);

  /* local i = 0 */
  lc_newclosuretable(L,lc378);
  enum { lc379 = 4 };
  assert((lua_gettop(L) == lc379));
  lua_pushnumber(L,0);
  lua_rawseti(L,lc379,79);
  assert(lua_gettop(L) == 4);

  /* local moses = require 'moses' */
  lc_newclosuretable(L,lc379);
  enum { lc380 = 5 };
  assert((lua_gettop(L) == lc380));
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  lua_rawseti(L,lc380,80);
  assert(lua_gettop(L) == 5);

  /* return function()
   *     if pIndex > #self.partitions then return nil end
   *     local partitionData = self.partitions[pIndex].data
   *     if not moses.isTable(partitionData) then return nil end
   *     i = i + 1
   *     if i > #partitionData then
   *       pIndex = pIndex + 1
   *       i = 1
   *       if pIndex > #self.partitions then return nil end
   *       partitionData = self.partitions[pIndex].data
   *     end
   *
   *     if pIndex <= #self.partitions and i <= #partitionData then
   *       return partitionData[i]
   *     end
   *   end */
  lua_pushvalue(L,lc380);
  lua_pushcclosure(L,stuart_RDD_toLocalIterator_1,1);
  return 1;
  assert(lua_gettop(L) == 5);
}


/* function(a,b) */
int stuart_RDD_top_1 (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* return a>b */
  const int lc402 = lua_lessthan(L,2,1);
  lua_pushboolean(L,lc402);
  return 1;
  assert(lua_gettop(L) == 2);
}


/* name: RDD:top
 * function(num) */
int stuart_RDD_top (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* local moses = require 'moses' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 3);

  /* local t = moses.sort(self:collect(), function(a,b) return a>b end) */
  lua_pushliteral(L,"sort");
  lua_gettable(L,3);
  lua_pushvalue(L,1);
  lua_pushliteral(L,"collect");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  lua_pushcfunction(L,stuart_RDD_top_1);
  lua_call(L,2,1);
  assert(lua_gettop(L) == 4);

  /* return moses.slice(t, 1, num) */
  const int lc404 = lua_gettop(L);
  lua_pushliteral(L,"slice");
  lua_gettable(L,3);
  lua_pushvalue(L,4);
  lua_pushnumber(L,1);
  lua_pushvalue(L,2);
  lua_call(L,3,LUA_MULTRET);
  return (lua_gettop(L) - lc404);
  assert(lua_gettop(L) == 4);
}


/* function(p) */
int stuart_RDD_treeAggregate_1 (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* return moses.reduce(p.data, seqOp, clone(zeroValue)) */
  const int lc409 = lua_gettop(L);
  lc_getupvalue(L,lua_upvalueindex(1),0,84);
  lua_pushliteral(L,"reduce");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  const int lc410 = lua_gettop(L);
  lua_pushliteral(L,"data");
  lua_gettable(L,1);
  lc_getupvalue(L,lua_upvalueindex(1),2,82);
  lc_getupvalue(L,lua_upvalueindex(1),1,83);
  lc_getupvalue(L,lua_upvalueindex(1),2,81);
  lua_call(L,1,LUA_MULTRET);
  lua_call(L,(lua_gettop(L) - lc410),LUA_MULTRET);
  return (lua_gettop(L) - lc409);
  assert(lua_gettop(L) == 1);
}


/* name: RDD:treeAggregate
 * function(zeroValue, seqOp, combOp) */
int stuart_RDD_treeAggregate (lua_State * L) {
  enum { lc_nformalargs = 4 };
  lua_settop(L,4);
  lc_newclosuretable(L,lua_upvalueindex(1));
  enum { lc406 = 5 };
  assert((lua_gettop(L) == lc406));
  lua_pushvalue(L,2);
  lua_rawseti(L,-2,81);
  lua_pushvalue(L,3);
  lua_rawseti(L,-2,82);

  /* local clone = require 'stuart.util'.clone */
  lc_newclosuretable(L,lc406);
  enum { lc407 = 6 };
  assert((lua_gettop(L) == lc407));
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart.util");
  lua_call(L,1,1);
  lua_pushliteral(L,"clone");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_rawseti(L,lc407,83);
  assert(lua_gettop(L) == 6);

  /* local moses = require 'moses' */
  lc_newclosuretable(L,lc407);
  enum { lc408 = 7 };
  assert((lua_gettop(L) == lc408));
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  lua_rawseti(L,lc408,84);
  assert(lua_gettop(L) == 7);

  /* local partiallyAggregated = moses.map(self.partitions, function(p)
   *     return moses.reduce(p.data, seqOp, clone(zeroValue))
   *   end) */
  lc_getupvalue(L,lc408,0,84);
  lua_pushliteral(L,"map");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_pushliteral(L,"partitions");
  lua_gettable(L,1);
  lua_pushvalue(L,lc408);
  lua_pushcclosure(L,stuart_RDD_treeAggregate_1,1);
  lua_call(L,2,1);
  assert(lua_gettop(L) == 8);

  /* return moses.reduce(partiallyAggregated, combOp) */
  const int lc412 = lua_gettop(L);
  lc_getupvalue(L,lc408,0,84);
  lua_pushliteral(L,"reduce");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_pushvalue(L,8);
  lua_pushvalue(L,4);
  lua_call(L,2,LUA_MULTRET);
  return (lua_gettop(L) - lc412);
  assert(lua_gettop(L) == 8);
}


/* name: RDD:union
 * function(other) */
int stuart_RDD_union (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* local moses = require 'moses' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 3);

  /* local t = moses.append(self:collect(), other:collect()) */
  lua_pushliteral(L,"append");
  lua_gettable(L,3);
  const int lc413 = lua_gettop(L);
  lua_pushvalue(L,1);
  lua_pushliteral(L,"collect");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  lua_pushvalue(L,2);
  lua_pushliteral(L,"collect");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,LUA_MULTRET);
  lua_call(L,(lua_gettop(L) - lc413),1);
  assert(lua_gettop(L) == 4);

  /* return self.context:parallelize(t) */
  const int lc414 = lua_gettop(L);
  lua_pushliteral(L,"context");
  lua_gettable(L,1);
  lua_pushliteral(L,"parallelize");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_pushvalue(L,4);
  lua_call(L,2,LUA_MULTRET);
  return (lua_gettop(L) - lc414);
  assert(lua_gettop(L) == 4);
}


/* function(e) */
int stuart_RDD_values_1 (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* return e[2] */
  lua_pushnumber(L,2);
  lua_gettable(L,1);
  return 1;
  assert(lua_gettop(L) == 1);
}


/* name: RDD:values
 * function() */
int stuart_RDD_values (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* local moses = require 'moses' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 2);

  /* local t = moses.map(self:collect(), function(e) return e[2] end) */
  lua_pushliteral(L,"map");
  lua_gettable(L,2);
  lua_pushvalue(L,1);
  lua_pushliteral(L,"collect");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  lua_pushcfunction(L,stuart_RDD_values_1);
  lua_call(L,2,1);
  assert(lua_gettop(L) == 3);

  /* return self.context:parallelize(t) */
  const int lc416 = lua_gettop(L);
  lua_pushliteral(L,"context");
  lua_gettable(L,1);
  lua_pushliteral(L,"parallelize");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_pushvalue(L,3);
  lua_call(L,2,LUA_MULTRET);
  return (lua_gettop(L) - lc416);
  assert(lua_gettop(L) == 3);
}


/* name: RDD:zip
 * function(other) */
int stuart_RDD_zip (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* local moses = require 'moses' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 3);

  /* local t = moses.zip(self:collect(), other:collect()) */
  lua_pushliteral(L,"zip");
  lua_gettable(L,3);
  const int lc416 = lua_gettop(L);
  lua_pushvalue(L,1);
  lua_pushliteral(L,"collect");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  lua_pushvalue(L,2);
  lua_pushliteral(L,"collect");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,LUA_MULTRET);
  lua_call(L,(lua_gettop(L) - lc416),1);
  assert(lua_gettop(L) == 4);

  /* return self.context:parallelize(t) */
  const int lc417 = lua_gettop(L);
  lua_pushliteral(L,"context");
  lua_gettable(L,1);
  lua_pushliteral(L,"parallelize");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_pushvalue(L,4);
  lua_call(L,2,LUA_MULTRET);
  return (lua_gettop(L) - lc417);
  assert(lua_gettop(L) == 4);
}


/* function(x,i) */
int stuart_RDD_zipWithIndex_1 (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* return {x,i-1} */
  lua_createtable(L,2,0);
  lua_pushvalue(L,1);
  lua_rawseti(L,-2,1);
  lua_pushnumber(L,1);
  lc_sub(L,2,-1);
  lua_remove(L,-2);
  lua_rawseti(L,-2,2);
  return 1;
  assert(lua_gettop(L) == 2);
}


/* name: RDD:zipWithIndex
 * function() */
int stuart_RDD_zipWithIndex (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* local moses = require 'moses' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 2);

  /* local t = moses.map(self:collect(), function(x,i)
   *     return {x,i-1}
   *   end) */
  lua_pushliteral(L,"map");
  lua_gettable(L,2);
  lua_pushvalue(L,1);
  lua_pushliteral(L,"collect");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  lua_pushcfunction(L,stuart_RDD_zipWithIndex_1);
  lua_call(L,2,1);
  assert(lua_gettop(L) == 3);

  /* return self.context:parallelize(t) */
  const int lc420 = lua_gettop(L);
  lua_pushliteral(L,"context");
  lua_gettable(L,1);
  lua_pushliteral(L,"parallelize");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_pushvalue(L,3);
  lua_call(L,2,LUA_MULTRET);
  return (lua_gettop(L) - lc420);
  assert(lua_gettop(L) == 3);
}


// ============================================================================
// Definition of the module Romtable.
// ============================================================================

const LUA_REG_TYPE stuart_rdd_map[] = {
  { LSTRKEY("__tostring")            , LFUNCVAL (stuart_RDD___tostring) },
  { LSTRKEY("_dict")                 , LFUNCVAL (stuart_RDD__dict) },
  { LSTRKEY("_flatten")              , LFUNCVAL (stuart_RDD__flatten) },
  { LSTRKEY("_flattenValues")        , LFUNCVAL (stuart_RDD__flattenValues) },
  { LSTRKEY("_init")                 , LFUNCVAL (stuart_RDD__init) },
  { LSTRKEY("aggregate")             , LFUNCVAL (stuart_RDD_aggregate) },
  { LSTRKEY("aggregateByKey")        , LFUNCVAL (stuart_RDD_aggregateByKey) },
  { LSTRKEY("cartesian")             , LFUNCVAL (stuart_RDD_cartesian) },
  { LSTRKEY("collect")               , LFUNCVAL (stuart_RDD_collect) },
  // collectAsMap
  { LSTRKEY("coalesce")              , LFUNCVAL (stuart_RDD_coalesce) },
  { LSTRKEY("combineByKey")          , LFUNCVAL (stuart_RDD_combineByKey) },
  { LSTRKEY("count")                 , LFUNCVAL (stuart_RDD_count) },
  { LSTRKEY("countApprox")           , LFUNCVAL (stuart_RDD_count) },
  { LSTRKEY("countByKey")            , LFUNCVAL (stuart_RDD_countByKey) },
  { LSTRKEY("countByValue")          , LFUNCVAL (stuart_RDD_countByValue) },
  { LSTRKEY("distinct")              , LFUNCVAL (stuart_RDD_distinct) },
  { LSTRKEY("filter")                , LFUNCVAL (stuart_RDD_filter) },
  // filterByRange
  { LSTRKEY("first")                 , LFUNCVAL (stuart_RDD_first) },
  { LSTRKEY("flatMap")               , LFUNCVAL (stuart_RDD_flatMap) },
  { LSTRKEY("flatMapValues")         , LFUNCVAL (stuart_RDD_flatMapValues) },
  { LSTRKEY("fold")                  , LFUNCVAL (stuart_RDD_fold) },
  { LSTRKEY("foldByKey")             , LFUNCVAL (stuart_RDD_foldByKey) },
  { LSTRKEY("foreach")               , LFUNCVAL (stuart_RDD_foreach) },
  { LSTRKEY("foreachPartition")      , LFUNCVAL (stuart_RDD_foreachPartition) },
  { LSTRKEY("glom")                  , LFUNCVAL (stuart_RDD_glom) },
  { LSTRKEY("groupBy")               , LFUNCVAL (stuart_RDD_groupBy) },
  { LSTRKEY("groupByKey")            , LFUNCVAL (stuart_RDD_groupByKey) },
  { LSTRKEY("_histogram")            , LFUNCVAL (stuart_RDD__histogram) },
  { LSTRKEY("histogram")             , LFUNCVAL (stuart_RDD_histogram) },
  { LSTRKEY("intersection")          , LFUNCVAL (stuart_RDD_intersection) },
  { LSTRKEY("isEmpty")               , LFUNCVAL (stuart_RDD_isEmpty) },
  { LSTRKEY("join")                  , LFUNCVAL (stuart_RDD_join) },
  { LSTRKEY("keyBy")                 , LFUNCVAL (stuart_RDD_keyBy) },
  { LSTRKEY("keys")                  , LFUNCVAL (stuart_RDD_keys) },
  { LSTRKEY("leftOuterJoin")         , LFUNCVAL (stuart_RDD_leftOuterJoin) },
  { LSTRKEY("lookup")                , LFUNCVAL (stuart_RDD_lookup) },
  { LSTRKEY("map")                   , LFUNCVAL (stuart_RDD_map) },
  { LSTRKEY("mapPartitions")         , LFUNCVAL (stuart_RDD_mapPartitions) },
  { LSTRKEY("mapPartitionsWithIndex"), LFUNCVAL (stuart_RDD_mapPartitionsWithIndex) },
  { LSTRKEY("mapValues")             , LFUNCVAL (stuart_RDD_mapValues) },
  { LSTRKEY("max")                   , LFUNCVAL (stuart_RDD_max) },
  { LSTRKEY("mean")                  , LFUNCVAL (stuart_RDD_mean) },
  { LSTRKEY("min")                   , LFUNCVAL (stuart_RDD_min) },
  { LSTRKEY("popStdev")              , LFUNCVAL (stuart_RDD_popStdev) },
  { LSTRKEY("popVariance")           , LFUNCVAL (stuart_RDD_popVariance) },
  { LSTRKEY("reduce")                , LFUNCVAL (stuart_RDD_reduce) },
  { LSTRKEY("reduceByKey")           , LFUNCVAL (stuart_RDD_reduceByKey) },
  { LSTRKEY("repartition")           , LFUNCVAL (stuart_RDD_repartition) },
  // sample
  { LSTRKEY("sampleStdev")           , LFUNCVAL (stuart_RDD_sampleStdev) },
  { LSTRKEY("sampleVariance")        , LFUNCVAL (stuart_RDD_sampleVariance) },
  { LSTRKEY("setName")               , LFUNCVAL (stuart_RDD_setName) },
  { LSTRKEY("sortBy")                , LFUNCVAL (stuart_RDD_sortBy) },
  { LSTRKEY("sortByKey")             , LFUNCVAL (stuart_RDD_sortByKey) },
  { LSTRKEY("stats")                 , LFUNCVAL (stuart_RDD_stats) },
  { LSTRKEY("stdev")                 , LFUNCVAL (stuart_RDD_popStdev) },
  { LSTRKEY("subtract")              , LFUNCVAL (stuart_RDD_subtract) },
  { LSTRKEY("subtractByKey")         , LFUNCVAL (stuart_RDD_subtractByKey) },
  { LSTRKEY("sum")                   , LFUNCVAL (stuart_RDD_sum) },
  { LSTRKEY("sumApprox")             , LFUNCVAL (stuart_RDD_sum) },
  { LSTRKEY("take")                  , LFUNCVAL (stuart_RDD_take) },
  // takeSample
  { LSTRKEY("toLocalIterator")       , LFUNCVAL (stuart_RDD_toLocalIterator) },
  { LSTRKEY("top")                   , LFUNCVAL (stuart_RDD_top) },
  { LSTRKEY("toString")              , LFUNCVAL (stuart_RDD___tostring) },
  { LSTRKEY("treeAggregate")         , LFUNCVAL (stuart_RDD_treeAggregate) },
  { LSTRKEY("treeReduce")            , LFUNCVAL (stuart_RDD_reduce) },
  { LSTRKEY("union")                 , LFUNCVAL (stuart_RDD_union) },
  { LSTRKEY("values")                , LFUNCVAL (stuart_RDD_values) },
  { LSTRKEY("variance")              , LFUNCVAL (stuart_RDD_popVariance) },
  { LSTRKEY("zip")                   , LFUNCVAL (stuart_RDD_zip) },
  { LSTRKEY("zipWithIndex")          , LFUNCVAL (stuart_RDD_zipWithIndex) },

  // class framework
  { LSTRKEY("__index")               , LRO_ROVAL(stuart_rdd_map) },
  { LSTRKEY("_class")                , LRO_ROVAL(stuart_rdd_map) },
  { LSTRKEY("classof")               , LFUNCVAL (stuart_RDD_classof) },
  { LSTRKEY("new")                   , LFUNCVAL (stuart_RDD_new) },

  { LNILKEY, LNILVAL }
};


// ============================================================================
// Registration of the module.
// ============================================================================
LUALIB_API int luaopen_stuart_rdd( lua_State *L ){
#if LUA_OPTIMIZE_MEMORY > 0
  return 0;
#else
  LREGISTER( L, AUXLIB_STUART_RDD, stuart_rdd_map );
  return 1;
#endif
}
