#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
#include "common.h"
#include "auxmods.h"
#include "lrotable.h"
#include "platform_conf.h"
#define MIN_OPT_LEVEL 2
#include "lrodefs.h"
#include <assert.h>
#include "lua2c.h"
#include "stuart_class.h"
#include "stuart_streaming_dstream.h"

#define MODULE "stuart.streaming.QueueInputDStream"
#define SUPERMODULE "stuart.streaming.DStream"


/* name: QueueInputDStream:_init
 * function(ssc, rdds, oneAtATime) */
int stuart_streaming_QueueInputDStream__init (lua_State * L) {
  enum { lc_nformalargs = 4 };
  lua_settop(L,4);

  /* local DStream = require 'stuart.streaming.DStream' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,SUPERMODULE);
  lua_call(L,1,1);
  assert(lua_gettop(L) == 5);

  /* DStream._init(self, ssc) */
  lua_pushliteral(L,"_init");
  lua_gettable(L,5);
  lua_pushvalue(L,1);
  lua_pushvalue(L,2);
  lua_call(L,2,0);
  assert(lua_gettop(L) == 5);

  /* self.queue = rdds */
  lua_pushvalue(L,3);
  lua_pushliteral(L,"queue");
  lua_insert(L,-2);
  lua_settable(L,1);
  assert(lua_gettop(L) == 5);

  /* self.oneAtATime = oneAtATime */
  lua_pushvalue(L,4);
  lua_pushliteral(L,"oneAtATime");
  lua_insert(L,-2);
  lua_settable(L,1);
  assert(lua_gettop(L) == 5);
  return 0;
}


/* name: M.classof
 * function(obj) */
int stuart_streaming_QueueInputDStream_classof (lua_State * L) {
  return stuart_class_shared_classof(L, MODULE);
}


/* name: M.new
 * function(...) */
int stuart_streaming_QueueInputDStream_new (lua_State * L) {
  return stuart_class_shared_new_with_super(L, MODULE, SUPERMODULE);
}


/* name: QueueInputDStream:poll
 * function() */
int stuart_streaming_QueueInputDStream_poll (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* if self.oneAtATime then */
  enum { lc1 = 1 };
  lua_pushliteral(L,"oneAtATime");
  lua_gettable(L,1);
  const int lc2 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc2) {

    /* return {table.remove(self.queue, 1)} */
    lua_createtable(L,1,0);
    const int lc3 = lua_gettop(L);
    lua_getfield(L,LUA_ENVIRONINDEX,"table");
    lua_pushliteral(L,"remove");
    lua_gettable(L,-2);
    lua_remove(L,-2);
    lua_pushliteral(L,"queue");
    lua_gettable(L,1);
    lua_pushnumber(L,1);
    lua_call(L,2,LUA_MULTRET);
    while ((lua_gettop(L) > lc3)) {
      lua_rawseti(L,lc3,(0 + (lua_gettop(L) - lc3)));
    }
    return 1;
    assert(lua_gettop(L) == 1);
  }
  else {

    /* else
     * return self.queue */
    lua_pushliteral(L,"queue");
    lua_gettable(L,1);
    return 1;
    assert(lua_gettop(L) == 1);
  }
  lua_settop(L,lc1);
  assert(lua_gettop(L) == 1);
  return 0;
}


// ============================================================================
// Definition of the module Romtable.
// ============================================================================

const LUA_REG_TYPE stuart_qinputdstream_map[] = {
  { LSTRKEY("_init")           , LFUNCVAL (stuart_streaming_QueueInputDStream__init) },
  { LSTRKEY("poll")            , LFUNCVAL (stuart_streaming_QueueInputDStream_poll) },

  // class framework
  { LSTRKEY("__index")         , LRO_ROVAL(stuart_qinputdstream_map) },
  { LSTRKEY("_base")           , LRO_ROVAL(stuart_dstream_map) },
  { LSTRKEY("_class")          , LRO_ROVAL(stuart_qinputdstream_map) },
  { LSTRKEY("classof")         , LFUNCVAL (stuart_streaming_QueueInputDStream_classof) },
  { LSTRKEY("new")             , LFUNCVAL (stuart_streaming_QueueInputDStream_new) },

  // inherited (not including __index, _class, _init, classof, and new)
  { LSTRKEY("_notify")         , LFUNCVAL (stuart_streaming_DStream__notify) },
  { LSTRKEY("count")           , LFUNCVAL (stuart_streaming_DStream_count) },
  { LSTRKEY("countByWindow")   , LFUNCVAL (stuart_streaming_DStream_countByWindow) },
  { LSTRKEY("foreachRDD")      , LFUNCVAL (stuart_streaming_DStream_foreachRDD) },
  { LSTRKEY("groupByKey")      , LFUNCVAL (stuart_streaming_DStream_groupByKey) },
  { LSTRKEY("map")             , LFUNCVAL (stuart_streaming_DStream_map) },
  { LSTRKEY("mapValues")       , LFUNCVAL (stuart_streaming_DStream_mapValues) },
  { LSTRKEY("poll")            , LFUNCVAL (stuart_streaming_DStream_poll) },
  { LSTRKEY("reduce")          , LFUNCVAL (stuart_streaming_DStream_reduce) },
  { LSTRKEY("start")           , LFUNCVAL (stuart_streaming_DStream_start) },
  { LSTRKEY("stop")            , LFUNCVAL (stuart_streaming_DStream_stop) },
  { LSTRKEY("transform")       , LFUNCVAL (stuart_streaming_DStream_transform) },
  { LSTRKEY("window")          , LFUNCVAL (stuart_streaming_DStream_window) },

  { LNILKEY, LNILVAL }
};


// ============================================================================
// Registration of the module.
// ============================================================================
LUALIB_API int luaopen_stuart_qinputdstream( lua_State *L ){
#if LUA_OPTIMIZE_MEMORY > 0
  return 0;
#else
  LREGISTER( L, AUXLIB_STUART_QINPUTDSTREAM, stuart_qinputdstream_map );
  return 1;
#endif
}
