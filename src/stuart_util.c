#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
#include "common.h"
#include "auxmods.h"
#include "lrotable.h"
#include "platform_conf.h"
#define MIN_OPT_LEVEL 2
#include "lrodefs.h"
#include <assert.h>
#include "lua2c.h"


/* name: M.clone
 * function(zeroValue) */
int stuart_util_clone (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* local moses = require 'moses' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 2);

  /* if type(zeroValue) ~= 'table' then */
  enum { lc2 = 2 };
  lua_getfield(L,LUA_ENVIRONINDEX,"type");
  lua_pushvalue(L,1);
  lua_call(L,1,1);
  lua_pushliteral(L,"table");
  const int lc3 = lua_equal(L,-2,-1);
  lua_pop(L,2);
  lua_pushboolean(L,lc3);
  lua_pushboolean(L,!(lua_toboolean(L,-1)));
  lua_remove(L,-2);
  const int lc4 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc4) {

    /* return moses.clone(zeroValue) */
    const int lc5 = lua_gettop(L);
    lua_pushliteral(L,"clone");
    lua_gettable(L,2);
    lua_pushvalue(L,1);
    lua_call(L,1,LUA_MULTRET);
    return (lua_gettop(L) - lc5);
    assert(lua_gettop(L) == 2);
  }
  lua_settop(L,lc2);
  assert(lua_gettop(L) == 2);

  /* if type(zeroValue.clone) == 'function' then */
  enum { lc6 = 2 };
  lua_getfield(L,LUA_ENVIRONINDEX,"type");
  lua_pushliteral(L,"clone");
  lua_gettable(L,1);
  lua_call(L,1,1);
  lua_pushliteral(L,"function");
  const int lc7 = lua_equal(L,-2,-1);
  lua_pop(L,2);
  lua_pushboolean(L,lc7);
  const int lc8 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc8) {

    /* return zeroValue:clone() */
    const int lc9 = lua_gettop(L);
    lua_pushvalue(L,1);
    lua_pushliteral(L,"clone");
    lua_gettable(L,-2);
    lua_insert(L,-2);
    lua_call(L,1,LUA_MULTRET);
    return (lua_gettop(L) - lc9);
    assert(lua_gettop(L) == 2);
  }
  lua_settop(L,lc6);
  assert(lua_gettop(L) == 2);

  /* if zeroValue.__typename ~= nil then */
  enum { lc10 = 2 };
  lua_pushliteral(L,"__typename");
  lua_gettable(L,1);
  lua_pushnil(L);
  const int lc11 = lua_equal(L,-2,-1);
  lua_pop(L,2);
  lua_pushboolean(L,lc11);
  lua_pushboolean(L,!(lua_toboolean(L,-1)));
  lua_remove(L,-2);
  const int lc12 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc12) {

    /* error('Cannot clone a Stuart- or Torch-style class; you must provide it a clone() function') */
    lua_getfield(L,LUA_ENVIRONINDEX,"error");
    lua_pushliteral(L,"Cannot clone a Stuart- or Torch-style class; you must provide it a clone() function");
    lua_call(L,1,0);
    assert(lua_gettop(L) == 2);
  }
  lua_settop(L,lc10);
  assert(lua_gettop(L) == 2);

  /* if zeroValue.class ~= nil then */
  enum { lc13 = 2 };
  lua_pushliteral(L,"class");
  lua_gettable(L,1);
  lua_pushnil(L);
  const int lc14 = lua_equal(L,-2,-1);
  lua_pop(L,2);
  lua_pushboolean(L,lc14);
  lua_pushboolean(L,!(lua_toboolean(L,-1)));
  lua_remove(L,-2);
  const int lc15 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc15) {

    /* error('Cannot clone a middleclass class; you must provide it a clone() function') */
    lua_getfield(L,LUA_ENVIRONINDEX,"error");
    lua_pushliteral(L,"Cannot clone a middleclass class; you must provide it a clone() function");
    lua_call(L,1,0);
    assert(lua_gettop(L) == 2);
  }
  lua_settop(L,lc13);
  assert(lua_gettop(L) == 2);

  /* return moses.clone(zeroValue) */
  const int lc16 = lua_gettop(L);
  lua_pushliteral(L,"clone");
  lua_gettable(L,2);
  lua_pushvalue(L,1);
  lua_call(L,1,LUA_MULTRET);
  return (lua_gettop(L) - lc16);
  assert(lua_gettop(L) == 2);
}


/* name: M.jsonDecode
 * function(s) */
int stuart_util_jsonDecode (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* local has_cjson, cjson = pcall(require, 'cjson') */
  lua_getfield(L,LUA_ENVIRONINDEX,"pcall");
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"cjson");
  lua_call(L,2,2);

  /* if has_cjson then */
  enum { lc15 = 3 };
  if (lua_toboolean(L,2)) {

    /* return cjson.decode(s) */
    const int lc16 = lua_gettop(L);
    lua_pushliteral(L,"decode");
    lua_gettable(L,3);
    lua_pushvalue(L,1);
    lua_call(L,1,LUA_MULTRET);
    return (lua_gettop(L) - lc16);
  }
  else {

    /* else
     * local lunajsonDecoder = require 'lunajson.decoder' */
    lua_getfield(L,LUA_ENVIRONINDEX,"require");
    lua_pushliteral(L,"lunajson.decoder");
    lua_call(L,1,1);

    /* return lunajsonDecoder()(s) */
    const int lc17 = lua_gettop(L);
    lua_pushvalue(L,4);
    lua_call(L,0,1);
    lua_pushvalue(L,1);
    lua_call(L,1,LUA_MULTRET);
    return (lua_gettop(L) - lc17);
  }
  lua_settop(L,lc15);
}


/* name: M.lodashCallIteratee
 * function (predicate, selfArg, ...) */
int stuart_util_lodashCallIteratee (lua_State * L) {
  enum { lc_nformalargs = 2 };
  if ((lua_gettop(L) < lc_nformalargs)) {
    lua_settop(L,lc_nformalargs);
  }
  const int lc_nactualargs = lua_gettop(L);
  const int lc_nextra = (lc_nactualargs - lc_nformalargs);

  /* local moses = require 'moses' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);

  /* predicate = predicate or moses.identity */
  lua_pushvalue(L,1);
  if (!(lua_toboolean(L,-1))) {
    lua_pop(L,1);
    lua_pushliteral(L,"identity");
    lua_gettable(L,(3 + lc_nextra));
  }
  lua_replace(L,1);

  /* if selfArg then */
  enum { lc18 = 3 };
  if (lua_toboolean(L,2)) {

    /* return predicate(selfArg, ...) */
    const int lc19 = lua_gettop(L);
    lua_pushvalue(L,1);
    const int lc20 = lua_gettop(L);
    lua_pushvalue(L,2);
    {int i; for (i=lc_nformalargs+1; i<=lc_nactualargs; i++) { lua_pushvalue(L, i); }}
    lua_call(L,(lua_gettop(L) - lc20),LUA_MULTRET);
    return (lua_gettop(L) - lc19);
  }
  else {

    /* else
     * return predicate(...) */
    const int lc21 = lua_gettop(L);
    lua_pushvalue(L,1);
    const int lc22 = lua_gettop(L);
    {int i; for (i=lc_nformalargs+1; i<=lc_nactualargs; i++) { lua_pushvalue(L, i); }}
    lua_call(L,(lua_gettop(L) - lc22),LUA_MULTRET);
    return (lua_gettop(L) - lc21);
  }
  lua_settop(L,(lc18 + lc_nextra));
  return 0;
}


/* name: M.lodashInRange
 * function (n, start, stop) */
int stuart_util_lodashInRange (lua_State * L) {
  enum { lc_nformalargs = 3 };
  lua_settop(L,3);

  /* local moses = require 'moses' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);

  /* local _start = moses.isNil(stop) and 0 or start or 0 */
  lua_pushliteral(L,"isNil");
  lua_gettable(L,4);
  lua_pushvalue(L,3);
  lua_call(L,1,1);
  if (lua_toboolean(L,-1)) {
    lua_pop(L,1);
    lua_pushnumber(L,0);
  }
  if (!(lua_toboolean(L,-1))) {
    lua_pop(L,1);
    lua_pushvalue(L,2);
  }
  if (!(lua_toboolean(L,-1))) {
    lua_pop(L,1);
    lua_pushnumber(L,0);
  }

  /* local _stop = moses.isNil(stop) and start or stop or 1 */
  lua_pushliteral(L,"isNil");
  lua_gettable(L,4);
  lua_pushvalue(L,3);
  lua_call(L,1,1);
  if (lua_toboolean(L,-1)) {
    lua_pop(L,1);
    lua_pushvalue(L,2);
  }
  if (!(lua_toboolean(L,-1))) {
    lua_pop(L,1);
    lua_pushvalue(L,3);
  }
  if (!(lua_toboolean(L,-1))) {
    lua_pop(L,1);
    lua_pushnumber(L,1);
  }

  /* return n >= _start and n < _stop */
  lua_pushboolean(L,lc_le(L,5,1));
  if (lua_toboolean(L,-1)) {
    lua_pop(L,1);
    const int lc26 = lua_lessthan(L,1,6);
    lua_pushboolean(L,lc26);
  }
  return 1;
}


/* name: M.split
 * function(str, sep) */
int stuart_util_split (lua_State * L) {
  lua_checkstack(L,21);
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* if string.find(str, sep) == nil then */
  enum { lc48 = 2 };
  lua_getfield(L,LUA_ENVIRONINDEX,"string");
  lua_pushliteral(L,"find");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_pushvalue(L,1);
  lua_pushvalue(L,2);
  lua_call(L,2,1);
  lua_pushnil(L);
  const int lc49 = lua_equal(L,-2,-1);
  lua_pop(L,2);
  lua_pushboolean(L,lc49);
  const int lc50 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc50) {

    /* return {str} */
    lua_createtable(L,1,0);
    lua_pushvalue(L,1);
    lua_rawseti(L,-2,1);
    return 1;
    assert(lua_gettop(L) == 2);
  }
  lua_settop(L,lc48);
  assert(lua_gettop(L) == 2);

  /* local result = {} */
  lua_newtable(L);
  assert(lua_gettop(L) == 3);

  /* local pattern = '(.-)' .. sep .. '()' */
  lua_pushliteral(L,"(.-)");
  lua_pushvalue(L,2);
  lua_pushliteral(L,"()");
  lua_concat(L,2);
  lua_concat(L,2);
  assert(lua_gettop(L) == 4);

  /* local nb = 0 */
  lua_pushnumber(L,0);
  assert(lua_gettop(L) == 5);

  /* local lastPos */
  lua_settop(L,(lua_gettop(L) + 1));
  assert(lua_gettop(L) == 6);

  /* for part, pos in string.gmatch(str, pattern) do
   * internal: local f, s, var = explist */
  enum { lc51 = 6 };
  lua_getfield(L,LUA_ENVIRONINDEX,"string");
  lua_pushliteral(L,"gmatch");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_pushvalue(L,1);
  lua_pushvalue(L,4);
  lua_call(L,2,3);
  while (1) {

    /* internal: local var_1, ..., var_n = f(s, var)
     *           if var_1 == nil then break end
     *           var = var_1 */
    lua_pushvalue(L,-3);
    lua_pushvalue(L,-3);
    lua_pushvalue(L,-3);
    lua_call(L,2,2);
    if (lua_isnil(L,-2)) {
      break;
    }
    lua_pushvalue(L,-2);
    lua_replace(L,-4);

    /* internal: local part with idx 10
     * internal: local pos with idx 11 */


    /* nb = nb + 1 */
    lua_pushnumber(L,1);
    lc_add(L,5,-1);
    lua_remove(L,-2);
    lua_replace(L,5);
    assert(lua_gettop(L) == 11);

    /* result[nb] = part */
    lua_pushvalue(L,10);
    lua_pushvalue(L,5);
    lua_insert(L,-2);
    lua_settable(L,3);
    assert(lua_gettop(L) == 11);

    /* lastPos = pos */
    lua_pushvalue(L,11);
    lua_replace(L,6);
    assert(lua_gettop(L) == 11);

    /* internal: stack cleanup on scope exit */
    lua_pop(L,2);
  }
  lua_settop(L,lc51);
  assert(lua_gettop(L) == 6);

  /* result[nb+1] = string.sub(str, lastPos) */
  lua_getfield(L,LUA_ENVIRONINDEX,"string");
  lua_pushliteral(L,"sub");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_pushvalue(L,1);
  lua_pushvalue(L,6);
  lua_call(L,2,1);
  lua_pushnumber(L,1);
  lc_add(L,5,-1);
  lua_remove(L,-2);
  lua_insert(L,-2);
  lua_settable(L,3);
  assert(lua_gettop(L) == 6);

  /* return result */
  lua_pushvalue(L,3);
  return 1;
  assert(lua_gettop(L) == 6);
}


/* name: M.urlParse
 * function(s) */
int stuart_util_urlParse (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* local url = require 'url' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"url");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 2);

  /* return url.parse(s) */
  const int lc52 = lua_gettop(L);
  lua_pushliteral(L,"parse");
  lua_gettable(L,2);
  lua_pushvalue(L,1);
  lua_call(L,1,LUA_MULTRET);
  return (lua_gettop(L) - lc52);
  assert(lua_gettop(L) == 2);
}


// ============================================================================
// Definition of the module Romtable.
// ============================================================================

const LUA_REG_TYPE stuart_util_map[] = {
  { LSTRKEY("clone")             , LFUNCVAL(stuart_util_clone) },
  { LSTRKEY("jsonDecode")        , LFUNCVAL(stuart_util_jsonDecode) },
  { LSTRKEY("lodashCallIteratee"), LFUNCVAL(stuart_util_lodashCallIteratee) },
  { LSTRKEY("lodashInRange")     , LFUNCVAL(stuart_util_lodashInRange) },
  { LSTRKEY("split")             , LFUNCVAL(stuart_util_split) },
  { LSTRKEY("urlParse")          , LFUNCVAL(stuart_util_urlParse) },
  { LNILKEY, LNILVAL }
};


// ============================================================================
// Registration of the module.
// ============================================================================
LUALIB_API int luaopen_stuart_util( lua_State *L ){
#if LUA_OPTIMIZE_MEMORY > 0
  return 0;
#else
  LREGISTER( L, AUXLIB_STUART_UTIL, stuart_util_map );
  return 1;
#endif
}
