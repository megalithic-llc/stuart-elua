<img align="right" src="stuart.png" width="70">

## Stuart on eLua

Because digital transformation is driving the need for embedded real-time intelligence to inform control loops at the device edge.

And because Spark Streaming in 32k of RAM on resource-constrained hardware is cool!

## Building

```sh
$ make

...
*********************************
Compiling eLua ...
CPU:            	STM32F103ZE
Board:          	stm3210e-eval
Platform:       	stm32
Allocator:      	newlib
Boot Mode:      	standard
Target:         	lua
Toolchain:      	codesourcery
ROMFS mode:     	verbatim
Version:        	v0.9-349-g2e6a3af
*********************************
...
Step 60/61 : VOLUME /export
 ---> Running in 70ad2a5643c4
Removing intermediate container 70ad2a5643c4
 ---> a855f67415b5
Step 61/61 : CMD cp -v elua_lua* /export
 ---> Running in 7afe8167847a
Removing intermediate container 7afe8167847a
 ---> d57979f562a8
Successfully built d57979f562a8
Successfully tagged elua:latest
docker run -v `pwd`:/export -it elua
'elua_lua_stm3210e-eval.bin' -> '/export/elua_lua_stm3210e-eval.bin'
'elua_lua_stm3210e-eval.elf' -> '/export/elua_lua_stm3210e-eval.elf'
'elua_lua_stm3210e-eval.hex' -> '/export/elua_lua_stm3210e-eval.hex'
```

Deploy:

```sh
$ stm32flash -w elua_lua_stm3210e-eval.bin /dev/tty.usbserial 

stm32flash 0.4

http://stm32flash.googlecode.com/

Using Parser : Raw BINARY
Interface serial_posix: 57600 8E1
Version      : 0x30
Option 1     : 0x00
Option 2     : 0x00
Device ID    : 0x0430 (XL-density)
- RAM        : 96KiB  (2048b reserved by bootloader)
- Flash      : 1024KiB (sector size: 2x2048)
- Option RAM : 16b
- System RAM : 6KiB
Write to memory
Erasing memory
Wrote address 0x08064340 (100.00%) Done.
```

## Using

Connect a terminal:

```sh
$ screen /dev/tty.usbserial 115200,cs8,-parenb,-cstopb,-hupclcd .
```

Run SparkPi, the "Hello World" of Spark:

```sh
eLua# lua /rom/examples/SparkPi.luo

Press CTRL+Z to exit Lua
INFO Running Stuart (Embedded Spark 2.2.0)
Pi is roughly 3.5789473684211
```

Run a "Spark Shell":

```sh
eLua# lua

Press CTRL+Z to exit Lua
Lua 5.1.4  Copyright (C) 1994-2011 Lua.org, PUC-Rio

sc = require 'stuart'.NewContext()
INFO Running Stuart (Embedded Spark 2.2.0)
```

## Testing

View the ROM fs, which contains many of Stuart's test suites:

```sh
eLua# ls

/wo

Total on /wo: 0 bytes

examples/SparkPi.luo           556 bytes
test/LocalFileSystem.luo       936 bytes
test/QueueInputDStream.luo     5524 bytes
test/class.luo                 3300 bytes
test/Context.luo               1548 bytes
test/stuart.luo                1144 bytes
test/util.luo                  1368 bytes
test/Partition.luo             1880 bytes
test/RDD.luo                   35052 bytes
test/SparkConf.luo             960 bytes
test/fileSystemFactory.luo     1140 bytes
test/StreamingContext.luo      1336 bytes

Total on /rom: 54744 bytes
```

Run a test suite:

```sh
eLua# lua /rom/test/RDD.luo

Press CTRL+Z to exit Lua
Begin test
INFO Running Stuart (Embedded Spark 2.2.0)
✓ aggregate() Examples 1
✓ aggregate() Examples 2
✓ aggregateByKey()
✓ cartesian()
✓ coalesce()
✓ context, sparkContext
✓ count()
✓ countByKey()
✓ countByValue()
✓ distinct()
...
```

Run a slightly more exciting realtime-oriented test suite:

```sh
eLua# lua /rom/test/QueueInputDStream.luo

Press CTRL+Z to exit Lua
Begin test
INFO Running Stuart (Embedded Spark 2.2.0)
✓ can queue RDDs into a DStream
✓ count()
✓ transform 1
✓ transform 2
✓ transform 3
End of test: 0 failures
```

## Versioning

This project uses semver, where the LuaRocks version of Stuart is the prefix (eg `2.0.0-0`), and a tagged release of this project uses a dotted suffix increment (eg `.0` to `.9`).

When a new version of this project is tagged, the Stuart version is re-tagged as a "latest". In other words, a release of this project would be performed as follows:

First release of `2.0.0-0` support:

```sh
$ git tag 2.0.0-0.0
$ git tag 2.0.0-0
$ git push --tags
```

Second release of `2.0.0-0` support:

```sh
$ git tag 2.0.0-0.1
$ git tag 2.0.0-0 --force
$ git push --tags --force
```
